
CREATE TABLE `description` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `vendor_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `gender` varchar(20) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `mobile` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `date_of_birth` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Vendor Registration', 'Procurement', 'User Setup', '1', 'vendor', 'list', '2', '2', '1');

CREATE TABLE `assembly_team` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `gender` varchar(20) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `mobile` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `date_of_birth` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Assembly Team', 'Procurement', 'User Setup', '2', 'assemblyTeam', 'list', '2', '2', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Purchase Order', 'Procurement', 'Purchase Order', '1', 'poEntry', 'list', '3', '2', '1');

CREATE TABLE `purchase_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `po_number` varchar(50) DEFAULT '',
  `id_vendor` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT '0',
  `paid_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `temp_purchase_order_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(256) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT '0',
  `price` float(20,2) DEFAULT '0',
  `total_price` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `purchase_order_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_po` int(20) DEFAULT '0',
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT '0',
  `price` float(20,2) DEFAULT '0',
  `total_price` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `purchase_order_details` CHANGE `total_amount` `total_price` FLOAT(20,2) NULL DEFAULT '0.00';

ALTER TABLE `temp_purchase_order_details` CHANGE `total_amount` `total_price` FLOAT(20,2) NULL DEFAULT '0.00';

ALTER TABLE `purchase_order` ADD `description` VARCHAR(1024) NULL DEFAULT '' AFTER `po_number`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'GRN Entry', 'Procurement', 'GRN', '1', 'grn', 'list', '4', '2', '1');

ALTER TABLE `purchase_order_details` ADD `balance_quantity` INT(20) NULL DEFAULT '0' AFTER `quantity`, ADD `received_quantity` INT(20) NULL DEFAULT '0' AFTER `balance_quantity`;

ALTER TABLE `temp_purchase_order_details` ADD `balance_quantity` INT(20) NULL DEFAULT '0' AFTER `quantity`, ADD `received_quantity` INT(20) NULL DEFAULT '0' AFTER `balance_quantity`;

CREATE TABLE `grn` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_po` int(20) DEFAULT '0',
  `grn_number` varchar(256) DEFAULT '',
  `id_vendor` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT '0',
  `paid_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `grn_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_grn` int(20) DEFAULT '0',
  `id_po_detail` int(20) DEFAULT '0',
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `total_quantity` int(20) DEFAULT '0',
  `quantity` int(20) DEFAULT '0',
  `balance_quantity` int(20) DEFAULT '0',
  `price` float(20,2) DEFAULT '0',
  `total_price` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `purchase_order_details` ADD `id_grn_detail` INT(20) NULL DEFAULT '0' AFTER `id_po`;

ALTER TABLE `grn` ADD `description` VARCHAR(1024) NULL DEFAULT '' AFTER `grn_number`;

ALTER TABLE `procurement_item` ADD `quantity` INT(20) NULL DEFAULT '0' AFTER `manufacturer`;

CREATE TABLE `product_quantity` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_item` int(20) DEFAULT 0,
  `id_grn` int(20) DEFAULT 0,
  `id_grn_detail` int(20) DEFAULT 0,
  `id_assembly_details`  int(20) DEFAULT 0,
  `previous_quantity`  int(20) DEFAULT 0,
  `grn_quantity` int(20) DEFAULT 0,
  `assembly_quantity` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Manufacturer', 'Procurement', 'User Setup', '3', 'manufacturer', 'list', '2', '2', '1');

CREATE TABLE `manufacturer` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `gender` varchar(20) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `mobile` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `city` varchar(512) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `vendor_details` ADD `city` VARCHAR(512) NULL DEFAULT '' AFTER `address_two`;

ALTER TABLE `assembly_team` ADD `city` VARCHAR(512) NULL DEFAULT '' AFTER `address_two`;

CREATE TABLE `procurement_item_has_vendors` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_item` int(20) DEFAULT 0,
  `id_vendor` int(20) DEFAULT 0,
  `price` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Assembly Return', 'Procurement', 'Product Returns', '1', 'assemblyReturn', 'list', '6', '2', '1');

CREATE TABLE `assembly_return` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_po` int(20) DEFAULT '0',
  `reference_number` varchar(256) DEFAULT '',
  `description` VARCHAR(1024) NULL DEFAULT '',
  `id_assembly` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT '0',
  `paid_amount` float(20,2) DEFAULT '0',
  `balance_amount` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `assembly_return_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_assembly_return` int(20) DEFAULT '0',
  `id_assembly_distribution_detail` int(20) DEFAULT '0',
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `total_quantity` int(20) DEFAULT '0',
  `quantity` int(20) DEFAULT '0',
  `balance_quantity` int(20) DEFAULT '0',
  `price` float(20,2) DEFAULT '0',
  `total_price` float(20,2) DEFAULT '0',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `assembly_return` CHANGE `id_po` `id_assembly_distribution` INT(20) NULL DEFAULT '0';

ALTER TABLE `assembly_distribution_details` CHANGE `received_quantity` `received_quantity` INT(20) NULL DEFAULT '0' COMMENT 'its Returned Quantity as Per the Flow';

ALTER TABLE `product_quantity` ADD `id_assembly_return` INT(20) NULL DEFAULT '0' AFTER `id_assembly_detail`, ADD `id_assembly_return_detail` INT(20) NULL DEFAULT '0' AFTER `id_assembly_return`;

ALTER TABLE `product_quantity` ADD `assembly_return_quantity` INT(20) NULL DEFAULT '0' AFTER `assembly_quantity`;

ALTER TABLE `assembly_distribution_details` CHANGE `id_grn_detail` `id_assembly_return_detail` INT(20) NULL DEFAULT '0';

ALTER TABLE `product_quantity` ADD `id_description` INT(20) NULL DEFAULT '0' AFTER `id_item`;
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Packages', 'Procurement', 'Setup', '4', 'packages', 'list', '1', '2', '1');



CREATE TABLE `packages` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(512) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `vendor_details` ADD `gst_in` VARCHAR(256) NULL DEFAULT '' AFTER `email`;
ALTER TABLE `organisation` ADD `gst_in` VARCHAR(256) NULL DEFAULT '' AFTER `email`;

ALTER TABLE `organisation` ADD `gst` FLOAT(20,2) NULL DEFAULT '0' AFTER `email`;

ALTER TABLE `temp_purchase_order_details` ADD `gst_tax` INT(20) NULL DEFAULT '0' AFTER `received_quantity`, ADD `tax_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `gst_tax`, ADD `total_tax_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `tax_amount`, ADD `total_price_before_tax` FLOAT(20,2) NULL DEFAULT '0' AFTER `total_tax_amount`;

ALTER TABLE `purchase_order_details` ADD `gst_tax` INT(20) NULL DEFAULT '0' AFTER `received_quantity`, ADD `tax_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `gst_tax`, ADD `total_tax_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `tax_amount`, ADD `total_price_before_tax` FLOAT(20,2) NULL DEFAULT '0' AFTER `total_tax_amount`;

ALTER TABLE `purchase_order` ADD `total_tax_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `id_vendor`, ADD `total_amount_before_tax` FLOAT(20,2) NULL DEFAULT '0' AFTER `total_tax_amount`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Unit', 'Procurement', 'Setup', '5', 'unit', 'list', '1', '2', '1');



  CREATE TABLE `unit` (
    `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(512) DEFAULT '',
    `code` varchar(512) DEFAULT '',
    `status` int(20) DEFAULT 0,
    `created_by` int(20) DEFAULT NULL,
    `created_dt_tm` datetime DEFAULT current_timestamp(),
    `updated_by` int(20) DEFAULT NULL,
    `updated_dt_tm` varchar(256) DEFAULT ''
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `procurement_item` ADD `thrushold` VARCHAR(256) NULL DEFAULT '' AFTER `moq`, ADD `lead_time` VARCHAR(256) NULL DEFAULT '' AFTER `thrushold`;

ALTER TABLE `organisation` ADD `cin` VARCHAR(256) NULL DEFAULT '' AFTER `gst_in`;

DELETE FROM `permissions` where module != 'Setup';

ALTER TABLE `roles` ADD `created_by` INT NULL DEFAULT '0' AFTER `status`, ADD `created_dt_tm` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT NULL DEFAULT '0' AFTER `created_dt_tm`, ADD `updated_dt_tm` TIMESTAMP NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `vendor_details` ADD `cin` VARCHAR(256) NULL DEFAULT '' AFTER `gst_in`;

ALTER TABLE `manufacturer` ADD `gst_in` VARCHAR(256) NULL DEFAULT '' AFTER `gender`, ADD `cin` VARCHAR(256) NULL DEFAULT '' AFTER `gst_in`;
------ Update Inventory From Here ------

UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 13;


























TRUNCATE table `purchase_order`;
TRUNCATE table `purchase_order_details`;
TRUNCATE table `temp_purchase_order_details`;
TRUNCATE table `grn`;
TRUNCATE table `grn_details`;
TRUNCATE table `assembly_distribution`;
TRUNCATE table `assembly_distribution_details`;
TRUNCATE table `temp_assembly_distribution_details`;
TRUNCATE table `assembly_return`;
TRUNCATE table `assembly_return_details`;
TRUNCATE table `product_quantity`;



























































