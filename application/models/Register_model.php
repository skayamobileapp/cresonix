<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Register_model extends CI_Model
{
	 function addNewRegistration($data) {
	 	$result = $this->db->insert('student', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
	 }

      function addtotemp($data) {
        $result = $this->db->insert('temp_cart', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
     }


       function deletefromtemp($id) {
         $this->db->where('id', $id);
        $this->db->delete('temp_cart');
        return TRUE;
     }

  function gerProgrammeFromSession($id) {
        $this->db->select('p.*,tc.id as tempid,tc.amount');
        $this->db->from('temp_cart as tc');
        $this->db->join('programme as p', 'tc.id_programme = p.id');
        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function checkid($data) {
        $this->db->select('tc.*');
        $this->db->from('temp_cart as tc ');
        
        $this->db->where('tc.id_session', $data['id_session']);
        $this->db->where('tc.id_programme', $data['id_programme']);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }



	 function duplicateCheck($email) {
	 	 $this->db->select('fc.*');
        $this->db->from('student as fc');
        $this->db->where('fc.email', $email);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
	 }


     function programmeOnly(){
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');    
                 $this->db->where("p.id_category='2'");

        $this->db->order_by('rand()');
        $this->db->limit(4);             
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

   
     function programme() {
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount,pt.name as cattype');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    

        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    


        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');     
        $this->db->order_by('rand()');
        $this->db->limit(4);             
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function programmeForCourse(){
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    

        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');      
        $this->db->where("p.id_category='1'");
        $this->db->order_by('rand()');

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function getAllByCategories($id) {
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount,pt.name as cattype');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    

        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');  

        if($name) {
            $this->db->where("p.id_category",$id);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function programmeForCourseByFilterByName($name) {
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount,pt.name as cattype,sl.name as studyname');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    

        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');  

        if($name) {
            $this->db->where("p.name like '%$name%'");
        }
                   

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function programmeForCourseByFilter($programmeType,$courseType,$studyType){
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount,pt.name as cattype,sl.name as studyname');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');   
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
    

        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');  

        if($programmeType) {
                    $this->db->where("p.id_programme_type in ($programmeType)");
        } 

        if($courseType) {
                    $this->db->where("p.id_category in ($courseType)");
        }  

        if($studyType) {
                    $this->db->where("p.id_study_level in ($studyType)");
        }    

        

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

 function programmeAwardList($id_programme)
    {
        $this->db->select('tphd.*, aw.code as award_code, aw.name as award_name, pc.code as program_condition_code, pc.name as program_condition_name');
        $this->db->from('programme_has_award as tphd');
        $this->db->join('award as aw', 'tphd.id_award = aw.id');     
        $this->db->join('programme_condition as pc', 'tphd.id_program_condition = pc.id');     
        $this->db->where('tphd.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

        function getProgramOverview($id) {
        $this->db->select('*');
        $this->db->from('programme_has_overview');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgrammeByProductTypeId($id) {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id_programme_type', $id);
        $this->db->order_by('name ASC');

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudyLevel(){
        $this->db->select('*');
        $this->db->from('study_level');
        $this->db->order_by('name ASC');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramSyllabus($id) {
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

     function getProductType() {
        $this->db->select('ct.*');
        $this->db->from('product_type as ct');
        $this->db->join('programme as p', 'p.id_programme_type=ct.id');
        $this->db->group_by('ct.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

    
     function getCategoryList() {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getCourses($id) {
        $this->db->select('tc.*,ct.name as categoryname, c.name as coursename');
        $this->db->from('temp_cart as tc');
        $this->db->join('course as c', 'tc.id_course = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


       function getCategoryById($id) {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
        $this->db->where('ct.id', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
       }
       function getAllCourses($id) {
        $this->db->select('tc.*,f.amount');
        $this->db->from('course as tc');
        $this->db->join('fee_structure_main as f', 'f.id_course = tc.id');
        $this->db->where('tc.id_category', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }




    function programmeAimDetailsById($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_aim as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function programmeStructureById($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_structure as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     

      function programmeCertificateById($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_certificate as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }


     function getCertificateNames($id_category){
         $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id_category', $id_category);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function programmeHasModules($id) {
        $this->db->select('p.*,pt.name as cattype,sl.name as studyname,fm.amount as amount');
        $this->db->from('programme_has_ref_program as f');
        $this->db->join('programme as p', 'p.id = f.id_child_programme'); 
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as fm', 'fm.id_programme = p.id','left');    

        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function getCoursesById($id) {
        $this->db->select('ct.*');
        $this->db->from('programme as ct');       
        $this->db->where('ct.id', $id); 
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


     function getCourseAmount($id) {
         $this->db->select('f.*');
        $this->db->from('fee_structure_main as f');
        $this->db->where('f.id_course', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

     function getStaffDetailsForProgramId($id) {
        $this->db->select('s.*');
        $this->db->from('programme_has_dean as pd');
        $this->db->join('staff as s', 'pd.id_staff = s.id');             

        $this->db->where('pd.id_programme', $id);
        $this->db->where("pd.status='1'");

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

     function getProgramById($id) {
        $this->db->select('p.*,f.amount');
        $this->db->from('programme as p');
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    

        $this->db->where('p.id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
     }

     function getTopicByProgramId($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_topic as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function getProgramReferences($id){
        $this->db->select('f.*');
        $this->db->from('programme_has_reference as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function getAssessmentByProgramId($id) {
        $this->db->select('f.*,e.name as component');
        $this->db->from('programme_has_assessment as f');
        $this->db->join('examination_components as e', 'f.id_examination_components = e.id');     

        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }
       function getNames($ids) {

       
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where("id ",$id);

         $query = $this->db->get();
         $result = $query->result();  
        return $result;
    }






}

