<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends CI_Model
{
    function roleListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('roles as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function roleListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('roles as c');
        if ($data['name'] != '') {
            $likeCriteria = "(c.role  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->order_by("c.id", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getRole($id)
    {
        $this->db->select('c.*');
        $this->db->from('roles as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewRole($data)
    {
        $this->db->trans_start();
        $this->db->insert('roles', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editRole($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('roles', $data);

        return $result;
    }

    function deleteRole($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('roles', $data);

        return $this->db->affected_rows();
    }

    function checkAccess($roleId, $code)
    {
        $this->db->select('permissions.id');
        $this->db->from('role_permissions');
        $this->db->join('permissions', 'role_permissions.id_permission = permissions.id');

        $this->db->where('role_permissions.id_role', $roleId);
        $this->db->where('code', $code);
        $query = $this->db->get();
        if (empty($query->row())) {
            return 1;
        } else {
            return 0;
        }
    }
    function hasExamActive($userId)
    {
        $this->db->select('*');
        $this->db->from('exam_student_tagging');
        $this->db->where('id_student', $userId);
        $this->db->where('status', 1);
        $query = $this->db->get();
        if (empty($query->row())) {
            return 1;
        } else {
            return 0;
        }
    }

    function rolePermissions($roleId)
    {
        $this->db->select('BaseTbl.id_permission');
        $this->db->from('role_permissions as BaseTbl');
        $this->db->where('id_role', $roleId);
        $query = $this->db->get();
        $result = $query->result_array();
        $ouput = array();
        foreach ($result as $permission) {
            array_push($ouput, $permission['id_permission']);
        }
        return $ouput;
    }

    function updateRolePermissions($permissions, $roleId)
    {
        $this->db->trans_start();
        $SQL = "delete from role_permissions where id_role = " . $roleId;
        $query = $this->db->query($SQL);
        foreach ($permissions as $permission) {
            $info = array('id_role' => $roleId, 'id_permission' => $permission);
            $this->db->insert('role_permissions', $info);
        }
        $this->db->trans_complete();

        return;
    }
}
