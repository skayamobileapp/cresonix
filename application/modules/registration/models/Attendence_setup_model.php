<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Attendence_setup_model extends CI_Model
{
    function attendenceSetupList()
    {
        $this->db->select('a.*, b.name as stateName, c.name as countryName, ecl.name as location');
        $this->db->from('attendence_setup as a');
        $this->db->join('state as b', 'a.id_state = b.id');
        $this->db->join('country as c', 'a.id_country = c.id');
        $this->db->join('attendence_setup_location as ecl', 'a.id_location = ecl.id');
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function attendenceSetupLocationListForRegistration()
    {
        $this->db->select('a.*, ecl.name as location');
        $this->db->from('attendence_setup as a');
        $this->db->join('state as b', 'a.id_state = b.id');
        $this->db->join('country as c', 'a.id_country = c.id');
        $this->db->join('attendence_setup_location as ecl', 'a.id_location = ecl.id');
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }


    function attendenceSetupListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, p.name as program_name, p.code as program_code');
        $this->db->from('attendence_setup as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if ($data['id_program'] != '')
        {
            $this->db->where('a.id_program', $data['id_program']);
        }
        if ($data['type'] != '')
        {
            $this->db->where('a.type', $data['type']);
        }
        
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAttendenceSetup($id)
    {
        $this->db->select('*');
        $this->db->from('attendence_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addAttendenceSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('attendence_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAttendenceSetup($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('attendence_setup', $data);
        return TRUE;
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function attendenceSetupLocationList()
    {
        $this->db->select('*');
        $this->db->from('attendence_setup_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}