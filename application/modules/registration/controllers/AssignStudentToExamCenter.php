<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssignStudentToExamCenter extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('assign_student_to_exam_center_model');
        $this->load->model('admission/student_model');
        $this->load->model('registration/exam_center_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('assign_student_to_exam_center.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['assignedStudentList'] = $this->assign_student_to_exam_center_model->assignedStudentList();
            
            // $name = $this->security->xss_clean($this->input->post('name'));

            // $data['searchName'] = $name;
            // $data['examCenterList'] = $this->exam_center_model->examCenterListSearch();


            $this->global['pageTitle'] = 'Inventory Management : Assigned Student To Exam Center';
            $this->loadViews("assign_student_to_exam_center/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('assign_student_to_exam_center.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                
                $data = array(
                   'id_student' => $id_student,
                    'id_exam_center' => $id_exam_center
                );

                $result = $this->assign_student_to_exam_center_model->addStudentToExamCenter($data);
                redirect('/registration/assignStudentToExamCenter/list');
            }

            $data['studentList'] = $this->assign_student_to_exam_center_model->studentList();
            
            $data['examCenterList'] = $this->exam_center_model->examCenterList();

            $this->global['pageTitle'] = 'Inventory Management : Add Student To Exam Center';
            $this->loadViews("assign_student_to_exam_center/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('assign_student_to_exam_center.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/assignStudentToExamCenter/list');
            }
            if($this->input->post())
            {
                
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                
                $data = array(
                   'id_student' => $id_student,
                    'id_exam_center' => $id_exam_center
                );

                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->assign_student_to_exam_center_model->editStudentToExamCenter($data,$id);
                redirect('/registration/assignStudentToExamCenter/list');
            }
            $data['getAssignedStudentList'] = $this->assign_student_to_exam_center_model->getAssignedStudentList($id);
            
            $data['studentList'] = $this->assign_student_to_exam_center_model->studentList();
            $data['examCenterList'] = $this->exam_center_model->examCenterList();

            $this->global['pageTitle'] = 'Inventory Management : Edit Assigned Student To Exam Center';
            $this->loadViews("assign_student_to_exam_center/edit", $this->global, $data, NULL);
        }
    }
}
