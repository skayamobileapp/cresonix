<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BulkWithdraw extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bulk_withdraw_model');
        $this->load->model('admission/student_model');
        $this->load->model('registration/exam_center_model');
        $this->load->model('registration/course_registration_model');
        $this->load->model('registration/exam_registration_model');
        $this->load->model('setup/programme_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('bulk_withdraw.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->course_registration_model->intakeList();
            $data['programList'] = $this->course_registration_model->programList();
            $data['courseList'] = $this->bulk_withdraw_model->courseList();

                // $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                // $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                // $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));

                $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
 

            $data['searchParam'] = $formData;



            $data['bulkWithdrawList'] = $this->bulk_withdraw_model->bulkWithdrawList($formData);

            // echo "<Pre>";print_r($data['bulkWithdrawList']);exit();


            $this->global['pageTitle'] = 'Inventory Management : Assigned Student To Exam Center';
            $this->loadViews("bulk_withdraw/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('bulk_withdraw.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                     // echo "<Pre>";print_r($this->input->post());exit();

                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_course_registration = $this->security->xss_clean($this->input->post('id_course_registration'));
                $id_course_registered_landscape = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                // $reason = $this->security->xss_clean($this->input->post('reason'));


                $course_landscape = $this->bulk_withdraw_model->getCourseRegisteredLandscape($id_course_registered_landscape);
                // echo "<Pre>";print_r($id_course_registration);exit();
            
                $master_data = array(
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_course' => $course_landscape->id,
                    'id_course_registered_landscape' => $id_course_registered_landscape,
                    'reason' => $reason,
                    'status' => 1,
                    'created_by' => $user_id
                );

                
                $inserted_master_id = $this->bulk_withdraw_model->addBulkWithdrawMaster($master_data);



                 for($i=0;$i<count($id_course_registration);$i++)
                 {

                    $course_registration = $this->bulk_withdraw_model->getCourseRegistration($id_course_registration[$i]);

                    
                // echo "<Pre>";print_r($course_registration);exit();

                     $data = array(
                    'id_course_registered' => $id_course_registration[$i],
                    'id_exam_register' => 0,
                    'id_bulk_withdraw_master' => $inserted_master_id,
                    // 'id_exam_center' => $course_registration->id_exam_event,
                    'id_course' => $course_registration->id_course,
                    'id_course_registered_landscape' => $course_registration->id_course_registered_landscape,
                    'id_semester' => $course_registration->id_semester,
                    'id_student' => $course_registration->id_student,
                    'id_intake' => $course_registration->id_intake,
                    'id_programme' => $course_registration->id_programme,
                    'reason' => $reason,
                    'status' => 1,
                    'created_by' => $user_id
                );
                     // echo "<Pre>";print_r($data);exit();
                $insert_id = $this->bulk_withdraw_model->addBulkWithdraw($data);
                    if ($insert_id)
                    {
                        $update = array(
                            'is_bulk_withdraw' => $insert_id
                        );
                        // $updated = $this->bulk_withdraw_model->updateExamRegistration($update,$exam_registration_data->id);                       
                        $updated = $this->bulk_withdraw_model->updateCourseRegistration($update,$course_registration->id); 

                        $check_apply_status = $this->bulk_withdraw_model->getFeeStructureActivityType('COURSE WITHDRAW','Application Level',$course_registration->id_programme);
                        if($check_apply_status)
                        {
                            $data['add'] = 1;
                            $this->bulk_withdraw_model->generateMainInvoice($data,$insert_id);
                        }


                    }
                }
                redirect('/registration/bulkWithdraw/list');
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['studentList'] = $this->bulk_withdraw_model->studentList();
            $data['examCenterList'] = $this->exam_center_model->examCenterList();

            $this->global['pageTitle'] = 'Inventory Management : Add Student To Exam Center';
            $this->loadViews("bulk_withdraw/add", $this->global, $data, NULL);
        }
    }


    function edit($id)
    {
        if ($this->checkAccess('bulk_withdraw.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/bulkWithdraw/list');
            }
            if($this->input->post())
            {
                
                redirect('/registration/bulkWithdraw/list');
            }


            
            $data['withdrawData'] = $this->bulk_withdraw_model->getWithdrawMaster($id);

            // $data['courseData'] = $this->course_registration_model->getStudentByStudentId($data['withdrawData']->id_course);
            $data['courseList'] = $this->bulk_withdraw_model->bulkWithdrawListByIntakeProgrammeStudent($id);

             // echo "<Pre>";print_r($data['courseList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Assigned Student To Exam Center';
            $this->loadViews("bulk_withdraw/edit", $this->global, $data, NULL);
        }
    }

     function getIntakes()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->course_registration_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control' onchange='getCourseLandscapeByProgNIntake()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;

        $table.="<option value=".$id.">".$intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }


    function getCourseLandscapeByProgNIntake()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->bulk_withdraw_model->getCourseLandscapeByByProgNIntake($tempData);

        // echo "<Pre>";print_r($student_list_data);exit;

        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_course_registered_landscape' id='id_course_registered_landscape' class='form-control' onchange='displayStudentData()'>";
        $table.="<option value=''>Select</option> 
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // echo "<Pre>";print_r($student_list_data[$i]);exit;
        // $id = $results[$i]->id_procurement_category;
        $id_course_registered_landscape = $student_list_data[$i]->id_course_registered_landscape;
        $code = $student_list_data[$i]->code;
        $name = $student_list_data[$i]->name;
        $table.="<option value=".$id_course_registered_landscape.">".$code. " - " . $name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }


    function displayStudentsByIdCourseRegisteredLandscape()
    {
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
         // echo "<Pre>"; print_r($tempData);exit();

        $temp_details = $this->bulk_withdraw_model->getStudentListByCourseRegistrationData($tempData);
        $course_details = $this->bulk_withdraw_model->getCourseRegisteredLandscape($tempData['id_course_registered_landscape']);

        // echo "<Pre>"; print_r($temp_details);exit();

            $id_course_registered_landscape = $tempData['id_course_registered_landscape'];
            $name = $course_details->name;
            $name = $course_details->name;
            $code = $course_details->code;
            $credit_hours = $course_details->credit_hours;
            $class_recurrence = $course_details->class_recurrence;
            $course_type = $course_details->course_type;




        $table = "

            <h4 class='sub-title'>Course Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd>$name</dd>
                            </dl>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd>$code</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Type :</dt>
                                <dd>$course_type</dd>
                            </dl>
                            <dl>
                                <dt>Credit Hours :</dt>
                                <dd>$credit_hours</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>



            <h4 class='sub-title'>Select Student For Course Withdraw</h4>




        <div class='custom-table'><table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Semester</th>
                    <th>Student Name</th>
                    <th>Email Id</th>
                    <th>NRIC</th>
                    <th>Phone No.</th>
                    <th>Gender</th>
                    <th>Program Scheme</th>
                    <th>DOB</th>
                    <th class='text-center'>Select Student</th>
                </tr></thead><tbody>";
                
        if($temp_details!=NULL)
        {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {


                    $id_student = $temp_details[$i]->id;
                    $id_course_registration = $temp_details[$i]->id_course_registered;
                    $full_name = $temp_details[$i]->full_name;
                    $nric = $temp_details[$i]->nric;
                    $phone = $temp_details[$i]->phone;
                    $email_id = $temp_details[$i]->email_id;
                    $gender = $temp_details[$i]->gender;
                    $program_scheme = $temp_details[$i]->program_scheme;
                    $date_of_birth = $temp_details[$i]->date_of_birth;
                    $semester_code = $temp_details[$i]->semester_code;
                    $semester_name = $temp_details[$i]->semester_name;


                    if($date_of_birth)
                    {
                        $date_of_birth = date('d-m-Y', strtotime($date_of_birth));
                    }


                        $table .= "
                        <tr>
                            <td>$semester_code - $semester_name</td>
                            <td>$full_name</td>
                            <td>$email_id</td>
                            <td>$nric</td>
                            <td>$phone</td>
                            <td>$gender</td>
                            <td>$program_scheme</td>
                            <td>$date_of_birth</td>
                            <td class='text-center'><input type='checkbox' name='id_course_registration[]' id='id_course_registration' value='$id_course_registration'></td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }









































    function getStudentByProgNIntake()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->course_registration_model->getStudentByProgNIntake($tempData);


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_student' id='id_student' class='form-control' onchange='displaydata()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // echo "<Pre>";print_r($student_list_data[$i]);exit;
        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $nric = $student_list_data[$i]->nric;
        $table.="<option value=".$id.">".$nric." - ".$full_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function displaydata()
    {
        

        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));

        // $temp_details = $this->bulk_withdraw_model->getCourseFromExamRegister($id_intake,$id_programme,$id_student);



        $temp_details = $this->bulk_withdraw_model->getCourseFromCourseRegistration($id_intake,$id_programme,$id_student);


        // echo "<pre>";print_r($temp_details);exit();

        // $examDetails = $this->exam_registration_model->getExamRegistrerDetails($id_intake,$id_programme,$id_student);


        

            $examCenterList = $this->exam_center_model->examCenterList();
        // echo "<pre>";print_r($temp_details);exit();

            $student_data = $this->course_registration_model->getStudentByStudentId($id_student);


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;


        // echo $temp_details;


            $table = "



        <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";



        if($temp_details==NULL)
        {

             $table.= "

        <h4> No Exam Registered With The Selected Data </h4>";

        }
        else
        {


        $table.= "

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <br>



        <h4> Select Exam Registration Courses For Withdraw </h4>


        <table class='table' id='list-table'>
                  <tr>
                    <th>Sl. No </th>
                    <th>Semester</th>
                    <th>Course </th>
                    <th>Credit Hours</th>
                    <th>Withdraw</th>
                </tr>";
                
        
            

                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $mainId = $temp_details[$i]->id;
                    $course_name = $temp_details[$i]->course_name;
                    $course_code = $temp_details[$i]->course_code;
                    $semester_name = $temp_details[$i]->semester_name;
                    $semester_code = $temp_details[$i]->semester_code;
                    $credit_hours = $temp_details[$i]->credit_hours;


                    $j=$i+1;
                    
                        $table.= "
                        <tr>
                            <td>

                                $j
                            </td>
                            <td>
                                $semester_code - $semester_name
                            </td>

                            <td>
                                $course_code - $course_name
                            </td>
                            <td>
                                $credit_hours
                            </td>
                            <td>

                                <input type='checkbox' name='id_course_registration[]' id='id_course_registration' value='$mainId'>
                            </td>

                            </tr>";



                        //     <select name='id_exam_center[]' class='form-control'>
                        //     <option value=''>select</option>";

                        // foreach ($examCenterList as $record)
                        // { 
                        //     $table.="<option value=".$record->id.">".$record->name."</option>";
                        // }

                        //     $table.= "</select>
                        //     </td>
                        // </tr>";
                    }
                $table.= "</table>";

        }
        
        echo $table;
    }
}
