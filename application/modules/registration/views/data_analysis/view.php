<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Data Analysis Learning Center</h3>
      <a href="../learningCenterNSemester" class=" btn btn-link"> Back</a>
    </div>


          <div class="form-container">
                <h4 class="form-group-title">Programme Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Programme Name :</dt>
                                <dd><?php echo ucwords($programme->name);?></dd>
                            </dl>
                            <dl>
                                <dt>Type :</dt>
                                <dd><?php echo $programme->internal_external ?></dd>
                            </dl>                          
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Programme Code :</dt>
                                <dd><?php echo $programme->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Total Cr Hrs :</dt>
                                <dd><?php echo $programme->total_cr_hrs; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


    <div class="custom-table">
       <table class="table" border='1'>
        
        <thead>
          <tr>
            <th rowspan="2">Course</th>
              <th colspan="<?php echo count($intakeList);?>" class="text-center">Full Time</th>
              <th colspan="<?php echo count($intakeList);?>" class="text-center">Part Time</th>
              <th colspan="<?php echo count($intakeList);?>" class="text-center">Online</th>
            </tr>

            <tr>
               

              <?php
              foreach ($intakeList as $record)
              { ?>
               <th class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
              <?php
              }
            ?>


               <?php
           
              foreach ($intakeList as $record)
              {
            ?>
              <th class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
            <?php
              }
            ?>

               <?php
           
              foreach ($intakeList as $record)
              {
            ?>
              <th class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
            <?php
              }
            ?>




            </tr>

        </thead>
             <?php 
             $this->dataanalysisModel = new Data_analysis_model();

             // $courseListByid

                foreach ($courseListByProgrammeIdForLandscapeCourses as $record)
               {
             ?>
                <tr>
                  <th>
                    <!-- <a href="<?php echo 'view/' . $record->id ; ?>" title="View"> -->
                      
                    <?php echo $record->course_code . " - " . $record->course_name; ?>
                    <!-- </a> -->
                      
                    </th>


                <?php foreach ($intakeList as $intakeindividual)
                {
                  //full time 
                  
                  $learning_mode = 'Full Time';
                  $id_course_registered_landscape = $record->id_course_registered_landscape;
                  $id_intake = $intakeindividual->id;

                  $fulltimeCount = $this->dataanalysisModel->getStudentByProgramIntakeCourse($id_course_registered_landscape,$id_program,$id_intake,$learning_mode);

              ?>
                <!-- <th class="text-center">
                  <a href="<?php echo 'view/' . $id_program . '/' . $id_intake ; ?>" title="View"><?php echo $fulltimeCount ?></a>
                </th> -->

                <th class="text-center"><?php echo $fulltimeCount ?></th>
              <?php
                }
              ?>


              <?php foreach ($intakeList as $intakeindividual)
                {

                  //full time 
                  $learning_mode = 'Part Time';
                  $id_course_registered_landscape = $record->id_course_registered_landscape;
                  $id_intake = $intakeindividual->id;

                  $partTimeCount = $this->dataanalysisModel->getStudentByProgramIntakeCourse($id_course_registered_landscape,$id_program,$id_intake,$learning_mode);

              ?>
                <!-- <th class="text-center">
                  <a href="<?php echo 'view/' . $id_program . '/' . $id_intake ; ?>" title="View"><?php echo $partTimeCount ?></a>
                </th> -->

                <th class="text-center"><?php echo $partTimeCount ?></th>
              <?php
                }
              ?>



              <?php foreach ($intakeList as $intakeindividual)
                {

                  //full time 
                  $learning_mode = 'Online';
                  $id_course_registered_landscape = $record->id_course_registered_landscape;
                  $id_intake = $intakeindividual->id;

                  $onlinecount = $this->dataanalysisModel->getStudentByProgramIntakeCourse($id_course_registered_landscape,$id_program,$id_intake,$learning_mode);

              ?>
                <!-- <th class="text-center">
                <a href="<?php echo 'view/' . $id_program . '/' . $id_intake ; ?>" title="View"><?php echo $onlinecount ?></a>
                </th> -->
                <th class="text-center"><?php echo $onlinecount ?></th>

              
              <?php
                }
              ?>


                
                </tr>

               <?php
                }
                ?>
    
      </table>

      <!-- <table class="table" border='1' style="display: none;">
        <thead >
          <tr>
            <th rowspan="3" >Programme</th>
            <?php
            if (!empty($intakeList))
            {
              foreach ($intakeList as $record)
              {
            ?>
              <th colspan='6' class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
            <?php
              }
            }
            ?>
         </tr>

             <tr>
            <?php 
             foreach ($intakeList as $record)
              {
            ?>
            <th class="text-center" colspan='2'>Blended </th>
            <th class="text-center" colspan='2'>Face to Face</th>
            <th class="text-center" colspan='2'>Online</th>
            <?php
              
            }
            ?>
          </tr>


           <tr>
            <?php 
             foreach ($intakeList as $record)
              {
            ?>
            <th class="text-center">Part Time </th>
            <th class="text-center">Full Time</th>
            <th class="text-center">Part Time </th>
            <th class="text-center">Full Time</th>
            <th class="text-center">Part Time </th>
            <th class="text-center">Full Time</th>
            <?php
              
            }
            ?>
          </tr>
        </thead>
      </table> -->

    </div>



  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
