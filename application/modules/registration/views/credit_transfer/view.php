<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Approve Credit Transfer</h3>
        </div>

         <?php
        if($studentDetails != '')
        {
            ?>

        <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <!-- <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl> -->
                        </div>
                    </div>
                </div>
            </div>

        <?php
        }
        ?>


        <div class="form-container">
            <h4 class="form-group-title">Credit Transfer Details</h4>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Id <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="application_type" name="application_type" value="<?php echo $creditTransferData->application_id ?>" readonly >
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Institution Type <span class='error-text'>*</span></label>
                        <select name="institution_type" id="institution_type" class="form-control" disabled>
                          <option value=''>Select</option>
                            <option value='Internal' <?php if($creditTransferData->institution_type =='Internal')
                              { echo "selected=selected";} ?> >Internal
                            </option>
                            <option value='External' <?php if($creditTransferData->institution_type =='External')
                            { echo "selected=selected";} ?> >External
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="application_type" name="application_type" value="<?php echo $creditTransferData->application_type ?>" readonly >
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $creditTransferData->id_semester)
                                    { echo "selected"; }
                                ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Date <span class='error-text'>*</span></label>
                        <input type="test" class="form-control" id="from_tm" name="from_tm" value="<?php echo 
                        date("d-m-Y", strtotime($creditTransferData->created_dt_tm))
                         ?>" readonly >
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="test" class="form-control" id="status" name="status" value="<?php 
                        if($creditTransferData->status=='0')
                        {
                             echo "Pending";
                          }
                          elseif($creditTransferData->status=='1'){
                            echo "Approved";
                          }elseif($creditTransferData->status=='2'){
                            echo "Rejected";
                          };?>

                        " readonly >
                    </div>
                </div>

            </div>

            <div class="row">




        <?php
        if($creditTransferData->status == '2')
        {
            ?>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $creditTransferData->reason ?>" readonly >
                    </div>
                </div>

        <?php
        }
        ?>

                

                <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($creditTransferData->status=='1') {
                                 echo "checked=checked";
                              };?>  disabled><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($creditTransferData->status=='0') {
                                 echo "checked=checked";
                              };?> disabled>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>            --> 
                

            </div>

        </div>


            
            <form id="form_applicant" action="" method="post">

                
            <?php
                if($creditTransferData->status=='0')
                {
                ?>



            <div class="form-container">
                <h4 class="form-group-title">Approval Details</h4>

                <div class="row">

                                        
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Approval <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                    <input type="radio" id="sd1" name="status" value="1" ><span class="check-radio" onclick="hideRejectField()"></span> Approve
                                </label>
                                <label class="radio-inline">

                                    <input type="radio" id="sd2" name="status" value="2"><span class="check-radio" onclick="showRejectField()"></span> Reject
                                </label>
                                </div>
                            </div>

                             <div class="col-sm-4" id="view_reject" style="display: none">
                                <div class="form-group">
                                    <label>Reason <span class='error-text'>*</span></label>
                                    <input type="text" id="reason" name="reason" class="form-control">
                                </div>
                            </div>

                </div>

            </div>
            

                    <?php
                        }
                    ?>
            



            <div class="button-block clearfix">
                <div class="bttn-group">

            <?php
                if($creditTransferData->status=='0')
                {
                ?>

                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>

                <?php
                }
            ?>
                    <a href="../approvalList" class="btn btn-link">Back</a>
                </div>
            </div>

    </form>




     <?php

                    if(!empty($creditTransferDetails))
                    {
                        ?>
                        <br>

                        <div class="form-container">
                                <h4 class="form-group-title">Course Details</h4>

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Institution</th>
                                        <th>Subject/Course</th>
                                        <th>Course Equivalent</th>
                                        <th>Credit Hours</th>
                                        <th>Grade</th>
                                        <th>Remarks</th>
                                         <!-- <th class="text-center">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($creditTransferDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $creditTransferDetails[$i]->institution_type; ?></td>
                                        <td><?php echo $creditTransferDetails[$i]->subject_course; ?></td>
                                        <td><?php echo $creditTransferDetails[$i]->e_course_code . " - " . $creditTransferDetails[$i]->e_course_name;?></td>
                                        <td><?php echo $creditTransferDetails[$i]->credit_hours; ?></td>
                                        <td><?php echo $creditTransferDetails[$i]->grade_code . " - " . $creditTransferDetails[$i]->grade_name;?></td>
                                        <td><?php echo $creditTransferDetails[$i]->remarks; ?></td>
                                        <!-- <td class="text-center">
                                        <a onclick="deleteDetailData(<?php echo $creditTransferDetails[$i]->id; ?>)">Delete</a>
                                        </td> -->

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function saveDetailData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};

        tempPR['subject_course'] = $("#subject_course").val();
        tempPR['institution_type'] = $("#institution_type_detail").val();
        tempPR['id_course'] = $("#id_course").val();
        tempPR['id_equivalent_course'] = $("#id_equivalent_course").val();
        tempPR['id_grade'] = $("#id_grade").val();
        tempPR['remarks'] = $("#remarks").val();
        tempPR['id_credit_transfer'] = <?php echo $creditTransferData->id; ?>;

            $.ajax(
            {
               url: '/registration/creditTransfer/saveDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                // $("#view_temp_details").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }

    function deleteDetailData(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/registration/creditTransfer/deleteDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view_temp_details").html(result);
                    window.location.reload();
                    // alert(result);
                    // window.location.reload();
               }
            });
    }
    

     $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                subject_course: {
                    required: true
                },
                institution_type_detail: {
                    required: true
                },
                id_course: {
                    required: true
                },
                 id_equivalent_course: {
                    required: true
                },
                 id_grade: {
                    required: true
                },
                 remarks: {
                    required: true
                }
            },
            messages: {
                subject_course: {
                    required: "<p class='error-text'>Subject Required</p>",
                },
                institution_type_detail: {
                    required: "<p class='error-text'>Select Institution Type In Institute Section</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_equivalent_course: {
                    required: "<p class='error-text'>Select Equivalent Course</p>",
                },
                id_grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                remarks: {
                    required: "<p class='error-text'>Remarks Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );

    $('select').select2();


     function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }


    $(document).ready(function()
    {
        $("#form_applicant").validate({
            rules: {
                status: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Application Status Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>