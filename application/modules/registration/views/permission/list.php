<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List of Countries</h3>
      <a href="add" class="btn btn-primary">+ Add Country</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
              <table id="permission-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Code</th>
                <th>Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($permissionRecords))
                    {
                        foreach($permissionRecords as $record)
                        {
                    ?>
                    <tr>
                        <td><?php echo $record->code ?></td>
                        <td><?php echo $record->description ?></td>
                        <td class="text-center">
                            <a href="<?php echo 'edit/'.$record->permissionId; ?>" title="Edit">Edit</a>
                            <a class="btn btn-sm btn-danger deletePermission" href="#" data-permissionid="<?php echo $record->permissionId; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
                <tfoot>
                <tr>
                <th>Code</th>
                <th>Description</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
       
    </div>
</div>
<script>
  $(function () {
    $("#permission-table").DataTable();
  });
</script>
