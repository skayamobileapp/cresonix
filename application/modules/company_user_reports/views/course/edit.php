<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Course </h3>
        </div>



        <form id="form_semester" action="" method="post">

            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Course Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Teaching Components</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">













                        <div class="form-container">
                            <h4 class="form-group-title">Course Details</h4>

                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Name <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $courseDetails->name;?>">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Code <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $courseDetails->code;?>">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Name In Other Language</label>
                                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $courseDetails->name_optional_language;?>">
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                
                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Course Type <span class='error-text'>*</span></label>
                                        <select name="id_course_type" id="id_course_type" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($courseTypeList))
                                            {
                                                foreach ($courseTypeList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $courseDetails->id_course_type)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->code . " - " . $record->name;  ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
 


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Course Description <span class='error-text'>*</span></label>
                                        <select name="id_course_description" id="id_course_description" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($courseDescriptionList))
                                            {
                                                foreach ($courseDescriptionList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $courseDetails->id_course_description)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->name;  ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> -->


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Faculty Program <span class='error-text'>*</span></label>
                                        <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($facultyProgramList))
                                            {
                                                foreach ($facultyProgramList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $courseDetails->id_faculty_program)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->code . " - " . $record->name;  ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group"W>
                                        <label>Credit Hours <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="credit_hours" name="credit_hours" value="<?php echo $courseDetails->credit_hours;?>">
                                    </div>
                                </div>
                                



                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select name="id_department" id="id_department" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($departmentList))
                                            {
                                                foreach ($departmentList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $courseDetails->id_department)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->code ." - " . $record->name;  ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>




                            </div>


                            <div class="row">

                                

                                 <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Status <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="status" id="status" value="1" <?php if($courseDetails->status=='1') {
                                                 echo "checked=checked";
                                              };?>><span class="check-radio"></span> Active
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="status" id="status" value="0" <?php if($courseDetails->status=='0') {
                                                 echo "checked=checked";
                                              };?>>
                                              <span class="check-radio"></span> In-Active
                                            </label>                              
                                        </div>                         
                                </div>

                            </div>

                           

                        </div>



                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                <a href="../list" class="btn btn-link">Cancel</a>
                            </div>
                        </div>


                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">













                        <div class="form-container">
                            <h4 class="form-group-title">Class Time Table</h4>

                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Total Hours <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="class_total_hr" name="class_total_hr" value="<?php echo $courseDetails->class_total_hr;?>" min="1">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Recurrence <span class='error-text'>*</span></label>
                                        <select name="class_recurrence" id="class_recurrence" class="form-control" style="width: 398px">
                                            <option value=''>Select</option>

                                          <option value='Weekly' <?php if($courseDetails->class_recurrence =='Weekly')
                                          { echo "selected=selected";} ?> >Weekly
                                          </option>

                                          <option value='Monthly' <?php if($courseDetails->class_recurrence =='Monthly')
                                            { echo "selected=selected";} ?> >Monthly
                                          </option>
                                        
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Recurrence Period <span class='error-text'>*</span></label>
                                        <select name="class_recurrence_period" id="class_recurrence_period" class="form-control" style="width: 398px">
                                            <option value=''>Select</option>

                                          <option value='Once' <?php if($courseDetails->class_recurrence_period =='Once')
                                          { echo "selected=selected";} ?> >Once
                                          </option>

                                          <option value='Twice' <?php if($courseDetails->class_recurrence_period =='Twice')
                                            { echo "selected=selected";} ?> >Twice
                                          </option>

                                          <option value='Trice' <?php if($courseDetails->class_recurrence_period =='Trice')
                                            { echo "selected=selected";} ?> >Trice
                                          </option>
                                        
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="form-container">
                            <h4 class="form-group-title">Tutorial Time Table</h4>


                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Total Hours <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="tutorial_total_hr" name="tutorial_total_hr" value="<?php echo $courseDetails->tutorial_total_hr;?>" min="1">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Recurrence <span class='error-text'>*</span></label>
                                        <select name="tutorial_recurrence" id="tutorial_recurrence" class="form-control" style="width: 398px">
                                            <option value=''>Select</option>

                                          <option value='Weekly' <?php if($courseDetails->tutorial_recurrence =='Weekly')
                                          { echo "selected=selected";} ?> >Weekly
                                          </option>

                                          <option value='Monthly' <?php if($courseDetails->tutorial_recurrence =='Monthly')
                                            { echo "selected=selected";} ?> >Monthly
                                          </option>
                                        
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Recurrence Period <span class='error-text'>*</span></label>
                                        <select name="tutorial_recurrence_period" id="tutorial_recurrence_period" class="form-control" style="width: 398px">
                                            <option value=''>Select</option>

                                          <option value='Once' <?php if($courseDetails->tutorial_recurrence_period =='Once')
                                          { echo "selected=selected";} ?> >Once
                                          </option>

                                          <option value='Twice' <?php if($courseDetails->tutorial_recurrence_period =='Twice')
                                            { echo "selected=selected";} ?> >Twice
                                          </option>

                                          <option value='Trice' <?php if($courseDetails->tutorial_recurrence_period =='Trice')
                                            { echo "selected=selected";} ?> >Trice
                                          </option>
                                        
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>



                        <div class="form-container">
                            <h4 class="form-group-title">Lab Time Table</h4>



                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Total Hours <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="lab_total_hr" name="lab_total_hr" value="<?php echo $courseDetails->lab_total_hr;?>" min="1">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Recurrence <span class='error-text'>*</span></label>
                                        <select name="lab_recurrence" id="lab_recurrence" class="form-control"  style="width: 398px">
                                            <option value=''>Select</option>

                                          <option value='Weekly' <?php if($courseDetails->lab_recurrence =='Weekly')
                                          { echo "selected=selected";} ?> >Weekly
                                          </option>

                                          <option value='Monthly' <?php if($courseDetails->lab_recurrence =='Monthly')
                                            { echo "selected=selected";} ?> >Monthly
                                          </option>
                                        
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Recurrence Period <span class='error-text'>*</span></label>
                                        <select name="lab_recurrence_period" id="lab_recurrence_period" class="form-control" style="width: 398px">
                                            <option value=''>Select</option>

                                          <option value='Once' <?php if($courseDetails->lab_recurrence_period =='Once')
                                          { echo "selected=selected";} ?> >Once
                                          </option>

                                          <option value='Twice' <?php if($courseDetails->lab_recurrence_period =='Twice')
                                            { echo "selected=selected";} ?> >Twice
                                          </option>

                                          <option value='Trice' <?php if($courseDetails->lab_recurrence_period =='Trice')
                                            { echo "selected=selected";} ?> >Trice
                                          </option>
                                        
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>




                        <div class="form-container">
                            <h4 class="form-group-title">Exam Time Table</h4>



                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Total Hours <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="exam_total_hr" name="exam_total_hr" value="<?php echo $courseDetails->exam_total_hr;?>" min="1">
                                    </div>
                                </div>

                            </div>

                        </div>



                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                <a href="../list" class="btn btn-link">Cancel</a>
                            </div>
                        </div>



                        </div>




                        


                        </div>
                    
                    </div>






                </div>


            </div>



            



        </form>
















        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>


<script type="text/javascript">
    $('select').select2();

    $(document).ready(function()
    {
        $("#form_semester").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                credit_hours:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_course_type:
                {
                    required: true
                },
                class_total_hr:
                {
                    required: true
                },
                class_recurrence:
                {
                    required: true
                },
                class_recurrence_period:
                {
                    required: true
                },
                tutorial_total_hr:
                {
                    required: true
                },
                tutorial_recurrence:
                {
                    required: true
                },
                tutorial_recurrence_period:
                {
                    required: true
                },
                lab_total_hr:
                {
                    required: true
                },
                lab_recurrence:
                {
                    required: true
                },
                lab_recurrence_period:
                {
                    required: true
                },
                exam_total_hr:
                {
                    required: true
                },
                id_course_description:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                credit_hours:
                {
                    required: "<p class='error-text'>Credit Hours Required</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_course_type:
                {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                class_total_hr:
                {
                    required: "<p class='error-text'>Total Cr. Hours Required</p>",
                },
                class_recurrence:
                {
                    required: "<p class='error-text'>Recurrence Required</p>",
                },
                class_recurrence_period:
                {
                    required: "<p class='error-text'>Recurrence Period Required</p>",
                },
                tutorial_total_hr:
                {
                    required: "<p class='error-text'>Total Cr. Hours Required</p>",
                },
                tutorial_recurrence:
                {
                    required: "<p class='error-text'>Recurrence Required</p>",
                },
                tutorial_recurrence_period:
                {
                    required: "<p class='error-text'>Recurrence Period Required</p>",
                },
                lab_total_hr:
                {
                    required: "<p class='error-text'>Total Cr. Hours Required</p>",
                },
                lab_recurrence:
                {
                    required: "<p class='error-text'>Recurrence Required</p>",
                },
                lab_recurrence_period:
                {
                    required: "<p class='error-text'>Recurrence Period Required</p>",
                },
                exam_total_hr:
                {
                    required: "<p class='error-text'>Exam Total Hours Required</p>",
                },
                id_course_description:
                {
                    required: "<p class='error-text'>Select Course Description</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>