<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Applicant extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->isApplicantLoggedIn();
        error_reporting(0);
    }

    function index()
    {
        $this->welcome();
    }


    function welcome()
    {
        // $formData['name'] = $this->security->xss_clean($this->input->post('name'));

        // $data['searchParam'] = $formData;

        $this->global['pageTitle'] = 'Inventory Management : Internship List';
        $this->loadViews("applicant_view/v", $this->global, NULL, NULL);
    }


    function getcustomreplay($idmessage,$idapplicant)
    {





        switch($idmessage){
            case '1' :
                       $message = "  <p class='cb-help-text'>1</p>
                       <p class='cb-answer'>Your application status is under review, It may take 48hours to know the final status '<strong>0</strong>' :For main menu</p>";
                       break;
case '2' :
                       $message = "  <p class='cb-help-text'>2</p><p class='cb-answer'>Fee for the program will be RM 1500 <br/>For more information Type '<strong>0</strong>' :For main menu</p> ";
                       break;
case '3' :
                       $message = "  <p class='cb-help-text'>3</p><p class='cb-answer'>Temporary Offer Letter will be sent to your eail id.  <br/> For more information Type '<strong>0</strong>' :For main menu</p>";
                       break;
case '4' :
                       $message = " <p class='cb-help-text'>4</p><p class='cb-answer'>Semester will start from Oct 4th 2020 <br/> For more information Type '<strong>0</strong>' :For main menu</p>";
                       break;

case '9' :
                       $message = " <p class='cb-answer'>Thank your for your time, our representative will call back to you, you can also email us at info@gmail.com</p>
                    <p class='cb-help-text'>For more information Type '<strong>0</strong>' :For main menu</p> ";
                       break;
       

case '0' :
                       $message = "<p><ul class='cb-help'>
                        <li>Type '<strong>1</strong>' : For Application Status</li>
                        <li>Type '<strong>2</strong>' : For Fee Amount</li>
                        <li>Type '<strong>3</strong>' : For Offer Letter</li>
                        <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
                        <li>Type '<strong>9</strong>' : To get a call from the representative</li>
                    </ul></p> ";
                       break;                                                                                   

        }

        echo $message;
        exit;

    }

    function edit()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            if($_POST['fileid'] != '')
            {

            // echo "<Pre>"; print_r($this->input->post());exit();


            for($f=0;$f<count($_POST['fileid']);$f++)
            {
                // echo "<Pre>";
                // print_r($_POST[$f]);exit();

                if($_FILES['file']['name'][$f])
                {
                    $fileArray = array();
                    $fileArray['id_document'] = $_POST['fileid'][$f];
                    $fileArray['file_name'] = $_FILES['file']['name'][$f];
                    $fileArray['id_applicant'] = $id;
                    $result = $this->applicant_model->addFileDownload($fileArray);
                }
            }

                
            }
            
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_program = $this->security->xss_clean($this->input->post('id_program'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));

            $sibbling_discount = $this->security->xss_clean($this->input->post('sibbling_discount'));
            $alumni_discount = $this->security->xss_clean($this->input->post('alumni_discount'));
            $employee_discount = $this->security->xss_clean($this->input->post('employee_discount'));
            $id_program_scheme = $this->security->xss_clean($this->input->post('id_program_scheme'));


            $is_submitted = $this->security->xss_clean($this->input->post('is_submitted'));
            $is_hostel = $this->security->xss_clean($this->input->post('is_hostel'));
            $id_degree_type = $this->security->xss_clean($this->input->post('id_degree_type'));
            $id_branch = $this->security->xss_clean($this->input->post('id_branch'));
            $program_scheme = $this->security->xss_clean($this->input->post('program_scheme'));

            $entry = $this->security->xss_clean($this->input->post('entry'));

            if($is_submitted == '')
            {
                $is_submitted = 0;
            }

            // echo "p" . $id_program . "- I". $id_intake;exit();
            // echo "S" . $is_submitted;exit();


            $salutationInfo = $this->applicant_model->getSalutation($salutation);


            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'email_id' => $email_id,
                'contact_email' => $contact_email,
                'password' => $password,
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'religion' => $religion,
                'nationality' => $nationality,
                'id_race' => $id_race,
                'id_program' => $id_program,
                'id_intake' => $id_intake,
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode,
                'sibbling_discount' => $sibbling_discount,
                'employee_discount' => $employee_discount,
                'alumni_discount' => $alumni_discount,
                'is_updated' => 1,
                'is_submitted' => $is_submitted,
                'is_hostel' => 1,
                'id_degree_type' => $id_degree_type,
                'id_program_scheme' => $id_program_scheme,
                'id_branch' => $id_branch,
                'updated_by' => $id_user
            );

            $data['pathway'] = 'DIRECT ENTRY';
            
            if($entry == 1)
            {
                $data['is_apeal_applied'] = 0;
                $data['pathway'] = 'APEL';
            }

            $data['id_program_requirement'] = $entry;


            if($is_submitted == '1')
            {
                $get_program_scheme = $this->applicant_model->getProgramScheme($id_program_scheme);

                $data['program_scheme'] = $get_program_scheme->mode_of_program . " - " . $get_program_scheme->mode_of_study;
                $data['submitted_date'] = date('Y-m-d H:i:s');


                // $data['id_program_requirement'] = $entry;
            }

            // echo "<Pre>"; print_r($data);exit();

            // $checkDuplicate = $this->applicant_model->checkDuplicateApplicant($data,$id);
            // echo "<pre>"; print_r($checkDuplicate);exit();
            // if($checkDuplicate)
            // {
            //     echo "Entered Profile E-Mail / Phone / NRIC Already Exist";exit();
            // }



            if($sibbling_discount == 'Yes')
            {
                $data['is_sibbling_discount'] = '0';
            }
            elseif($sibbling_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }


            if($employee_discount == 'Yes')
            {
                $data['is_employee_discount'] = '0';
            }
            elseif($employee_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }


            if($alumni_discount == 'Yes')
            {
                $data['is_alumni_discount'] = '0';
            }
            elseif($alumni_discount == 'No')
            {
                $data['is_alumni_discount'] = '3';
            }

            $sibbling_name = $this->security->xss_clean($this->input->post('sibbling_name'));
            $sibbling_nric = $this->security->xss_clean($this->input->post('sibbling_nric'));

                 $sibbileData = array(
                    'id_applicant' => $id,
                    'sibbling_name' => $sibbling_name,
                    'sibbling_nric' => $sibbling_nric,
                );

            $checkSibblingDicount = $this->applicant_model->getSibblingDiscountByApplicantId($id);

            if($checkSibblingDicount)
            {
                $result = $this->applicant_model->editSibblingDetails($sibbileData, $id);
            }
            else
            {
                // $sibbileData['id_applicant'] = $id;
                if($sibbling_discount == 'Yes')
                {
                    $result = $this->applicant_model->addNewSibblingDiscount($sibbileData);
                }
            }

                

            $employee_name = $this->security->xss_clean($this->input->post('employee_name'));
            $employee_nric = $this->security->xss_clean($this->input->post('employee_nric'));
            $employee_designation = $this->security->xss_clean($this->input->post('employee_designation'));

            $employData = array(
                    'id_applicant' => $id,
                    'employee_name' => $employee_name,
                    'employee_nric' => $employee_nric,
                    'employee_designation' => $employee_designation
                );
            
            $checkEmployeeDicount = $this->applicant_model->getEmployeeDiscountApplicantId($id);

            if($checkEmployeeDicount)
            {
                $result = $this->applicant_model->editEmployeeDetails($employData, $id);
            }
            else
            {
                if($employee_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->applicant_model->addNewEmployeeDiscount($employData);
                }
            }




            $alumni_name = $this->security->xss_clean($this->input->post('alumni_name'));
            $alumni_nric = $this->security->xss_clean($this->input->post('alumni_nric'));
            $alumni_email = $this->security->xss_clean($this->input->post('alumni_email'));

            $alumniData = array(
                    'id_applicant' => $id,
                    'alumni_name' => $alumni_name,
                    'alumni_nric' => $alumni_nric,
                    'alumni_email' => $alumni_email
                );
            

            $checkAlumniDicount = $this->applicant_model->getAlumniDiscountApplicantId($id);

            // echo "<Pre>"; print_r($checkAlumniDicount);exit;
            if($checkAlumniDicount)
            {
                $result = $this->applicant_model->editAlumniDetails($alumniData, $id);
            }
            else
            {
                if($alumni_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->applicant_model->addNewAlumniDiscount($alumniData);
                }
            }


            // $result = $this->applicant_model->editEmployeeDetails($employData, $id);

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/edit');
        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
                // echo "<Pre>"; print_r($data);exit;


        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();
            $data['countryList'] = $this->applicant_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_model->stateList();
            $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
            $data['raceList'] = $this->applicant_model->raceListByStatus('1');
            $data['religionList'] = $this->applicant_model->religionListByStatus('1');
            $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
            $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');

            $data['sibblingDiscountDetails'] = $this->applicant_model->getSibblingDiscountByApplicantId($id);

            if(!empty($data['sibblingDiscountDetails']))
            {
                if($data['sibblingDiscountDetails']->sibbling_status != 'Pending')
                {
                    $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
                }
            }
            $data['employeeDiscountDetails'] = $this->applicant_model->getEmployeeDiscountApplicantId($id);
            if(!empty($data['employeeDiscountDetails']))
            {

                if($data['employeeDiscountDetails']->employee_status != 'Pending')
                {
                    $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
                }
            }
            $data['alumniDiscountDetails'] = $this->applicant_model->getAlumniDiscountApplicantId($id);
            if(!empty($data['alumniDiscountDetails']))
            {

                if($data['alumniDiscountDetails']->alumni_status != 'Pending')
                {
                    $data['alumniDiscountDetails'] = $this->applicant_model->getApplicantAlumniDiscountDetails($id);
                }
            }
            

                // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';
            $this->loadViews("applicant_view/edit", $this->global, $data, NULL);
        }
    }

    function step1()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
       

        if($this->input->post())
        {
                      
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
            $country_code = $this->security->xss_clean($this->input->post('country_code'));
           
            $salutationInfo = $this->applicant_model->getSalutation($salutation);

            if($nric == '')
            {
                $nric = $passport;
            }


            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'country_code' => $country_code,
                'email_id' => $email_id,
                'contact_email' => $contact_email,
                'password' => $password,
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'religion' => $religion,
                'is_updated' => 1,
                'is_hostel' => 1,
                'nationality' => $nationality,
                'id_race' => $id_race,
                
            );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/step2');
        }
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status !='Draft')
        {
            redirect('/applicant/applicant/rstep1');

        }

        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['countryList'] = $this->applicant_model->countryListByStatusForPhoneCode('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1'); 
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
 
        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';            
        $this->loadViews("applicant_view/step1", $this->global, $data, NULL);
    }


    function step2()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
           if($this->input->post())
        {
                      
            $present_address_same_as_mailing_address = $this->security->xss_clean($this->input->post('present_address_same_as_mailing_address'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
            $salutationInfo = $this->applicant_model->getSalutation($salutation);


            $whatsapp_number = $this->security->xss_clean($this->input->post('whatsapp_number'));
            $linked_in = $this->security->xss_clean($this->input->post('linked_in'));
            $facebook_id = $this->security->xss_clean($this->input->post('facebook_id'));
            $twitter_id = $this->security->xss_clean($this->input->post('twitter_id'));
            $ig_id = $this->security->xss_clean($this->input->post('ig_id'));

            $data = array(
                'present_address_same_as_mailing_address' => $present_address_same_as_mailing_address,
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode,
                'whatsapp_number' => $whatsapp_number,
                'linked_in' => $linked_in,
                'facebook_id' => $facebook_id,
                'twitter_id' => $twitter_id,
                'ig_id' => $ig_id
            );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/step3');
        }
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }


        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');  
        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';            

        $this->loadViews("applicant_view/step2", $this->global, $data, NULL);
    }
    function step3()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);


        // print_r($data['getApplicantDetails']);exit;

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/applicant/applicant/view');
        }



       if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit();

            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_program = $this->security->xss_clean($this->input->post('id_program'));
             $id_program_scheme = $this->security->xss_clean($this->input->post('id_program_scheme'));
            // $id_program_has_scheme = $this->security->xss_clean($this->input->post('id_program_has_scheme'));
            $id_branch = $this->security->xss_clean($this->input->post('id_branch'));
            $id_university = $this->security->xss_clean($this->input->post('id_university'));
            $id_program_structure_type = $this->security->xss_clean($this->input->post('id_program_structure_type'));


            $id_english_test = $this->security->xss_clean($this->input->post('id_english_test'));
            $english_year = $this->security->xss_clean($this->input->post('english_year'));
            $english_grade = $this->security->xss_clean($this->input->post('english_grade'));
            $id_degree_type = $this->security->xss_clean($this->input->post('id_degree_type'));
            $entry = $this->security->xss_clean($this->input->post('id_program_requirement'));



            $program = $this->applicant_model->getProgramDetails($id_program);

            $id_degree_type = $program->id_education_level;


            $id_program_has_scheme = '1';
            if($data['getApplicantDetails']->nationality!='1') {
                $id_program_has_scheme = '3';
            }

            $idbranchResult = $this->applicant_model->getBranchesByPartnerUniversity($id_university);
            if($idbranchResult) {

            $id_branch = $idbranchResult[0]->id;
        }






            $data = array(
                'id_program' => $id_program,
                'id_intake' => $id_intake,
                'id_degree_type' => $program->id_education_level,
                'id_program_scheme' => $id_program_scheme,
                'id_program_structure_type' => $id_program_structure_type,
                'id_program_has_scheme' => $id_program_has_scheme,
                'id_university' => $id_university,
                'id_branch' => $id_branch,
                'id_english_test' => $id_english_test,
                'english_year' => $english_year,
                'english_grade' => $english_grade,
                'id_program_requirement'=>$entry

            );
            
            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/step4');
        }



        $data['intakeList'] = $this->applicant_model->intakeList();
        // $data['programList'] = $this->applicant_model->programList();
        // $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        // $data['stateList'] = $this->applicant_model->stateList();
        // $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');  
        $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');

        // echo "<Pre>"; print_r($data['partnerUniversityList']);exit();

        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';  
        $this->loadViews("applicant_view/step3", $this->global, $data, NULL);
    }
    
    function step4()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            if($_POST['fileid'] != '')
            {

            // echo "<Pre>"; print_r($this->input->post());exit();

            // echo "<Pre>"; print_r($_POST['fileid']);exit();

            for($f=0;$f<count($_POST['fileid']);$f++)
            {
                // echo "<Pre>"; print_r($_POST['fileid']);exit();
                // echo "<Pre>";
                // print_r($_POST[$f]);exit();

                if($_FILES['file']['name'][$f])
                {

                    // echo "<Pre>"; print_r($_FILES['file']['name'][$f]);exit();

                    $certificate_name = $_FILES['file']['name'][$f];
                    $certificate_size = $_FILES['file']['size'][$f];
                    $certificate_tmp =$_FILES['file']['tmp_name'][$f];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate');

                    $file = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate');



                    $fileArray = array();

                    if($file)
                    {
                        $fileArray['file'] = $file;
                    }

                    $fileArray['id_document'] = $_POST['fileid'][$f];
                    $fileArray['file_name'] = $_FILES['file']['name'][$f];
                    $fileArray['id_applicant'] = $id;
                    $result = $this->applicant_model->addFileDownload($fileArray);
                }
            }

            redirect('/applicant/applicant/step5');
                
            }
        }
         $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        $data['getFileList'] = $this->applicant_model->getDocumentByProgrammeId($data['getApplicantDetails']->id_program);

        $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);


        // echo "<Pre>"; print_r($data['getFileList']);exit();



        $this->global['pageTitle'] = 'Applicant Portal : Applicant Step 4';  
        $this->loadViews("applicant_view/step4", $this->global, $data, NULL);
    }

    function step5()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            
            $discountData = $this->input->post();

            $this->applicant_model->deleteApplicantdiscount($id);


            for($f=0;$f<count($discountData['discount']);$f++)
            {
                // echo "<Pre>";
                $fileArray = array();
                

                    $fileArray['id_discount'] = $discountData['discount'][$f];
                    $fileArray['id_applicant'] = $id;
                    $result = $this->applicant_model->addDiscountApplied($fileArray);
                
            }

            redirect('/applicant/applicant/step6');
                
            
        }

         $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

               $data['applicantDiscountDetails'] = $this->applicant_model->getApplicantDiscountDetailsByApplicant($id);


        $data['discountDetails'] = $this->applicant_model->getApplicantDiscountDetails($data['getApplicantDetails']->id_intake);



        $this->loadViews("applicant_view/step5", $this->global, $data, NULL);
    }

    function step6()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;


        $datagetApplicantDetails = $this->applicant_model->getApplicantDetailsById($id);



        if($this->input->post())
        {
            $entry = $this->security->xss_clean($this->input->post('entry'));
            $submitapp = $this->security->xss_clean($this->input->post('submitapp'));


            $data['pathway'] = 'DIRECT ENTRY';

            $data['applicant_status'] = $submitapp;
            if($submitapp=='Submitted') {
                $is_submitted = $data['is_submitted'] = 1;
            } else {
                $is_submitted = $data['is_submitted'] = 0;
            }
            
            if($entry == 1)
            {
                $data['is_apeal_applied'] = 0;
                $data['pathway'] = 'APEL';
            }

            // $data['id_program_requirement'] = $entry;

            if($is_submitted == '1')
            {
                $applicant_data = $this->applicant_model->getApplicantDetailsById($id);

                $id_program_scheme = $applicant_data->id_program_scheme;
                $id_program_has_scheme = $applicant_data->id_program_has_scheme;
                $id_intake = $applicant_data->id_intake;
                $id_program = $applicant_data->id_program;
                $qualification = $applicant_data->qualification;




                $get_program_scheme = $this->applicant_model->getProgramScheme($id_program_scheme);
                // $program_landscape = $this->applicant_model->getProgramLandscape($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);


                // echo "<Pre>";print_r($program_landscape);exit();

                if($qualification == 'POSTGRADUATE')
                {
                    $data['phd_duration'] = 1;
                }


                $data['program_scheme'] = $get_program_scheme->mode_of_program . " - " . $get_program_scheme->mode_of_study;


                $program = $this->applicant_model->getProgramDetails($id_program);
                // $id_fee_structure = 0;

                if($program)
                {


                    //get the idfee structure by program,scheme,learnng mode, intake

                    $id_fee_structure = $this->applicant_model->getFeeStructureByProgramIntakeLearningMode($id_program,$id_program_scheme,$id_intake);
                    
                    
                     $data['id_fee_structure'] = $id_fee_structure;


                    // $program_mode = $program->mode;

                    // if($program_mode == 1)
                    // {
                    //     $program_landscape = $this->applicant_model->getProgramLandscapeByLearningMode($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);

                    //     if($program_landscape)
                    //     {
                    //         $data['id_program_landscape'] = $program_landscape->id;
                    //     }    
                    // }
                    // elseif($program_mode == 0)
                    // {
                    //     $program_landscape = $this->applicant_model->getProgramLandscapeByMultipleLearningMode($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);
                    //      if($program_landscape)
                    //     {
                    //         $data['id_program_landscape'] = $program_landscape->id_program_landscape;
                    //     }
                    // }



                    $program_landscape = $this->applicant_model->getProgramLandscapeByLearningMode($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);
                    
                    // echo "<Pre>";print_r($program_landscape);exit();

                    if($program_landscape)
                    {
                        $data['id_program_landscape'] = $program_landscape->id;
                    }
                }

                $data['submitted_date'] = date('Y-m-d H:i:s');


                // $data['id_program_requirement'] = $entry;
            }


            // echo "<Pre>";print_r($data);exit();

            
            $result = $this->applicant_model->editApplicantDetails($data,$id);

            if($is_submitted=='1') {
            redirect('/applicant/applicant/dashboard');

            } else {
            redirect('/applicant/applicant/step6');

            }
            // $this->view();

           
        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        
        
        $this->loadViews("applicant_view/step6", $this->global, $data, NULL);
    }


    function rstep1()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1'); 
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
 
        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';            
        $this->loadViews("applicant_view/rstep1", $this->global, $data, NULL);
    }


    function rstep2()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');  
        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';            

        $this->loadViews("applicant_view/rstep2", $this->global, $data, NULL);
    }
    function rstep3()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);





        $data['intakeList'] = $this->applicant_model->intakeList();
        // $data['programList'] = $this->applicant_model->programList();
        // $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        // $data['stateList'] = $this->applicant_model->stateList();
        // $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');  
        $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');

        // echo "<Pre>"; print_r($data['partnerUniversityList']);exit();

        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';  
        $this->loadViews("applicant_view/rstep3", $this->global, $data, NULL);
    }
    
    function rstep4()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        
         $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        $data['getFileList'] = $this->applicant_model->getDocumentByProgrammeId($data['getApplicantDetails']->id_program);

        $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);

        $this->loadViews("applicant_view/rstep4", $this->global, $data, NULL);
    }

    function rstep5()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

      

         $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

               $data['applicantDiscountDetails'] = $this->applicant_model->getApplicantDiscountDetailsByApplicant($id);


        $data['discountDetails'] = $this->applicant_model->getApplicantDiscountDetails($data['getApplicantDetails']->id_intake);



        $this->loadViews("applicant_view/rstep5", $this->global, $data, NULL);
    }

    function rstep6()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
        $datagetApplicantDetails = $this->applicant_model->getApplicantDetailsById($id);
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);        
        $this->loadViews("applicant_view/rstep6", $this->global, $data, NULL);
    }

    function chat()
    {
        $id = $this->session->id_applicant;


        $data['applicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);


        $this->global['pageTitle'] = 'Inventory Management : Change Password';
        $this->loadViews("applicant_view/chat", $this->global, $data, NULL);
    }

    function changePasssword()
    {
        $id = $this->session->id_applicant;


        if($this->input->post())
        {
            $pwd = $this->security->xss_clean($this->input->post('pwd'));
            $cnfpwd = $this->security->xss_clean($this->input->post('cnfpwd'));

          

            $data = array(
                    'password' => md5($pwd)
                );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
        }

        $data['applicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);


        $this->global['pageTitle'] = 'Inventory Management : Change Password';
        $this->loadViews("applicant_view/change_password", $this->global, $data, NULL);
    }

    function view()
    {
                redirect('/applicant/applicant/rstep1');

        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

       

        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetails($id);
        // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
        $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
        $data['alumniDiscountDetails'] = $this->applicant_model->getApplicantAlumniDiscountDetails($id);
        $data['receiptStatus'] = $this->applicant_model->getReceiptStatus($id);
        $data['degreeTypeList'] = $this->applicant_model->qualificationList();
        $data['temp_offer_letter'] = $this->applicant_model->getTemperoryOfferLetterByIntake($data['getApplicantDetails']->id_intake);
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['branchList'] = $this->applicant_model->branchListByStatus();
        $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');
        $data['schemeList'] = $this->applicant_model->schemeListByStatus('1');
        $data['programStructureTypeList'] = $this->applicant_model->programStructureTypeListByStatus('1');


        $data['requiremntListList'] = $this->applicant_model->programRequiremntListList();
        $data['programEntryRequirementList'] = $this->applicant_model->programEntryRequirementList($data['getApplicantDetails']->id_program);
        $data['getProgramDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);

        // $data['programDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);

        $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);


        $data['feeStructureDetails'] = $this->applicant_model->getfeeStructureMasterByApplicant($id);

        $id_fee_structure = $this->applicant_model->getfeeStructureMasterByApplicantID($id);
        $fee_structure = $this->applicant_model->getfeeStructureMaster($id_fee_structure);
        
        $data['id_fee_structure'] = $id_fee_structure;
        $data['feeStructure'] = $fee_structure;

        $data['partnerUniversity'] = $this->applicant_model->getPartnerUniversityDetails($data['getApplicantDetails']->id_university);
        
        // echo "<Pre>";print_r($data['feeStructureDetails']);exit;


        $invoiceDetails = $this->applicant_model->applicantInvoice($id);
        $data['applicantInvoice'] = $invoiceDetails;

        if($invoiceDetails)
        {
            $data['applicantInvoiceDetails'] = $this->applicant_model->applicantInvoiceDetails($invoiceDetails->id);
            $data['applicantInvoiceDiscountDetails'] = $this->applicant_model->getMainInvoiceDiscountDetails($invoiceDetails->id);
        }




        // echo "<Pre>";print_r($data['applicantInvoiceDiscountDetails']);exit;

        $this->global['pageTitle'] = 'Inventory Management : View Applicant';
        $this->loadViews("applicant_view/view", $this->global, $data, NULL);

        

    }

    function feestructure()
    {
        $id = $this->session->id_applicant;

        
         $applicant_information = $this->applicant_model->getApplicantInformation($id);

         $currency = 'USD';
         if($applicant_information->nationality=='1') {
            $currency = 'MYR';
         }

        // $organisation = $this->applicant_model->getOrganisation();

         $applicantNAme = $applicant_information->full_name;
         $program_name = $applicant_information->program_name;
         $intake_name = $applicant_information->intake_name;
         $nric = $applicant_information->nric;

         $mail_address1 = $applicant_information->mail_address1;
         $mail_address2 = $applicant_information->mail_address2;
         $mailing_city = $applicant_information->mailing_city;
         $mailing_zipcode = $applicant_information->mailing_zipcode;
         $nric = $applicant_information->nric;
         $created_dt_tm = $applicant_information->created_dt_tm;


         $mode_of_program = $applicant_information->mode_of_program;
         $mode_of_study = $applicant_information->mode_of_study;

         $branchname = $applicant_information->branchname;

         $id_fee_structure = $applicant_information->id_fee_structure;
        $data['applicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

         $data['feedetails'] = $this->applicant_model->getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure,$currency);

        $this->loadViews("applicant_view/feestructure", $this->global, $data, NULL);
    }



    function dashboard()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

               

       

        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsForDraft($id);
         // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
        $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
        $data['alumniDiscountDetails'] = $this->applicant_model->getApplicantAlumniDiscountDetails($id);
        $data['receiptStatus'] = $this->applicant_model->getReceiptStatus($id);
        $data['degreeTypeList'] = $this->applicant_model->qualificationList();
        $data['temp_offer_letter'] = $this->applicant_model->getTemperoryOfferLetterByIntake($data['getApplicantDetails']->id_intake);
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['branchList'] = $this->applicant_model->branchListByStatus();
        $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');
        $data['schemeList'] = $this->applicant_model->schemeListByStatus('1');
        $data['programStructureTypeList'] = $this->applicant_model->programStructureTypeListByStatus('1');


        $data['requiremntListList'] = $this->applicant_model->programRequiremntListList();
        $data['programEntryRequirementList'] = $this->applicant_model->programEntryRequirementList($data['getApplicantDetails']->id_program);
        $data['getProgramDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);

        // $data['programDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);

        $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);


        $data['feeStructureDetails'] = $this->applicant_model->getfeeStructureMasterByApplicant($id);

        $id_fee_structure = $this->applicant_model->getfeeStructureMasterByApplicantID($id);
        $fee_structure = $this->applicant_model->getfeeStructureMaster($id_fee_structure);
        
        $data['id_fee_structure'] = $id_fee_structure;
        $data['feeStructure'] = $fee_structure;

        $data['partnerUniversity'] = $this->applicant_model->getPartnerUniversityDetails($data['getApplicantDetails']->id_university);
        


        $invoiceDetails = $this->applicant_model->applicantInvoice($id);
        $data['applicantInvoice'] = $invoiceDetails;

        if($invoiceDetails)
        {
            $data['applicantInvoiceDetails'] = $this->applicant_model->applicantInvoiceDetails($invoiceDetails->id);
            $data['applicantInvoiceDiscountDetails'] = $this->applicant_model->getMainInvoiceDiscountDetails($invoiceDetails->id);
        }


        // echo "<Pre>";print_r($data['applicantInvoiceDiscountDetails']);exit;

        $this->global['pageTitle'] = 'EAG Applicant Portal : Applicant Dashboard';
        $this->loadViews("applicant_view/dashboard", $this->global, $data, NULL);
    }


    function dashboardnew()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

               

       

        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsForDraft($id);
         // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
        $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
        $data['alumniDiscountDetails'] = $this->applicant_model->getApplicantAlumniDiscountDetails($id);
        $data['receiptStatus'] = $this->applicant_model->getReceiptStatus($id);
        $data['degreeTypeList'] = $this->applicant_model->qualificationList();
        $data['temp_offer_letter'] = $this->applicant_model->getTemperoryOfferLetterByIntake($data['getApplicantDetails']->id_intake);
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['branchList'] = $this->applicant_model->branchListByStatus();
        $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');
        $data['schemeList'] = $this->applicant_model->schemeListByStatus('1');
        $data['programStructureTypeList'] = $this->applicant_model->programStructureTypeListByStatus('1');


        $data['requiremntListList'] = $this->applicant_model->programRequiremntListList();
        $data['programEntryRequirementList'] = $this->applicant_model->programEntryRequirementList($data['getApplicantDetails']->id_program);
        $data['getProgramDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);

        // $data['programDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);

        $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);


        $data['feeStructureDetails'] = $this->applicant_model->getfeeStructureMasterByApplicant($id);

        $id_fee_structure = $this->applicant_model->getfeeStructureMasterByApplicantID($id);
        $fee_structure = $this->applicant_model->getfeeStructureMaster($id_fee_structure);
        
        $data['id_fee_structure'] = $id_fee_structure;
        $data['feeStructure'] = $fee_structure;

        $data['partnerUniversity'] = $this->applicant_model->getPartnerUniversityDetails($data['getApplicantDetails']->id_university);
        


        $invoiceDetails = $this->applicant_model->applicantInvoice($id);
        $data['applicantInvoice'] = $invoiceDetails;

        if($invoiceDetails)
        {
            $data['applicantInvoiceDetails'] = $this->applicant_model->applicantInvoiceDetails($invoiceDetails->id);
            $data['applicantInvoiceDiscountDetails'] = $this->applicant_model->getMainInvoiceDiscountDetails($invoiceDetails->id);
        }


        // echo "<Pre>";print_r($data['applicantInvoiceDiscountDetails']);exit;

        $this->global['pageTitle'] = 'EAG Applicant Portal : Applicant Dashboard';
        $this->loadViews("applicant_view/dashboardnew", $this->global, $data, NULL);
    }

    function generateTempOfferLetter()
    {
        // $base_url = $_SERVER['HTTP_HOST'];
         $id = $this->session->id_applicant;

         $this->getMpdfLibrary();
        
         $applicant_information = $this->applicant_model->getApplicantInformation($id);

         $currency = 'USD';
         if($applicant_information->nationality=='1') {
            $currency = 'MYR';
         }

         // print_r($applicant_information);

         $applicantNAme = $applicant_information->full_name;
         $program_name = $applicant_information->program_name;
         $intake_name = $applicant_information->intake_name;
         $nric = $applicant_information->nric;

         $mail_address1 = $applicant_information->mail_address1;
         $mail_address2 = $applicant_information->mail_address2;
         $mailing_city = $applicant_information->mailing_city;
         $mailing_zipcode = $applicant_information->mailing_zipcode;
         $nric = $applicant_information->nric;
         $created_dt_tm = date('d-m-Y',strtotime($applicant_information->created_dt_tm));
         $created_tm = date('H:i:s',strtotime($applicant_information->created_dt_tm));


         $mode_of_program = $applicant_information->mode_of_program;
         $mode_of_study = $applicant_information->mode_of_study;


         $duration = $applicant_information->min_duration.' - '.$applicant_information->max_duration.' Months';


         $branchname = $applicant_information->branchname;

         $id_fee_structure = $applicant_information->id_fee_structure;

         $feedetails = $this->applicant_model->getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure,$currency);

         $facultyDetailsList = $this->applicant_model->getFacultyDetails($applicant_information->id_program);
         $faculty = $facultyDetailsList->description;



         $file_data="<table width='100%' border='1' style='font-family:arial;line-height: 1.6;'>
                        <tr>
                          <th rowspan='2'>Payment Details</th>
                          <th colspan='2'>Payment</th>
                        </tr>
                        <tr>
                          <th>Ringgit Malaysia (RM)</th>
                          <th>Dollars (USD) <br/>
                            RM 1 = $0.25</th>
                          </tr>

  ";

  for($i=0;$i<count($feedetails);$i++) {
    $feename = $feedetails[$i]->fee_structure;
    $amount = $feedetails[$i]->amount;
    $fee_structure_code = $feedetails[$i]->fee_structure_code;

    if($feename=='Registration Fee' || $feename=='Online Material Fees') {
        $regamount = $amount;
        $regusamount = ($amount/4);
    }
    if($feename=='Recurring Semester Fee' || $feename=='Semester Fees') {
        $semamount = $amount;
        $semusamount = ($amount/4);
    }
     if($fee_structure_code=='TUT') {
                $tuitamount = $amount;
        $tuitusamount = ($amount/4);

    }
    $usdamount = ($amount/4);
                        // $file_data.="<tr>
                        //  <td>$feename</td>
                        //  <td>RM $amount</td>
                        //  <td>$ $usdamount</td>
                        //  </tr>";

}
                         
                         $file_data.="<tr>
                          <td><b>Registration Fee (one-off)</b><br/>

Payable in full before registration (non-refundable).
</td><td>RM $regamount</td><td>$ $regusamount</td></tr>
<tr><td><b>Semester Fee</b><br/>

 To be paid on second semester and onwards.
</td><td>RM $semamount</td><td>$ $semusamount</td></tr>
<tr>
                          <td><b>Tuition Fee </b><br/> The fee is to be paid by student each semester according to the number of credit hours registered.
</td><td>RM $tuitamount</td><td>$ $tuitusamount</td></tr>
<tr>
  <td><b>Accommodation fee (per semester) -</b>
if you are studying in Malaysia
and require Accommodation
• Payment upon arrival</td><td>RM 500 (oncampus)</td><td>RM 800 (Off Campus)</td></tr>
<tr>
  <td><b>MUET (Malaysian University
English Test)</b></td><td>NIL</td><td>NIL</td></tr>
<tr>
  <td><b>International student EMGS</b>This fee is applicable if you are studying in Malaysia<br/>
  <b>New Visa Processing Fee</b> (one-off for the 1st Year) <br/>
  <b>Renewal Visa Processing Fee </b>(to be paid by following year onwards)
  <br/></td><td><br/>RM 2079.00 <br/>  <br/>RM 758.40</td><td><br/>$ 519.75 <br/> <br/>$ 189.60</td></tr>
<tr>
  <td><b>Immigration Fee (Yearly)</b> This fee is applicable if you are studying in Malaysia<br/> Student pass and visa Endorsement (Payment upon approval)</td>
  <td>RM 80.00</td><td>RM 20.00</td></tr>
<tr>
  <td><b>Personal Bond</b> This fee is applicable if you are studying in Malaysia<br/> The charges for personal Bond will be charged upon arrival and will be refuned upon completion of studies. <br/> The personal Bond will not be refunded should any situation arise where the student is found to either misused their visa or violated any regulations applicable to International Studies in Malaysia </td><td>RM 1500.00</td><td>$ 375.00</td></tr> <tr><td></br/></td></tr>";

                          $file_data.="</table>";  


        // $id_branch = $applicant_information->id_branch;

        // if($id_branch == 1)
        // {
        //     $branchname = $organisation->short_name . " - " . $organisation->name;
        // }




             $templateResult = $this->applicant_model->gettemplate('1');
             $message = "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='http://eag.camsedu.com/assets/images/usas.jpg' width='250px' style='display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;' /></p>";
            $message = $message.$templateResult->message;

        
                     $message = str_replace("@studentname",$applicantNAme,$message);
                     $message = str_replace("@program",$program_name,$message);

                     $message = str_replace("@nric",$nric,$message);
                     $message = str_replace("@intake",$intake_name,$message);

                     $message = str_replace("@mail_address1",$mail_address1,$message);
                     $message = str_replace("@mail_address2",$mail_address2,$message);
                     $message = str_replace("@mailing_city",$mailing_city,$message);
                     $message = str_replace("@mailing_zipcode",$mailing_zipcode,$message);


                     $message = str_replace("@mode_of_program",$mode_of_program,$message);
                     $message = str_replace("@mode_of_study",$mode_of_study,$message);

                                          $message = str_replace("Online","ODL",$message);


                     $message = str_replace("@duration",$duration,$message);

                     $message = str_replace("@faculty",$faculty,$message);

                     


                     $message = str_replace("@branchname",$branchname,$message);
                     $message = str_replace("@fee_structure",$file_data,$message);


                      $message = str_replace("@created_tm",$created_tm,$message);
                      $message = str_replace("@created_dt",$created_dt_tm,$message);


                      $message = str_replace("@created_dt",$created_dt_tm,$message);
                      $message = str_replace("@REGISTRATIONFEE",$regamount,$message);
                      



                // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
           // include("/var/www/html/eag/assets/mpdf/vendor/autoload.php");
                  // require_once __DIR__ . '/vendor/autoload.php';
            $mpdf=new \Mpdf\Mpdf(); 




           

   // echo $message;exit;



            $mpdf->WriteHTML($message);
            $mpdf->Output('OFFER_LETTER_' . $applicantNAme . '_' . $nric .'.pdf', 'D');
            // $mpdf->Output('Offer_letter.pdf', 'D');
            exit;
    }

    function submitApplication()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            $is_submitted = $this->security->xss_clean($this->input->post('is_submitted'));

            $data = array(
                'is_submitted' => 0,
                'updated_by' => $id_user
            );

            $result = $this->applicant_model->editApplicantDetails($data,$id);
        }

        redirect('/applicant/applicant/edit');
    }

    function editProgram()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            $formData = $this->input->post();

           
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_program = $this->security->xss_clean($this->input->post('id_program'));

            $data = array(
                'id_program' => $id_program,
                'id_intake' => $id_intake,
                'updated_by' => $id_user
            );
            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/applicant/applicant/edit');
        }
        $data['getApplicantDetails'] = $this->applicant_model->getApplicant($id);
        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            $this->view();
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();        
            $this->global['pageTitle'] = 'Inventory Management : View Applicant';
            $this->loadViews("applicant_view/edit_program", $this->global, $data, NULL);
            // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        }
    }

    

    function getStateByCountry($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     
                 </script>
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getStateByCountryPermanent($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
        <script type='text/javascript'>
             
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getProgrammeByEducationLevelId($id_education_level)
    {
            // echo "<Pre>"; print_r($id_education_level);exit;
            $intake_data = $this->applicant_model->getProgrammeByEducationLevelId($id_education_level);

             $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_program' id='id_program' class='form-control' onchange='getIntakeByProgramme(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->applicant_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();

                
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }

    function getProgramSchemeByProgramId($id_program)
    {
        // It's A Learning Mode After Flow Change
         $intake_data = $this->applicant_model->getProgramSchemeByProgramId($id_program);
        
        // Multiple Programme Mode Ignored For Demo On 09-11-2020
        // $intake_data = $this->applicant_model->getProgramLandscapeSchemeByProgramId($id_program);

        // echo "<Pre>"; print_r($intake_data);exit;
        
        $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program_scheme' id='id_program_scheme' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function getSchemeByProgramId($id_program)
    {
        // echo "<Pre>"; print_r($id_program);exit;
        // It's Actual Scheme What We Required
         $intake_data = $this->applicant_model->getSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program_has_scheme' id='id_program_has_scheme' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $code = $intake_data[$i]->code;
            $name = $intake_data[$i]->description;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }


    function getProgramStructureTypeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_program);exit;
        // It's Actual Scheme What We Required
         $intake_data = $this->applicant_model->getProgramStructureTypeByProgramId($id_programme);

        // echo "<Pre>"; print_r($intake_data);exit;


        $table = "
        
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_program_structure_type' id='id_program_structure_type' class='form-control' required>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $code = $intake_data[$i]->code;
        $name = $intake_data[$i]->name;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";
        }

        $table.="</select>";

        echo $table;
    }


    function getDocumentByProgramme($id_programme)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getDocumentByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;


            $table="<div class='row'>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;

            // <span class='error-text'>*</span>
            
            $table.="<div class='col-sm-3'>
                      <label>$name </label>
                      <input type='file' name='file[]'>
                      <input type='hidden' name='fileid[]' value='$id' />
                     </div>";
            }
            $table.="</div>";

            echo $table;
    }


    function getBranchByProgram($id_programme)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getBranchByProgramId($id_programme);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function getBranchesByPartnerUniversity($id_partner_university)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getBranchesByPartnerUniversity($id_partner_university);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getProgramByPartnerUniversity($id_partner_university) {

        // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getProgramByPartnerUniversity($id_partner_university);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program' id='id_program' class='form-control' onchange='getprogramScheme(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;


    }

    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->applicant_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;


    }


    function checkenglish($id_program) {

         $programDetails = $this->applicant_model->programDetails($id_program);
      

         echo $programDetails->is_english_required;


    }


    function getIndividualEntryRequirement($id_program)
    {

                $id = $this->session->id_applicant;
        $id_user = $this->session->userId;


        $datagetApplicantDetails = $this->applicant_model->getApplicantDetailsById($id);


        $programEntryRequirementList = $this->applicant_model->programEntryRequirementList($id_program);
        $programDetails = $this->applicant_model->programDetails($id_program);



        if($programDetails->is_apel=='1') {
                    $programEntryAppelRequirementList = $this->applicant_model->programAppelRequirementList($id_program);

        }


        $table = '';


        
        // echo "<Pre>"; print_r($table);exit;

        if(!empty($programEntryAppelRequirementList))
        {
             $checked = "";
             if($datagetApplicantDetails->id_program_requirement=='99999') {
                                     $checked = "checked=checked";
             }

            $table.= '
             <div class="form-container">
                        <h4 class="form-group-title" style="padding-left:20px;">Program APEL Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead><tr>
                                 <th>';
                            if($datagetApplicantDetails->id_program_requirement=='99999') {
                                $table.='
                                 <input type="radio" name="id_program_requirement" value="99999" checked="checked"/>';
                            } else {
                                 $table.='
                                 <input type="radio" name="id_program_requirement" value="99999"/>';

                            }
                                $table.='Flexible entry via Accreditation of Prior Experiential Learning (APEL) Entry Requirements:</th>
                                </tr>
                            </thead>
                            <tbody>';


                             $total = 0;
                              for($i=0;$i<count($programEntryAppelRequirementList);$i++)
                             { 
                                $checked = "";
                                if($programEntryAppelRequirementList[$i]->id==$datagetApplicantDetails->id_program_requirement) {
                                     $checked = "checked=checked";
                                }
                                $data="";
                                $j = $i+1;

                                
                               
                                    $data.= $and . " " . $programEntryAppelRequirementList[$i]->other_description. " .";
                               

                                $identry = $programEntryAppelRequirementList[$i]->id;

                            // print_r($identry);exit;



                                $table.="
                                <tr>
                                <td>$j - $data</td>
                                 </tr>";


                          } 
                        $table.='
                            </tbody>
                        </table>
                      </div>

                    </div> ';

        }


        if(!empty($programEntryRequirementList))
        {
            $table.= '
             <div class="form-container">
                        <h4 class="form-group-title" style="padding-left:20px;">Program Requirement Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                 <th>Entry Requirement</th>
                                </tr>
                            </thead>
                            <tbody>';


                             $total = 0;
                              for($i=0;$i<count($programEntryRequirementList);$i++)
                             { 
                                $checked = "";
                                if($programEntryRequirementList[$i]->id==$datagetApplicantDetails->id_program_requirement) {
                                     $checked = "checked=checked";
                                }
                                $data="";
                                $j = $i+1;

                                if($programEntryRequirementList[$i]->age == 1)
                                {
                                    $data.= "Age Min. " . $programEntryRequirementList[$i]->min_age . " Year, Max. " . $programEntryRequirementList[$i]->max_age . " Year" ;
                                    $and = ", AND ";
                                }
                                if($programEntryRequirementList[$i]->education == 1)
                                {
                                    $data.= $and . "Education Requirement is : " 
                                    // . $programEntryRequirementList[$i]->qualification_code  .  " - "
                                     . $programEntryRequirementList[$i]->qualification_name ;
                                    $and = " ,  AND ";

                                }
                                if($programEntryRequirementList[$i]->work_experience == 1)
                                {
                                    $data.= $and . "Work Experience is : " . $programEntryRequirementList[$i]->work_code . " - " . $programEntryRequirementList[$i]->work_name . ", With Min. Experience Of " . $programEntryRequirementList[$i]->min_work_experience . " Years";
                                    $and = " ,  AND ";
                                    
                                }
                                if($programEntryRequirementList[$i]->other == 1)
                                {
                                    $data.= $and . " " . $programEntryRequirementList[$i]->other_description. " .";
                                }

                                $identry = $programEntryRequirementList[$i]->id;

                            // print_r($identry);exit;



                                $table.="
                                <tr>
                                <td><input type='radio' name='id_program_requirement' value='$identry' <?php echo $checked;?>  $data</td>
                                 </tr>";


                          } 
                        $table.='
                            </tbody>
                        </table>
                      </div>

                    </div> ';

        }else
        {
            $table = '<p> No Requirements Available</p>';
        }

        print_r($table);exit;


    }


    function checkFeeStructure()
    {
        $id_session = $this->session->session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $result = $this->applicant_model->checkFeeStructure($tempData);
        
        echo $result;exit;
    
    }

    function deleteApplicantUploadedDocument($id)
    {
        $deleted = $this->applicant_model->deleteApplicantUploadedDocument($id);
        echo "success";exit;
    }


    function generateReceiptAndMoveApplicant()
    {

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>"; print_r($tempData);exit;

        $id_applicant = $tempData['id_applicant'];
        $applicant = $this->applicant_model->getApplicantDetails($id_applicant);
        
        $id_university = $applicant->id_university;

        // echo "<Pre>"; print_r($id_university);exit;

        if($id_university > 1)
        {
            $partner_university = $this->applicant_model->getPartnerUniversityDetails($id_university);

            $billing_to = $partner_university->billing_to;
            

            // echo "<Pre>"; print_r($partner_university);exit;

            if($billing_to == 'Partner University')
            {
                $added_new_student = $this->applicant_model->addNewStudent($id_applicant);
                

                if($added_new_student)
                {
                    $update_applicant['applicant_status'] = 'Migrated';

                    $updated_student = $this->applicant_model->editApplicantDetails($update_applicant,$id_applicant);
                    // echo "<Pre>"; print_r($updated_student);exit;
                    return 1;
                }
            }
            elseif ($billing_to == 'Student')
            {
                $id_main_invoice = $tempData['id_main_invoice'];
                $total_amount = $tempData['total_amount'];
                $paid_amount = $tempData['paid_amount'];
                $balance_amount = $tempData['balance_amount'];
                $currency = $tempData['currency'];

                $applicant = $this->applicant_model->getApplicantDetailsById($id_applicant);
                
                $receipt_number = $this->applicant_model->generateReceiptNumber($tempData);

                $receipt['receipt_amount'] = $balance_amount;
                $receipt['receipt_number'] = $receipt_number;
                $receipt['type'] = 'Applicant'; 
                $receipt['remarks'] = 'Applicant Fee Payment'; 
                $receipt['id_student'] = $id_applicant; 
                $receipt['id_intake'] = $applicant->id_intake; 
                $receipt['id_program'] = $applicant->id_program;
                $receipt['receipt_date'] = date('Y-m-d'); 
                $receipt['currency'] = $currency; 
                $receipt['approval_status'] = 1; 
                $receipt['status'] = 1;
                

                $id_receipt = $this->applicant_model->addReceipt($receipt);

                if($id_receipt)
                {
                    $main_invoice_data['balance_amount'] = 0;
                    $main_invoice_data['paid_amount'] = $paid_amount + $balance_amount;



                    $updated_main_invoice = $this->applicant_model->updateMainInvoice($main_invoice_data,$id_main_invoice);

                    $receipt_details['id_receipt'] = $id_receipt;
                    $receipt_details['id_main_invoice'] = $id_main_invoice;
                    $receipt_details['invoice_amount'] = $balance_amount;
                    $receipt_details['paid_amount'] = $balance_amount;
                    $receipt_details['approval_status'] = 1;
                    $receipt_details['status'] = $status;

                    $id_receipt_details = $this->applicant_model->addReceiptDetails($receipt_details);

                    if($id_receipt_details)
                    {

                        $receipt_paid_details['id_receipt'] = $id_receipt;
                        $receipt_paid_details['id_payment_type'] = 'CASH';
                        $receipt_paid_details['payment_reference_number'] = 'CASH';
                        $receipt_paid_details['paid_amount'] = $balance_amount;
                        $receipt_paid_details['approval_status'] = 1;
                        $receipt_paid_details['status'] = 1;

                        $id_receipt_paid_details = $this->applicant_model->addReceiptPaidDetails($receipt_paid_details);


                        if($id_receipt_paid_details)
                        {
                            $added_new_student = $this->applicant_model->addNewStudent($id_applicant);
                            
                            if($added_new_student)
                            {
                                $update_applicant['applicant_status'] = 'Migrated';
                                $updated_student = $this->applicant_model->editApplicantDetails($update_applicant,$id_applicant);

                                echo "Applicant Migrated";exit;
                            }
                        }
                    }
                }

                echo "Done";exit;

            }
        }
        else
        {

            $id_main_invoice = $tempData['id_main_invoice'];
            $total_amount = $tempData['total_amount'];
            $paid_amount = $tempData['paid_amount'];
            $balance_amount = $tempData['balance_amount'];
            $currency = $tempData['currency'];


            $applicant = $this->applicant_model->getApplicantDetailsById($id_applicant);
            
            $receipt_number = $this->applicant_model->generateReceiptNumber($tempData);

            $receipt['receipt_amount'] = $balance_amount;
            $receipt['receipt_number'] = $receipt_number;
            $receipt['type'] = 'Applicant'; 
            $receipt['remarks'] = 'Applicant Fee Payment'; 
            $receipt['id_student'] = $id_applicant; 
            $receipt['id_intake'] = $applicant->id_intake; 
            $receipt['id_program'] = $applicant->id_program; 
            $receipt['receipt_date'] = date('Y-m-d'); 
            $receipt['currency'] = $currency; 
            $receipt['approval_status'] = 1; 
            $receipt['status'] = 1;
            

            $id_receipt = $this->applicant_model->addReceipt($receipt);

            if($id_receipt)
            {
                $main_invoice_data['balance_amount'] = 0;
                $main_invoice_data['paid_amount'] = $paid_amount + $balance_amount;



                $updated_main_invoice = $this->applicant_model->updateMainInvoice($main_invoice_data,$id_main_invoice);

                $receipt_details['id_receipt'] = $id_receipt;
                $receipt_details['id_main_invoice'] = $id_main_invoice;
                $receipt_details['invoice_amount'] = $balance_amount;
                $receipt_details['paid_amount'] = $balance_amount;
                $receipt_details['approval_status'] = 1;
                $receipt_details['status'] = $status;

                $id_receipt_details = $this->applicant_model->addReceiptDetails($receipt_details);

                if($id_receipt_details)
                {

                    $receipt_paid_details['id_receipt'] = $id_receipt;
                    $receipt_paid_details['id_payment_type'] = 'CASH';
                    $receipt_paid_details['payment_reference_number'] = 'CASH';
                    $receipt_paid_details['paid_amount'] = $balance_amount;
                    $receipt_paid_details['approval_status'] = 1;
                    $receipt_paid_details['status'] = 1;

                    $id_receipt_paid_details = $this->applicant_model->addReceiptPaidDetails($receipt_paid_details);


                    if($id_receipt_paid_details)
                    {
                        
                        $added_new_student = $this->applicant_model->addNewStudent($id_applicant);
                        
                        if($added_new_student)
                        {
                            $update_applicant['applicant_status'] = 'Migrated';
                            $updated_student = $this->applicant_model->editApplicantDetails($update_applicant,$id_applicant);

                            echo "Applicant Migrated";exit;
                        }
                    }
                }
            }

            echo "Done";exit;
        }
    }

    function uploadProfilePic()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;
       

            // 
        if($_FILES)
        {

            if($_FILES['profile_pic'])
            {

                $certificate_name = $_FILES['profile_pic']['name'];
                $certificate_size = $_FILES['profile_pic']['size'];
                $certificate_tmp =$_FILES['profile_pic']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                // $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Profile Picture');

                $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Profile Picture');



                      
                if($image_file)
                {

                    $data = array(
                        'profile_pic' => $image_file
                        
                    );


                    $result = $this->applicant_model->editApplicantDetails($data,$id);

                    if($result)
                    {
                        $sessionArray = array(
                            'applicant_profile_pic'=>  $image_file
                            );

                        $this->session->set_userdata($sessionArray);
                    }

                }
            redirect('/applicant/applicant/dashboard');
            }
        }

        $data['applicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
 
        $this->global['pageTitle'] = 'EAG Applicant Portal : Upload Profile Pic';            
        $this->loadViews("applicant_view/upload_profile_pic", $this->global, $data, NULL);
    }

    function logout()
    {
        $sessionArray = array('id_applicant'=> '',                    
                    'applicant_name'=> '',
                    'email_id'=> '',
                    'nric'=> '',
                    'applicant_profile_pic'=>  '',
                    'applicant_last_login'=>  '',
                    'isApplicantLoggedIn' => FALSE
            );

        $this->session->set_userdata($sessionArray);
        $this->isApplicantLoggedIn();
    }
}