<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_model extends CI_Model
{
    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function addFileDownload($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_document', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addDiscountApplied($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', 1);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function gettemplate($id)
    {
        $this->db->select('*');
        $this->db->from('communication_template');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $intake = $query->row();
        return $intake;
    }

    function getPartnerUniversityDetails($id)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $intake = $query->row();
        return $intake;
    }

    function schemeListByStatus($status)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programStructureTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result; 
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getApplicantDetailsById($id)
    {
        $this->db->select('a.*, qs.name as qualification, pu.billing_to,pu.name as uniname');
        $this->db->from('applicant as a');
        $this->db->join('education_level as qs','a.id_degree_type = qs.id','left');
        $this->db->join('partner_university as pu','a.id_university = pu.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function getApplicantDiscountDetails($intake) {
        $this->db->select('d.id,dt.name,dt.description,d.start_date,d.end_date');
        $this->db->from('discount as d');
        $this->db->join('discount_type as dt','d.id_discount_type = dt.id','left');
        $this->db->where('d.id_intake', $intake);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getApplicantDiscountDetailsByApplicant($id) {
         $this->db->select('*');
        $this->db->from('applicant_has_discount');
        $this->db->where('id_applicant', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }


    function getFacultyDetails($idprogram) {



          $this->db->select('a.*');
         $this->db->from('faculty_program as a');
         $this->db->join('staff as s', 's.id_faculty_program = a.id');
         $this->db->join('programme_has_dean as pd', 'pd.id_staff = s.id');

         $this->db->where('pd.id_programme', $idprogram);
        $query = $this->db->get();

        $applicant = $query->row();

        return $applicant;
    }


    function getApplicantInformation($id)
    {
        $this->db->select('a.*, p.code as program_code, p.name as program_name, i.year as intake_year, i.name as intake_name,ps.mode_of_program,ps.mode_of_study,o.name as branchname,ps.min_duration,ps.max_duration');
        $this->db->from('applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme_has_scheme as ps', 'a.id_program_scheme  = ps.id','left');
        $this->db->join('organisation_has_training_center as o', 'a.id_branch = o.id','left');

        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $applicant = $query->row();
        return $applicant;
    }

    function getTemperoryOfferLetterByIntake($id)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $intake = $query->row();

        $temp_offer_letter =$intake->is_temp_offer_letter;

        return $temp_offer_letter;
    }

    function getApplicant($id)
    {
        $this->db->select('a.is_submitted, a.applicant_status, p.name as program_name, p.code as program_code, i.name as intake_name, i.year as intake_year');
        $this->db->from('applicant as a');
            $this->db->join('programme as p', 'a.id_program = p.id');
            $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function getApplicantDetails($id)
    {
       
            $this->db->select('a.*, pu.billing_to,i.is_temp_offer_letter');
            $this->db->from('applicant as a');
            $this->db->join('partner_university as pu','a.id_university = pu.id','left');
            $this->db->join('intake as i', 'a.id_intake = i.id');

            $this->db->where('a.id', $id);
            $query = $this->db->get();
            return $query->row();
        
    }
    
    function getApplicantDetailsForDraft($id)
    {
       
            $this->db->select('a.*,i.is_temp_offer_letter');
            $this->db->from('applicant as a');
            $this->db->join('intake as i', 'a.id_intake = i.id','left');

            $this->db->where('a.id', $id);
            $query = $this->db->get();
            return $query->row();
        
    }

    function addNewApplicant($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewSibblingDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_sibbling_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewEmployeeDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_employee_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewAlumniDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_alumni_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant', $data);
        return TRUE;
    }

    function editSibblingDetails($data, $id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->update('applicant_has_sibbling_discount', $data);
        return TRUE;
    }

    function editEmployeeDetails($data, $id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->update('applicant_has_employee_discount', $data);
        return TRUE;
    }

    function editAlumniDetails($data, $id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->update('applicant_has_alumni_discount', $data);
        return TRUE;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

    function countryListByStatusForPhoneCode($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('phone_code !=', '');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgramLandscapeSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.id_learning_mode) as id_learning_mode');
        $this->db->from('add_learning_mode_to_program_landscape as ihs');
        $this->db->join('programme_has_scheme as phs','ihs.id_learning_mode = phs.id');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();

        // echo "<Pre>"; print_r($results);exit;

        $details = array();
        foreach ($results as $result)
        {
            $id_learning_mode = $result->id_learning_mode;
            $learning_mode = $this->getLearningModeById($id_learning_mode);

        // echo "<Pre>"; print_r($learning_mode);exit;
            if($learning_mode)
            {
                array_push($details, $learning_mode);
            }
        }
        // echo "<Pre>"; print_r($details);exit;

        return $details;
    }

    function getLearningModeById($id)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id', $id);
        $query = $this->db->get();
        $results = $query->row();
        return $results;
    }

    function getSchemeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $this->db->select('DISTINCT(phs.id_scheme) as id_scheme');
        $this->db->from('program_has_scheme as phs');
        $this->db->join('scheme as sch', 'phs.id_scheme = sch.id');
        $this->db->where('phs.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();
            
        // echo "<Pre>"; print_r($results);exit;

        $details = array();

        foreach ($results as $result)
        {

            $id_scheme = $result->id_scheme;

            $scheme = $this->getScheme($id_scheme);
            if($scheme)
            {
                $result = $scheme;
                array_push($details, $result);
            }
        }

        return $details;
    }

    function getScheme($id_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.id', $id_scheme);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramScheme($id_program_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id', $id_program_scheme);
        $query = $this->db->get();
        return $query->row();
    }

     function getProgramSchemeBySchemeAndProgramId($scheme_id,$idprogram)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id', $id_program_scheme);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramLandscape($id_intake,$id_program,$id_program_scheme,$id_program_has_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_landscape as ihs');
        $this->db->where('ihs.id_intake', $id_intake);
        $this->db->where('ihs.id_programme', $id_program);
        $this->db->where('ihs.program_scheme', $id_program_has_scheme);
        $this->db->where('ihs.learning_mode', $id_program_scheme);
        $this->db->order_by("ihs.id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramLandscapeByLearningMode($id_intake,$id_program,$learning_mode,$id_program_has_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_landscape as ihs');
        // $this->db->join('programme_has_scheme as almpl', 'almpl.id_program_landscape = ihs.id');
        // $this->db->join('add_learning_mode_to_program_landscape as almpl', 'almpl.id_program_landscape = ihs.id');
        $this->db->where('ihs.id_intake', $id_intake);
        $this->db->where('ihs.id_programme', $id_program);
        // $this->db->where('ihs.program_scheme', $id_program_has_scheme);
        // $this->db->where('ihs.learning_mode', $learning_mode);
        $this->db->order_by("ihs.id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramLandscapeByMultipleLearningMode($id_intake,$id_program,$learning_mode,$id_program_has_scheme)
    {
        $this->db->select('almpl.*');
        $this->db->from('add_learning_mode_to_program_landscape as almpl');
        $this->db->join('programme_landscape as pl', 'almpl.id_program_landscape = pl.id');
        $this->db->where('almpl.id_intake', $id_intake);
        $this->db->where('almpl.id_program', $id_program);
        $this->db->where('pl.program_scheme', $id_program_has_scheme);
        $this->db->where('pl.learning_mode', $learning_mode);
        $this->db->order_by("almpl.id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramDetails($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id_program);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeByEducationLevelId($id_education_level)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        // $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_education_level', $id_education_level);
        $this->db->where('ihs.status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


     function getDocumentByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(d.id) as id, d.*');
        $this->db->from('documents_program_details as dp');
        $this->db->join('documents as d', 'dp.id_document = d.id');
        $this->db->where('dp.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function checkFeeStructure($data)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $data['id_education_level']);
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('id_programme', $data['id_program']);
        $this->db->where('id_learning_mode', $data['id_program_scheme']);
        $this->db->where('id_program_scheme', $data['id_program_has_scheme']);
        $this->db->where('status', '1');
        $query = $this->db->get();
        // print_r($query->row());exit();
        $result = $query->row();
        if($result)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    function getSibblingDiscountByApplicantId($id_applicant)
    {
        $this->db->select('ahsd.*');
        $this->db->from('applicant_has_sibbling_discount as ahsd');
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getEmployeeDiscountApplicantId($id_applicant)
    {
        $this->db->select('ahemd.*');
        $this->db->from('applicant_has_employee_discount as ahemd');
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getAlumniDiscountApplicantId($id_applicant)
    {
        $this->db->select('ahemd.*');
        $this->db->from('applicant_has_alumni_discount as ahemd');
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function checkDuplicateApplicant($data,$id)
    {
            // echo "<pre>"; print_r($id);exit();

        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('email_id', $data['email_id']);
        $this->db->or_where('phone', $data['phone']);
        $this->db->or_where('nric', $data['nric']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getReceiptStatus($id_applicant)
    {
        $type = "Applicant";
        $zero= "0";
        $one= "1";
        $this->db->select('*');
        $this->db->from('receipt');
        // $likeCriteria = "(id_student  = '" . $id_applicant . "' and type  ='" . $type . "' and (status  ='" . $zero . "' or status  ='" . $one . "'))";
        $likeCriteria = "(id_student  = '" . $id_applicant . "' and type  ='" . $type . "')";
        $this->db->where($likeCriteria);

        // $this->db->where('id_student', $id_applicant);
        // $this->db->where('status', '0');
        // $this->db->or_where('status', '1');
        $query = $this->db->get();
        return $query->row();
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }


    function getApplicantSibblingDiscountDetails($id_applicant)
    {
        $this->db->select('ahsd.*, usr.name as user_name');
        $this->db->from('applicant_has_sibbling_discount as ahsd');
        $this->db->join('tbl_users as usr', 'ahsd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantEmployeeDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_employee_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantAlumniDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_alumni_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getBranchByProgramId($id_program)
    {
        $this->db->select('DISTINCT(ahemd.id_training_center) as id_training_center');
        $this->db->from('organisation_training_center_has_program as ahemd');
        $this->db->join('organisation_has_training_center as ohtc', 'ahemd.id_training_center = ohtc.id');
        $this->db->where('ahemd.id_program', $id_program);
        $query = $this->db->get();
        $results = $query->result();

        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }


        foreach ($results as $result)
        {
            $training_center = $this->getTrainingCenterById($result->id_training_center);
            array_push($details, $training_center);
        }
        return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getUniversityListByStatus($status)
    {
        // $organisation = $this->getOrganisaton();
         $details = array();

        // if($organisation)
        // {
        //     array_push($details, $organisation);
        // }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    }


    function getFeeStructureByProgramIntakeLearningMode($idprogram,$id_program_scheme,$id_intake) {


         $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_intake', $id_intake);
        $this->db->where('id_programme', $idprogram);
        $this->db->where('id_learning_mode', $id_program_scheme);
        $query = $this->db->get();

        $result = $query->row();
        
        if($result)
        {
            return $result->id;
        }
        else
        {
            return 0;
        }


    }


    function getBranchesByPartnerUniversity($id_organisation)
    {
        $this->db->select('ahemd.*');
        $this->db->from('organisation_has_training_center as ahemd');
        $this->db->where('ahemd.id_organisation', $id_organisation);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }


    function getProgramByPartnerUniversity($id_organisation)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id_partner_university', $id_organisation);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }

    function getTrainingCenterById($id)
    {
        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function branchListByStatus()
    {
        // $organisation = $this->getOrganisaton();
        $details = array();

        // if($organisation)
        // {
        //     array_push($details, $organisation);
        // }

        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
           array_push($details, $result);
        }
        return $details;
    }

    function getIntakeDetails($id)
    {
        $this->db->select('a.*');
        $this->db->from('intake as a');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function programEntryRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $this->db->where('ier.entry_type', 'ENTRY');
        $query = $this->db->get();
        return $query->result();
    }

     function programAppelRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $this->db->where('ier.entry_type', 'APPEL');
        $query = $this->db->get();
        return $query->result();
    }

    function programDetails($id_program)
    {
        $this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->where('a.id', $id_program);
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function programRequiremntListList()
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $query = $this->db->get();
        return $query->result();
    }

    function getApplicantUploadedFiles($id_applicant)
    {
        $this->db->select('shd.*, d.code as document_code, d.name as document_name');
        $this->db->from('applicant_has_document as shd');
        $this->db->join('documents as d','shd.id_document = d.id');
        $this->db->where('shd.id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteApplicantUploadedDocument($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('applicant_has_document');
        return TRUE;
    }


    function deleteApplicantdiscount($id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->delete('applicant_has_discount');
        return TRUE;
    }

    function getProgramStructureTypeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(tphd.id_program_type) as id_program_type');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->where('tphd.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();

        // echo "<Pre>";print_r($results);exit();

        $details = array();

        foreach ($results as $result)
        {
            $id_program_type = $result->id_program_type;
            $program_structure_type = $this->getProgramStructureType($id_program_type);

            if($program_structure_type)
            {
                array_push($details, $program_structure_type);
            }
        }
        
        return  $details;
    }

    function getProgramStructureType($id)
    {
        $this->db->select('shd.*');
        $this->db->from('program_type as shd');
        $this->db->where('shd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getfeeStructureMasterByApplicantID($id_applicant)
    {
        $applicant = $this->getApplicantDetailsById($id_applicant);
        
        // echo "<Pre>";print_r($applicant);exit();

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        // $this->db->where('id', $applicant->id_fee_structure);
        // $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        // $this->db->where('id_partner_university', $applicant->id_university);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();
        // echo "<Pre>";print_r($result);exit();
        
        if($result)
        {
            return $result->id;
        }
        else
        {
            return 0;
        }
    }

    function getfeeStructureMaster($id)
    {
        $this->db->select('a.*, cs.name as currency_name, cs.code as currency_code');
        $this->db->from('fee_structure_master as a');
        $this->db->join('currency_setup as cs', 'a.id_currency = cs.id','left');
        $this->db->where('a.id', $id);
        // $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        $this->db->where('a.status', '1');
        $query = $this->db->get();

        $result = $query->row();

        return $result;
    }


    function getfeeStructureMasterByApplicant($id_applicant)
    {
        $applicant = $this->getApplicantDetailsById($id_applicant);

        $id_university = $applicant->id_university;
        
        // echo "<Pre>";print_r($id_university);exit();

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        // $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        // $this->db->where('id_partner_university', $applicant->id_university);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();

        // echo "<Pre>";print_r($result);exit();


        if($result)
        {
            $id_fee_structure = $result->id;
            
            // if($id_university == 1)
            // { 
                $nationality = $applicant->nationality;
                $currency = 'MYR';

                if($nationality == '1')
                {
                    $currency = 'MYR';
                }
                else
                {
                    $currency = 'USD';
                }

                // echo "<Pre>";print_r($nationality);exit();

                $fee_structure = $this->getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure,$currency);
                
                // echo "<Pre>";print_r($fee_structure);exit();
                
                return $fee_structure;
            // }
            // else
            // {
            //     $fee_structure = $this->getfeeStructureDetailsByIdFeeStructureMasterForPartneruniversity($id_fee_structure,$id_university);

            //     $details = array();

            //     foreach ($fee_structure as $value)
            //     {
            //         // echo "<Pre>";print_r($value);exit();
            //         $is_installment = $value->is_installment;
            //         $data['id_fee_structure'] = $value->id;
            //         $data['id_fee_structure_master'] = $value->id_program_landscape;

            //         if($is_installment == 1)
            //         {
            //             $total_amount = 0;

            //             $installment_details = $this->getTrainingCenterInstallmentDetails($data);

            //             foreach ($installment_details as $detail)
            //             {
            //                $total_amount = $total_amount + $detail->amount; 
            //             }

            //             $value->amount = $total_amount;
            //         }

            //         array_push($details, $value);
            //     }

            //     return $details;
            // }
        }
        else
        {
            return array();
        }
    }


     

    function getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure_master,$currency)
    {

        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left');
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getfeeStructureDetailsByIdFeeStructureMasterForPartneruniversity($id_fee_structure_master,$id_university)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->join('fee_structure as fss', 'fst.id_fee_structure = fss.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.id_training_center', $id_university);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        
        return $result;
    }


    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function applicantInvoice($id_applicant)
    {

        $this->db->select('fst.*');
        $this->db->from('main_invoice as fst');  
        $this->db->join('applicant as fm', 'fst.id_student = fm.id','left'); 
        $this->db->where('fst.id_student', $id_applicant);
        $this->db->where('fst.type', 'Applicant');
        $this->db->where('fst.is_migrate_applicant !=', 0);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->row();
        // echo "<Pre>";print_r($applicant);exit();
        // echo "<Pre>";print_r($result);exit();

        return $result;
    }

    function applicantInvoiceDetails($id_main_invoice)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode ');
        $this->db->from('main_invoice_details as fst');  
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->where('fst.id_main_invoice', $id_main_invoice);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');

        $this->db->select('j.*');
        $this->db->from('receipt as j');
        $this->db->order_by("j.id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
        $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
        
        return $jrnumber;        
    }

    function addReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateMainInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function addNewStudent($id)
    {
        $query = $this->db->select('*')->from('applicant')->where('id',$id)->get();
        foreach ($query->result() as $row)
        {
            // echo "<Pre>";print_r($row);exit();
            unset($row->id);
            unset($row->is_sibbling_discount);
            unset($row->is_employee_discount);
            unset($row->is_alumni_discount);
            unset($row->is_apeal_applied);
            unset($row->id_apeal_status);
            unset($row->approved_by);
            unset($row->email_verified);
            unset($row->is_updated);
            unset($row->is_submitted);
            unset($row->submitted_date);
            unset($row->apel_reject_reason);
            unset($row->created_dt_tm);
            unset($row->is_invoice_generated);
            unset($row->country_code);
            unset($row->english_year);
            unset($row->english_grade);
            unset($row->id_english_test);
            
            
            
            $row->status = '1';
            $row->current_semester = '1';
            $row->applicant_status = 'Approved';
            $row->id_applicant = $id;
            $row->phd_duration = 1;
            $row->current_deliverable = date('M-Y');

            $this->db->insert('student',$row);
            $insert_id = $this->db->insert_id();

            if($insert_id)
            {
                $this->addNewDeliverableHistory($insert_id);
            }
        }

        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewDeliverableHistory($id_student)
    {
        $id_user = $this->session->userId;

        $data['id_student'] = $id_student;
        $data['old_deliverable_term'] = '';
        $data['new_deliverable_term'] = date('M-Y');
        $data['created_by'] = $id_user;

        $this->db->trans_start();
        $this->db->insert('student_deliverable_history',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addReceiptPaidDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_paid_details',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
}