<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgramType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('program_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('program_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['programTypeList'] = $this->program_type_model->programTypeListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Program Type List';
            $this->loadViews("program_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('program_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->program_type_model->addNewProgramType($data);
                redirect('/cm/programType/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Program Type';
            $this->loadViews("program_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('program_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/cm/programType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->program_type_model->editProgramType($data,$id);
                redirect('/cm/programType/list');
            }
            $data['programType'] = $this->program_type_model->getProgramType($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit ProgramType';
            $this->loadViews("program_type/edit", $this->global, $data, NULL);
        }
    }
}
