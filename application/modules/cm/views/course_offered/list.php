<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Course Offered</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Course Offered</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">
                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Scheme</label>
                      <div class="col-sm-8">
                        <select name="id_scheme" id="id_scheme" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($schemeList)) {
                            foreach ($schemeList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_scheme'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->code . " - " . $record->description;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Code</th>
            <th>Name</th>
            <th>Academic Year</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Type</th>
            <th>Sequence</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($semesterDetails))
          {
            $i=1;
            foreach ($semesterDetails as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code; ?></td>
                <td><?php echo $record->name; ?></td>
                <td><?php echo $record->academic_year; ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->start_date)) ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->end_date)) ?></td>
                <td><?php echo $record->semester_type; ?></td>
                <td><?php echo $record->semester_sequence; ?></td>
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">
                    Edit
                  </a>
               </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
</script>