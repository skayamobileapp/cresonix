<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// $k = array_search($csR, $id_fields)!==false;


$is_prdt_type = array_search('1',$productFieldList)!==false;
$is_provider = array_search('2',$productFieldList)!==false;
$is_title = array_search('3',$productFieldList)!==false;
$is_code = array_search('4',$productFieldList)!==false;
$is_category_type = array_search('5',$productFieldList)!==false;
$is_education_level = array_search('6',$productFieldList)!==false;
$is_price = array_search('7',$productFieldList)!==false;
$is_duration = array_search('8',$productFieldList)!==false;
$is_total_cr_hrs = array_search('9',$productFieldList)!==false;
$is_name = array_search('10',$productFieldList)!==false;
$is_student_learning_hours = array_search('11',$productFieldList)!==false;
$is_delievry_mode = array_search('12',$productFieldList)!==false;
$is_competency_level = array_search('13',$productFieldList)!==false;
$is_short_description = array_search('14',$productFieldList)!==false;
$is_target_audience = array_search('15',$productFieldList)!==false;
$is_skill = array_search('16',$productFieldList)!==false;
$is_staff_discount = array_search('17',$productFieldList)!==false;
$is_student_discount = array_search('18',$productFieldList)!==false;
$is_award = array_search('19',$productFieldList)!==false;
$is_tag_modules_for_student = array_search('20',$productFieldList)!==false;

// echo "<Pre>";print_r($programme_tabs);exit();
?>

<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <form id="form_programme" action="" method="post" enctype="multipart/form-data">


        <ul class="page-nav-links">
            <li class="active"><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>
            




          <!-- <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            <li><a href="/prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programme/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->


        </ul>

        <div class="form-container">
            <h4 class="form-group-title">Product Details</h4>
            
            <div class="row">

               

               <div class="col-sm-4" <?php
              if($is_prdt_type < 1)
              {
                // echo "<Pre>";print_r($is_prdt_type);exit();

                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Product Type <span class='error-text'>*</span></label>
                     <select name="id_programme_type" id="id_programme_type" class="form-control" disabled>
                        <option value="">Select</option>
                        <?php
                           if (!empty($productTypeSetupList))
                           {
                               foreach ($productTypeSetupList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_programme_type == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>


               

              <div class="col-sm-4" <?php
              if($is_code < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Code <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="code" name="code" value="<?php echo $programmeDetails->code; ?>" onblur="getProgrammeCodeDuplication()">
                  </div>
               </div>


               


               <div class="col-sm-4" <?php
              if($is_name < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Name <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeDetails->name; ?>" onblur="getProgrammeNameDuplication()">
                  </div>
               </div>


            <!-- </div>

            <div class="row"> -->


               <div class="col-sm-4" <?php
              if($is_student_learning_hours < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Student Learning Hours <span class='error-text'>*</span></label>
                     <input type="number" class="form-control" id="student_learning_hours" name="student_learning_hours" value="<?php echo $programmeDetails->student_learning_hours; ?>">
                  </div>
               </div>


               



               

              <div class="col-sm-4" <?php
              if($is_total_cr_hrs < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Total Cr. Hours </label>
                     <input type="number" class="form-control" id="total_cr_hrs" name="total_cr_hrs" value="<?php echo $programmeDetails->total_cr_hrs; ?>">
                  </div>
              </div>

              <div class="col-sm-4" <?php
              if($is_provider < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Product Owner <span class='error-text'>*</span></label>
                     <select name="internal_external" id="internal_external" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="Internal" <?php if ($programmeDetails->internal_external == 'Internal')
                           {
                               echo "selected";
                           } ?>>Internal</option>
                        <option value="External" <?php if ($programmeDetails->internal_external == 'External')
                           {
                               echo "selected";
                           } ?>>External</option>
                     </select>
                  </div>
              </div>

            <!-- </div>

            <div class="row"> -->

               <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                  <div class="form-group">
                     <label>Partner Name <span class='error-text'>*</span></label>
                     <select name="id_partner_university" id="id_partner_university" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($partnerList))
                           {
                               foreach ($partnerList as $record)
                               { 
                                if ($record->id == $programmeDetails->id_partner_university)
                                  {?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($record->id == $programmeDetails->id_partner_university)
                              {
                                  echo "selected=selected";
                              } ?>>
                           <?php echo $record->code . " - " . $record->name; ?>
                        </option>
                        <?php
                                }
                              }
                           }
                           ?>
                     </select>
                  </div>
               </div>

              <div class="col-sm-4" <?php
              if($is_category_type < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Category Type <span class='error-text'>*</span></label>
                     <select name="id_category" id="id_category" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($categoryList))
                           {
                               foreach ($categoryList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_category == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>

              <div class="col-sm-4" <?php
              if($is_duration < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <label>Max Duration<span class='error-text'>*</span></label>
                  <div class="row">
                     <div class="col-sm-6">
                        <input type="number" class="form-control" id="max_duration" name="max_duration" min='1' value="<?php echo $programmeDetails->max_duration; ?>">
                     </div>
                     <div class="col-sm-6">
                        <select name="duration_type" id="duration_type" class="form-control" required>
                           <option value="">Select</option>
                           <option value="Days"
                              <?php
                                 if ($programmeDetails->duration_type == 'Days')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Days</option>
                           <option value="Weeks"
                              <?php
                                 if ($programmeDetails->duration_type == 'Weeks')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Weeks</option>
                           <option value="Months"
                              <?php
                                 if ($programmeDetails->duration_type == 'Months')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Months</option>
                           <option value="Years"
                              <?php
                                 if ($programmeDetails->duration_type == 'Years')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Years</option>
                        </select>
                     </div>
                  </div>
               </div>

              <div class="col-sm-4" <?php
              if($is_delievry_mode < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Delivery Mode <span class='error-text'>*</span></label>
                     <select name="id_delivery_mode" id="id_delivery_mode" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($deliveryModeList))
                           {
                               foreach ($deliveryModeList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_delivery_mode == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>


            <!-- </div>

            <div class="row"> -->


              <div class="col-sm-4" <?php
              if($is_competency_level < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Competency Level <span class='error-text'>*</span></label>
                     <select name="id_study_level" id="id_study_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($studyLevelList))
                           {
                               foreach ($studyLevelList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_study_level == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
               
               <!-- <div class="col-sm-4">
                  <div class="form-group">
                     <label>Product Trending Type<span class='error-text'>*</span></label>
                     <select name="trending" id="trending" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="HOME" <?php if ($programmeDetails->trending == 'HOME')
                           {
                               echo "selected";
                           } ?>>HOME</option>
                        <option value="TRENDING" <?php if ($programmeDetails->trending == 'TRENDING')
                           {
                               echo "selected";
                           } ?>>TRENDING</option>
                        <option value="POPULAR" <?php if ($programmeDetails->trending == 'POPULAR')
                           {
                               echo "selected";
                           } ?>>POPULAR</option>
                     </select>
                  </div>
               </div> -->


              <div class="col-sm-4" <?php
              if($is_education_level < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <label>Level </label>
                     <select name="id_education_level" id="id_education_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($educationLevelList))
                           {
                               foreach ($educationLevelList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_education_level == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>


            

                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Language <span class='error-text'>*</span></label>
                     <select name="language" id="language" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="1" <?php if ($programmeDetails->language == '1')
                           {
                               echo "selected";
                           } ?>>English</option>
                        <option value="2" <?php if ($programmeDetails->language == '2')
                           {
                               echo "selected";
                           } ?>>Bahasa Melayu</option>
                     </select>
                  </div>
               </div>


            </div>

            <div class="row">



              <div class="col-sm-4" <?php
              if($is_price < 1)
              {
                echo ' style="display: none;"';
              }
                ?>>
                  <div class="form-group">
                     <p>Is Free Course <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="is_free_course" id="is_free_course" value="1" 
                     <?php if ($programmeDetails->is_free_course == '1')
                        {
                            echo "checked";
                        }; ?>><span class="check-radio"></span> Yes
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="is_free_course" id="is_free_course" value="0" <?php if ($programmeDetails->is_free_course == '0')
                        {
                            echo "checked";
                        }; ?>>
                     <span class="check-radio"></span> No
                     </label>
                  </div>
               </div>



               <!-- <div class="col-sm-4">
                  <div class="form-group">
                     <p>Sold Seperately <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="sold_separately" id="sold_separately" value="1" 
                     <?php if ($programmeDetails->sold_separately == '1')
                        {
                            echo "checked";
                        }; ?>><span class="check-radio"></span> Yes
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="sold_separately" id="sold_separately" value="0" <?php if ($programmeDetails->sold_separately == '0')
                        {
                            echo "checked";
                        }; ?>>
                     <span class="check-radio"></span> No
                     </label>
                  </div>
               </div> -->



            



                <div class="col-sm-4">
                  <div class="form-group">
                     <label>FILE 
                     <span class='error-text'>*</span>
                     <?php
                        if ($programmeDetails->image != '')
                        {
                        ?>
                     <a href="<?php echo '/assets/images/' . $programmeDetails->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $programmeDetails->image; ?>)" title="<?php echo $programmeDetails->image; ?>"> View </a>
                     <?php
                        }
                        ?>
                     </label>
                     <input type="file" name="image" id="image">
                     <span style="font-size: 12px; color: #c22026;">Width:825px; Height:490px;</span>
                  </div>
                </div>

            




              <div class="col-sm-4">
                  <div class="form-group">
                     <label>Status <span class='error-text'>*</span></label>
                     <select name="status" id="status" class="form-control" onchange="showRejectField()">
                        <option value="">Select</option>
                        <?php
                           if (!empty($statusList))
                           {
                               foreach ($statusList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->status == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>




               

            </div>

            <div class="row">
              


              <!-- <?php
              if ($programmeDetails->percentage > 0 && $programmeDetails->status != '0')
              {
              ?> -->


                <div class="col-sm-4" id="view_percentage" style="display: none">
                    <div class="form-group">
                        <label>Percentage <span class='error-text'>*</span></label>
                        <input type="number" id="percentage" name="percentage" class="form-control" min="1" max="100" value="<?php echo $programmeDetails->percentage; ?>">
                    </div>
                </div>
                


              <!-- <?php
              }
              ?> -->





              <!--  <?php
              if ($programmeDetails->status > '5')
              {
              ?> -->

                <div class="col-sm-4" id="view_reason" style="display: none">
                  <div class="form-group">
                     <label>Reason <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $programmeDetails->reason; ?>">
                  </div>
               </div>


             <!--  <?php
              }
              ?> -->


              
            </div>


            <div class="row">

              
              <div class="col-sm-8">
                  <div class="form-group">
                     <label>Short Description <span class='error-text'>*</span></label>
                     <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="short_description" id="short_description"><?php echo $programmeDetails->short_description; ?></textarea>
                  </div>
               </div>


            </div>

            <div class="row">


               <div class="col-sm-8">
                  <div class="form-group">
                     <label>Target Audience <span class='error-text'>*</span></label>
                     <textarea class="form-control z-depth-1" rows="3" placeholder="Target Audience..." name="target_audience" id="target_audience"><?php echo $programmeDetails->target_audience; ?></textarea>
                  </div>
               </div>


            </div>


   
        </div>


      <div class="button-block clearfix">
         <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" >Save</button>
            <a href="../list" class="btn btn-link">Back</a>
         </div>
      </div>


      <div id="view_fields" style="display: none;">
      </div>
   
   </div>


         </form>

</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>


<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script type="text/javascript">



CKEDITOR.replace('short_description',{
  width: "100%",
  height: "100px"

}); 
CKEDITOR.replace('target_audience',{
  width: "100%",
  height: "100px"

}); 
</script>

<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script>

  $('select').select2();

  // getProductFieldsByProductType();

  $(function()
  {
      showPartner();
      $(".datepicker").datepicker({
      changeYear: true,
      changeMonth: true,
      });
  });

   showRejectField();

   function showRejectField()
    {
      var status = $("#status").val();

      // alert(status);

      if(status == '5')
      {
      // alert(status+ "per");
        $("#view_percentage").show();
        $("#view_reason").hide();
      }
      else if(status != '')
      {
      // alert(status);
        $("#view_percentage").hide();
        $("#view_reason").show();
      }
    }
   
   
   function showPartner(){
       var value = $("#internal_external").val();
       if(value=='Internal') {
            $("#partnerdropdown").hide();
   
       } else if(value=='External') {
            $("#partnerdropdown").show();
   
       }
   }


  function getProductFieldsByProductType()
  {
    var id_programme_type = $("#id_programme_type").val();
    // alert(id_programme_type);
    $.ajax(
        {
           url: '/prdtm/programme/getProductFieldsByProductType/'+id_programme_type,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {

              $("#view_fields").html(result);
              
              // window.location.reload();
           }
        });
  }



   function getProgrammeCodeDuplication()
   {
      var code = $("#code").val()

      if(code != '')
      {

        var tempPR = {};
        tempPR['code'] = code;
        tempPR['name'] = '';
        tempPR['id_programme'] = <?php echo $programmeDetails->id ?>;
        

        $.ajax(
        {
           url: '/prdtm/programme/getProgrammeDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Code Not Allowed, Programme Already Registered With The Given Code : '+ code );
                  $("#code").val('');
              }
           }
        });
      }
   }


   function getProgrammeNameDuplication()
   {
      var name = $("#name").val()

      if(name != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = name;
        tempPR['id_programme'] = <?php echo $programmeDetails->id ?>;
        

        $.ajax(
        {
           url: '/prdtm/programme/getProgrammeDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Name Not Allowed, Programme Already Registered With The Given Name : '+ name );
                  $("#name").val('');
              }
           }
        });
      }
   }
   
  

   $(document).ready(function()
   {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                internal_external: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                id_category: {
                    required: true
                },
                id_category_setup: {
                    required: true
                },
                id_programme_type: {
                    required: true
                },
                max_duration: {
                    required: true
                },
                duration_type: {
                    required: true
                },
                trending: {
                    required: true
                },
                target_audience: {
                    required: true
                },
                student_learning_hours: {
                    required: true
                },
                short_description: {
                    required: true
                },
                id_delivery_mode: {
                    required: true
                },
                id_study_level: {
                    required: true
                },
                sold_separately: {
                    required: true
                },
                status: {
                    required: true
                },
                language: {
                    required: true
                },
                is_free_course: {
                    required: true
                },
                percentage: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                internal_external: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                id_category: {
                    required: "<p class='error-text'>Select Product Type</p>",
                },
                id_category_setup: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_programme_type: {
                    required: "<p class='error-text'>Select Category Type</p>",
                },
                max_duration: {
                    required: "<p class='error-text'>Max. Duration Required</p>",
                },
                duration_type: {
                    required: "<p class='error-text'>Select Duration Type</p>",
                },
                trending: {
                    required: "<p class='error-text'>Select Status Type</p>",
                },
                target_audience: {
                    required: "<p class='error-text'>Tagset Audience Reuired</p>",
                },
                student_learning_hours: {
                    required: "<p class='error-text'>Learning Hours Required</p>",
                },
                short_description: {
                    required: "<p class='error-text'>Short Description Required</p>",
                },
                id_delivery_mode: {
                    required: "<p class='error-text'>Select Delicery Mode</p>",
                },
                id_study_level: {
                    required: "<p class='error-text'>Select Competency Level</p>",
                },
                sold_separately: {
                    required: "<p class='error-text'>Select Sold Seperately</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                language: {
                    required: "<p class='error-text'>Select Language</p>",
                },
                is_free_course: {
                    required: "<p class='error-text'>Select Is Free Course</p>",
                },
                percentage: {
                    required: "<p class='error-text'>Percentage Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
   
   
   
</script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
