<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>


            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>
            
            
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Programme Assessment Details</h4>

             
                <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Assesment <span class='error-text'>*</span></label>
                       <select name="id_assesment" id="id_assesment" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                             if (!empty($assesmentList))
                             {
                                 foreach ($assesmentList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"
                              <?php
                              if($record->id == $programAssesment->id_assesment)
                              {
                                echo 'selected';
                              }
                              ?>
                              ><?php echo $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                  </div>

                </div>




                 <div class="button-block clearfix">
                  <div class="form-group">

                      <button type="submit" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                      <?php
                    if($id_programme_assesment != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../newassessment/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                   </div>
                 </div>

         </div>
     
      </form>


      <?php

        if(!empty($assessmentDetailsList))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Programme Assessment Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Assessment</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($assessmentDetailsList);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $assessmentDetailsList[$i]->assessment_name;?></td>
                            <td class="text-center">
                                <a href='/prdtm/programme/newassessment/<?php echo $id_programme;?>/<?php echo $assessmentDetailsList[$i]->id;?>'>Edit</a> | 
                                <a onclick="deleteAssesmentData(<?php echo $assessmentDetailsList[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>



      <?php
      }
      ?>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();


	CKEDITOR.replace('overview',{
	  width: "100%",
	  height: "300px"

	}); 

   function deleteAssesmentData(id)
  	{
    	var cnf= confirm('Do you really want to delete?');
    	if(cnf==true)
    	{

	    $.ajax(
	        {
	           url: '/prdtm/programme/deleteAssesmentData/'+id,
	           type: 'GET',
	           error: function()
	           {
	            alert('Something is wrong');
	           },
	           success: function(result)
	           {
	              window.location.reload();
	           }
	        });
	    }
  }


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">




</script>