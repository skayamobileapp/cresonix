<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">

       <ul class="page-nav-links">
            <li><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>
            
            
          <!--   <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Skills</a></li>

            <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li class="active"><a href="/prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            <li><a href="/prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programme/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->

        </ul>
      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Topic Details</h4>
            <div class="row">
              
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Topic Name <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="topic" name="topic" value="<?php echo $topicDetails[0]->topic;?>" required>
                  </div>
               </div>

                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Learning Objective <span class='error-text'>*</span></label>
                     <select name='id_programme_has_syllabus[]' id='id_programme_has_syllabus' multiple>
                      <option value="">Select</option>
                      <?php 
                      for($i=0;$i<count($syllabus);$i++)
                      { ?>
                      
                      <option value="<?php echo $syllabus[$i]->id;?>"><?php echo $syllabus[$i]->learning_objective;?></option>
                    
                    <?php
                      }
                      ?> 
                     </select>
                  </div>
               </div>
             </div>
             <div class="row">

                <div class="col-sm-10">
                  <div class="form-group">
                     <label>Learning Objective <span class='error-text'>*</span></label>
                     <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"><?php echo $topicDetails[0]->message;?></textarea>
                  </div>
               </div>

               
               <div class="col-sm-4 pt-10">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg" value="Topic" name="save">Save</button>
                    <?php
                    if($id_topic != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../topic/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                 </div>
               </div>
            </div>
         </div>
     
      </form>
       <div class="custom-table">
        <table class="table" id="list-table">
          <thead>
            <tr>
              <th>Sl. No</th>
              <th>Topic</th>
              <th>Learning Objective</th>
              <th>Edit</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            
            $i=1;
              foreach ($topic as $record) {

              $this->load->model('programme_model');

              $namesArray = array();

              $namesArray = $this->programme_model->getNames($record->id_programme_has_syllabus);
              $objectivenames = '';
              for($l=0;$l<count($namesArray);$l++)
              {
                if($l > 0)
                {
                  $objectivenames = $objectivenames.','.$namesArray[$l]->learning_objective;
                }
                else
                {
                  $objectivenames = $namesArray[$l]->learning_objective;
                }
              }
            ?>
                <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo ucfirst($record->topic) ?></td>
                  </td>
                  <td><?php echo $objectivenames;?></td>
                  <td>
                    <a href='/prdtm/programme/topic/<?php echo $id_programme;?>/<?php echo $record->id;?>'>Edit</a></td>
                  <td class="text-center">
                    <a onclick="deleteTopicData(<?php echo $record->id; ?>)">Delete</a>
                  </td>
                </tr>
            <?php
            $i++;
              }
            
            ?>
          </tbody>
        </table>
       </div>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{
  width: "800px",
  height: "80px"
  });

  $('select').select2();

  function deleteTopicData(id)
  {
    var cnf= confirm('Do you really want to delete?');
    if(cnf==true)
    {

    $.ajax(
        {
           url: '/prdtm/programme/deleteTopicData/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              window.location.reload();
           }
        });
    }
  }

</script>
