<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_name_model extends CI_Model
{
    function examNameList()
    {
        $this->db->select('*');
        $this->db->from('exam_name');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function examNameListSearch($data)
    {
        $this->db->select('a.*');
        $this->db->from('exam_name as a');
        if ($data['name'] != '')
        {
            $likeCriteria = "(a.name  LIKE '%" . $data['name'] . "%' or a.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
    
    function getExamNameDetails($id)
    {
        $this->db->select('*');
        $this->db->from('exam_name');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewExamName($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_name', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamNameDetails($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_name', $data);
        return $result;
    }
}

