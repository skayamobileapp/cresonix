<div class="sidebar-nav">
                    <h4>Profile</h4>
                    <ul>
                        <li><a href="/student/editProfile/edit">Edit Profile</a></li>
                        <li><a href="/student/studentRecord/view">Record View</a></li>
                        <!-- <li><a href="/student/profile/logout">Logout</a></li> -->
                    </ul>
                    <h4>Scholarship</h4>
                    <ul>
                        <li><a href="/student/scholarshipApplication/list">Scholarship</a></li>
                    </ul>
                    <h4>Finance</h4>
                    <ul>
                        <li><a href="/student/statementOfAccount/viewInvoice">Invoice</a></li>
                        <li><a href="/student/statementOfAccount/viewReceipt">Payment & Receipt</a></li>
                        <li><a href="/student/statementOfAccount/viewSummary">Account Summary</a></li>
                        <li><a href="/student/statementOfAccount/viewDiscount">Discount List</a></li>
                        <li><a href="/student/statementOfAccount/viewSponser">Sponser Info. If Any</a></li>
                    </ul>
                    <h4>Internship / Project</h4>
                    <ul>
                        <li><a href="/student/internshipApplication/list">Internship Application</a></li>
                        <li><a href="/student/projectReportSubmission/list">Project Report Submission</a></li>
                        <li><a href="/student/placement/list">Placement Details</a></li>
                        <!-- <li><a href="/student/courseRegistration/courseWithdraw">Course Withdraw</a></li> -->
                    </ul>
                    <h4>Registration</h4>
                    <ul>
                        <li><a href="/student/courseRegistration/add">Course Registration</a></li>
                        <li><a href="/student/courseRegistration/courseWithdraw">Course Withdraw</a></li>
                        <li><a href="/student/applyChangeProgramme/list">Apply Change Program</a></li>
                        <li><a href="/student/applyChangeLearningMode/list">Apply Change Learning Mode</a></li>
                    </ul>
                    <h4>Examination</h4>
                    <ul>
                        <li><a href="/student/examination/timeTableSlip">Exam Timetable & Slip</a></li>
                        <li><a href="/student/examination/latestExamResults">Latest Exam Result</a></li>
                        <li><a href="/student/examination/partialTranscript">Partial Transcript</a></li>
                        <li><a href="/student/examination/resitApplication">Resit Application</a></li>
                        <li><a href="/student/examination/remarkingApplication">Remarking Application</a></li>
                    </ul>

                    <?php
                    if($student_education_level  == 'POSTGRADUATE' || $student_education_level  == 'MASTER')
                    {
                        ?>
                        
                    <h4>Research</h4>
                    <ul>
                        <!-- <li><a href="/student/Proposal/list">Proposal</a></li>
                        <li><a href="/student/articleship/list">Articleship</a></li>
                        <li><a href="/student/Professionalpracricepaper/list">Professional Practice Paper</a></li>
                        <li><a href="/student/deliverables/list">Deliverables</a></li>-->
                        <li><a href="/student/supervisor/list">Apply Supervisor Change</a></li>
                        <li><a href="/student/colloquium/list">Apply Colloquium</a></li>
                    </ul> 

                    <h4>Research Stage 1</h4>
                    <ul>
                        <li><a href="/student/deliverables/list">Research Proposal</a></li>
                        <li><a href="/student/proposalReporting/list">Research Proposal Reporting</a></li>
                        <!-- <li><a href="/student/articleship/list">Articleship</a></li>
                        <li><a href="/student/Professionalpracricepaper/list">Professional Practice Paper</a></li>
                        <li><a href="/student/supervisor/list">Apply Supervisor Change</a></li>
                        <li><a href="/student/colloquium/list">Apply Colloquium</a></li> -->
                    </ul>
                    <h4>Stage 2</h4>
                    <ul>
                        <li><a href="/student/proposalReporting/list2">Listing of students and their research progress report (Chapter 3)</a></li>
                    </ul>

                    <h4>Stage 3</h4>
                    <ul>
                        <li><a href="/student/proposalReporting/list3">Listing of students and their research progress report (Chapter 4)</a></li>
                    </ul>

                    <h4>Stage 4 ( Reporting )</h4>
                    <ul>
                        <li><a href="/student/toc/list">Research TOC</a></li>
                        <li><a href="/student/abstractt/list">Research Abstract</a></li>
                        <li><a href="/student/bound/list">5 copies of hard bound submission</a></li>
                        <li><a href="/student/sco/list">Research Soft Copy Of File</a></li>
                        <li><a href="/student/ppt/list">Research Power Point Presentation</a></li>
                    </ul>


                        <?php
                    }
                    ?>
                </div>