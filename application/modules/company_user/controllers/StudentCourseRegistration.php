<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentCourseRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_course_registration_model');
        $this->isCompanyUserLoggedIn();
    }

    function add()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;
            $id_invoice = '0';            
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            $formData = $this->input->post();
            
            $id_programme = $this->security->xss_clean($this->input->post('id_program'));
            $id_students = $this->security->xss_clean($this->input->post('id_student'));

            // echo "<Pre>";print_r($id_students);exit;
            if($id_students == '')
            {
                $student_count = 0;
            }
            else
            {
                $student_count = count($id_students);
            }

            
            if($student_count > 0)
            {
                // echo "<Pre>";print_r($student_count);exit;

                $id_fee_structure_master = 0;

                $fee_structure_master = $this->student_course_registration_model->getFeeStructureMaster($id_programme);

                if($fee_structure_master)
                {
                    $id_fee_structure_master = $fee_structure_master->id;
                }
                
                $get_data['id_programme'] = $id_programme;
                $get_data['id_fee_structure_master'] = $id_fee_structure_master;
                $get_data['currency'] = 'MYR';

                // echo "<Pre>";print_r($get_data);exit;

                $detail_data = $this->student_course_registration_model->getFeeStructureByData($get_data);

                // echo "<Pre>";print_r($detail_data);exit;

                if(!empty($detail_data))
                {
                    $finance_configuration = $this->student_course_registration_model->getFinanceConfiguration();
                    $gst_percentage = $finance_configuration->tax_sst;

                    $invoice_number = $this->student_course_registration_model->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'CORPORATE';
                    $invoice['fee_type'] = 'Registration';
                    // $invoice['remarks'] = 'Course Registration Fee';
                    $invoice['id_application'] = '0';
                    $invoice['id_student'] = $id_company;
                    $invoice['id_program'] = $id_programme;
                    $invoice['currency'] = 1;
                    $invoice['total_amount'] = '0';
                    $invoice['balance_amount'] = '0';
                    $invoice['paid_amount'] = '0';
                    $invoice['amount_before_gst'] = '0';
                    $invoice['gst_amount'] = '0';
                    $invoice['gst_percentage'] = $gst_percentage;
                    $invoice['status'] = 0;
                    $invoice['is_migrate_applicant'] = 0;
                    $invoice['created_by'] = $id_company_user;

                    
                    $inserted_id = $this->student_course_registration_model->addNewMainInvoice($invoice);

                    $id_invoice = $inserted_id;

                    // echo "<Pre>";print_r($inserted_id);exit;

                    $total_amount = 0;
                    $total_amount_before_gst = 0;
                    $total_gst_amount = 0;
                    $total_discount_amount = 0;
                    $sibling_discount_amount = 0;
                    $employee_discount_amount = 0;
                    $alumni_discount_amount = 0;

                    // echo "<Pre>";print_r($detail_data);exit;

                    foreach ($detail_data as $fee_structure)
                    {
                        $gst_tax = '0';
                        $one_percent = 0;
                        $details_gst_amount = 0;
                        $amount_before_gst = 0;

                        $is_installment = $fee_structure->is_installment;
                        $id_training_center = $fee_structure->id_training_center;
                        $id_fee_item = $fee_structure->id_fee_item;
                        $amount = $fee_structure->amount;


                        $fee_setup = $this->student_course_registration_model->getFeeSetup($id_fee_item);
                        // echo "<Pre>";print_r($fee_setup);exit;
                        
                        if($fee_setup)
                        {
                            $gst_tax = $fee_setup->gst_tax;
                        }
                        
                        $amount_before_gst = $amount;

                        if($gst_tax == '1')
                        {
                            if($gst_percentage > 0)
                            {
                                $one_percent = $amount * 0.01;
                                $details_gst_amount = $one_percent * $gst_percentage;
                            }

                            // echo "<Pre>";print_r($details_gst_amount);exit;



                            if($details_gst_amount > 0)
                            {
                                $amount = $amount + $details_gst_amount;
                            }
                        }


                            $data = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_item' => $id_fee_item,
                                'amount' => $amount * $student_count,
                                'amount_before_gst' => $amount_before_gst * $student_count,
                                'gst_amount' => $details_gst_amount,
                                'total_gst_amount' => $details_gst_amount * $student_count,
                                'gst_percentage' => $gst_percentage,
                                'price' => $amount,
                                'price_before_gst' => $amount_before_gst,
                                'quantity' => $student_count,
                                'id_reference' => $fee_structure->id,
                                'description' => 'Course Registration Fee',
                                'status' => 1,
                                'created_by' => $id_company_user
                            );

                        // echo "<Pre>";print_r($data);exit;

                            $total_amount = $total_amount + ($amount * $student_count);
                            $total_amount_before_gst = $total_amount_before_gst + ($amount_before_gst * $student_count);
                            $total_gst_amount = $total_gst_amount + ($details_gst_amount * $student_count);

                            $this->student_course_registration_model->addNewMainInvoiceDetails($data);
                        // }
                        // echo "<Pre>";print_r($data);exit;
                    }

                    $total_invoice_amount = $total_amount;


                    $invoice_update['total_amount'] = $total_invoice_amount;
                    $invoice_update['balance_amount'] = $total_invoice_amount;
                    $invoice_update['invoice_total'] = $total_invoice_amount;
                    $invoice_update['amount_before_gst'] = $total_amount_before_gst;
                    $invoice_update['gst_amount'] = $total_gst_amount;
                    $invoice_update['total_discount'] = 0;
                    $invoice_update['paid_amount'] = '0';
                    $this->student_course_registration_model->editMainInvoice($invoice_update,$inserted_id);


                    if($inserted_id)
                    {
                        // echo "<Pre>";print_r($inserted_id);exit;

                        $inserted_student_data = $this->student_course_registration_model->addNewMainInvoiceHasStudents($id_students,$inserted_id,$total_invoice_amount);
                    }
                }
            }





            if($id_invoice != '0')
            {
                foreach ($id_students as $id_student)
                {
                    $programme = $this->student_course_registration_model->getProgramme($id_programme);

                    if($programme)
                    {
                        // echo "<Pre>"; print_r($programme);exit;
                        
                        $max_duration = $programme->max_duration;
                        $duration_type = $programme->duration_type;

                        $start_date = date('Y-m-d');

                        $data_student_has_programme = array(
                            'id_student' => $id_student,
                            'id_performa_invoice' => $id_invoice,
                            'id_programme' => $id_programme,
                            'start_date' => $start_date,
                            'end_date' => date('Y-m-d', strtotime($start_date . "+" . $max_duration . " " . $duration_type) ),
                            'status' => 0
                        );
                        // echo "<Pre>"; print_r($data_student_has_programme);exit;

                        $id_student_has_programme = $this->student_course_registration_model->addNewStudentHasProgramme($data_student_has_programme);

                    }
                }
            }

            
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['programList'] = $this->student_course_registration_model->programListByStatus('1');
        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Inventory Management : Advisor Taagging';
        $this->loadViews("student_course_registration/add", $this->global, $data, NULL);
    }

    function searchStudents()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_company'] = $id_company;
        $tempData['id_company_user'] = $id_company_user;

        // $staffList = $this->student_course_registration_model->staffListByStatus('1');
        
        $student_data = $this->student_course_registration_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script> ";


         $table .= "
         <br>
         <h4> Select Employee For Programme Registration</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Employee Name</th>
                    <th>Employee NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll' onClick='validate()'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $company_name = $student_data[$i]->company_name;
                $registration_number = $student_data[$i]->registration_number;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>
                    <td>$email_id</td>                      
                    <td>$phone</td>
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}