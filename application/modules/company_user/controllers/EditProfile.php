<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EditProfile extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('edit_profile_model');
        $this->isCompanyUserLoggedIn();
    }

    function edit()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;
            
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            $formData = $this->input->post();
            
            $name = $this->security->xss_clean($this->input->post('name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $designation = $this->security->xss_clean($this->input->post('designation'));
            $user_name = $this->security->xss_clean($this->input->post('user_name'));

            $data = array(
                'name' => $name, 
                'phone' => $phone, 
                'email' => $email, 
                'designation' => $designation, 
                'user_name' => $user_name, 
            );

            $this->edit_profile_model->editCompanyUser($data,$id_company_user);

            
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['programList'] = $this->edit_profile_model->programListByStatus('1');
        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Inventory Management : Advisor Taagging';
        $this->loadViews("student_course_registration/add", $this->global, $data, NULL);
    }

    function searchStudents()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_company'] = $id_company;
        $tempData['id_company_user'] = $id_company_user;

        // $staffList = $this->edit_profile_model->staffListByStatus('1');
        
        $student_data = $this->edit_profile_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script> ";


         $table .= "
         <br>
         <h4> Select Employee For Programme Registration</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Employee Name</th>
                    <th>Employee NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Company</th>
                    <th style='text-align: center;'>Advisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $company_name = $student_data[$i]->company_name;
                $registration_number = $student_data[$i]->registration_number;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$registration_number - $company_name</td>
                    <td style='text-align: center;'>$ic_no - $advisor_name</td>
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}