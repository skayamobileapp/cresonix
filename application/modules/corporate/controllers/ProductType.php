<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProductType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('product_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            
            $data['productTypeList'] = $this->product_type_model->productTypeList();

            // print_r($subjectDetails);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Product Type';
            $this->loadViews("product_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('product_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'status' => $status

                );
            
                $result = $this->product_type_model->addNewProductType($data);
                redirect('/prdtm/productType/list');
            }

            $this->global['pageTitle'] = 'Inventory Management : Add Product Type';
            $this->loadViews("product_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('product_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/productType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'status' => $status

                );

                $result = $this->product_type_model->editProductTypeDetails($data,$id);
                redirect('/prdtm/productType/list');
            }
            $data['productType'] = $this->product_type_model->getProductTypeDetails($id);
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Product Type';
            $this->loadViews("product_type/edit", $this->global, $data, NULL);
        }
    }
}
