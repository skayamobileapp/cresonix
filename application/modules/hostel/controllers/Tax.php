<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Tax extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tax_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('tax.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['taxList'] = $this->tax_model->taxListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Tax List';
            $this->loadViews("tax/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('tax.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'percentage' => $percentage,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $duplicate_row = $this->tax_model->checkTaxCodeDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Tax Code Not Allowed";exit();
                }

                $result = $this->tax_model->addNewTax($data);
                redirect('/glsetup/tax/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Tax';
            $this->loadViews("tax/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('tax.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/tax/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'percentage' => $percentage,
                    'status' => $status
                );

                $duplicate_row = $this->tax_model->checkTaxCodeDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Tax Code Not Allowed";exit();
                }

                $result = $this->tax_model->editTax($data,$id);
                redirect('/glsetup/tax/list');
            }
            $data['tax'] = $this->tax_model->getTax($id);
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Tax';
            $this->loadViews("tax/edit", $this->global, $data, NULL);
        }
    }
}
