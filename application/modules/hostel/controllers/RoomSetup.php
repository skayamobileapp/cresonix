<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class RoomSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('room_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_hostel'] = $this->security->xss_clean($this->input->post('id_hostel'));
            $formData['id_building'] = $this->security->xss_clean($this->input->post('id_building'));
            $formData['id_block'] = $this->security->xss_clean($this->input->post('id_block'));
            $data['searchParam'] = $formData;

            $data['hostelList'] = $this->room_setup_model->getHostelRegistrationListByStatus('1');
            $data['apartmentList'] = $this->room_setup_model->getBuildingListByStatus('1');
            $data['blockList'] = $this->room_setup_model->blockListByStatus('1');
            $data['roomList'] = $this->room_setup_model->roomListListSearch($formData);
            // echo "<Pre>";print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Room Setup';
            $this->loadViews("room_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('room_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_room_type = $this->security->xss_clean($this->input->post('id_room_type'));
                $id_hostel = $this->security->xss_clean($this->input->post('id_hostel'));
                $id_building = $this->security->xss_clean($this->input->post('id_building'));
                $id_block = $this->security->xss_clean($this->input->post('id_block'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 3,
                    'id_hostel' => $id_hostel,
                    'id_parent' => $id_block,
                    'id_room_type' => $id_room_type,
                    'max_capacity' => $max_capacity,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->room_setup_model->addNewHostelRoom($data);

                if($result)
                {
                    $result = $this->room_setup_model->moveTempToDetails($result);
                }
                // $duplicate_row = $this->room_setup_model->checkRoomTypeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }

                // $result = $this->room_setup_model->addNewRoomType($data);
                redirect('/hostel/roomSetup/list');
            }
            else
            {
                $result = $this->room_setup_model->deleteTempRoomInventoryAllotmentBySession($id_session);

            }
            $data['hostelList'] = $this->room_setup_model->getHostelRegistrationListByStatus('1');
            $data['roomTypeList'] = $this->room_setup_model->getRoomTypeListByStatus('1');
            $data['inventoryList'] = $this->room_setup_model->inventoryListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Room Setup';
            $this->loadViews("room_setup/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('room_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/roomSetup/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_room_type = $this->security->xss_clean($this->input->post('id_room_type'));
                $id_hostel = $this->security->xss_clean($this->input->post('id_hostel'));
                $id_building = $this->security->xss_clean($this->input->post('id_building'));
                $id_block = $this->security->xss_clean($this->input->post('id_block'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 3,
                    'id_hostel' => $id_hostel,
                    'id_parent' => $id_block,
                    'id_room_type' => $id_room_type,
                    'name' => $name,
                    'max_capacity' => $max_capacity,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                // $duplicate_row = $this->room_setup_model->checkRoomTypeDuplicationEdit($data,$id);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }
                
                $result = $this->room_setup_model->editHostelRoom($data,$id);
                redirect('/hostel/roomSetup/list');
            }
            $data['hostelList'] = $this->room_setup_model->getHostelRegistrationListByStatus('1');
            $data['buildingList'] = $this->room_setup_model->getBuildingList();
            $data['blockList'] = $this->room_setup_model->blockList();
            $data['roomSetup'] = $this->room_setup_model->getHostelRoom($id);
            $id_block = $data['roomSetup']->id_parent;
            $data['blockSetup'] = $this->room_setup_model->getHostelRoom($id_block);
            $data['roomTypeList'] = $this->room_setup_model->getRoomTypeListByStatus('1');
            $data['inventoryList'] = $this->room_setup_model->inventoryListByStatus('1');
            $data['id_room'] = $id;
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Room Setup';
            $this->loadViews("room_setup/edit", $this->global, $data, NULL);
        }
    }

     function getBuildingListByHostelId($id_hostel)
    {
            // echo "<Pre>"; print_r($id_hostel);exit;
            $results = $this->room_setup_model->getBuildingListByHostelId($id_hostel);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
                <select name='id_building' id='id_building' class='form-control' onchange='getBlockListData()'>
                <option value=''>Select</option>

                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $code = $results[$i]->code;
            $table.="<option value=" . $id . ">" . $code . " - " . $name . 
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

     function getBlockList()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        $data['level'] = 2;
            // echo "<Pre>"; print_r($data);exit;
        $results = $this->room_setup_model->getHostelRoomByData($data);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_block' id='id_block' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $code = $results[$i]->code;
            $table.="<option value=" . $id . ">" . $code . " - " . $name . 
                    "</option>";

            }

            $table.="
            </select>";

            echo $table;
            exit;
    }


     function tempAdd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->room_setup_model->addTempRoomInventoryAllotment($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;
        // }
        $data = $this->displaytempdata();
        
        echo $data;
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->room_setup_model->getTempRoomInventoryAllotmentBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Inventory</th>
                    <th>Quantity</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $inventory = $temp_details[$i]->inventory_code . " - " . $temp_details[$i]->inventory_name;
                    $quantity = $temp_details[$i]->quantity;
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$inventory</td>
                            <td>$quantity</td>                            
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempRoomInventory($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempRoomInventory($id)
    {
        // echo "<Pre>";print_r($id);exit;
        $inserted_id = $this->room_setup_model->deleteTempRoomInventoryAllotment($id);
        $data = $this->displaytempdata();
        echo $data; 
    }


    function addInventoryData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        $id_room = $tempData['id_room'];
        
        $inserted_id = $this->room_setup_model->addRoomInventoryAllotment($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;
        // }
        $data = $this->displayInvData($id_room);
        
        echo $data;
    }



    function displayInvData($id_room)
    {
        
        $temp_details = $this->room_setup_model->getRoomInventoryAllotment($id_room); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Inventory</th>
                    <th>Quantity</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $inventory = $temp_details[$i]->inventory_code . " - " . $temp_details[$i]->inventory_name;
                    $quantity = $temp_details[$i]->quantity;
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$inventory</td>
                            <td>$quantity</td>                            
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteRoomInventory($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteRoomInventory()
    {
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);e
        $id_room = $tempData['id_room'];
        $inserted_id = $this->room_setup_model->deleteRoomInventory($tempData['id']);
        $data = $this->displayInvData($id_room);
        echo $data;exit;
    }
}
