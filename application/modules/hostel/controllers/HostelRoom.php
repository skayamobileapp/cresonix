<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class HostelRoom extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('hostel_room_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('hostel_registration.list') == 1)
        // if ($this->checkAccess('hostel_room.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
 
            $data['searchParam'] = $formData;

            $data['staffList'] = $this->hostel_room_model->staffListByStatus('1');
            $data['hostelRegistrationList'] = $this->hostel_room_model->hostelRegistrationListSearch($formData);
               // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : Hostel Registration List';
            $this->loadViews("hostel_room/list", $this->global, $data, NULL);
        }
    }


    function add($id_hostel)
    {

               // echo "<Pre>"; print_r($id_hostel);exit;

        if ($this->checkAccess('hostel_room.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_hostel == null)
            {
                redirect('/hostel/hostelRoom/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $name = $this->security->xss_clean($this->input->post('name'));

                $complete_code = $code1 . $code;
            
                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'level' => 1,
                    'id_hostel' => $id_hostel,
                    'name' => $name
                );

                $result = $this->hostel_room_model->addNewHostelRoom($data);
                // redirect(['HTTP_REFERER']);
                redirect('/hostel/hostelRoom/add/'.$id_hostel);
            }
            $activity['level'] = '1';
            $activity['id_hostel'] = $id_hostel;
            $activity['id_parent'] = '';

            $data['hostelRoomList'] = $this->hostel_room_model->hostelRoomListByLevel($activity); 
            $data['hostelDetails'] = $this->hostel_room_model->getHostelRegistration($id_hostel);   
               // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : Add Building';
            $this->loadViews("hostel_room/add", $this->global, $data, NULL);
        }
    }

    function level2($id_parent,$id_hostel)
    {
        if ($this->checkAccess('hostel_room.level2') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $complete_code = $code1 . $code;

            
                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'level' => 2,
                    'id_parent' => $id_parent,
                    'id_hostel' => $id_hostel,
                    'name' => $name
                );
            
                $result = $this->hostel_room_model->addNewHostelRoom($data);
                // redirect(['HTTP_REFERER']);
                redirect('/hostel/hostelRoom/level2/'.$id_parent.'/'.$id_hostel);
            }
            $activity['level'] = '2';
            $activity['id_hostel'] = $id_hostel;
            $activity['id_parent'] = $id_parent;

            $data['hostelRoomBuilding'] = $this->hostel_room_model->getHostelRoom($id_parent);   
            $data['hostelRoomList'] = $this->hostel_room_model->hostelRoomListByLevel($activity);   
            $data['hostelDetails'] = $this->hostel_room_model->getHostelRegistration($id_hostel); 
               // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Add Block';
            $this->loadViews("hostel_room/level2", $this->global, $data, NULL);
        }
    }

    function level3($id_block,$id_building,$id_hostel)
    {
        if ($this->checkAccess('hostel_room.level3') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code2 = $this->security->xss_clean($this->input->post('code2'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $id_room_type = $this->security->xss_clean($this->input->post('id_room_type'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $complete_code = $code2 . $code;

                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'level' => 3,
                    'id_parent' => $id_block,
                    'id_hostel' => $id_hostel,
                    'id_room_type' => $id_room_type,
                    'max_capacity' => $max_capacity,
                    'name' => $name
                );
            
                $result = $this->hostel_room_model->addNewHostelRoom($data);
                // redirect(['HTTP_REFERER']);
                redirect('/hostel/hostelRoom/level3/'.$id_block.'/'.$id_building . '/' . $id_hostel);
            }

            $activity['level'] = '3';
            $activity['id_parent'] = $id_block;
            $activity['id_hostel'] = $id_hostel;

            $data['hostelRoomBuilding'] = $this->hostel_room_model->getHostelRoom($id_building);
            $data['hostelRoomBlock'] = $this->hostel_room_model->getHostelRoom($id_block);
            $data['hostelRoomList'] = $this->hostel_room_model->hostelRoomListByLevel($activity);   
            $data['hostelDetails'] = $this->hostel_room_model->getHostelRegistration($id_hostel);
            $data['roomTypeList'] = $this->hostel_room_model->getRoomTypeListByStatus('1');
            $data['id_hostel'] = $id_hostel;

            // echo "<Pre>";print_r($data);exit();
            $this->global['pageTitle'] = 'Inventory Management : Add Account Code';
            $this->loadViews("hostel_room/level3", $this->global, $data, NULL);
        }
    }

    function level1Edit($id,$id_hostel)
    {
        if ($this->checkAccess('hostel_room.level1_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/hostelRoom/add');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $name = $this->security->xss_clean($this->input->post('name'));
                
                $complete_code = $code1 . $code;
            
                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'name' => $name
                );
                
                $result = $this->hostel_room_model->editHostelRoom($data,$id);
                redirect('/hostel/hostelRoom/add/'.$id_hostel);
            }
            $data['hostelDetails'] = $this->hostel_room_model->getHostelRegistration($id_hostel);
            $data['hostelRoom'] = $this->hostel_room_model->getHostelRoom($id);
            // echo "<Pre>";print_r($data);exit();
            $this->global['pageTitle'] = 'Inventory Management : Edit Account Level 1 Code';
            $this->loadViews("hostel_room/level1_edit", $this->global, $data, NULL);
        }
    }


    function level2Edit($id = NULL,$id_building = NULL,$id_hostel = NULL)
    {
        if ($this->checkAccess('hostel_room.level2_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/hostelRoom/level2/'.$id_parent);
            }
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));

                $complete_code = $code1 . $code;
            
                $data = array(
                    'short_code' => $code,
                    'code' => $complete_code,
                    'name' => $name
                );
                
                $result = $this->hostel_room_model->editHostelRoom($data,$id);
                redirect('/hostel/hostelRoom/level2/'.$id_building.'/'.$id_hostel);
            }
            $inventory['level'] = '2';
            $inventory['id_room'] = $id;
            $data['inventoryAllotmentList'] = $this->hostel_room_model->getInventoryAllotmentList($inventory);

            $data['inventoryList'] = $this->hostel_room_model->inventoryListByStatus('1');
            $data['hostelRoom'] = $this->hostel_room_model->getHostelRoom($id);
            $data['hostelRoomBuilding'] = $this->hostel_room_model->getHostelRoom($id_building);
            $data['hostelDetails'] = $this->hostel_room_model->getHostelRegistration($id_hostel);

            $this->global['pageTitle'] = 'Inventory Management : Edit Account Level 2 Code';
            $this->loadViews("hostel_room/level2_edit", $this->global, $data, NULL);
        }
    }

    function level3Edit($id = NULL,$id_block = NULL,$id_building = NULL,$id_hostel = NULL)
    {
        if ($this->checkAccess('hostel_room.level3_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/hostelRoom/level3/'.$id_block.'/'.$id_building.'/'.$id_hostel);
            }
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code2 = $this->security->xss_clean($this->input->post('code2'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_room_type = $this->security->xss_clean($this->input->post('id_room_type'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
            
                $complete_code = $code2 . $code;

                $data = array(
                    'short_code' => $code,
                    'code' => $complete_code,
                    'max_capacity' => $max_capacity,
                    'id_room_type' => $id_room_type,
                    'name' => $name
                );
                
                $result = $this->hostel_room_model->editHostelRoom($data,$id);
                redirect('/hostel/hostelRoom/level3/'.$id_block.'/'.$id_building. '/'.$id_hostel );
            }
            
            $inventory['level'] = '3';
            $inventory['id_room'] = $id;
            $data['inventoryAllotmentList'] = $this->hostel_room_model->getInventoryAllotmentList($inventory);

            $data['inventoryList'] = $this->hostel_room_model->inventoryListByStatus('1');

            $data['hostelRoom'] = $this->hostel_room_model->getHostelRoom($id);
            $data['hostelRoomBuilding'] = $this->hostel_room_model->getHostelRoom($id_building);
            $data['hostelRoomBlock'] = $this->hostel_room_model->getHostelRoom($id_block);
            $data['hostelDetails'] = $this->hostel_room_model->getHostelRegistration($id_hostel);
            $data['roomTypeList'] = $this->hostel_room_model->getRoomTypeListByStatus('1');
            

            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Inventory Management : Edit Account Level 3 Code';
            $this->loadViews("hostel_room/level3_edit", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('hostel_room.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/hostel_room/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                // $duplicate_row = $this->hostel_room_model->checkHostelRoomDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Account Code Not Allowed";exit();
                // }
                
                $result = $this->hostel_room_model->editHostelRoom($data,$id);
                redirect('/hostel/hostelRoom/list');
            }
            $data['hostelRoom'] = $this->hostel_room_model->getHostelRoom($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Account Code';
            $this->loadViews("hostel_room/edit", $this->global, $data, NULL);
        }
    }

    function addInventoryAllotment()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->hostel_room_model->addInventoryAllotment($tempData);
        
        echo "Sucess";exit(); 
    }

    function deleteInventoryAllotment($id_inventory_allotment)
    {
        $deleted_inventory = $this->hostel_room_model->deleteInventoryAllotment($id_inventory_allotment);
        echo "success";exit();
    }
}
