<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class HostelRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('hostel_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('hostel_registration.list') == 1)
        // if ($this->checkAccess('hostel_registration.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
 
            $data['searchParam'] = $formData;

            $data['staffList'] = $this->hostel_registration_model->staffListByStatus('1');
            $data['hostelRegistrationList'] = $this->hostel_registration_model->hostelRegistrationListSearch($formData);
               // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : Hostel Registration List';
            $this->loadViews("hostel_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('hostel_registration.add') == 1)
        // if ($this->checkAccess('hostel_registration.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;
            	$id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));

                
                $data = array(
					'name' => $name,
					'code' => $code,
                    'type' => $type,
                    'id_staff' => $id_staff,
                    'contact_number' => $contact_number,
					'max_capacity' => $max_capacity,
					'address' => $address,
					'landmark' => $landmark,
					'city' => $city,
					'id_state' => $id_state,
					'id_country' => $id_country,
                    'zipcode' => $zipcode,
					'status' => $status,
					'created_by' => $user_id
                );
                $inserted_id = $this->hostel_registration_model->addNewHostelRegistration($data);
                redirect('/hostel/hostelRegistration/list');
            }
            $data['countryList'] = $this->hostel_registration_model->countryListByStatus('1');
            $data['staffList'] = $this->hostel_registration_model->staffListByStatus('1');
               // echo "<Pre>"; print_r($data);exit;

            
            $this->global['pageTitle'] = 'Inventory Management : Add Hostel Registration';
            $this->loadViews("hostel_registration/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('hostel_registration.edit') == 1)
        // if ($this->checkAccess('hostel_registration.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/hostelRegistration/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;

	            $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));

                
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'type' => $type,
                    'id_staff' => $id_staff,
                    'contact_number' => $contact_number,
                    'max_capacity' => $max_capacity,
                    'address' => $address,
                    'landmark' => $landmark,
                    'city' => $city,
                    'id_state' => $id_state,
                    'id_country' => $id_country,
                    'zipcode' => $zipcode,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                //print_r($data);exit;
                $result = $this->hostel_registration_model->editHostelRegistration($data,$id);
                redirect('/hostel/hostelRegistration/list');
            }
            // $data['studentList'] = $this->hostel_registration_model->studentList();
            $data['hostelRegistration'] = $this->hostel_registration_model->getHostelRegistration($id);
            $data['countryList'] = $this->hostel_registration_model->countryListByStatus('1');
            $data['stateList'] = $this->hostel_registration_model->stateListByStatus('1');
            $data['staffList'] = $this->hostel_registration_model->staffListByStatus('1');
            
               // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Hostel Registration';
            $this->loadViews("hostel_registration/edit", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
        $results = $this->hostel_registration_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_state' id='id_state' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}
