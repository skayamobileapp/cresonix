<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BuildingSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('building_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('building_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_hostel'] = $this->security->xss_clean($this->input->post('id_hostel'));
            $data['searchParam'] = $formData;

            $data['hostelList'] = $this->building_setup_model->getHostelRegistrationListByStatus('1');
            $data['buildingList'] = $this->building_setup_model->buildingListListSearch($formData);
            // echo "<Pre>";print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Apartment Setup';
            $this->loadViews("building_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('building_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_hostel = $this->security->xss_clean($this->input->post('id_hostel'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 1,
                    'id_hostel' => $id_hostel,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->building_setup_model->addNewHostelRoom($data);
            
                // $duplicate_row = $this->building_setup_model->checkRoomTypeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }

                // $result = $this->building_setup_model->addNewRoomType($data);
                redirect('/hostel/buildingSetup/list');
            }
            $data['hostelList'] = $this->building_setup_model->getHostelRegistrationListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Apartment Setup';
            $this->loadViews("building_setup/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('building_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/buildingSetup/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_hostel = $this->security->xss_clean($this->input->post('id_hostel'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 1,
                    'id_hostel' => $id_hostel,
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                // $duplicate_row = $this->building_setup_model->checkRoomTypeDuplicationEdit($data,$id);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }
                
                $result = $this->building_setup_model->editHostelRoom($data,$id);
                redirect('/hostel/buildingSetup/list');
            }
            $data['hostelList'] = $this->building_setup_model->getHostelRegistrationListByStatus('1');
            $data['buildingSetup'] = $this->building_setup_model->getHostelRoom($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Apartment Setup';
            $this->loadViews("building_setup/edit", $this->global, $data, NULL);
        }
    }
}
