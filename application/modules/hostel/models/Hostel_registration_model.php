<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hostel_registration_model extends CI_Model
{
    function hostelRegistrationList()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('hostel_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function hostelRegistrationListSearch($data)
    {
        $this->db->select('fc.*, c.name as country, s.name as state, st.name as staff_name, st.ic_no');
        $this->db->from('hostel_registration as fc');
        $this->db->join('staff as st', 'fc.id_staff = st.id');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(fc.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_staff'] != '')
        {
            $this->db->where('fc.id_staff', $data['id_staff']);
        }
        if ($data['type'] != '')
        {
            $this->db->where('fc.type', $data['type']);
        }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('fc.id_intake', $data['id_intake']);
        // }

        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getHostelRegistration($id)
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('hostel_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewHostelRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewHostelRegistrationDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_registration_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editHostelRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('hostel_registration', $data);
        return TRUE;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
}

