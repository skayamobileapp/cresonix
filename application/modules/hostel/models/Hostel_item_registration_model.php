<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hostel_item_registration_model extends CI_Model
{

    function hostelItemRegistrationListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_item_registration');
        if (!empty($data))
        {
            $likeCriteria = "(code  LIKE '%" . $data . "%' or name  LIKE '%" . $data . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getHostelItemRegistration($id)
    {
        $this->db->select('*');
        $this->db->from('hostel_item_registration');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkHostelItemRegistrationDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_item_registration');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkHostelItemRegistrationDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('hostel_item_registration');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewHostelItemRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_item_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editHostelItemRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('hostel_item_registration', $data);
        return TRUE;
    }
}

