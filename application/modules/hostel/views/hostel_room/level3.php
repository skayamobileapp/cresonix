<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        
        <form id="form_grade" action="" method="post">

            <div class="page-title clearfix">
            <h3>Hostel Details</h3>
            </div>
            <div class="form-container">
                <h4 class="form-group-title">Hostel Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Hostel Name :</dt>
                                <dd><?php echo ucwords($hostelDetails->name);?></dd>
                            </dl>
                            <dl>
                                <dt>Staff Incharge :</dt>
                                <dd><?php echo $hostelDetails->ic_no . " - " . $hostelDetails->staff_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel Address :</dt>
                                <dd>
                                    <?php echo $hostelDetails->address ?></dd>
                            </dl> 
                            <dl>
                                <dt>Hostel City :</dt>
                                <dd><?php echo $hostelDetails->city ?></dd>
                            </dl>  
                                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Hostel Code :</dt>
                                <dd><?php echo $hostelDetails->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Contact Number :</dt>
                                <dd><?php echo $hostelDetails->contact_number ?></dd>
                            </dl>  
                            <dl>
                                <dt>Hostel Landmark :</dt>
                                <dd><?php echo $hostelDetails->landmark; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel State :</dt>
                                <dd><?php echo $hostelDetails->state; ?></dd>
                            </dl>
                            
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-container">
                <h4 class="form-group-title">Building Details</h4>
                <div class='data-list'>
                    <div class='row'>
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Building Name :</dt>
                                <dd><?php echo ucwords($hostelRoomBuilding->name);?></dd>
                            </dl>   
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Building Code :</dt>
                                <dd><?php echo $hostelRoomBuilding->short_code ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-container">
                <h4 class="form-group-title">Block / Level Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Block / Level Name :</dt>
                                <dd><?php echo ucwords($hostelRoomBlock->name);?></dd>
                            </dl>   
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Block / Level Code :</dt>
                                <dd><?php echo $hostelRoomBlock->short_code ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <br>


            <div class="page-title clearfix">
            <h3>Add Room</h3>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Room Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code">
                            <input type="hidden" class="form-control" id="code1" name="code1" value="<?php echo $hostelRoomBlock->code ?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Max. Capacity <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="max_capacity" name="max_capacity" min="1" max="10">
                        </div>
                    </div>

                </div>

                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Room Type <span class='error-text'>*</span></label>
                            <select name="id_room_type" id="id_room_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($roomTypeList))
                                {
                                    foreach ($roomTypeList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>

                
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="<?php echo '../../../level2/'.$hostelRoomBuilding->id.'/'.$id_hostel; ?>" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>

        <br>

        <div class="form-container">
                <h4 class="form-group-title">Room List</h4>

            <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                        <th>Building CODE</th>
                        <th>Block / Level CODE</th>
                         <th>CODE</th>
                         <th>Name</th>
                         <th>Room Type</th>
                         <th>Max. Capacity</th>
                         <th style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($hostelRoomList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $hostelRoomBuilding->short_code;?></td>
                        <td><?php echo $hostelRoomBlock->short_code;?></td>
                        <td><?php echo $hostelRoomList[$i]->short_code;?></td>
                        <td><?php echo $hostelRoomList[$i]->name;?></td>
                        <td><?php echo $hostelRoomList[$i]->room_type_code . " - " . $hostelRoomList[$i]->room_type_name;?></td>
                        <td><?php echo $hostelRoomList[$i]->max_capacity;?></td>
                        <td style="text-align: center;">
                            <a class="btn btn-sm btn-edit" href="<?php echo '../../../level3Edit/' . $hostelRoomList[$i]->id. '/' . $hostelRoomList[$i]->id_parent. '/' . $hostelRoomBuilding->id.'/'.$hostelDetails->id; ?>" title="Edit">Edit</a>
                        </td>
                         </tr>
                      <?php 
                  } 
                  ?>
                    </tbody>
                </table>
            </div>

        </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                },
                 max_capacity: {
                    required: true
                },
                id_room_type: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                max_capacity : {
                    required: "<p class='error-text'>Max. Capacity Required</p>",
                },
                id_room_type : {
                    required: "<p class='error-text'>Room Type Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
