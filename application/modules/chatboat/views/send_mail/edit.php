<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Sponser Has Students</h3>
        </div>
        <form id="form_sponser_has_students" action="" method="post">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Sponser *</label>
                        <select name="id_sponser" id="id_sponser" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($sponserList))
                            {
                                foreach ($sponserList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $sponserHasStudentsDetails->id_sponser)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student *</label>
                        <select name="id_student" id="id_student" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($studentList))
                            {
                                foreach ($studentList as $record)
                                {?>
                                   <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $sponserHasStudentsDetails->id_student)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount *</label>
                        <input type="number" class="form-control" id="amount" name="amount" value="<?php echo $sponserHasStudentsDetails->amount;?>">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
     $(document).ready(function() {
        $("#form_sponser_has_students").validate({
            rules: {
                id_sponser: {
                    required: true
                },
                id_student: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_sponser: {
                    required: "<p class='error-text'>Select Sponser</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
