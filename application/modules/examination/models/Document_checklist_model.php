<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Document_checklist_model extends CI_Model
{
    function documentList()
    {
        $this->db->select('d.*, c.category_name as category, p.name as program');
        $this->db->from('document_checklist as d');
        $this->db->join('category_type as c', 'd.id_category = c.id');
        $this->db->join('programme as p', 'd.id_program = p.id');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getDocumentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('document_checklist');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDocument($data)
    {
        $this->db->trans_start();
        $this->db->insert('document_checklist', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDocumentDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('document_checklist', $data);
        return TRUE;
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }
}

