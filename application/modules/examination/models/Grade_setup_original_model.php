<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grade_setup_model extends CI_Model
{
    function gradeSetupList()
    {
        $this->db->select('*');
        $this->db->from('grade_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

     function gradeSetupListSearch($data)
    {
        $this->db->select('gs.*, s.name as intake_name,p.name as program_name,c.name as coursename,a.name as awardname');
        $this->db->from('grade_setup as gs');
        $this->db->join('intake as s', 'gs.id_intake = s.id','left');
        $this->db->join('programme p ', 'gs.id_program = p.id','left');
        $this->db->join('course c ', 'gs.id_course = c.id','left');
        $this->db->join('award a ', 'gs.id_award = a.id','left');

        // if (!empty($search))
        // {
        //     $likeCriteria = "(name  LIKE '%" . $search . "%' or name_optional_language  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%' or foundation  LIKE '%" . $search . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if ($data['based_on']!='')
        {
            $this->db->where('p.based_on', $data['based_on']);
        }
        if ($data['id_intake'] !='')
        {
            $this->db->where('p.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] !='')
        {
            $this->db->where('p.id_semester', $data['id_semester']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function awardListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function gradeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function addTempGradeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_grade_setup_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempGradeDetailsBySession($id_session)
    {
        $this->db->select('tgsd.*, g.name as grade_name, g.description as grade_description');
        $this->db->from('temp_grade_setup_details as tgsd');
        $this->db->join('grade as g', 'tgsd.id_grade = g.id');
        $this->db->where('tgsd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function deleteTempGradeDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_grade_setup_details');
       return TRUE;
    }

    function addNewGradeSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('grade_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    function moveTempGradeSetupDetailsToMainDetails($id_grade_setup)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempGradeDetailsBySes($id_session);
        
        foreach ($temp_details as $temp_detail)
        {
            $temp_detail->id_grade_setup = $id_grade_setup;
            unset($temp_detail->id);
            unset($temp_detail->id_session);

            $added_detail = $this->addNewGradeSetupDetails($temp_detail);
        }
        
        $deleted = $this->deleteTempGradeDetailsBySession($id_session);
    }

    function getTempGradeDetailsBySes($id_session)
    {
        $this->db->select('tgsd.*');
        $this->db->from('temp_grade_setup_details as tgsd');
        $this->db->join('grade as g', 'tgsd.id_grade = g.id');
        $this->db->where('tgsd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function addNewGradeSetupDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('grade_setup_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


     function upGradeSetupDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('grade_setup_details', $data);
        return TRUE;
    }
    

    function deleteTempGradeDetailsBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_grade_setup_details');
       return TRUE;
    }

    function getGradeSetup($id)
    {
        $this->db->select('tgsd.*');
        $this->db->from('grade_setup as tgsd');
        $this->db->where('tgsd.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function getGradeSetupDetails($id_grade_setup)
    {
        $this->db->select('tgsd.*, tgsd.id_grade as grade_name');
        $this->db->from('grade_setup_details as tgsd');
        $this->db->where('tgsd.id_grade_setup', $id_grade_setup);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function getGradeSetupDetailsById($id_grade_setup)
    {
        $this->db->select('tgsd.*');
        $this->db->from('grade_setup_details as tgsd');
        $this->db->where('tgsd.id', $id_grade_setup);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }


    function deleteGradeDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('grade_setup_details');
       return TRUE;
    }

    function editGradeSetup($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('grade_setup', $data);
        return TRUE;
    }
}