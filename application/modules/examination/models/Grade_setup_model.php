<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grade_setup_model extends CI_Model
{
    function categoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function categoryTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function productTypeSetupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function gradeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programmeListSearch($data)
    {
        $this->db->select('p.*, md.id as marks_disctributed, st.name as status_name');
        $this->db->from('programme as p');
        $this->db->join('marks_distribution_setup as md', 'p.id = md.id_programme','left');
        $this->db->join('status_table as st', 'p.status = st.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(p.name  LIKE '%" . $data['name'] . "%' or p.name_optional_language  LIKE '%" . $data['name'] . "%' or p.code  LIKE '%" . $data['name'] . "%' or p.foundation  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_category'] != '')
        {
            $this->db->where('p.id_category', $data['id_category']);
        }
        if ($data['id_category_setup'] != '')
        {
            $this->db->where('p.id_category_setup', $data['id_category_setup']);
        }
        if ($data['id_programme_type'] != '')
        {
            $this->db->where('p.id_programme_type', $data['id_programme_type']);
        }
        if($data['id_partner_university'] != '')
        {
            $this->db->where('p.id_partner_university', $data['id_partner_university']);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }

    function saveMarksDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('marks_distribution_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getMarkDistributionByProgramme($id_programme)
    {
        $this->db->select('md.*, g.name as grade');
        $this->db->from('marks_distribution_setup as md');
        $this->db->join('grade as g', 'md.id_grade = g.id');
        $this->db->where('md.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getMarkDistribution($id)
    {
        $this->db->select('*');
        $this->db->from('marks_distribution_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteMarksDistribution($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('marks_distribution_setup');
        return TRUE;
    }
    
    function addMarkDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('marks_distribution_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMarkDistribution($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('marks_distribution_setup', $data);
        return TRUE;
    }
}