<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grade_model extends CI_Model
{
    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function gradeList()
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function gradeListSearch($search)
    {
        $this->db->select('g.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('grade as g');
        $this->db->join('programme as p', 'g.id_programme = p.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(g.name  LIKE '%" . $data['name'] . "%' or g.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['id_programme']))
        {
            $this->db->where('g.id_programme', $data['id_programme']);
        }
        $this->db->order_by("g.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getGradeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewGrade($data)
    {
        $this->db->trans_start();
        $this->db->insert('grade', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editGradeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('grade', $data);
        return TRUE;
    }
}

