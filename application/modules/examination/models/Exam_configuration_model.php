<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_configuration_model extends CI_Model
{

    function examConfigurationListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('exam_configuration');
        // if (!empty($search))
        // {
        //     $likeCriteria = "(name  LIKE '%" . $search . "%' or description  LIKE '%" . $search . "%')";
        //     $this->db->where($likeCriteria);
        // }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getExamConfiguration()
    {
        $this->db->select('*');
        $this->db->from('exam_configuration');
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function editExamConfiguration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_configuration', $data);
        return TRUE;
    }
}

