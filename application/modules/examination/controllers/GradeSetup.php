<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GradeSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grade_setup_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category_type'));
            $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));

            $data['searchParam'] = $formData;

            // $data['programmeList'] = $this->grade_setup_model->programmeList();
            // $data['categoryTypeList'] = $this->grade_setup_model->categoryTypeListByStatus('1');
            $data['categoryList'] = $this->grade_setup_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->grade_setup_model->productTypeSetupListByStatus('1');

            $data['programmeList'] = $this->grade_setup_model->programmeListSearch($formData);

                // echo "<Pre>";print_r($data['programmeList']);exit();


            $this->global['pageTitle'] = 'Inventory Management : Program List';
            $this->loadViews("grade_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_programme,$id_marks_distribution=NULL)
    {
        if ($this->checkAccess('grade_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $min = $this->security->xss_clean($this->input->post('min'));
                $max = $this->security->xss_clean($this->input->post('max'));
                $id_grade = $this->security->xss_clean($this->input->post('id_grade'));
                $is_fail = $this->security->xss_clean($this->input->post('is_fail'));

                
                $data = array(
                   'id_programme' => $id_programme,
                   'min' => $min,
                   'max' => $max,
                   'id_grade' => $id_grade,
                   'is_fail' => $is_fail,
                   'status' => 1,
                   'created_by' => $user_id
                );

                // echo "<Pre>";print_r($id_marks_distribution);exit();
                if($id_marks_distribution > 0)
                {
                    $result = $this->grade_setup_model->editMarkDistribution($data,$id_marks_distribution);
                }
                else
                {
                    $result = $this->grade_setup_model->addMarkDistribution($data);
                }
                redirect('/examination/gradeSetup/add/'.$id_programme);
            }

            $data['id_programme'] = $id_programme;
            $data['id_marks_distribution'] = $id_marks_distribution;
            $data['programme'] = $this->grade_setup_model->getProgrammeById($id_programme);
            $data['markDistribution'] = $this->grade_setup_model->getMarkDistribution($id_marks_distribution);
            $data['markDistributionByProgramme'] = $this->grade_setup_model->getMarkDistributionByProgramme($id_programme);
            $data['gradeList'] = $this->grade_setup_model->gradeListByStatus('1');



                // echo "<Pre>";print_r($data['programme']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Mark Distribution';
            $this->loadViews("grade_setup/add", $this->global, $data, NULL);
        }
    }

    function deleteMarksDistribution($id)
    {
        $deleted = $this->grade_setup_model->deleteMarksDistribution($id);
        echo $deleted;exit();
    }
}
