<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Transcript extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transcript_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('transcript.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if($this->input->post())
            {

                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));

                

                $id_course_register = $this->transcript_model->addCoureRegister($master_data);
                
                if($insert_id)
                {
                   $invoice_generated = $this->transcript_model->generateNewMainInvoiceForTranscript($details,$id_student,$id_semester);
                }

                redirect('/registration/transcript/list');
            }




            $data['intakeList'] = $this->transcript_model->intakeList();
            $data['programList'] = $this->transcript_model->programList();


            // echo "<Pre>";print_r($data['transcriptList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Transcript';
            $this->loadViews("transcript/list", $this->global, $data, NULL);
        }
    }


    function view($id_student = NULL)
    {
        if ($this->checkAccess('transcript.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            if ($id_student == null)
            {
                redirect('/examination/transcript/list');
            }

            $data['studentDetails'] = $this->transcript_model->getStudent($id_student);
            // $data['studentDetails'] = $this->remarking_application_model->getCourseRegistrationByStudentId($id_student);

            // echo "<Pre>";print_r($data['studentDetails']);exit();

            $this->global['pageTitle'] = 'Inventory Management : View Transcript';
            $this->loadViews("transcript/view", $this->global, $data, NULL);
        }
    }



    function getIntakes()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->transcript_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control'>";
         // onchange='displaySearch()'
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getCourseRegisteredByProgNIntake()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->transcript_model->getCourseRegisteredByProgNIntake($tempData);
        
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_course_registered_landscape' id='id_course_registered_landscape' class='form-control' onchange='displaySearch()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // echo "<Pre>";print_r($student_list_data[$i]);exit;
        // $id = $results[$i]->id_procurement_category;
        $id_course_registered_landscape = $student_list_data[$i]->id_course_registered_landscape;
        $code = $student_list_data[$i]->code;
        $name = $student_list_data[$i]->name;
        $table.="<option value=".$id_course_registered_landscape.">".$code. " - " . $name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function displayStudentsByIdCourseRegisteredLandscape()
    {
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

         // echo "<Pre>"; print_r($tempData);exit();

        // $id_course_registered_landscape = $tempData['id_course_registered_landscape'];

        $temp_details = $this->transcript_model->getStudentsByCourseLandscapeFromCourseRegistration($tempData);
        // $student_data = $this->transcript_model->getCourseByLandscapeId($id_course_registered_landscape);

        // echo "<Pre>"; print_r($temp_details);exit();


            // $student_name = $student_data->full_name;
            // $student_nric = $student_data->nric;
            // $email = $student_data->email_id;
            // $nric = $student_data->nric;
            // $intake_name = $student_data->intake_name;
            // $id_intake = $student_data->id_intake;
            // $programme_name = $student_data->programme_name;
            // $program_scheme = $student_data->program_scheme;




        $table = "
        <h4 class='sub-title'>Student Transcript Details</h4>




        <div class='custom-table'><table class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Student Name</th>
                    <th>Email Id</th>
                    <th>NRIC</th>
                    <th>Phone No.</th>
                    <th>Gender</th>
                    <th>Program Scheme</th>
                    <th>DOB</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>";
                
        if($temp_details!=NULL)
        {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {


                    // $id_course_registration = $temp_details[$i]->id_course_registration;
                    $id_student = $temp_details[$i]->id_student;
                    $full_name = $temp_details[$i]->full_name;
                    $nric = $temp_details[$i]->nric;
                    $email_id = $temp_details[$i]->email_id;
                    $phone = $temp_details[$i]->phone;
                    $program_scheme = $temp_details[$i]->program_scheme;
                    $gender = $temp_details[$i]->gender;
                    $date_of_birth = $temp_details[$i]->date_of_birth;

                    if($date_of_birth)
                    {
                        $date_of_birth = date('d-m-Y', strtotime($date_of_birth));
                    }



                            // <td>$semester_code - $semester_name</td>
                        $table .= "
                        <tr>
                            <td>$full_name</td>
                            <td>$email_id</td>
                            <td>$nric</td>
                            <td>$phone</td>
                            <td>$gender</td>
                            <td>$program_scheme</td>
                            <td>$date_of_birth</td>
                            <td>
                                <a href='view/$id_student' title='View'>View</a>
                            </td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }



    function getStudentByProgNIntake()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->transcript_model->getStudentByProgNIntake($tempData);


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_student' id='id_student' class='form-control' onchange='displaydata()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // echo "<Pre>";print_r($student_list_data[$i]);exit;
        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $nric = $student_list_data[$i]->nric;
        $table.="<option value=".$id.">".$nric. " - " . $full_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }



    function displaydata()
    {
        
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));
         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->transcript_model->getSemesterRegistrationByStudent($id_intake,$id_programme,$id_student);
        $student_data = $this->transcript_model->getStudentByStudentId($id_student);
        $CummulativeCredits = 0;
           // echo "<Pre>"; print_r($temp_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $program_scheme = $student_data->program_scheme;




        $table = "<h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme</dt>
                                <dd>$program_scheme</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";
     
  $CreditsEarned = 0;
                $gpaEarned = 0;
                  $gpacnt = 0;
                     $gpa = 0;
                     $cnt = 0;

                         $table.= "<div class='custom-table'>
                         <table class='table' id='list-table'>
                <thead>
                  <tr>
                    <th width='15%'>Code</th>
                    <th width='35%'>Course</th>
                    <th width='25%'>Credit</th>
                    <th width='25%'>Grade</th>
                </tr>
                  
                </thead>";

                    for($i=0;$i<count($temp_details);$i++)
                    {


                                            $semester_name = $temp_details[$i]->name;
                    $semester_code = $temp_details[$i]->code;



                $id_semester = $temp_details[$i]->id;
              

        $courseDetails = $this->transcript_model->getCourseRegistrationByStudent($id_intake,$id_programme,$id_student,$id_semester);
                   
                    for($j=0;$j<count($courseDetails);$j++)
                    {

                        $cnt = $cnt+1;

                


                    $course_name = $courseDetails[$j]->course_name;
                    $course_code = $courseDetails[$j]->course_code;
                    $semester_name = $courseDetails[$j]->semester_name;
                    $semester_code = $courseDetails[$j]->semester_code;
                    $start_date = $courseDetails[$j]->start_date;
                    $end_date = $courseDetails[$j]->end_date;
                    $credit_hours = $courseDetails[$j]->credit_hours;
                    $grade = $courseDetails[$j]->grade;


                    $getCPACount = $this->transcript_model->getGPACount($id_intake,$id_programme,$grade);
                    if($getCPACount) {
                        $gpaEarned = $gpaEarned + $getCPACount[0]->point;
                    }
                 

                    $total_course_marks = $courseDetails[$j]->total_course_marks;
                    $total_obtained_marks = $courseDetails[$j]->total_obtained_marks;
                    $total_course_marks = $courseDetails[$j]->total_course_marks;

                    $CreditsEarned = $CreditsEarned + $credit_hours;

                    $CummulativeCredits = $CummulativeCredits + $credit_hours;


                    $semester_session = date('Y-m', strtotime($start_date));

                    if($grade!='')
                    {
                        $gpacnt++;
                    }


                            // <td>$semester_code - $semester_name</td>
                       
                   $table.="
                        <tr>
                            <td>$course_code</td>
                            <td>$course_name</td>
                            <td>$credit_hours</td>
                            <td>$grade</td>
                        </tr>";
                    }
                                        $cgpa = round($CreditsEarned / $cnt,2);
                                        if($gpacnt>0) {
                                        $gpa = round($gpaEarned / $gpacnt,2);
                                    }


                    

                    }

                         $table.="
                        <tr>
                        <td colspan='2'></td>
                            <td><b>Credits Earned : $CreditsEarned</b></td>
                            <td><b>CGPA :$gpa</b></td>
                        </tr>";

                                        $table.= "</tbody></table></div>";



           
        
        echo $table;
    }



    function displaydataold()
    {
        
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));
         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->transcript_model->getSemesterRegistrationByStudent($id_intake,$id_programme,$id_student);
        $student_data = $this->transcript_model->getStudentByStudentId($id_student);
        $CummulativeCredits = 0;
           // echo "<Pre>"; print_r($temp_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $program_scheme = $student_data->program_scheme;




        $table = "<h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme</dt>
                                <dd>$program_scheme</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";
     

                    for($i=0;$i<count($temp_details);$i++)
                    {


                                            $semester_name = $temp_details[$i]->name;
                    $semester_code = $temp_details[$i]->code;



                         $table.= "<div class='custom-table'>
                         <table class='table' id='list-table'>
                <thead>
                  <tr>
                    <th width='15%'>Code</th>
                    <th width='35%'>Course</th>
                    <th width='25%'>Credit</th>
                    <th width='25%'>Grade</th>
                </tr>
                  <tr>
                    <th colspan='4'>Semester: $semester_name </th>
                </tr>
                </thead>";
                $id_semester = $temp_details[$i]->id;
                $CreditsEarned = 0;
                $gpaEarned = 0;

        $courseDetails = $this->transcript_model->getCourseRegistrationByStudent($id_intake,$id_programme,$id_student,$id_semester);
                     $gpacnt = 0;
                     $gpa = 0;
                    for($j=0;$j<count($courseDetails);$j++)
                    {

                        $cnt = $j+1;

                


                    $course_name = $courseDetails[$j]->course_name;
                    $course_code = $courseDetails[$j]->course_code;
                    $semester_name = $courseDetails[$j]->semester_name;
                    $semester_code = $courseDetails[$j]->semester_code;
                    $start_date = $courseDetails[$j]->start_date;
                    $end_date = $courseDetails[$j]->end_date;
                    $credit_hours = $courseDetails[$j]->credit_hours;
                    $grade = $courseDetails[$j]->grade;


                    $getCPACount = $this->transcript_model->getGPACount($id_intake,$id_programme,$grade);
                    if($getCPACount) {
                        $gpaEarned = $gpaEarned + $getCPACount[0]->point;
                    }
                 

                    $total_course_marks = $courseDetails[$j]->total_course_marks;
                    $total_obtained_marks = $courseDetails[$j]->total_obtained_marks;
                    $total_course_marks = $courseDetails[$j]->total_course_marks;

                    $CreditsEarned = $CreditsEarned + $credit_hours;

                    $CummulativeCredits = $CummulativeCredits + $credit_hours;


                    $semester_session = date('Y-m', strtotime($start_date));

                    if($grade!='')
                    {
                        $gpacnt++;
                    }


                            // <td>$semester_code - $semester_name</td>
                       
                   $table.="
                        <tr>
                            <td>$course_code</td>
                            <td>$course_name</td>
                            <td>$credit_hours</td>
                            <td>$grade</td>
                        </tr>";
                    }
                                        $cgpa = round($CreditsEarned / $cnt,2);
                                        if($gpacnt>0) {
                                        $gpa = round($gpaEarned / $gpacnt,2);
                                    }


                     $table.="
                        <tr>
                            <td colspan='2'><b>GPA :$gpa</b></td>
                            <td colspan='2'><b>CGPA : $cgpa</b></td>
                        </tr>";
                         $table.="
                        <tr>
                            <td colspan='2'><b>Credits Earned : $CreditsEarned</b></td>
                            <td colspan='2'><b>Cummulative Credits Earned: $CummulativeCredits</b></td>
                        </tr>";

                                        $table.= "</tbody></table></div>";

                    }


           
        
        echo $table;
    }

    function getIntakesForView()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->transcript_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control' onchange='viewData()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $table.="<option value=".$id.">".$intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function viewData()
    {
        
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));
         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->transcript_model->getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student);
        $student_data = $this->transcript_model->getStudentByStudentId($id_student,$id_intake,$id_programme);

        // echo "<Pre>";print_r($temp_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;



        $table = "

            <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>



            <h4 class='sub-title'>Transcript Details</h4>




        <div class='custom-table'>
        <table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Course Name</th>
                    <th>Course Code</th>
                    <th>Pre-REquisite</th>
                    <th>Total Cr. Hours</th>
                </tr></thead><tbody>";
                
        if($temp_details!=NULL) {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {
                    // $mainId = $temp_details[$i]->id;
                    $name = $temp_details[$i]->name;
                    $name_in_malay = $temp_details[$i]->name_in_malay;
                    $code = $temp_details[$i]->code;
                    $pre_requisite = $temp_details[$i]->pre_requisite;
                    $min_total_cr_hrs = $temp_details[$i]->min_total_cr_hrs;
                    $j=$i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>
                            <td>$code</td>
                            <td>$pre_requisite</td>
                            <td>$min_total_cr_hrs</td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }
}

