<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Award extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('award.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));

            $data['searchParameters'] = $formData;
            $data['awardList'] = $this->award_model->awardListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Award List';
            $this->loadViews("award/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('award.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $starting_serial_alphabet = $this->security->xss_clean($this->input->post('starting_serial_alphabet'));
                $starting_serial_number = $this->security->xss_clean($this->input->post('starting_serial_number'));
                $description = $this->security->xss_clean($this->input->post('description'));

                $level = $this->security->xss_clean($this->input->post('level'));
                $id_certificate_template = $this->security->xss_clean($this->input->post('id_certificate_template'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'level' => $level,
                    'description' => $description,
                    'starting_serial_alphabet' => $starting_serial_alphabet,
                    'starting_serial_number' => $starting_serial_number,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user,
                    'id_certificate_template'=>$id_certificate_template
                );

                $result = $this->award_model->addNewAward($data);
                redirect('/examination/award/list');
            }

                $data['certificate'] = $this->award_model->getCerficateTemplate();

            $this->global['pageTitle'] = 'Inventory Management : Add Award';
            $this->loadViews("award/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('award.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/award/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $starting_serial_alphabet = $this->security->xss_clean($this->input->post('starting_serial_alphabet'));
                $starting_serial_number = $this->security->xss_clean($this->input->post('starting_serial_number'));
                $description = $this->security->xss_clean($this->input->post('description'));

                $level = $this->security->xss_clean($this->input->post('level'));
                $id_certificate_template = $this->security->xss_clean($this->input->post('id_certificate_template'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'level' => $level,
                    'description' => $description,
                    'starting_serial_alphabet' => $starting_serial_alphabet,
                    'starting_serial_number' => $starting_serial_number,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'id_certificate_template'=>$id_certificate_template
                );

                $result = $this->award_model->editAward($data,$id);
                redirect('/examination/award/list');
            }

            $data['id_award'] = $id;
            $data['awardDetails'] = $this->award_model->getAward($id);
            $data['certificate'] = $this->award_model->getCerficateTemplate();

            $this->global['pageTitle'] = 'Inventory Management : Edit Award';
            $this->loadViews("award/edit", $this->global, $data, NULL);
        }
    }


    function condition($id,$id_award_condition = NULL)
    {
        if ($this->checkAccess('award.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/award/list');
            }
            if($this->input->post())
            {
                // echo '<Pre>';print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_condition = $this->security->xss_clean($this->input->post('id_condition'));
            
                $data = array(
                    'id_award' => $id,
                    'id_condition' => $id_condition,
                    'status' => 1,
                    'created_by' => $id_user
                );

                if($id_award_condition > 0)
                {
                    $result = $this->award_model->editAwardCondition($data,$id_award_condition);
                }else
                {
                    $result = $this->award_model->addAwardCondition($data);
                }

                redirect('/examination/award/condition/'.$id);
            }
            
            $data['id_award'] = $id;
            $data['id_award_condition'] = $id_award_condition;
            $data['awardDetails'] = $this->award_model->getAward($id);
            $data['awardConditionList'] = $this->award_model->getAwardConditionList($id);
            $data['awardCondition'] = $this->award_model->getAwardCondition($id_award_condition);
            $data['conditionList'] = $this->award_model->getConditionListByStatus('1');

            // echo '<Pre>';print_r($awardCondition);exit();

            $this->global['pageTitle'] = 'Inventory Management : Edit Award';
            $this->loadViews("award/condition", $this->global, $data, NULL);
        }
    }


    function assesment($id,$id_award_assesment = NULL)
    {
        if ($this->checkAccess('award.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/award/list');
            }
            if($this->input->post())
            {
                // echo '<Pre>';print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_condition = $this->security->xss_clean($this->input->post('id_condition'));
            
                $data = array(
                    'id_award' => $id,
                    'id_condition' => $id_condition,
                    'status' => 1,
                    'created_by' => $id_user
                );

                if($id_award_assesment > 0)
                {
                    $result = $this->award_model->editAwardCondition($data,$id_award_assesment);
                }else
                {
                    $result = $this->award_model->addAwardCondition($data);
                }

                redirect('/examination/award/assesment/'.$id);
            }
            
            $data['id_award'] = $id;
            $data['id_award_assesment'] = $id_award_assesment;
            $data['awardDetails'] = $this->award_model->getAward($id);
            $data['awardConditionList'] = $this->award_model->getAwardConditionList($id);
            $data['awardCondition'] = $this->award_model->getAwardCondition($id_award_assesment);
            $data['conditionList'] = $this->award_model->getConditionListByStatus('1');

            // echo '<Pre>';print_r($awardCondition);exit();

            $this->global['pageTitle'] = 'Inventory Management : Edit Award';
            $this->loadViews("award/assesment", $this->global, $data, NULL);
        }
    }  

    function activities($id,$id_award_assesment = NULL)
    {
        if ($this->checkAccess('award.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/award/list');
            }
            if($this->input->post())
            {
                // echo '<Pre>';print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_condition = $this->security->xss_clean($this->input->post('id_condition'));
            
                $data = array(
                    'id_award' => $id,
                    'id_condition' => $id_condition,
                    'status' => 1,
                    'created_by' => $id_user
                );

                if($id_award_assesment > 0)
                {
                    $result = $this->award_model->editAwardCondition($data,$id_award_assesment);
                }else
                {
                    $result = $this->award_model->addAwardCondition($data);
                }

                redirect('/examination/award/activities/'.$id);
            }
            
            $data['id_award'] = $id;
            $data['id_award_assesment'] = $id_award_assesment;
            $data['awardDetails'] = $this->award_model->getAward($id);
            $data['awardConditionList'] = $this->award_model->getAwardConditionList($id);
            $data['awardCondition'] = $this->award_model->getAwardCondition($id_award_assesment);
            $data['conditionList'] = $this->award_model->getConditionListByStatus('1');

            // echo '<Pre>';print_r($awardCondition);exit();

            $this->global['pageTitle'] = 'Inventory Management : Edit Award';
            $this->loadViews("award/activities", $this->global, $data, NULL);
        }
    }   


    function saveAwardConditionDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo '<Pre>';print_r($tempData);exit();

        $id_award_condition = $this->award_model->addAwardCondition($tempData);
        echo $id_award_condition;exit;
    }

    function deleteAwardConditionDetails($id)
    {
        $edit_data['status'] = 0;
        $deleted = $this->award_model->editAwardCondition($edit_data,$id);
        echo $deleted;exit;
    }

    function getCertificate($id)
    {
        $certificate = $this->award_model->getCertificate($id);

        if($certificate)
        {
            echo $certificate->template_file;exit;
        }
        else
        {
            echo '';exit;
        }
    }
}
