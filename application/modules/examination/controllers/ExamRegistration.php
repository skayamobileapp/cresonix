<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_registration.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['examRegistrationList'] = $this->exam_registration_model->examRegistrationListSearch($formData);
            $data['programList'] = $this->exam_registration_model->programListByStatus('1');
            $data['intakeList'] = $this->exam_registration_model->intakeListByStatus('1');

                // echo "<Pre>";print_r($data['examRegistrationList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Mark Distribution';
            $this->loadViews("exam_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_registration.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit();

                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_programme_landscape = $this->security->xss_clean($this->input->post('id_programme_landscape'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_exam_event = $this->security->xss_clean($this->input->post('id_exam_event'));
                $total_capacity = $this->security->xss_clean($this->input->post('total_capacity'));
                $id_course_registered = $this->security->xss_clean($this->input->post('id_course_registered'));
                $id_course_registered_landscape = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));

                $data = array(
                   'id_intake' => $id_intake,
                   'id_programme' => $id_program,
                   'id_programme_landscape' => $id_programme_landscape,
                    'id_exam_event' => $id_exam_event,
                    'id_course_registered_landscape' => $id_course_registered_landscape,
                    'total_capacity' => $total_capacity,
                    'status' => 1,
                    'created_by' => $user_id
                );

                $result = $this->exam_registration_model->addExamination($data);
                

                redirect('/examination/examRegistration/list');



                // $generated_number = $this->exam_registration_model->getCourseRegistrationById();
                
                
            }
            else
            {
                    // $result = $this->exam_registration_model->deleteTempMarkDistributionBySession($id_session);
            }

            $data['programList'] = $this->exam_registration_model->programListByStatus('1');
            $data['intakeList'] = $this->exam_registration_model->intakeListByStatus('1');
            $data['examEventList'] = $this->exam_registration_model->examEventListByStatus('1');



                // echo "<Pre>";print_r($time_in_12_hour_format);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Exam Registration';
            $this->loadViews("exam_registration/add", $this->global, $data, NULL);
        }
    }

    function tagStudent($id = NULL)
    {
        if ($this->checkAccess('exam_registration.tag_student') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/examRegistration/list');
            }

            
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                // echo "<Pre>";print_r($this->input->post());exit();

                $id_course_registered = $this->security->xss_clean($this->input->post('id_course_registered'));


                    for($i=0;$i<count($id_course_registered);$i++)
                    {

                    $course_registered_details = $this->exam_registration_model->getCourseRegistrationById($id_course_registered[$i]);
                    $examination = $this->exam_registration_model->getExamination($id);

                    // echo "<Pre>";print_r($course_registered_details);exit();

                         $data = array(
                        'id_exam' => $id,
                        'id_student' => $course_registered_details->id_student,
                        'id_programme' => $course_registered_details->id_programme,
                        'id_intake' => $course_registered_details->id_intake,
                        'id_exam_event' => $examination->id_exam_event,
                        'id_course' => $course_registered_details->id_course,
                        'id_course_registered_landscape' => $course_registered_details->id_course_registered_landscape,
                        'total_capacity' => $examination->total_capacity,
                        'id_course_registered' => $id_course_registered[$i],
                        'created_by' => $user_id
                    );
                         // echo "<Pre>";print_r($data);exit();
                    $insert_id = $this->exam_registration_model->addExamRegistration($data);

                        if($insert_id)
                        {
                            $data_update['is_exam_registered'] = $insert_id;
                            $updated_registration = $this->exam_registration_model->updateCourseRegistration($data_update,$id_course_registered[$i]);
                        }

                    }


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->exam_registration_model->editMarkDistribution($data,$id);
                redirect('/examination/examRegistration/tagStudent/'. $id);
            }




            $data['examination'] = $this->exam_registration_model->getExamination($id);
            $data['examRegistrationDetails'] = $this->exam_registration_model->getExamRegistration($id);
            $data['programList'] = $this->exam_registration_model->programListByStatus('1');
            $data['intakeList'] = $this->exam_registration_model->intakeListByStatus('1');
            $data['examEventList'] = $this->exam_registration_model->examEventListByStatus('1');

            $data['courseLandscape'] = $this->exam_registration_model->getCourseForLandscape($data['examination']->id_course_registered_landscape);
            $data['programmeLandscapeList'] = $this->exam_registration_model->programmeLandscapeListByStatus('1');

            $data['id_course_registered_landscape'] = $data['examination']->id_course_registered_landscape;

            // echo "<Pre>"; print_r($data['examRegistrationDetails']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Exam Tagging';
            $this->loadViews("exam_registration/tag_student", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_registration.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/examRegistration/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();
                
                $data = array(
                   'name' => $name,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'status' => $status,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->exam_registration_model->editMarkDistribution($data,$id);
                redirect('/examination/examRegistration/list');
            }




            $data['examination'] = $this->exam_registration_model->getExamination($id);
            $data['examRegistrationDetails'] = $this->exam_registration_model->getExamRegistration($id);
            $data['programList'] = $this->exam_registration_model->programListByStatus('1');
            $data['intakeList'] = $this->exam_registration_model->intakeListByStatus('1');
            $data['examEventList'] = $this->exam_registration_model->examEventListByStatus('1');
            $data['programmeLandscapeList'] = $this->exam_registration_model->programmeLandscapeListByStatus('1');


            

            

            // echo "<Pre>"; print_r($data['examRegistrationDetails']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Exam Registration';
            $this->loadViews("exam_registration/edit", $this->global, $data, NULL);
        }
    }

    function tagList()
    {
        if ($this->checkAccess('exam_registration.tag_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['examRegistrationList'] = $this->exam_registration_model->examRegistrationListSearch($formData);
            $data['programList'] = $this->exam_registration_model->programListByStatus('1');
            $data['intakeList'] = $this->exam_registration_model->intakeListByStatus('1');

                // echo "<Pre>";print_r($data['examRegistrationList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Mark Distribution';
            $this->loadViews("exam_registration/exam_registration_list", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('exam_registration.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['examRegistrationList'] = $this->exam_registration_model->examRegistrationListSearch($formData);
            $data['programList'] = $this->exam_registration_model->programListByStatus('1');
            $data['intakeList'] = $this->exam_registration_model->intakeListByStatus('1');
                // echo "<Pre>";print_r($data['examRegistrationList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Mark Distribution';
            $this->loadViews("exam_registration/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('exam_registration.approve') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/examRegistration/approvallist');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                
                $data = array(
                   'status' => $status,
                    'reason' => $reason,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->exam_registration_model->editMarkDistribution($data,$id);
                redirect('/examination/examRegistration/approvalList');
            }

            $data['courseList'] = $this->exam_registration_model->courseListByStatus('1');
            $data['programList'] = $this->exam_registration_model->programListByStatus('1');
            $data['intakeList'] = $this->exam_registration_model->intakeListByStatus('1');
            $data['componentList'] = $this->exam_registration_model->examComponentListByStatus('1');
            $data['programmeLandscapeList'] = $this->exam_registration_model->programmeLandscapeListByStatus('1');



            $data['examRegistration'] = $this->exam_registration_model->getMarkDistribution($id);
            $data['examRegistrationDetails'] = $this->exam_registration_model->getMarkDistributionDetailsByIdMarkDistribution($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Mark Distribution';
            $this->loadViews("exam_registration/view", $this->global, $data, NULL);
        }
    }

    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->exam_registration_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='getLandscapeListByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getLandscapeListByProgramIdNIntakeId()
    {
        
        $data = $this->security->xss_clean($this->input->post('data'));
         // echo "<Pre>"; print_r($data);exit();
        $id_intake = $data['id_intake'];
        $id_programme = $data['id_programme'];

        $temp_details = $this->exam_registration_model->getLandscapeListByProgramIdNIntakeId($data);

        // echo "<Pre>"; print_r($temp_details);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_programme_landscape' id='id_programme_landscape' class='form-control' onchange='getCoursesByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            $name = $temp_details[$i]->name;
            $program_landscape_type = $temp_details[$i]->program_landscape_type;
            // $code = $temp_details[$i]->code;

            $table.="<option value=".$id.">".$program_landscape_type . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getCoursesByProgramIdNIntakeId()
    {
        
        $data = $this->security->xss_clean($this->input->post('data'));
         // echo "<Pre>"; print_r($data);exit();
        $id_intake = $data['id_intake'];
        $id_programme = $data['id_programme'];
         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->exam_registration_model->getProgramLandscapeCourses($data);

        // echo "<Pre>"; print_r($temp_details);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_course_registered_landscape' id='id_course_registered_landscape' class='form-control' >";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            // $id_course_registered = $temp_details[$i]->id_course_registered;
            $name = $temp_details[$i]->name;
            $code = $temp_details[$i]->code;
            $id_course_registered_landscape = $temp_details[$i]->id_course_registered_landscape;


            $table.="<option value=".$id_course_registered_landscape.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getCourseForLandscape()
    {
        $id_course_registered_landscape = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));

        $course_landscpae = $this->exam_registration_model->getCourseForLandscape($id_course_registered_landscape);
        

        $student_data = $this->exam_registration_model->getStudentsByIdCourses($id_course_registered_landscape);
        // echo "<Pre>"; print_r($student_data);exit();

        $table  = "";
        if(!empty($course_landscpae))
        {

            $pre_requisite = $course_landscpae->pre_requisite;
           $course_type = $course_landscpae->course_type;
           $name = $course_landscpae->name;
           $code = $course_landscpae->code;
           $id_course = $course_landscpae->id_course;




        $table  .= "



             <h4 class='sub-title'>Course Registration Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <input type='hidden' class='form-control' name='id_course' id='id_course' value='$id_course'>
                                <dt>Course Name :</dt>
                                <dd>$name</dd>
                            </dl>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd>$code</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Pre-Requisite :</dt>
                                <dd>
                                    $pre_requisite
                                </dd>
                            </dl>
                            <dl>
                                <dt>Course Type :</dt>
                                <dd>$course_type</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";

            }



        if(!empty($student_data))
        {


                $table .= "
         <br>
         <h4>Students For Exam Registration</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Advisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $id_course_registered = $student_data[$i]->id_course_registered;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>                           
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                          
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$ic_no - $advisor_name</td>                  
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_course_registered[]' name='id_course_registered[]' class='check' value='".$id_course_registered."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";


         }
        else
        {
            echo "<h4 class='sub-title'>No Data Found</h4>";exit;
        }

        print_r($table);exit();
    }






    function searchStudent()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $student_data = $this->exam_registration_model->searchStudent($tempData);
        // echo "<Pre>"; print_r($student_data);exit();

        $table  = "";

        if(!empty($student_data))
        {


                $table .= "
         <br>
         <h4>Students For Exam Registration</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Advisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $id_course_registered = $student_data[$i]->id_course_registered;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>                           
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                          
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$ic_no - $advisor_name</td>                  
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_course_registered[]' name='id_course_registered[]' class='check' value='".$id_course_registered."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";


         }
        else
        {
            echo "<h4 class='sub-title'>No Student Course Registration Found</h4>";exit;
        }

        print_r($table);exit();
    }


    function daleteExamRegistrationData($id_exam_registration)
    {

        $exam_registration = $this->exam_registration_model->getExamRegistrationData($id_exam_registration);

        // print_r($exam_registration);exit();

        $data_update['is_exam_registered'] = 0;
        $updated_registration = $this->exam_registration_model->updateCourseRegistration($data_update,$exam_registration->id_course_registered);
        if($updated_registration)
        {
            $exam_registration = $this->exam_registration_model->deleteExamRegistration($id_exam_registration);
        }

        echo "success";exit;
    }

















    function saveTempDetailData()
    {
        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->exam_registration_model->saveTempDetailData($tempData);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->exam_registration_model->getTempMarkDistributionDetailsBySessionId($id_session);

        // echo "<Pre>";print_r($temp_details);exit;



        if(!empty($temp_details))
        {
            
        $table ="
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Exam Components</th>
                    <th>Pass Compulsary</th>
                    <th>Pass Marks</th>
                    <th>Max. Marks</th>
                    <th>Attendance Status</th>
                    <th class='text-center'>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $id = $temp_details[$i]->id;
                        $is_pass_compulsary = $temp_details[$i]->is_pass_compulsary;
                        $pass_marks = $temp_details[$i]->pass_marks;
                        $max_marks = $temp_details[$i]->max_marks;
                        $attendance_status = $temp_details[$i]->attendance_status;
                        $component_code = $temp_details[$i]->component_code;
                        $component_name = $temp_details[$i]->component_name;

                        if($is_pass_compulsary == 1)
                        {
                            $is_pass_compulsary = 'Yes';
                        }else
                        {
                            $is_pass_compulsary = 'No';
                        }

                        if($attendance_status == 1)
                        {
                            $attendance_status = 'Yes';
                        }else
                        {
                            $attendance_status = 'No';
                        }


                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$component_code - $component_name</td>
                            <td>$is_pass_compulsary</td>
                            <td>$pass_marks</td>
                            <td>$max_marks</td>
                            <td>$attendance_status</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table = "";

        }


        return $table;
    }

    function deleteTempData($id)
    {
        $id_session = $this->session->my_session_id;

        $deleted = $this->exam_registration_model->deleteTempMarkDistribution($id);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    


    function getCentersByLocatioin($id_location)
    {
            $results = $this->exam_registration_model->getCentersByLocatioin($id_location);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function saveDetailData()
    {

        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->exam_registration_model->addMarkDistributionDetails($tempData);

        // $data = $this->displayTempData();

        echo "success";exit();
    }

    function deleteDetailData($id)
    {
        $deleted = $this->exam_registration_model->deleteDetailData($id);
        echo 'success';exit;
    }
}

