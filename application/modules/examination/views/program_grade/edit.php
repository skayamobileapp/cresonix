<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program Grade</h3>
        </div>
        <form id="form_programme_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Program Grade Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programGradeDetails->id_program)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade <span class='error-text'>*</span></label>
                        <select name="id_grade" id="id_grade" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($gradeList))
                            {
                                foreach ($gradeList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programGradeDetails->id_grade)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name . " - " . $record->description;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade Description <span class='error-text'>*</span></label>
                        <input type="text" name="description" id="description" class="form-control" value="<?php echo $programGradeDetails->description; ?>" >
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programGradeDetails->id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programGradeDetails->id_semester)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Maximum Marks</label>
                        <input type="number" name="maxmarks" id="maxmarks" class="form-control" value="<?php echo $programGradeDetails->maxmarks; ?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Rank <span class='error-text'>*</span></label>
                        <input type="text" name="rank" id="rank" class="form-control" value="<?php echo $programGradeDetails->rank; ?>" >
                    </div>
                </div>

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Minimum Marks <span class='error-text'>*</span></label>
                        <input type="number" name="minmarks" id="minmarks" class="form-control" value="<?php echo $programGradeDetails->minmarks; ?>" >
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result <span class='error-text'>*</span></label> <br>
                        <label class="radio-inline">
                          <input type="radio" name="result" id="result" value="pass" <?php if($programGradeDetails->result =='pass'){ echo 'checked';} ?>><span class="check-radio"></span> Pass
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="result" id="result" value="fail" <?php if($programGradeDetails->result =='fail'){ echo 'checked';} ?>><span class="check-radio"></span> Fail
                        </label>
                    </div>
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
     $(document).ready(function() {
        $("#form_programme_grade").validate({
            rules: {
                id_intake: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_grade: {
                    required: true
                },
                 description: {
                    required: true
                },
                 minmarks: {
                    required: true
                },
                 maxmarks: {
                    required: true
                },
                 rank: {
                    required: true
                },
                 result: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Description</p>",
                },
                minmarks: {
                    required: "<p class='error-text'>Enter Min. Marks</p>",
                },
                maxmarks: {
                    required: "<p class='error-text'>Enter Max. Marks</p>",
                },
                rank: {
                    required: "<p class='error-text'>Enter Rank</p>",
                },
                result: {
                    required: "<p class='error-text'>Select Result</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>