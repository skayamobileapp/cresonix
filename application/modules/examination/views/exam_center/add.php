<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Exam Center</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Exam Center Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select State <span class='error-text'>*</span></label>
                        <span id='view_state'></span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>
                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Location <span class='error-text'>*</span></label>
                        <select name="id_location" id="id_location" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($locationList))
                            {
                                foreach ($locationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                
                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>


    function getStateByCountry(id)
    {

        $.get("/registration/examCenter/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }




    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 id_country: {
                    required: true
                },
                 id_state: {
                    required: true
                },
                 city: {
                    required: true
                },
                 zipcode: {
                    required: true
                },
                 address: {
                    required: true
                },
                 status: {
                    required: true
                },
                id_location : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Country required</p>",
                },
                id_state: {
                    required: "<p class='error-text'>State required</p>",
                },
                city: {
                    required: "<p class='error-text'>City required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>