<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Marks Entry For Students</h3>
      <!-- <a href="edit" class="btn btn-primary">+ Add Applicant</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form id="form_search" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control selitemIcon" onchange="getCoursesBySemester(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    </div>


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Intake <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" autocomplete="false" placeholder="Select" id="dummy_course">
                        <span id="view_semester"></span>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" autocomplete="false" placeholder="Select" id="dummy_course">
                        <span id="view_course"></span>
                      </div>
                    </div>

                    
                    

                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="studentList" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

  <?php
    if(!empty($applicantList))
    {

      ?>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Applicant Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>NRIC</th>
            <th>Programme</th>
            <th>Intake</th>
            <th>Applicant Status</th>
            <th class="text-center">ACtion</th>
          </tr>
        </thead>
        <tbody>

          <?php          
            $i=1;
            foreach ($applicantList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo ucwords($record->full_name); ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php echo $record->phone ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->program_code . " - " . $record->program ?></td>
                <td><?php echo $record->intake ?></td>
                <td><?php echo $record->applicant_status ?></td>
                <td class="text-center">
                  <a href="<?php echo 'addMarksEntry/' . $searchParam['id_program'] . '/' . $searchParam['id_intake'] . '/' . $searchParam['id_course'] . '/' . $record->id_course_registration . '/'. $record->id; ?>" title="ADD">Add Marks</a> 
                  <!-- | <a href="/student/profile" target="_blank" >Student Login</a> -->
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          ?>
        </tbody>
      </table>
    </div>

    <?php
    }
    ?>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>


<script type="text/javascript">

    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }



    function getCoursesBySemester(id)
    {
        if(id != '')
        {

        $.get("/examination/remarkingApplication/getCoursesBySemester/"+id,
            function(data, status)
            {
                $("#dummy_course").hide();
                $("#view_semester").html(data);
            }
            );
        }
        else
        {
            $("#view_semester").hide();
            $("#dummy_course").show();
            $("#id_course").val('');
        }
    }


    function getStudentMarkEntryDetailsByIdSemesterNIdCourse()
    {

        var id_semester = $("#id_semester").val();
        var id_course_registered_landscape = $("#id_course").val();
        // alert(id_student);
        if (id_semester != '' && id_course_registered_landscape != '')
        {
            $.ajax(
            {
               url: '/examination/remarkingApplication/getStudentMarkEntryDetailsByIdSemesterNIdCourse',
                type: 'POST',
               data:
               {
                'id_semester': id_semester,
                'id_course_registered_landscape': id_course_registered_landscape,
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#dummy_course").hide();                
                  $("#view_course").html(result);
                
               }
            }); 
        }
        else
        {
            $("#view_course").hide();
            $("#dummy_course").show();
            // $("#id_course").val('');
        }    
    }


     $(document).ready(function() {
        $("#form_search").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_course_registered: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>