<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Student Transcript</h3>

            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
            
        </div>






        <div class="form-container">
            <h4 class="form-group-title"> Student Transcript Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Transcript</a>
                    </li>    
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Partial Transcript</a>
                    </li>                
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">


                            <br>

                            <div id="view">
                            </div>
                   


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">


                            <br>

                            <div id="view_transcript">
                            </div>
                        



                        </div> 
                    </div>


                </div>

            </div>
        </div> 





        





     



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>




    function displaydata()
    {

        var id_intake = <?php echo $studentDetails->id_intake ?>;
        var id_programme = <?php echo $studentDetails->id_program ?>;
        var id_student = <?php echo $studentDetails->id ?>;
        // alert(id_student);

        if(id_student != '')
        {

            $.ajax(
            {
               url: '/examination/transcript/displaydata',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                
                    $("#view").html(result);
                    // $("#view_transcript").html(result);

                
               }
            });

        }        
    }


    function displayDataOld()
    {

        var id_intake = <?php echo $studentDetails->id_intake ?>;
        var id_programme = <?php echo $studentDetails->id_program ?>;
        var id_student = <?php echo $studentDetails->id ?>;
        // alert(id_student);

        if(id_student != '')
        {

            $.ajax(
            {
               url: '/examination/transcript/displaydataold',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                
                    // $("#view").html(result);
                    $("#view_transcript").html(result);

                
               }
            });

        }        
    }




    $(document).ready(function()
    {
        displaydata();
        displayDataOld();
    });
    


$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );

    $('select').select2();

</script>