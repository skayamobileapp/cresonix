<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Rrelease</h3>
        </div>
        <form id="form_release" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Rrelease Details</h4>        
                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Semester <span class='error-text'>*</span></label>
                            <select name="id_semester" id="id_semester" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($semesterList))
                                {
                                    foreach ($semesterList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $release->id_semester)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
        

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Barring Type <span class='error-text'>*</span></label>
                            <select name="id_barring_type" id="id_barring_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($barringTypeList))
                                {
                                    foreach ($barringTypeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $release->id_barring_type)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Student <span class='error-text'>*</span></label>
                            <select name="id_student" id="id_student" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($studentList))
                                {
                                    foreach ($studentList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $release->id_student)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $release->reason; ?>">
                        </div>
                    </div>    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Release Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="release_date" name="release_date" value="<?php echo  date('d-m-Y',strtotime($release->release_date)); ?>" autocomplete="off">
                        </div>
                    </div> 

                    

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($release->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($release->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_release").validate(
        {
            rules:
            {
                id_student:
                {
                    required: true
                },
                id_barring_type:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                release_date:
                {
                    required: true
                }
            },
            messages:
            {
                id_student:
                {
                    required: "Select Student",
                },
                id_barring_type:
                {
                    required: "Select Barring Type",
                },
                id_semester:
                {
                    required: "Select Semester",
                },
                reason:
                {
                    required: "Reason Required",
                },
                release_date:
                {
                    required: "Select Release Date",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
  </script>