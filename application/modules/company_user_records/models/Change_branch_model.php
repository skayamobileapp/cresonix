<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Change_branch_model extends CI_Model
{
    
    function changeBranchList()
    {
        $this->db->select('b.*, stu.full_name as student, pro.name as programme');
        $this->db->from('change_branch as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('programme as pro', 'b.id_programme = pro.id');
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function changeBranchListSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, stu.email_id, pro.name as programme, stu.nric, pro.code as programme_code, inta.name as intake_name, inta.year as intake_year, sem.name as semester_name, sem.code as semester_code, ohts.code as new_branch_code, ohts.name as new_branch_name,  ohts1.code as branch_code, ohts1.name as branch_name');
        $this->db->from('change_branch as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_new_semester = sem.id');
        $this->db->join('programme as pro', 'b.id_programme = pro.id');
        $this->db->join('intake as inta', 'b.id_intake = inta.id');
        $this->db->join('organisation_has_training_center as ohts', 'b.id_new_branch = ohts.id');
        $this->db->join('organisation_has_training_center as ohts1', 'b.id_branch = ohts1.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_programme']))
        {
            $likeCriteria = "(b.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_student']))
        {
            $likeCriteria = "(b.id_student  LIKE '%" . $formData['id_student'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getChangeBranch($id)
    {
        $this->db->select('*');
        $this->db->from('change_branch');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }
    

    function addNewChangeBranch($data)
    {
        $this->db->trans_start();
        $this->db->insert('change_branch', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function deleteChangeBranch($id, $stateInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return $this->db->affected_rows();
    }

    function studentList()
    {
        $this->db->select('*, full_name as name');
        // $this->db->from('student');
        $this->db->from('student');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function changeBranchListForApprovalSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, stu.email_id, pro.name as programme, stu.nric, pro.code as programme_code, inta.name as intake_name, inta.year as intake_year, sem.name as semester_name, sem.code as semester_code, ohts.code as new_branch_code, ohts.name as new_branch_name,  ohts1.code as branch_code, ohts1.name as branch_name');
        $this->db->from('change_branch as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_new_semester = sem.id');
        $this->db->join('programme as pro', 'b.id_programme = pro.id');
        $this->db->join('intake as inta', 'b.id_intake = inta.id');
        $this->db->join('organisation_has_training_center as ohts', 'b.id_new_branch = ohts.id');
        $this->db->join('organisation_has_training_center as ohts1', 'b.id_branch = ohts1.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_programme']))
        {
            $this->db->where('b.id_programme', $formData['id_programme']);
        }
        if (!empty($formData['id_student']))
        {
            $this->db->where('b.id_student', $formData['id_student']);
        }
        if (!empty($formData['id_branch']))
        {
            $this->db->where('b.id_branch', $formData['id_branch']);
        }
        if (!empty($formData['id_new_branch']))
        {
            $this->db->where('b.id_new_branch', $formData['id_new_branch']);
        }
        
        $where_pending = "(b.status  = '0')";
        $this->db->where($where_pending);
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    // function editApplyProgrammeListList($array)
    // {
    //   $status = ['status'=>'1'];
    //   $this->db->where_in('id', $array);
    //   $this->db->update('change_branch', $status);
    //   // $this->db->set('status', $status);
    // }


    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        // echo "<Pre>";print_r($result);exit();
        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.id as id_intake, i.name as intake_name, br.code as branch_code, br.name as branch_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('organisation_has_training_center as br', 's.id_branch = br.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function branchListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('organisation_has_training_center');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFeeByProgrammeNIntake($data)
    {
        $id_programme = $data['id_programme'];
        $id_intake = $data['id_intake'];
        $id_program_scheme = $data['id_program_scheme'];
        // echo "<Pre>";  print_r($data);exit;

        $this->db->select('fs.*, fi.name as fee_setup, fi.code as fee_code, p.name as programme_name, i.name as intake_name');
        $this->db->from('fee_structure as fs');
        $this->db->join('programme as p', 'fs.id_programme = p.id'); 
        $this->db->join('intake as i', 'fs.id_intake = i.id'); 
        $this->db->join('fee_setup as fi', 'fs.id_fee_item = fi.id'); 
        $this->db->where('fs.id_programme', $id_programme);
        $this->db->where('fs.id_intake', $id_intake);
        $this->db->where('fs.id_program_scheme', $id_program_scheme);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByData($data)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id_program', $data['id_programme']);
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.id_branch', $data['id_branch']);
        $this->db->where('s.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function editChangeBranch($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('change_branch', $data);
        return TRUE;
    }

    function editStudent($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function moveChangeBranchToHistory($id_change_branch)
    {
        $id_user = $this->session->userId;

        $change_branch = $this->getChangeBranch($id_change_branch);

        unset($change_branch->id);
        unset($change_branch->created_dt_tm);
        unset($change_branch->created_by);
        unset($change_branch->updated_by);
        unset($change_branch->updated_dt_tm);

        $data = array(
            'id_student' => $change_branch->id_student, 
            'id_change_branch' => $id_change_branch, 
            'id_branch' => $change_branch->id_branch, 
            'id_new_branch' => $change_branch->id_new_branch, 
            'id_program' => $change_branch->id_programme, 
            'id_intake' => $change_branch->id_intake,
            'status' => 1,
            'created_by' => $id_user
        );

        $inserted_id = $this->addNewChangeBranchHistory($data);
        if($inserted_id)
        {
            $id_student = $change_branch->id_student;
            $id_new_branch = $change_branch->id_new_branch;

            $student_data['id_branch'] = $id_new_branch;

            $this->editStudent($student_data,$id_student);
        }
        return $inserted_id;
    }

    function addNewChangeBranchHistory($data)
    {
        $this->db->trans_start();
        $this->db->insert('change_branch_history', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editApplyProgrammeList($data, $id)
    {
        $user_id = $this->session->userId;

        $this->db->where('id', $id);
        $this->db->update('change_branch', $data);

        $data_row = $this->getChangeBranch($id);

        // echo "<Pre>";print_r($data_row);exit();

        $id_student = $data_row->id_student;
        $id_program = $data_row->id_programme;
        $id_intake = $data_row->id_intake;
        $id_program_scheme = $data_row->id_new_program_scheme;

        $invoice_number = $this->generateMainInvoiceNumber();


        $invoice['invoice_number'] = $invoice_number;
        $invoice['type'] = 'Student';
        $invoice['remarks'] = 'Student Apply For Migration / Change Of Scheme';
        $invoice['id_application'] = '0';
        $invoice['id_program'] = $id_program;
        $invoice['id_intake'] = $id_intake;
        $invoice['id_student'] = $id_student;
        $invoice['total_amount'] = '0';
        $invoice['balance_amount'] = '0';
        $invoice['paid_amount'] = '0';
        $invoice['status'] = '1';
        $invoice['created_by'] = $user_id;

        $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);
        $update = $this->editStudentData($id_program_scheme,$id_intake,$id_student);

        if($update)
        {

            $inserted_id = $this->addNewMainInvoice($invoice);
            $total_amount = 0;
            foreach ($fee_structure_data as $fee_structure)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure->id_fee_item,
                        'amount' => $fee_structure->amount,
                        'status' => '1',
                        'created_by' => $user_id
                    );
                $total_amount = $total_amount + $fee_structure->amount;
                $this->addNewMainInvoiceDetails($data);

            }

            // $total_amount = number_format($total_amount, 2, '.', ',');
            // echo "<Pre>";print_r($total_amount);exit;

            $invoice_update['total_amount'] = $total_amount;
            $invoice_update['invoice_total'] = $total_amount;
            $invoice_update['balance_amount'] = $total_amount;
            $invoice_update['paid_amount'] = '0';
            // $invoice_update['inserted_id'] = $inserted_id;
            // echo "<Pre>";print_r($invoice_update);exit;
            $this->editMainInvoice($invoice_update,$inserted_id);
        }
        return TRUE;
    }

    function getFeeStructure($id_programme,$id_intake,$id_program_scheme)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $query = $this->db->get();
        $fee_structure = $query->result();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function editStudentData($id_program_scheme,$id_intake,$id)
    {
        $user_id = $this->session->userId;

        $program_scheme = $this->getProgramScheme($id_program_scheme);

        $data = array(
                    'id_program_scheme' => $id_program_scheme,
                    'program_scheme' => $program_scheme->mode_of_program . " - " . $program_scheme->mode_of_study,
                    'id_intake' => $id_intake,
                    'updated_by' => $user_id
                );

        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function getProgramScheme($id)
    {
        
        $this->db->select('s.*');
        $this->db->from('programme_has_scheme as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getPreviousBranchByProgramId($id_program)
    {
        $this->db->select('DISTINCT(ihs.id_training_center) as id_training_center');
        $this->db->from('organisation_training_center_has_program as ihs');
        $this->db->where('ihs.id_program', $id_program);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $branch = $this->getBranchDetails($result->id_training_center);

            array_push($details, $branch);
        }

        return $details;

    }

    function getBranchesListByProgramId($id_program)
    {
        $this->db->select('DISTINCT(ihs.id_training_center) as id_training_center');
        $this->db->from('organisation_training_center_has_program as ihs');
        $this->db->where('ihs.id_program', $id_program);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $branch = $this->getBranchDetails($result->id_training_center);

            array_push($details, $branch);
        }

        return $details;
        
    }

    function getBranchDetails($id)
    {
        $this->db->select('s.*');
        $this->db->from('organisation_has_training_center as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }




    function getFeeStructureActivityType($type,$trigger,$id_program)
    {
        $this->db->select('s.*');
        $this->db->from('fee_structure_activity as s');
        $this->db->join('activity_details as a', 's.id_activity = a.id');
        $this->db->where('a.name', $type);
        $this->db->where('s.trigger', $trigger);
        $this->db->where('s.id_program', $id_program);
        $this->db->where('s.status', 1);
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoice($data,$id_apply_change_status)
    {
        $user_id = $this->session->userId;


        $id_student = $data['id_student'];
        $add = $data['add'];

        $student_data = $this->getStudent($id_student);

        $nationality = $student_data->nationality;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;

        if($add == 1)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('CHANGE BRANCH','Application Level',$id_program);
        }
        elseif($add == 0)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('CHANGE BRANCH','Approval Level',$id_program);
        }

        if($fee_structure_data)
        {



            $currency = $fee_structure_data->id_currency;
            $invoice_amount = $fee_structure_data->amount_local;


            // if($nationality == 'Malaysian')
            // {
            //     $currency = 'MYR';
            //     $invoice_amount = $fee_structure_data->amount_local;
            // }
            // elseif($nationality == 'Other')
            // {
            //     $currency = 'USD';
            //     $invoice_amount = $fee_structure_data->amount_international;
            // }



            $invoice_number = $this->generateMainInvoiceNumber();


            $invoice['invoice_number'] = $invoice_number;
            $invoice['type'] = 'Student';
            $invoice['remarks'] = 'Student Change Branch';
            $invoice['id_application'] = '0';
            $invoice['id_program'] = $id_program;
            $invoice['id_intake'] = $id_intake;
            $invoice['id_student'] = $id_student;
            $invoice['id_student'] = $id_student;
            $invoice['currency'] = $currency;
            $invoice['total_amount'] = $invoice_amount;
            $invoice['invoice_total'] = $invoice_amount;
            $invoice['balance_amount'] = $invoice_amount;
            $invoice['paid_amount'] = '0';
            $invoice['status'] = '1';
            $invoice['created_by'] = $user_id;

            // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

            
            // $update = $this->editStudentData($id_program_scheme,$id_program,$id_intake,$id_student);
            
            $inserted_id = $this->addNewMainInvoice($invoice);

            if($inserted_id)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure_data->id_fee_setup,
                        'amount' => $invoice_amount,
                        'status' => 1,
                        'quantity' => 1,
                        'price' => $invoice_amount,
                        'id_reference' => $id_apply_change_status,
                        'description' => 'APPLY CHANGE BRANCH',
                        'created_by' => $user_id
                    );

                $this->addNewMainInvoiceDetails($data);
            }
        }
        
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
}
