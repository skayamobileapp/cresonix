<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Award_model extends CI_Model
{
    function awardList()
    {
        $this->db->select('a.*');
        $this->db->from('award as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function awardListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('award as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.description  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_programme']))
        {
            $likeCriteria = "(a.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAward($id)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAward($data)
    {
        $this->db->trans_start();
        $this->db->insert('award', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAward($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('award', $data);
        return TRUE;
    }
}

