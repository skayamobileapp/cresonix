<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Late_registration_model extends CI_Model
{
    function lateRegistrationList()
    {
        $this->db->select('*');
        $this->db->from('late_registration');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function lateRegistrationListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('late_registration');
        if (!empty($search))
        {
            $likeCriteria = "(semester_type  LIKE '%" . $search . "%' or no_of_days  LIKE '%" . $search . "%' or effective_date  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getLateRegistration($id)
    {
        $this->db->select('*');
        $this->db->from('late_registration');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewLateRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('late_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editLateRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('late_registration', $data);
        return TRUE;
    }
}

