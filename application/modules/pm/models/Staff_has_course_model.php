<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class staff_has_course_model extends CI_Model
{
    function staffHasCourseList()
    {
        $this->db->select('shc.*,s.salutation, s.name as staff, c.name as course');
        $this->db->from('staff_has_course as shc');
        $this->db->join('staff as s','shc.id_staff = s.id');
        $this->db->join('course as c','shc.id_course = c.id');
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         //print_r($result);exit();     
         return $result;
    }

    function staffHasCourseListSearch($formData)
    {
        $this->db->select('shc.*,s.salutation, s.name as staff, c.name as course');
        $this->db->from('staff_has_course as shc');
        $this->db->join('staff as s','shc.id_staff = s.id');
        $this->db->join('course as c','shc.id_course = c.id');
        if($formData['id_staff']) {
            $likeCriteria = "(shc.id_staff  LIKE '%" . $formData['id_staff'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_course']) {
            $likeCriteria = "(shc.id_course  LIKE '%" . $formData['id_course'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         //print_r($result);exit();     
         return $result;
    }

    function getstaffHasCourse($id)
    {
        $this->db->select('shc.*,s.salutation, s.name as staff, c.name as course');
        $this->db->from('staff_has_course as shc');
        $this->db->join('staff as s','shc.id_staff = s.id');
        $this->db->join('course as c','shc.id_course = c.id');
        $this->db->where('shc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewstaffHasCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editstaffHasCourse($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('staff_has_course', $data);
        return TRUE;
    }
}