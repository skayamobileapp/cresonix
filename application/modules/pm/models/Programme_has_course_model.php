<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_has_course_model extends CI_Model
{
    function programmeHasCourseList()
    {
        $this->db->select('phc.*, p.name as programme, pl.name as programme_landscape, c.name as course, s.name as semester');
        $this->db->from('programme_has_course as phc');
        $this->db->join('programme as p', 'phc.id_programme = p.id');
        $this->db->join('programme_landscape as pl', 'phc.id_programme_landscape = pl.id');
        $this->db->join('course as c', 'phc.id_course = c.id');
        $this->db->join('semester as s', 'phc.id_semester = s.id');
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function programmeHasCourseListSearch($formData)
    {
        $this->db->select('phc.*, p.name as programme, pl.name as programme_landscape, c.name as course, s.name as semester');
        $this->db->from('programme_has_course as phc');
        $this->db->join('programme as p', 'phc.id_programme = p.id');
        $this->db->join('programme_landscape as pl', 'phc.id_programme_landscape = pl.id');
        $this->db->join('course as c', 'phc.id_course = c.id');
        $this->db->join('semester as s', 'phc.id_semester = s.id');
        if($formData['id_programme_landscape']) {
            $likeCriteria = "(phc.id_programme_landscape  LIKE '%" . $formData['id_programme_landscape'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_programme']) {
            $likeCriteria = "(phc.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }

         if($formData['id_course']) {
            $likeCriteria = "(phc.id_course  LIKE '%" . $formData['id_course'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_semester']) {
            $likeCriteria = "(phc.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function getProgrammeHasCourseDetails($id)
    {
        $this->db->select('phc.*, p.name as programme, pl.name as programme_landscape, c.name as course, s.name as semester');
        $this->db->from('programme_has_course as phc');
        $this->db->join('programme as p', 'phc.id_programme = p.id');
        $this->db->join('programme_landscape as pl', 'phc.id_programme_landscape = pl.id');
        $this->db->join('course as c', 'phc.id_course = c.id');
        $this->db->join('semester as s', 'phc.id_semester = s.id');
        $this->db->where('phc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgrammeHasCourseDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeHasCourseDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_course', $data);
        return TRUE;
    }

    function getProgramLandscapeByProgramId($id_program)
    {
        $this->db->select('pl.*, p.name as program_name, p.code as program_code');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->where('pl.status', '1');
        $this->db->where('pl.id_programme', $id_program);
        $this->db->order_by("pl.name", "ASC");
         $query = $this->db->get();
         
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }
}

