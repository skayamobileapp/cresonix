<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Department extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('department_model');
        $this->load->model('academic_year_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('department.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['departmentDetails'] = $this->department_model->departmentListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Department List';
            //print_r($subjectDetails);exit;
            $this->loadViews("department/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('department.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'status'=>$status
                );
                $result = $this->department_model->addNewDepartment($data);
                redirect('/setup/department/edit/'.$result);
            }
            //print_r($data['stateList']);exit;
            //$this->load->model('department_model');
            $this->global['pageTitle'] = 'Inventory Management : Add Department';
            $this->loadViews("department/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('department.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/department/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'status'=>$status
                );
                
                $result = $this->department_model->editDepartment($data,$id);
                redirect('/setup/department/list');
            }
            $data['departmentDetails'] = $this->department_model->getDepartment($id);
            $data['departmentHasStaff'] = $this->department_model->getDepartmentHasStaff($id);
            $data['staffList'] = $this->department_model->staffListByStatus('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Department';
            $this->loadViews("department/edit", $this->global, $data, NULL);
        }
    }

    function addStaffDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->department_model->addStaffDetails($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteStaffDetails($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->department_model->deleteStaffDetails($id);
        
        echo "Success"; 
    }
}
