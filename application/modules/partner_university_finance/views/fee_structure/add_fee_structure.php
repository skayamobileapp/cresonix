<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
                <h3>Add Fee Structure </h3>
            <a href="<?php echo '../list' ?>" class="btn btn-link"> < Back</a>
        </div>


            
            <!-- <div class="page-title clearfix"> -->
                <!-- <a href="<?php echo '../../show_intake/' . $programme->id; ?>" class="btn btn-link">Back</a> -->
            <!-- </div> -->




          <div class="form-container">
                <h4 class="form-group-title">Fee Structure Main Details</h4> 

            <div class="row">
                  

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Code <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->code; ?>" readonly="readonly">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Name <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->name; ?>" readonly="readonly">
                      </div>
                  </div>    

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Name Optional Language</label>
                          <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $getProgrammeLandscapeLocal->name_optional_language; ?>" readonly="readonly">
                      </div>
                  </div>     



            </div>

            <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $getProgrammeLandscapeLocal->program_code . " - " . $getProgrammeLandscapeLocal->program; ?>" readonly="readonly">
                      </div>
                  </div>



                  <!-- <div class="col-sm-4">
                      <div class="form-group">
                          <label>From Intake</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $intake->year . " - " . $intake->name; ?>" readonly="readonly">
                      </div>
                  </div>





                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Program Scheme</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->scheme_code . " - " . $getProgrammeLandscapeLocal->scheme_name; ?>" readonly="readonly">
                      </div>
                  </div>


            </div>

            <div class="row">


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Learning Mode</label>
                          <input type="text" class="form-control" id="learmning_mode" name="learmning_mode" value="<?php echo $getProgrammeLandscapeLocal->mode_of_program . " - " . $getProgrammeLandscapeLocal->mode_of_study; ?>" readonly="readonly">
                      </div>
                  </div> -->



                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Currency <span class='error-text'>*</span></label>
                          <select name="id_currency" id="id_currency" class="form-control" disabled="true">
                              <option value="">Select</option>
                              <?php
                              if (!empty($currencyList))
                              {
                                  foreach ($currencyList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php
                                        if($record->id == $getProgrammeLandscapeLocal->id_currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name;?>
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                  </div>

                  
            </div>

          </div>


          <!-- <div class="form-container">
                <h4 class="form-group-title">Program Scheme Details</h4> 

            <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Mode Of Program</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $programScheme->mode_of_program; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Mode Of Study</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $programScheme->mode_of_study; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Min. Duration (Months)</label>
                          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $programScheme->min_duration; ?>" readonly="readonly" >
                      </div>
                  </div>
            </div>

            <div class="row">

              <div class="col-sm-4">
                      <div class="form-group">
                          <label>Max. Duration (Months)</label>
                          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $programScheme->max_duration; ?>" readonly="readonly" >
                      </div>
                  </div>


            </div>

          </div>


 -->






















        <br>


        <!-- <div class="page-title clearfix">
            <a href="<?php echo '../copyFeeStructure/' . $id_program_landscape . '/' . $id_programme; ?>" class="btn btn-link">Copy Fee Structure</a>
        </div> -->






          <div class="form-container">
            <h4 class="form-group-title">Fee Structure Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Local Students</a>
                    </li>    
                    
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">International Students</a>
                    </li>  
                    
                    <!-- <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Partner University</a>
                    </li> -->
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">








                        <form id="form_one" action="" method="post">

                          <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (Malaysian) Details</h4> 

                              <div class="row">

                                    <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_item" id="one_id_fee_item" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->code . " - " . $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>


                                      <!-- <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Trigger On <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_structure_trigger" id="one_id_fee_structure_trigger" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($getFeeStructureTriggerList))
                                                  {
                                                      foreach ($getFeeStructureTriggerList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div> -->


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Amount <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="one_amount" name="one_amount">
                                          </div>
                                      </div>

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Currency <span class='error-text'>*</span></label>
                                              <input type="text" class="form-control" id="one_currency" name="one_currency" value="<?php echo $getProgrammeLandscapeLocal->currency_name; ?>" readonly>
                                          </div>
                                      </div>

                                  </div>


                                  <div class="row">
                                
                                      <div class="col-sm-3">
                                          <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                      </div>

                                  </div>

                            </div>


                        </form>


                        <!-- <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Total Amount</label>
                                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $getProgrammeLandscapeLocal->total_amount; ?>" readonly="readonly" >
                              </div>
                          </div>

                        </div> -->


                        <div class="form-container">
                            <h4 class="form-group-title">Fee Structure List</h4> 


                        

                        <div class="custom-table">
                          <table class="table" id="list-table">
                            <thead>
                              <tr>
                                <th>Sl. No</th>
                                <th>Fee Item</th>
                                <th>Tax Applicable</th>
                                <!-- <th>Tax Percentage</th>
                                <th>Tax Amount (If)</th>
                                <th>Amount</th> -->
                                <th>Total Amount</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($feeStructureLocalList))
                              {
                                $i = 1;
                                $total_amount = 0;
                                foreach ($feeStructureLocalList as $record)
                                {
                                  $tax_amount = ($record->amount * 0.01) * $tax_percentage;
                                  $tax_amount = number_format($tax_amount, 2, '.', ',');
                              ?>
                                  <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->fee_structure_code . " - " . $record->fee_structure ?></td>
                                    <td>
                                        <?php
                                        if($record->gst_tax == '1')
                                        {
                                          echo 'Yes';
                                        }else
                                        {
                                          echo 'No';
                                        }
                                        ?>
                                      </td>
                                    <!-- <td><?php echo $tax_percentage ?></td>
                                    <td><?php echo $tax_amount; ?></td> 
                                    <td><?php echo $record->amount ?></td>-->
                                    <td><?php echo $record->amount; ?></td>
                                    <td>
                                      <a onclick="tempDelete(<?php echo $record->id; ?>)" title="Delete">Delete</a>
                                    </td>
                                  </tr>
                              <?php
                              $total_amount = $total_amount + $record->amount;
                              $i++;
                                }
                                 $total_amount = number_format($total_amount, 2, '.', ',');
                                ?>

                                <tr >
                                    <td bgcolor="" colspan="3"></td>
                                    <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                    <td bgcolor="">
                          <input type="hidden" id="local_amount" name="local_amount" value="<?php echo $total_amount; ?>">

                          <b><?php echo $total_amount ?></b></td>
                                    <!-- <td class="text-center"> -->
                                    <td bgcolor=""></td>
                                  </tr>
                                <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>

                      </div>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_two" action="" method="post">


                            <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (International) Details</h4> 

                              <div class="row">

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="two_id_fee_item" id="two_id_fee_item" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->code . " - " . $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>


                                      <!-- <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Trigger On <span class='error-text'>*</span></label>
                                              <select name="two_id_fee_structure_trigger" id="two_id_fee_structure_trigger" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($getFeeStructureTriggerList))
                                                  {
                                                      foreach ($getFeeStructureTriggerList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div> -->


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Amount <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="two_amount" name="two_amount">
                                          </div>
                                      </div>

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Currency <span class='error-text'>*</span></label>
                                              <input type="text" class="form-control" id="two_currency" name="two_currency" value="<?php echo $getProgrammeLandscapeLocal->currency_name; ?>" readonly>
                                          </div>
                                      </div>


                                    </div>


                                  <div class="row">
                                
                                      <div class="col-sm-3">
                                          <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveDataUSD()">Add</button>
                                      </div>
                                  </div>

                            </div>

                           


                        </form>


                       <!--  <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Total Amount</label>
                                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $getProgrammeLandscapeInternational->total_amount; ?>" readonly="readonly" >
                              </div>
                          </div>

                        </div> -->


                        <div class="form-container">
                              <h4 class="form-group-title">Fee Structure List</h4> 


                          

                          <div class="custom-table">
                            <table class="table" id="list-table">
                              <thead>
                                <tr>
                                  <th>Sl. No</th>
                                  <th>Fee Item</th>
                                  <th>Tax Applicable</th>
                                  <!-- <th>Tax Percentage (If)</th>
                                  <th>Tax Amount (If)</th>
                                  <th>Amount</th> -->
                                  <th>Total Amount</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                if (!empty($feeStructureInternationalList))
                                {
                                  $i = 1;
                                  $total_amount = 0;
                                  foreach ($feeStructureInternationalList as $record)
                                  {
                                    $tax_amount = ($record->amount * 0.01) * $tax_percentage;
                                    $tax_amount = number_format($tax_amount, 2, '.', ',');
                                ?>
                                    <tr>
                                      <td><?php echo $i ?></td>
                                      <td><?php echo $record->fee_structure_code . " - " . $record->fee_structure ?></td>
                                      <td>
                                        <?php
                                        if($record->gst_tax == '1')
                                        {
                                          echo 'Yes';
                                        }else
                                        {
                                          echo 'No';
                                        }
                                        ?>
                                      </td>
                                      <!-- <td><?php echo $tax_percentage ?></td>
                                      <td><?php echo $tax_amount; ?></td>
                                      <td><?php echo $record->amount ?></td> -->
                                      <td><?php echo $record->amount ; ?></td>
                                        <td>
                                      <a onclick="tempDelete(<?php echo $record->id; ?>)" title="Edit">Delete</a>
                                      </td>
                                    </tr>
                                <?php
                                $total_amount = $total_amount + $record->amount;
                                $i++;
                                  }
                                   $total_amount = number_format($total_amount, 2, '.', ',');
                                  ?>

                                  <tr >
                                      <td bgcolor="" colspan="3"></td>
                                      <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                      <td bgcolor="">
                          <input type="hidden" id="international_amount" name="international_amount" value="<?php echo $total_amount; ?>">
                                        <b><?php echo $total_amount ?></b></td>
                                      <!-- <td class="text-center"> -->
                                      <td bgcolor=""></td>
                                    </tr>
                                  <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>

                        </div>


                        </div> 
                    </div>

















                    <div role="tabpanel" class="tab-pane" id="tab_three">
                        <div class="col-12">






                          <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (Partner University) Details</h4> 

                            <form id="form_three" action="" method="post">

                              <div class="row">

                                    
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Partner University <span class='error-text'>*</span></label>
                                            <select name="three_id_training_center" id="three_id_training_center" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($partnerUniversityList))
                                                {
                                                    foreach ($partnerUniversityList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->name;?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Fee Type <span class='error-text'>*</span></label>
                                            <select name="three_is_installment" id="three_is_installment" class="form-control" onchange="showInstallments(this.value)">
                                                <option value="">Select</option>
                                                <option value="0">Per Semester</option>
                                                <option value="1">Installment</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3" id="view_fee_item" style="display: none">
                                        <div class="form-group">
                                            <label>Fee Item <span class='error-text'>*</span></label>
                                            <select name="three_id_fee_item" id="three_id_fee_item" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($feeSetupList))
                                                {
                                                    foreach ($feeSetupList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->name;?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3" id="view_amount">
                                        <div class="form-group">
                                            <label>Amount <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="three_amount" name="three_amount">
                                        </div>
                                    </div>


                                  <!-- </div>

                                  <div class="row"> -->

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Currency <span class='error-text'>*</span></label>
                                            <select name="three_currency" id="three_currency" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($currencyList))
                                                {
                                                    foreach ($currencyList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->name;?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>  


                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Trigger / Total Installments <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="three_installments" name="three_installments">
                                        </div>
                                    </div>                                  

                                    
                                
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveDataPartnerUSD()">Add</button>
                                    </div>




                                </div>

                              </form>

                            </div>

                           




                       <!--  <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Total Amount</label>
                                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $getProgrammeLandscapeInternational->total_amount; ?>" readonly="readonly" >
                              </div>
                          </div>

                        </div> -->


                        <div class="form-container">
                              <h4 class="form-group-title">Fee Structure List</h4> 


                          

                          <div class="custom-table">
                            <table class="table" id="list-table">
                              <thead>
                                <tr>
                                  <th>Sl. No</th>
                                  <th>Partner University Code</th>
                                  <th>Partner University Name</th>
                               <!--    <th>Contact Number</th>
                                  <th>Contact Email</th>
                                  <th>Location</th> -->
                                  <th>Fee Item</th>
                                  <th>Fee Type</th>
                                  <th>Trigger / Installments</th>
                                  <th>Currency</th>
                                  <th>Amount</th>
                                  <th class="text-center">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                if (!empty($feeStructurePInternationalList))
                                {
                                  $i = 1;
                                  $total_amount = 0;
                                  foreach ($feeStructurePInternationalList as $record)
                                  {
                                ?>
                                    <tr>
                                      <td><?php echo $i ?></td>
                                      <td><?php echo $record->code ?></td>
                                      <td><?php echo $record->name ?></td>
                                    <!--  <td><?php echo $record->contact_number ?></td>
                                      <td><?php echo $record->email ?></td>
                                      <td><?php echo $record->city ?></td> -->
                                      <td><?php echo $record->fee_code . " - " . $record->fee_name ?></td>
                                      <td><?php
                                      if($record->is_installment == 1)
                                      {
                                        echo 'Installment'; 
                                      }
                                      elseif($record->is_installment == 0)
                                      {
                                        echo 'Per Semester';
                                      }?></td>
                                      <td><?php echo $record->installments ?></td>
                                      <td><?php echo $record->currency_code . " - " . $record->currency ?></td>
                                      <td><?php echo $record->amount ?></td>
                                      <!-- <td><?php echo $record->frequency_code . " - " . $record->frequency_mode ?></td> -->
                                      <!-- <td class="text-center"> -->
                                      <td class="text-center">

                                      <a onclick="deleteDataByTrainingCenter(<?php echo $record->id_fee_structure ; ?>)" title="Delete">Delete</a>

                                      <?php if($record->is_installment == 1)
                                      {
                                        ?>
                                      |
                                      
                                      <a onclick="viewFeeByTrainingCenter(<?php echo $record->id_fee_structure; ?>)" title="View">View</a>

                                      <?php
                                      }
                                      ?>                                      
                                      </td>
                                    </tr>
                                <?php
                               
                                $i++;
                                  }
                                  ?>

                                 <!--  <tr >
                                      <td bgcolor="" colspan="3"></td>
                                      <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                      <td bgcolor=""><b><?php echo $total_amount ?></b></td>
                                      <td bgcolor=""></td>
                                    </tr> -->
                                  <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>

                        </div>


                        </div> 
                    </div>


                </div>

            </div>
        </div> 








































        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form id="form_four" action="" method="post">


                  <div class="form-container">
                    <h4 class="form-group-title"> Installment Details</h4>

                    <div class="row">



                        <input type="hidden" class="form-control" id="trigger_id_fee_structure" name="trigger_id_fee_structure" readonly>

                        <input type="hidden" class="form-control" id="trigger_id_training_center" name="trigger_id_training_center" readonly>
                        <input type="hidden" class="form-control" id="trigger_id_program_landscape" name="trigger_id_program_landscape" readonly>






                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Installment Trigger On
                                  <!-- Hided This Due To Change In Flow As 2 Fields Required type and Total -->
                                 <!-- <?php echo $getProgrammeLandscapeLocal->program_landscape_type; ?> -->
                                 <span class='error-text'>*</span></label>
                                <select name="trigger_id_semester" id="trigger_id_semester" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        for ($i=1; $i <= 8
                                          // $getProgrammeLandscapeLocal->total_semester
                                           ; $i++)
                                        {?>
                                    <option value="<?php echo $i;  ?>">
                                        <?php echo $i;?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                              <div class="form-group">
                                  <label>Fee Item <span class='error-text'>*</span></label>
                                  <select name="trigger_id_fee_item" id="trigger_id_fee_item" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($feeSetupList))
                                      {
                                          foreach ($feeSetupList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              ><?php echo $record->code . " - " . $record->name;?>
                                              </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>




                          <div class="col-sm-3">
                              <div class="form-group">
                                  <label>Trigger Fee On <span class='error-text'>*</span></label>
                                  <select name="id_fee_structure_trigger" id="id_fee_structure_trigger" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($getFeeStructureTriggerList))
                                      {
                                          foreach ($getFeeStructureTriggerList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              ><?php echo $record->name;?>
                                              </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>





                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Amount <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_amount" name="triggering_amount">
                            </div>
                        </div>


                      </div>



                      <div class="row">



                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Total Installment <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_installment_nos" name="triggering_installment_nos" readonly>
                            </div>
                        </div>


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Currency <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="triggering_data_currency" name="triggering_data_currency" readonly>
                            </div>
                        </div>
                        

                    </div>


                  </div>
              
              </form>

              <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="saveInstallmentData()">Add</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>








                <div class="form-container">
                        
                <div class="row">
                    <div id='view_model'>
                    </div>
                </div>



                </div>

            </div>
            </div>

          </div>
        </div>












        

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    updateMasterAmount();

    function updateMasterAmount()
    {
        var tempPR = {};
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";

        tempPR['id'] = id_program_landscape;
        tempPR['amount'] = $("#local_amount").val();
        tempPR['international_amount'] = $("#international_amount").val();
        
        // alert(tempPR['international_amount']);

            $.ajax(
            {
               url: '/partner_university_finance/feeStructure/updateFeeStructureMasterAmount',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // window.location.reload();
               }
            });
      }

  

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id_frequency_mode").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }



    function showInstallments(is_installment)
    {
      // alert(is_installment);
      if(is_installment == 0)
      {
        $('#view_amount').show();
        $('#view_fee_item').show();
      }else
      {

        $('#view_amount').hide();
        $('#view_fee_item').hide();
      }
    }



    function saveData()
    {

      if($('#form_one').valid())
      {

        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_training_center'] = <?php echo '1'; ?>;
        tempPR['id_fee_item'] = $("#one_id_fee_item").val();
        tempPR['id_fee_structure_trigger'] = $("#one_id_fee_structure_trigger").val();
        tempPR['currency'] = 'MYR';
        tempPR['amount'] = $("#one_amount").val();
        tempPR['id_frequency_mode'] = $("#one_id_frequency_mode").val();

            $.ajax(
            {
               url: '/partner_university_finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                window.location.reload();
               }
            });

      }
        
    }


    function saveDataUSD()
    {

      if($('#form_two').valid())
        {


        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_training_center'] = <?php echo '1'; ?>;
        tempPR['id_fee_item'] = $("#two_id_fee_item").val();
        tempPR['id_fee_structure_trigger'] = $("#two_id_fee_structure_trigger").val();
        tempPR['currency'] = 'USD';
        tempPR['amount'] = $("#two_amount").val();
        tempPR['id_frequency_mode'] = $("#two_id_frequency_mode").val();
            $.ajax(
            {
               url: '/partner_university_finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                window.location.reload();
               }
            });

          }
        
    }



    function saveDataPartnerUSD()
    {

      if($('#form_three').valid())
        {


        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_fee_item'] = $("#three_id_fee_item").val();
        tempPR['currency'] = $("#three_currency").val();
        tempPR['amount'] = $("#three_amount").val();
        tempPR['id_frequency_mode'] = $("#three_id_frequency_mode").val();
        tempPR['id_training_center'] = $("#three_id_training_center").val();
        tempPR['is_installment'] = $("#three_is_installment").val();
        tempPR['installments'] = $("#three_installments").val();



        // alert(tempPR['three_id_fee_item']);
            $.ajax(
            {
               url: '/partner_university_finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == 0)
                {
                  alert('Duplicate Training Center Data Not Allowed');
                }
                else
                {
                  window.location.reload();
                }
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });

          }
    }

    function viewFeeByTrainingCenter(id_fee_structure)
    {
      // alert(id_fee_structure);
      var tempPR = {};

        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        // tempPR['id_training_center'] = id_training_center;
        tempPR['id_fee_structure'] = id_fee_structure;
        tempPR['id_program_landscape'] = id_program_landscape;

        // alert(tempPR['three_id_fee_item']);
            $.ajax(
            {
               url: '/partner_university_finance/feeStructure/getTrainingCenterFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // if(result == 0)
                // {
                //   alert('Duplicate Training Center Data Not Allowed');
                // }
                // alert(result);

                $("#view_model").html(result);

                var installment_amount_selected = $("#installment_amount_selected").val();
                var installment_nos = $("#installment_nos").val();
                var id_fee_structure = $("#id_fee_structure").val();     
                var id_training_center = $("#id_data_training_center").val();   
                var id_program_landscape = $("#id_data_program_landscape").val();
                var data_currency = $("#data_currency").val();







                $("#triggering_amount").val(installment_amount_selected);
                $("#triggering_installment_nos").val(installment_nos);
                $("#trigger_id_fee_structure").val(id_fee_structure);
                $("#trigger_id_training_center").val(id_training_center);
                $("#trigger_id_program_landscape").val(id_program_landscape);
                $("#triggering_data_currency").val(data_currency);

                $('#myModal').modal('show');
                // window.location.reload();
               }
            });
    }


    function saveInstallmentData()
    {

      if($('#form_four').valid())
        {
          var tempPR = {};

          tempPR['id_fee_item'] = $("#trigger_id_fee_item").val();
          tempPR['id_fee_structure_trigger'] = $("#id_fee_structure_trigger").val();
          tempPR['id_fee_structure'] = $("#trigger_id_fee_structure").val();
          tempPR['amount'] = $("#triggering_amount").val();
          tempPR['id_training_center'] = $("#trigger_id_training_center").val();
          tempPR['id_semester'] = $("#trigger_id_semester").val();
          tempPR['id_program_landscape'] = $("#trigger_id_program_landscape").val();

            $.ajax(
            {
               url: '/partner_university_finance/feeStructure/saveInstallmentData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
          }
    }




    function tempDelete(id)
    {
      // alert(id);
         $.ajax(
            {
               url: '/partner_university_finance/feeStructure/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

    function deleteDataByTrainingCenter(id_training_center)
    {
        $.ajax(
            {
               url: '/partner_university_finance/feeStructure/deleteTrainingCenterByTrainingCenterId/'+id_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

    function deleteSemesterDataByIdFeeStructureTrainingCenter(id_fee_training_center)
    {
      $.ajax(
            {
               url: '/partner_university_finance/feeStructure/deleteSemesterDataByIdFeeStructureTrainingCenter/'+id_fee_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

 //    function tempDelete(id){

 //     $.get("/finance/feeStructure/tempDelete/"+id, function(data, status){
   
 //        // $("#selectsubcategory").html(data);
 //    });
 // }


    function getTempData(id) {
        $.ajax(
            {
               url: '/partner_university_finance/feeStructure/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    $(document).ready(function()
    {
        $("#form_one").validate({
            rules: {
                one_id_fee_item: {
                    required: true
                },
                one_amount: {
                    required: true
                },
                one_id_frequency_mode: {
                    required: true
                },
                one_id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                one_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                one_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                one_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                one_id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_two").validate({
            rules: {
                two_id_fee_item: {
                    required: true
                },
                two_amount: {
                    required: true
                },
                two_id_frequency_mode: {
                    required: true
                },
                two_id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                two_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                two_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                two_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                two_id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
    {

        $("#form_three").validate({
            rules: {
                three_id_fee_item: {
                    required: true
                },
                three_amount: {
                    required: true
                },
                three_id_frequency_mode: {
                    required: true
                },
                three_id_training_center: {
                    required: true
                },
                three_is_installment: {
                    required: true
                },
                three_currency: {
                    required: true
                },
                three_installments: {
                  required: true
                },
                id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                three_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                three_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                three_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                three_id_training_center: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                three_is_installment: {
                    required: "<p class='error-text'>Select Installment Status</p>",
                },
                three_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                three_installments: {
                    required: "<p class='error-text'>Installments Required</p>",
                },
                id_fee_structure_trigger: {
                    required: "<p class='error-text'>Installments Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });








    $(document).ready(function()
    {
        $("#form_four").validate({
            rules: {
                trigger_id_semester: {
                    required: true
                },
                trigger_id_fee_item: {
                  required: true
                }
            },
            messages: {
                trigger_id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                trigger_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    

</script>
