<?php $this->load->helper("form");?>
<form id="form_performa_invoice" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

            <div class="page-title clearfix">
                <h3>Fee Structure</h3>
                <a href="../../list" class="btn btn-link btn-back">‹ Back</a>
            </div>


            <div class="page-title clearfix">
                <h4>Programme Details</h4>
            </div>

            <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->name; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme Code</label>
                          <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->code; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Name Optional Language</label>
                          <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
                      </div>
                  </div>
              </div>

              <div class="page-title clearfix">
                <h4>Intake Details</h4>
            </div>

            <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Intake Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $intake->name; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Year</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $intake->year; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Start Date</label>
                          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo date("d-m-Y", strtotime($intake->start_date)); ?>" readonly="readonly" >
                      </div>
                  </div>
              </div>


            <h3>Add Fee Items</h3>
            <button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>
            <div class="row">
                <div id="view"></div>
            </div>

            <div class="custom-table">
              <table class="table" id="list-table">
                <thead>
                  <tr>
                    <th>Fee Item</th>
                    <th>Frequency Mode</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
if (!empty($feeStructureDetails)) {
	foreach ($feeStructureDetails as $record) {
		?>
                      <tr>
                        <td><?php echo $record->fee_structure ?></td>
                        <td><?php echo $record->frequency_mode ?></td>
                        <td><?php echo $record->amount ?></td>
                      </tr>
                  <?php
}
}
?>
                </tbody>
              </table>
            </div>



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../../list" class="btn btn-link">Cancel</a>
                </div>
            </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Fee Structure</h4>
      </div>
      <div class="modal-body">
         <h4></h4>


            <input type="text" class="form-control" id="id" name="id">
             <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Item *</label>
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                                <option value="">Select</option>
                                <?php
if (!empty($feeSetupList)) {
	foreach ($feeSetupList as $record) {?>
                                        <option value="<?php echo $record->id; ?>"
                                        ><?php echo $record->name; ?>
                                        </option>
                                <?php
}
}
?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount *</label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Frequency Mode *</label>
                            <select name="id_frequency_mode" id="id_frequency_mode" class="form-control">
                                <option value="">Select</option>
                                <?php
if (!empty($frequencyModeList)) {
	foreach ($frequencyModeList as $record) {?>
                                        <option value="<?php echo $record->id; ?>"
                                        ><?php echo $record->name; ?>
                                        </option>
                                <?php
}
}
?>
                            </select>
                        </div>
                    </div>
                </div>


             <div id="view">
             <div>


      </div>
      <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="saveData()">Add</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</form>
<script>

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id_frequency_mode").val('');
        $("#id").val('0');
        $('#myModal').modal('show');

    }
    function saveData()
    {


        var tempPR = {};
        var id_fee_setup = "<?php echo $feeStructure->id; ?>";
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        tempPR['id_frequency_mode'] = $("#id_frequency_mode").val();
        tempPR['id'] = $("#id").val();
        tempPR['id_fee_setup'] = id_fee_setup;
            $.ajax(
            {
               url: '/partner_university_finance/feeStructure/tempaddExisting',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                $('#myModal').modal('hide');
               }
            });

    }

    function deleteTempData(id)
    {
         $.ajax(
            {
               url: '/partner_university_finance/feeStructure/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }
</script>
