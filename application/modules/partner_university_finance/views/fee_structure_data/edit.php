<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Fee Structure </h3>
        </div>





            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Fee Structure Details</a>
                    </li>

                    <li role="presentation"><a href="#program_fee_details" class="nav-link border rounded text-center"
                            aria-controls="program_fee_details" role="tab" data-toggle="tab">Fee Details</a>
                    </li>

                    <li role="presentation"><a href="#program_mode" class="nav-link border rounded text-center"
                            aria-controls="program_mode" role="tab" data-toggle="tab">Programme Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Program Scheme</a>
                    </li>

                    
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">




            <form id="form_programme" action="" method="post">

                <div class="form-container">
                <h4 class="form-group-title">Fee Structure Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Code <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="code" name="code" value="<?php echo $feeStructure->code; ?>">
                            </div>
                        </div>


                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $feeStructure->name; ?>">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Debt Account Code <span class='error-text'>*</span> </label>
                                <input type="text" class="form-control" id="debt_account_code" name="debt_account_code" value="<?php echo $feeStructure->debt_account_code; ?>">
                            </div>
                        </div>

                        
                    </div>

                     <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Select Intake <span class='error-text'>*</span></label>
                                <select name="id_intake" id="id_intake" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($intakeList))
                                    {
                                        foreach ($intakeList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                                <?php 
                                                if($feeStructure->id_intake == $record->id)
                                                {
                                                    echo "selected";
                                                }
                                                ?>

                                            ><?php echo $record->name;?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Select Currency <span class='error-text'>*</span></label>
                                <select name="currency" id="currency" class="form-control">
                                    <option value="">Select</option>
                                    <option value="MYR"
                                    <?php 
                                    if($feeStructure->currency == 'MYR')
                                    {
                                        echo "selected";
                                    }
                                    ?>
                                    >MYR</option>
                                    <option value="USD"
                                    <?php
                                    if($feeStructure->currency == 'USD')
                                    {
                                        echo "selected";
                                    }
                                    ?>
                                    >USD</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>


                


            


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>


            </form>


        <!-- </div> -->
                        



                        </div> 
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="program_fee_details">
                        <div class="mt-4">


                        <form id="form_fee_details" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title">Fee Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Fee Item <span class='error-text'>*</span></label>
                                            <select name="id_fee_item" id="id_fee_item" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($feeItemList))
                                                {
                                                    foreach ($feeItemList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Amount <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="fee_amount" name="fee_amount">
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveFeeDetails()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                              


                            </form>



                            <?php

                            if(!empty($getFeeStructureDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Fee Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Fee Item</th>
                                                 <th>Frequency Mode</th>
                                                 <th>Amount</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getFeeStructureDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getFeeStructureDetails[$i]->fee_code . " - " . $getFeeStructureDetails[$i]->fee_name;?></td>
                                                <td><?php echo $getFeeStructureDetails[$i]->frequency_mode_code . " - " . $getFeeStructureDetails[$i]->frequency_mode_name;?></td>
                                                <td><?php echo $getFeeStructureDetails[$i]->amount;?></td>
                                                <td>
                                                <a onclick="deleteFeeSetupDetails(<?php echo $getFeeStructureDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div>
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_mode">
                        <div class="mt-4">








                        <form id="form_program_details" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Programme Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program <span class='error-text'>*</span></label>
                                            <select name="id_program" id="id_program" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programList))
                                                {
                                                    foreach ($programList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveProgramData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                            </form>



                            <?php

                            if(!empty($getFeeStructureProgramDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Program Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Program</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getFeeStructureProgramDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getFeeStructureProgramDetails[$i]->program_code . " - " . $getFeeStructureProgramDetails[$i]->program_name;?></td>
                                                <td>
                                                <a onclick="deleteProgramDetails(<?php echo $getFeeStructureProgramDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>








                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                        <form id="form_scheme" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Scheme Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Scheme <span class='error-text'>*</span></label>
                                            <select name="id_scheme" id="id_scheme" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($schemeList))
                                                {
                                                    foreach ($schemeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->description; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveSchemeData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                            </form>



                            <?php

                            if(!empty($getSchemeDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Scheme Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Scheme</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getSchemeDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getSchemeDetails[$i]->scheme_code . " - " . $getSchemeDetails[$i]->scheme_name;?></td>
                                                <td>
                                                <a onclick="deleteSchemeDetails(<?php echo $getSchemeDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>




                    




                </div>


            </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    showPartner();
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>

<script>
    function showPartner()
    {
        var value = $("#internal_external").val();
        if(value=='Internal') {
             $("#partnerdropdown").hide();

        } else if(value=='External') {
             $("#partnerdropdown").show();

        }
    }

    function saveProgramData()
    {
        if($('#form_program_details').valid())
        {

        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_fee_structure'] = <?php echo $feeStructure->id;?>;
            $.ajax(
            {
               url: '/finance/feeStructureData/saveProgramDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }





    function deleteProgramDetails(id)
    {
        $.ajax(
            {
               url: '/finance/feeStructureData/deleteProgramDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }

    function saveSchemeData()
    {
        if($('#form_scheme').valid())
        {

        var tempPR = {};
        tempPR['id_scheme'] = $("#id_scheme").val();
        tempPR['id_fee_structure'] = <?php echo $feeStructure->id;?>;
            $.ajax(
            {
               url: '/finance/feeStructureData/saveSchemeDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
               }
            });
        }
    }


    function deleteSchemeDetails(id)
    {
        $.ajax(
            {
               url: '/finance/feeStructureData/deleteSchemeDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }

    function saveFeeDetails()
    {
        if($('#form_fee_details').valid())
        {

        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#fee_amount").val();
        tempPR['id_fee_structure'] = <?php echo $feeStructure->id;?>;
            $.ajax(
            {
               url: '/finance/feeStructureData/saveFeeSetupDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
               }
            });
        }
    }


    function deleteFeeSetupDetails(id)
    {
        $.ajax(
            {
               url: '/finance/feeStructureData/deleteFeeSetupDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }




    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                debt_account_code: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                currency: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                debt_account_code: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                currency: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


     


    $(document).ready(function()
    {
        $("#form_program_details").validate({
            rules: {
                id_program: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function()
    {
        $("#form_scheme").validate({
            rules: {
                id_scheme: {
                    required: true
                }
            },
            messages: {
                id_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function()
    {
        $("#form_fee_details").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                fee_amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                fee_amount: {
                    required: "<p class='error-text'>Fee Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    

</script>