<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CurrencyRateSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('currency_rate_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('currency_rate_setup.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['currencyList'] = $this->currency_rate_setup_model->currencyListSearch($name);

            $this->global['pageTitle'] = 'Inventory Management : Currency Rate Setup List';
            $this->loadViews("currency_rate_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('currency_rate_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;   

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $prefix = $this->security->xss_clean($this->input->post('prefix'));
                $suffix = $this->security->xss_clean($this->input->post('suffix'));
                $decimal_place = $this->security->xss_clean($this->input->post('decimal_place'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'prefix' => $prefix,
                    'suffix' => $suffix,
                    'decimal_place' => $decimal_place,
                    'status' => $status,
                    'created_by' => $user_id
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->currency_rate_setup_model->addNewCurrencyRateSetup($data);
                redirect('/finance/currencyRateSetup/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Currency Rate Setup';
            $this->loadViews("currency_rate_setup/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('currency_rate_setup.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId; 

            if ($id == null)
            {
                redirect('/finance/currencyRateSetup/list');
            }
            if($this->input->post())
            {
                $exchange_rate = $this->security->xss_clean($this->input->post('exchange_rate'));
                $min_rate = $this->security->xss_clean($this->input->post('min_rate'));
                $max_rate = $this->security->xss_clean($this->input->post('max_rate'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));

            
                $data = array(
                    'id_currency' => $id,
                    'exchange_rate' => $exchange_rate,
                    'min_rate' => $min_rate,
                    'max_rate' => $max_rate,
                    'effective_date' => date('Y-m-d', strtotime($effective_date)),
                    'status' => 1,
                    'created_by' => $user_id
                );

                $result = $this->currency_rate_setup_model->addCurrencyRateSetup($data);
                redirect('/finance/currencyRateSetup/edit/'.$id);
            }

            $data['currency'] = $this->currency_rate_setup_model->getCurrencySetup($id);
            $data['currencyRateSetup'] = $this->currency_rate_setup_model->getCurrencyRateSetup($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Currency Rate Setup';
            $this->loadViews("currency_rate_setup/edit", $this->global, $data, NULL);
        }
    }

    function deleteRateSetup($id)
    {
        $result = $this->currency_rate_setup_model->deleteRateSetup($id);

        echo "success";exit;

    }
}
