<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeStructureData extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_structure_data_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('fee_structure.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $data['fee_structure_dataList'] = $this->fee_structure_data_model->fee_structure_dataList();

            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['feeStructureList'] = $this->fee_structure_data_model->feeStructureListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Program List';
            $this->loadViews("fee_structure_data/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('fee_structure.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $debt_account_code = $this->security->xss_clean($this->input->post('debt_account_code'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                
                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'debt_account_code' => $debt_account_code,
                    'id_intake' => $id_intake,
                    'currency' => $currency,
                    'created_by' => $id_user
                );

                $inserted_id = $this->fee_structure_data_model->addNewFeeStructureMain($data);
                redirect('/finance/feeStructureData/edit/'. $inserted_id);
            }

            $data['intakeList'] = $this->fee_structure_data_model->intakeListByStatus('1');
            // $data['programList'] = $this->fee_structure_data_model->programListByStatus('1');            

            // echo "<Pre>";print_r($data['programList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Programme';
            $this->loadViews("fee_structure_data/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('fee_structure.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeStructureData/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $debt_account_code = $this->security->xss_clean($this->input->post('debt_account_code'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                
                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'debt_account_code' => $debt_account_code,
                    'id_intake' => $id_intake,
                    'currency' => $currency,
                    'created_by' => $id_user
                );

                // echo "<Pre>";print_r($data);exit;
                $result = $this->fee_structure_data_model->editFeeStructure($data,$id); 
                redirect('/finance/feeStructureData/list');
            }
            
            $data['feeStructure'] = $this->fee_structure_data_model->getFeeStructureMain($id);
            $data['getFeeStructureProgramDetails'] = $this->fee_structure_data_model->getFeeStructureProgramDetails($id);
            $data['getFeeStructureDetails'] = $this->fee_structure_data_model->getFeeStructureFeeItemDetails($id);
            $data['getSchemeDetails'] = $this->fee_structure_data_model->getSchemeDetails($id);




            $data['intakeList'] = $this->fee_structure_data_model->intakeListByStatus('1');
            $data['programList'] = $this->fee_structure_data_model->programListByStatus('1');  
            $data['programList'] = $this->fee_structure_data_model->programListByStatus('1');  
            $data['schemeList'] = $this->fee_structure_data_model->schemeListByStatus('1');
            $data['feeItemList'] = $this->fee_structure_data_model->feeItemListByStatus('1');


            // $data['feeStructure'] = $this->fee_structure_data_model->getProgrammeHasCourse($id);
            // $data['awardList'] = $this->award_model->awardList();
            // $data['programTypeList'] = $this->fee_structure_data_model->programTypeListByStatus('1');

            // $data['partnerList'] = $this->fee_structure_data_model->partnerUniversityListSearch();

            // $data['fee_structure_dataDetails'] = $this->fee_structure_data_model->getProgrammeDetails($id);
            // echo "<Pre>";print_r($data['fee_structure_dataDetails']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Edit Programme';
            $this->loadViews("fee_structure_data/edit", $this->global, $data, NULL);
        }
    }

    function saveProgramDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->fee_structure_data_model->saveProgramDetails($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteProgramDetails($id_details)
    {
        $inserted_id = $this->fee_structure_data_model->deleteProgramDetails($id_details);
        
        echo "Success"; 
    }



    function saveSchemeDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->fee_structure_data_model->saveSchemeDetails($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteSchemeDetails($id_details)
    {
        $inserted_id = $this->fee_structure_data_model->deleteSchemeDetails($id_details);
        
        echo "Success"; 
    }



    function saveFeeSetupDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->fee_structure_data_model->saveFeeSetupDetails($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteFeeSetupDetails($id_details)
    {
        $inserted_id = $this->fee_structure_data_model->deleteFeeSetupDetails($id_details);
        
        echo "Success"; 
    }


}
