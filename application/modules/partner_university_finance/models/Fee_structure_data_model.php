<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_structure_data_model extends CI_Model
{
    function feeStructureList()
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function feeStructureListSearch($search)
    {
        $this->db->select('fsm.*, i.name as intake_name');
        $this->db->from('fee_structure_main as fsm');
        $this->db->join('intake as i', 'fsm.id_intake = i.id');
        if (!empty($search))
        {
            $likeCriteria = "(fsm.name  LIKE '%" . $search . "%' or fsm.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFeeStructureMain($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_main');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureProgramDetails($id)
    {
        $this->db->select('fshp.*, p.code as program_code, p.name as program_name');
        $this->db->from('fee_structure_has_program as fshp');
        $this->db->join('programme as p', 'fshp.id_program = p.id');
        $this->db->where('fshp.id_fee_structure', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getFeeStructureFeeItemDetails($id)
    {
    	$this->db->select('fshp.*, p.code as fee_code, p.name as fee_name, fm.code as frequency_mode_code, fm.name as frequency_mode_name');
        $this->db->from('fee_structure_details as fshp');
        $this->db->join('fee_setup as p', 'fshp.id_fee_item = p.id');
        $this->db->join('frequency_mode as fm', 'p.id_frequency_mode = fm.id');
        $this->db->where('fshp.id_fee_structure', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getSchemeDetails($id)
    {
    	$this->db->select('fshp.*, p.code as scheme_code, p.description as scheme_name');
        $this->db->from('fee_structure_has_scheme as fshp');
        $this->db->join('scheme as p', 'fshp.id_scheme = p.id');
        $this->db->where('fshp.id_fee_structure', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewFeeStructureMain($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_main', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editFeeStructure($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_structure_main', $data);
        return TRUE;
    }

    function intakeListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function schemeListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('scheme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function feeItemListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('fee_setup');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function programListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function editCurrencyNot($data, $id)
    {
        $this->db->where('id !=', $id);
        $this->db->update('currency_setup', $data);
        return TRUE;
    }

    function saveProgramDetails($data)
    {
    	$this->db->trans_start();
        $this->db->insert('fee_structure_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteProgramDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fee_structure_has_program');
        return TRUE;
    }

    function saveSchemeDetails($data)
    {
    	$this->db->trans_start();
        $this->db->insert('fee_structure_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteSchemeDetails($id)
    {
    	$this->db->where('id', $id);
        $this->db->delete('fee_structure_has_scheme');
        return TRUE;
    }


    function saveFeeSetupDetails($data)
    {
    	$this->db->trans_start();
        $this->db->insert('fee_structure_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteFeeSetupDetails($id)
    {
    	$this->db->where('id', $id);
        $this->db->delete('fee_structure_details');
        return TRUE;
    }
}

