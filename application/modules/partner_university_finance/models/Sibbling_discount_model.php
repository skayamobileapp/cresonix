<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sibbling_discount_model extends CI_Model
{
    function sibblingList($name)
    {
        $this->db->select('*');
        $this->db->from('sibbling_discount');
         if (!empty($name)) {
            $likeCriteria = "(name  LIKE '%" . $name . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getSibblingDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('sibbling_discount');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSibblingDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('sibbling_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSibblingDiscountDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('sibbling_discount', $data);
        return TRUE;
    }
}