<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Account_code_model extends CI_Model
{
    function accountCodeList()
    {
        $this->db->select('*');
        $this->db->from('financial_account_code');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function accountCodeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('financial_account_code');
        if (!empty($search))
        {
            $likeCriteria = "(type  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAccountCode($id)
    {
        $this->db->select('*');
        $this->db->from('financial_account_code');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAccountCode($data)
    {
        $this->db->trans_start();
        $this->db->insert('financial_account_code', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAccountCode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('financial_account_code', $data);
        return TRUE;
    }
}

