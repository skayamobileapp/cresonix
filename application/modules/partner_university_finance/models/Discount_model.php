<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Discount_model extends CI_Model
{
    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function discountTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('discount_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function discountListSearch($data)
    {
        $this->db->select('a.*, dt.name as discount_type_name, i.year as intake_year, i.name as intake_name, cs.name as currency_name');
        $this->db->from('discount as a');
        $this->db->join('discount_type as dt', 'a.id_discount_type = dt.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('currency_setup as cs', 'a.currency = cs.id','left');
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if ($data['id_intake'] != '')
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
        if ($data['id_discount_type'] != '')
        {
            $this->db->where('a.id_discount_type', $data['id_discount_type']);
        }
        $this->db->order_by("a.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDiscount($id)
    {
        $this->db->select('*');
        $this->db->from('discount');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDiscount($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('discount', $data);
        return TRUE;
    }
}

