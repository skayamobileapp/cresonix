<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Alumni_discount_model extends CI_Model
{
    function alumniList($name)
    {
        $this->db->select('*');
        $this->db->from('alumni_discount');
         if (!empty($name)) {
            $likeCriteria = "(name  LIKE '%" . $name . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getAlumniDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('alumni_discount');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAlumniDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('alumni_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAlumniDiscountDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('alumni_discount', $data);
        return TRUE;
    }
}