<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Vendor extends BaseController
{
    public function __construct()
    {
        parent::__construct();
                $this->load->model('vendor_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('vendor.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['mobile'] = $this->security->xss_clean($this->input->post('mobile'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = ''; 
            $data['searchParam'] = $formData;

            $data['vendorList'] = $this->vendor_model->vendorListSearch($formData);

            // $data['vendorList'] = $this->vendor_model->vendorList();
            // print_r($formData);exit;
            $this->global['pageTitle'] = 'Inventory Management :List Vendor';
            $this->loadViews("vendor/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('vendor.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if($this->input->post())
            {
                
                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $generated_number = $this->vendor_model->generateVendorNumber();

                $vname = $this->security->xss_clean($this->input->post('vname'));
                $vemail = $this->security->xss_clean($this->input->post('vemail'));
                $vphone = $this->security->xss_clean($this->input->post('vphone'));
                $vnric = $this->security->xss_clean($this->input->post('vnric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $vmobile = $this->security->xss_clean($this->input->post('vmobile'));
                $vaddress_one = $this->security->xss_clean($this->input->post('vaddress_one'));
                $vaddress_two = $this->security->xss_clean($this->input->post('vaddress_two'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $vcountry = $this->security->xss_clean($this->input->post('vcountry'));
                $vstate = $this->security->xss_clean($this->input->post('vstate'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $vzipcode = $this->security->xss_clean($this->input->post('vzipcode'));
                $gst_in = $this->security->xss_clean($this->input->post('gst_in'));
                $cin = $this->security->xss_clean($this->input->post('cin'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $vname,
                    'code' => $generated_number,
                    'email' => $vemail,
                    'phone' => $vphone,
                    'nric' => $vnric,
                    'gst_in' => $gst_in,
                    'cin' => $cin,
                    'gender' => $gender,
                    'mobile' => $vmobile,
                    'address_one' => $vaddress_one,
                    'address_two' => $vaddress_two,
                    'city' => $city,
                    'country' => $vcountry,
                    'state' => $vstate,
                    'zipcode' => $vzipcode,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->vendor_model->addNewVendor($data);
                redirect('/procurement/vendor/list');
            }

            $data['countryList'] = $this->vendor_model->countryListByStatus('1');
            // $data['bankList'] = $this->vendor_model->bankListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Vendor';
            $this->loadViews("vendor/add", $this->global, $data, NULL);

        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('vendor.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if ($id == null)
            {
                redirect('/procurement/vendor/list');
            }
            if($this->input->post())
            {
                $vname = $this->security->xss_clean($this->input->post('vname'));
                $vemail = $this->security->xss_clean($this->input->post('vemail'));
                $vphone = $this->security->xss_clean($this->input->post('vphone'));
                $vnric = $this->security->xss_clean($this->input->post('vnric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $vmobile = $this->security->xss_clean($this->input->post('vmobile'));
                $vaddress_one = $this->security->xss_clean($this->input->post('vaddress_one'));
                $vaddress_two = $this->security->xss_clean($this->input->post('vaddress_two'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $vcountry = $this->security->xss_clean($this->input->post('vcountry'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $vstate = $this->security->xss_clean($this->input->post('vstate'));
                $vzipcode = $this->security->xss_clean($this->input->post('vzipcode'));
                $gst_in = $this->security->xss_clean($this->input->post('gst_in'));
                $cin = $this->security->xss_clean($this->input->post('cin'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $vname,
                    'email' => $vemail,
                    'phone' => $vphone,
                    'nric' => $vnric,
                    'gst_in' => $gst_in,
                    'cin' => $cin,
                    'gender' => $gender,
                    'mobile' => $vmobile,
                    'address_one' => $vaddress_one,
                    'address_two' => $vaddress_two,
                    'city' => $city,
                    'country' => $vcountry,
                    'state' => $vstate,
                    'zipcode' => $vzipcode,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                // echo "<Pre>"; print_r($data);exit;

                
                $inserted_id = $this->vendor_model->editVendor($data,$id);
                redirect('/procurement/vendor/list');

            }

            $data['vendorRegistration'] = $this->vendor_model->getVendor($id);
            $data['countryList'] = $this->vendor_model->countryListByStatus('1');

            // echo "<Pre>"; print_r($data['vendorRegistration']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Vendor';
            $this->loadViews("vendor/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('vendor.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = 'Draft';
 
            $data['searchParam'] = $formData;

            $data['vendorList'] = $this->vendor_model->vendorListSearch($formData);


 
            // $data['vendorList'] = $this->vendor_model->vendorList();


            $this->global['pageTitle'] = 'Inventory Management :Approval List Vendor';
            // print_r($formData);exit;
            $this->loadViews("vendor/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('vendor.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $date = date("Y-m-d");
            if ($id == null)
            {
                redirect('/procurement/vendor/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'vendor_status' => $status,
                    'reason' => $reason
                );

                if($status == 'Approved')
                {
                    $data['expire_date'] = date('Y-m-d', strtotime('+3 years'));
                }
            // echo "<Pre>"; print_r($data);exit;

                $result = $this->vendor_model->updateVendor($data,$id);
                redirect('/procurement/vendor/approvalList');

            }

            $data['vendorRegistration'] = $this->vendor_model->getVendor($id);
            $data['vendorCompany'] = $this->vendor_model->getVendorCompanyByVendorId($id);
            $data['vendorBankList'] = $this->vendor_model->getVendorBankListByVendorId($id);
            $data['vendorBillingList'] = $this->vendor_model->getVendorBillingListByVendorId($id);
            $data['stateList'] = $this->vendor_model->stateListByStatus('1');
            $data['countryList'] = $this->vendor_model->countryListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Vendor';
            $this->loadViews("vendor/view", $this->global, $data, NULL);
        }
    }

    function blockList()
    {
        if ($this->checkAccess('vendor.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;

            $data['vendorList'] = $this->vendor_model->vendorListSearchForBlocList($formData);


 
            // $data['vendorList'] = $this->vendor_model->vendorList();


            $this->global['pageTitle'] = 'Inventory Management :Approval List Vendor';
            // print_r($formData);exit;
            $this->loadViews("vendor/block_list", $this->global, $data, NULL);
        }
    }

    function block($id = NULL)
    {
        if ($this->checkAccess('vendor.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/vendor/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'vendor_status' => $status,
                    'reason' => $reason
                );

                
                 $result = $this->vendor_model->updateVendor($data,$id);
                redirect('/procurement/vendor/blockList');

            }

            $data['vendorRegistration'] = $this->vendor_model->getVendor($id);
            $data['vendorCompany'] = $this->vendor_model->getVendorCompanyByVendorId($id);
            $data['vendorBankList'] = $this->vendor_model->getVendorBankListByVendorId($id);
            $data['vendorBillingList'] = $this->vendor_model->getVendorBillingListByVendorId($id);
            $data['stateList'] = $this->vendor_model->stateListByStatus('1');
            $data['countryList'] = $this->vendor_model->countryListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Vendor';
            $this->loadViews("vendor/block", $this->global, $data, NULL);
        }
    }

    function tempAddBank()
    {
        $id_session = $this->session->my_session_id;
        // $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
        // $address = $this->security->xss_clean($this->input->post('address'));
        // $country = $this->security->xss_clean($this->input->post('country'));
        // $state = $this->security->xss_clean($this->input->post('state'));
        // $city = $this->security->xss_clean($this->input->post('city'));
        // $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
        // $branch_no = $this->security->xss_clean($this->input->post('branch_no'));
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit();
        $tempData['id_session'] = $id_session;
        // echo "<Pre>";print_r($tempData);exit();
        // $data = array(
        //        'id_session' => $id_session,
        //        'id_bank' => $id_bank,
        //        'address' => $address,
        //        'country' => $country,
        //        'state' => $state,
        //        'city' => $city,
        //        'zipcode' => $zipcode,
        //        'branch_no' => $branch_no,
        //        'acc_no' => $acc_no
        //     );

        $inserted_id = $this->vendor_model->addNewTempBank($tempData);
        $table = $this->displayTempBankBySession();
        echo $table;
    }

    function displayTempBankBySession()
    {
        $id_session = $this->session->my_session_id;

        $details = $this->vendor_model->getTempBank($id_session);

        if(!empty($details))
        {  
        
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Bank Name</th>
                    <th>Branch Number</th>
                    <th>Account Number</th>
                    <th>Address</th>
                    <th>Country</th>
                    <th>State</th>
                    <th>City</th>
                    <th>Zipcode</th>
                    <th>Action</th>
                </tr>
                </thead>";
                for($i=0;$i<count($details);$i++)
                {
                    $id = $details[$i]->id;
                    $bank_name = $details[$i]->bank_name;
                    $bank_code = $details[$i]->bank_code;
                    $id_bank = $details[$i]->id_bank;
                    $branch_no = $details[$i]->branch_no;
                    $acc_no = $details[$i]->acc_no;
                    $country = $details[$i]->country;
                    $state = $details[$i]->state;
                    $city = $details[$i]->city;
                    $address = $details[$i]->address;
                    $zipcode = $details[$i]->zipcode;
                    $j = $i+1;

                    $table .= "
                <tbody>
                <tr>

                    <td>$j</td>
                    <td>$bank_code - $bank_name</td>
                    <td>$branch_no</td>
                    <td>$acc_no</td>
                    <td>$address</td>
                    <td>$country</td>
                    <td>$state</td>
                    <td>$city</td>
                    <td>$zipcode</td>
                    <td>
                        <a onclick='deleteTempBank($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
                </tbody>
            </table>
            </div>";
            echo $table;
        }
        

    }

    function deleteTempBank($id)
    {
        // echo "<Pre>";print_r($id);exit();
        // $id = $this->input->get('id');
        $this->vendor_model->deleteTempBank($id);
        $table = $this->displayTempBankBySession();
        echo $table;
    }

    function tempAddBilling()
    {
        $id_session = $this->session->my_session_id;
        $name = $this->security->xss_clean($this->input->post('name'));
        $nric = $this->security->xss_clean($this->input->post('nric'));
        $email = $this->security->xss_clean($this->input->post('email'));
        $phone = $this->security->xss_clean($this->input->post('phone'));

        $data = array(
               'id_session' => $id_session,
               'name' => $name,
               'nric' => $nric,
               'email' => $email,
               'phone' => $phone
            );

        $inserted_id = $this->vendor_model->addNewTempBilling($data);
        $table = $this->displayBillingPersonDetailsBySession();
        echo $table;
    }


    function displayBillingPersonDetailsBySession()
    {
        $id_session = $this->session->my_session_id;

        $details = $this->vendor_model->getTempBilling($id_session);

        if(!empty($details))
        {  
            $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Name</th>
                    <th>NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>";
                for($i=0;$i<count($details);$i++)
                {
                    $id = $details[$i]->id;
                    $name = $details[$i]->name;
                    $nric = $details[$i]->nric;
                    $email = $details[$i]->email;
                    $phone = $details[$i]->phone;
                    $j = $i+1;
                    $table .= "
            <tbody>
                <tr>
                    <td>$j</td>
                    <td>$name</td>
                    <td>$nric</td>
                    <td>$email</td>
                    <td>$phone</td>
                   <td>
                        <a onclick='deleteTempBillById($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </tbody>
        </table>
        </div>";
        echo $table;
        }
    }


    function deleteTempBill($id)
    {
        // echo "<Pre>";print_r($id);exit();
         $this->vendor_model->deleteTempBilling($id);
        $table = $this->displayBillingPersonDetailsBySession();
        echo $table;
    }



    function getStateByCountry($id_country)
    {
        $results = $this->vendor_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='vstate' id='vstate' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getStateByCountryCompany($id_country)
    {
        $results = $this->vendor_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='cstate' id='cstate' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }

    function getStateByCountryBank($id_country)
    {
        $results = $this->vendor_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='bstate' id='bstate' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }
}
