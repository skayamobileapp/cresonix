<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PcIncoming extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pc_model');
        $this->load->model('assembly_distribution_model');
        $this->load->model('pc_incoming_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('pc.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['pcList'] = $this->pc_incoming_model->pcListSearch($name);
            // echo "<Pre>"; print_r($data['pcList']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : List pc Category';
            $this->loadViews("pc_incoming/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('pc.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $qty = $this->security->xss_clean($this->input->post('qty'));
                $id_pc = $this->security->xss_clean($this->input->post('id_pc'));
                $id_assembly_distributor = 
                $this->security->xss_clean($this->input->post('id_assembly_distributor'));

            
                $data = array(
                    'qty' => $qty,
                    'id_pc' => $id_pc,
                    'id_assembly_distribution' => $id_assembly_distributor,
                    'created_by' => $id_user
                );
                $this->pc_incoming_model->addNewpcIncoming($data);


                $pcDetails = $this->pc_model->getpcDetails($id_pc);
                $pcDetailsArray['stock'] = $pcDetails->stock + $qty;

                $this->pc_model->editpc($pcDetailsArray,$id_pc);

                  $dataincoming = array(
                    'qty' => $qty,
                    'id_pc' => $id_pc,
                    'id_assembly_distribution' => $id_assembly_distributor,
                    'type'=>'Incoming'


                );
                $this->pc_incoming_model->addNewpcsumary($dataincoming);

                redirect('/procurement/pcIncoming/list');
            }
            $assemblyddata = array();
            $data['assemblyDistributionList'] = $this->assembly_distribution_model->getAssemblyDistributionListSearch($assemblyddata);
            $data['pcList'] = $this->pc_model->pcListSearch($assemblyddata);        
            $this->global['pageTitle'] = 'Inventory Management : Add pc Category';
            $this->loadViews("pc_incoming/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('pc.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if ($id == null)
            {
                redirect('/procurement/pc/list');
            }
            if($this->input->post())
            {

                $qty = $this->security->xss_clean($this->input->post('qty'));
                $id_pc = $this->security->xss_clean($this->input->post('id_pc'));
                $id_assembly_distributor = 
                $this->security->xss_clean($this->input->post('id_assembly_distributor'));

            
                $data = array(
                    'qty' => $qty,
                    'id_pc' => $id_pc,
                    'id_assembly_distribution' => $id_assembly_distributor,
                    'created_by' => $id_user,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')


                );
            
                           
                
                $result = $this->pc_incoming_model->editpc($data,$id);
                redirect('/procurement/pc/list');
            }
                        $assemblyddata = array();
            $data['assemblyDistributionList'] = $this->assembly_distribution_model->getAssemblyDistributionListSearch($assemblyddata);
            $data['pcList'] = $this->pc_model->pcListSearch($assemblyddata);        

            $data['pcincoming'] = $this->pc_incoming_model->getpcDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit pc Category';
            $this->loadViews("pc_incoming/edit", $this->global, $data, NULL);
        }
    }   
}