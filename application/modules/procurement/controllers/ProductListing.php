<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProductListing extends BaseController
{
    public function __construct()
    {
        try
        {
            parent::__construct();
            $this->load->model('product_listing_model');
                    $this->load->model('procurement_category_model');
        $this->load->model('assembly_distribution_model');

            $this->isLoggedIn();
        }
        catch(Exception $e)
        {
            echo "<Pre>";print_r("Exception Generating On Model Loading In Controller : \n\n\n".$e);exit;
        }
    }

    function writeoff()
    {

        if ($this->checkAccess('assembly_distribution.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_assembly'] = $this->security->xss_clean($this->input->post('id_assembly'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['assemblyDistributionList'] = $this->assembly_distribution_model->getAssemblyDistributionListSearchNew($formData);

            $data['assemblyTeamList'] = $this->assembly_distribution_model->assemblyTeamListByStatus('1');

            // echo '<Pre>';print_r($data['assemblyDistributionList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("product_listing/writeoff", $this->global, $data, NULL);
        }
    }

    function list()
    {

        if ($this->checkAccess('procurement_item.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_procurement_category'] = $this->security->xss_clean($this->input->post('id_procurement_category'));
            $formData['id_procurement_sub_category'] = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));
            $data['searchParameters'] = $formData; 

            $data['procurementItemList'] = $this->product_listing_model->procurementItemListSearch($formData);

         
            $data['procurementCategoryList'] = $this->product_listing_model->procurementCategoryList();
            $data['procurementSubCategoryList'] = $this->product_listing_model->procurementSubCategoryList();

            $this->global['pageTitle'] = 'Inventory Management : List Procurement Item';
            $this->loadViews("product_listing/list", $this->global, $data, NULL);

        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_item.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/ProductListing/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }
            
            $data['id_item'] = $id;

            $data['product'] = $this->product_listing_model->getProcurementItem($id);
            $data['productQuantity'] = $this->product_listing_model->getProductQuantityListByIdItem($id);            
            
            // echo '<Pre>';print_r($data['product']);exit();     

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("product_listing/edit", $this->global, $data, NULL);
        }
    }

   

    function category()
    {
      
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryListSearch($name);
            // echo "<Pre>"; print_r($data['procurementCategoryList']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("product_listing/category", $this->global, $data, NULL);
        
    }

    function pc()
    {
      
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['procurementCategoryList'] = $this->procurement_category_model->procurementpc($name);
            
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("product_listing/pc", $this->global, $data, NULL);
        
    }


    function viewdetails($id=NULL)
    {
      
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['procurementCategoryList'] = $this->procurement_category_model->pcSummary($id);
            
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("product_listing/viewdetails", $this->global, $data, NULL);
        
    }
}