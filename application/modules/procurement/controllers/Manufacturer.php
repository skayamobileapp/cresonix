<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Manufacturer extends BaseController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model('manufacturer_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('manufacturer.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['mobile'] = $this->security->xss_clean($this->input->post('mobile'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = ''; 
            $data['searchParam'] = $formData;

            $data['manufacturerList'] = $this->manufacturer_model->manufacturerListSearch($formData);

            // $data['manufacturerList'] = $this->manufacturer_model->manufacturerList();
            // print_r($formData);exit;
            $this->global['pageTitle'] = 'Inventory Management :List Manufacturer';
            $this->loadViews("manufacturer/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('manufacturer.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if($this->input->post())
            {
                
                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $generated_number = $this->manufacturer_model->generateManufacturerNumber();

                $vname = $this->security->xss_clean($this->input->post('vname'));
                $vemail = $this->security->xss_clean($this->input->post('vemail'));
                $vphone = $this->security->xss_clean($this->input->post('vphone'));
                $vnric = $this->security->xss_clean($this->input->post('vnric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $vmobile = $this->security->xss_clean($this->input->post('vmobile'));
                $vaddress_one = $this->security->xss_clean($this->input->post('vaddress_one'));
                $vaddress_two = $this->security->xss_clean($this->input->post('vaddress_two'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $vcountry = $this->security->xss_clean($this->input->post('vcountry'));
                $vstate = $this->security->xss_clean($this->input->post('vstate'));
                $vzipcode = $this->security->xss_clean($this->input->post('vzipcode'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $gst_in = $this->security->xss_clean($this->input->post('gst_in'));
                $cin = $this->security->xss_clean($this->input->post('cin'));
                $status='1';


                $data = array(
                    'name' => $vname,
                    'code' => $generated_number,
                    'email' => $vemail,
                    'phone' => $vphone,
                    'nric' => $vnric,
                    'gst_in' => $gst_in,
                    'cin' => $cin,
                    'gender' => $gender,
                    'mobile' => $vmobile,
                    'address_one' => $vaddress_one,
                    'address_two' => $vaddress_two,
                    'city' => $city,
                    'country' => $vcountry,
                    'state' => $vstate,
                    'zipcode' => $vzipcode,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->manufacturer_model->addNewManufacturer($data);
                redirect('/procurement/manufacturer/list');
            }

            $data['countryList'] = $this->manufacturer_model->countryListByStatus('1');
            // $data['bankList'] = $this->manufacturer_model->bankListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Manufacturer';
            $this->loadViews("manufacturer/add", $this->global, $data, NULL);

        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('manufacturer.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if ($id == null)
            {
                redirect('/procurement/manufacturer/list');
            }
            if($this->input->post())
            {
                $vname = $this->security->xss_clean($this->input->post('vname'));
                $vemail = $this->security->xss_clean($this->input->post('vemail'));
                $vphone = $this->security->xss_clean($this->input->post('vphone'));
                $vnric = $this->security->xss_clean($this->input->post('vnric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $vmobile = $this->security->xss_clean($this->input->post('vmobile'));
                $vaddress_one = $this->security->xss_clean($this->input->post('vaddress_one'));
                $vaddress_two = $this->security->xss_clean($this->input->post('vaddress_two'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $vcountry = $this->security->xss_clean($this->input->post('vcountry'));
                $vstate = $this->security->xss_clean($this->input->post('vstate'));
                $vzipcode = $this->security->xss_clean($this->input->post('vzipcode'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $gst_in = $this->security->xss_clean($this->input->post('gst_in'));
                $cin = $this->security->xss_clean($this->input->post('cin'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $vname,
                    'email' => $vemail,
                    'phone' => $vphone,
                    'nric' => $vnric,
                    'gst_in' => $gst_in,
                    'cin' => $cin,
                    'gender' => $gender,
                    'mobile' => $vmobile,
                    'address_one' => $vaddress_one,
                    'address_two' => $vaddress_two,
                    'city' => $city,
                    'country' => $vcountry,
                    'state' => $vstate,
                    'zipcode' => $vzipcode,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                // echo "<Pre>"; print_r($data);exit;

                
                $inserted_id = $this->manufacturer_model->editManufacturer($data,$id);
                redirect('/procurement/manufacturer/list');

            }

            $data['manufacturer'] = $this->manufacturer_model->getManufacturer($id);
            $data['countryList'] = $this->manufacturer_model->countryListByStatus('1');

            // echo "<Pre>"; print_r($data['manufacturerRegistration']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Manufacturer';
            $this->loadViews("manufacturer/edit", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
        $results = $this->manufacturer_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='vstate' id='vstate' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function displayBillingPersonDetailsBySession()
    {
        $id_session = $this->session->my_session_id;

        $details = $this->manufacturer_model->getTempBilling($id_session);

        if(!empty($details))
        {  
            $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Name</th>
                    <th>NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>";
                for($i=0;$i<count($details);$i++)
                {
                    $id = $details[$i]->id;
                    $name = $details[$i]->name;
                    $nric = $details[$i]->nric;
                    $email = $details[$i]->email;
                    $phone = $details[$i]->phone;
                    $j = $i+1;
                    $table .= "
            <tbody>
                <tr>
                    <td>$j</td>
                    <td>$name</td>
                    <td>$nric</td>
                    <td>$email</td>
                    <td>$phone</td>
                   <td>
                        <a onclick='deleteTempBillById($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </tbody>
        </table>
        </div>";
        echo $table;
        }
    }

}
