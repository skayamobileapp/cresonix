<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Role (RoleController)
 * Role Class to control all role related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Role extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('role_model');
        $this->load->model('permission_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('role.list') == 0)
        {
            $this->loadAccessRestricted();
        } else {
            if ($this->input->method() == "post") {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('role', 'Role', 'trim|strtolower');
                $role = $this->security->xss_clean($this->input->post('role'));
            } else {
                $role = '';
            }
            $data['role'] = $role;

            $data['roleRecords'] = $this->role_model->roleListing($role);

            $this->global['pageTitle'] = 'School : Role Listing';

            $this->loadViews("role/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('role.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;


            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();
                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $permissions = $this->input->post('id_permission');

                $roleInfo = array(
                    'role' => $role,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $roleId = $this->role_model->addNewRole($roleInfo);
                // $this->role_model->updateRolePermissions($permissions, $roleId);

                if ($roleId > 0)
                {
                    $this->session->set_flashdata('success', 'New Role created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Role creation failed');
                }

                redirect('/setup/role/edit/'.$roleId);
            }

            $data['permissions'] = $this->permission_model->permissionListing();

            $this->global['pageTitle'] = 'School : Add New Role';
            $this->loadViews("role/add", $this->global, $data, NULL);
        }
    }

    function addNewRole()
    {
        if ($this->checkAccess('role.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            $this->load->library('form_validation');

            $this->form_validation->set_rules('role', 'Role', 'trim|required');

            if ($this->form_validation->run() == FALSE)
            {
                $this->add();
            }
            else
            {
                // echo "<Pre>";print_r($this->input->post());exit();
                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $permissions = $this->input->post('permissions');

                $roleInfo = array(
                    'role' => $role,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $roleId = $this->role_model->addNewRole($roleInfo);
                $this->role_model->updateRolePermissions($permissions, $roleId);

                if ($roleId > 0) {
                    $this->session->set_flashdata('success', 'New Role created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Role creation failed');
                }

                redirect('/setup/role/list');
            }
        }
    }


    /**
     * This function is used load role edit information
     * @param number $roleId : Optional : This is role id
     */
    function edit($roleId = NULL)
    {
        if ($this->checkAccess('role.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($roleId == null)
            {
                redirect('/setup/role/list');
            }
            if($this->input->post())
            {

                // echo "<pre>";print_r($this->input->post());die;

                $roleList = $this->input->post('checkrole');
                $role = $this->input->post('role');
                $status = $this->input->post('status');

                $update_data = array(
                    'role'=> $role,
                    'status'=> $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $this->role_model->editRole($update_data,$roleId);

                $deleteoldrold = $this->role_model->deletePermission($roleId);

                for($i=0;$i<count($roleList);$i++)
                {
                      $data = array(
                            'id_role' => $roleId,
                            'id_permission' => $roleList[$i],
                            'created_by' => $id_user
                        );
 
                    $result = $this->role_model->addPermission($data);

                }



                // $role = $this->security->xss_clean($this->input->post('role'));
                // $status = $this->security->xss_clean($this->input->post('status'));
                
                     
                // $data = array(
                //     'role' => $role,
                //     'status' => $status
                // );              
                // $result = $this->role_model->editRole($data,$roleId);
                redirect('/setup/role/list');
            }

            $data['permissions'] = $this->permission_model->permissionListing();
            $data['roleInfo'] = $this->role_model->getRoleInfo($roleId);
            $data['rolepermissions'] = $this->role_model->rolePermissions($roleId);
            $getrolelist = $this->role_model->getMenu();
            $data['role'] = $getrolelist;

            $data['menuList'] = $this->role_model->menuList();

            $this->global['pageTitle'] = 'School : Edit Role';
            // echo "<pre>";print_r($data);die;
            $this->loadViews("role/edit", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the role information
     */
    function editRole()
    {
        if ($this->checkAccess('role.edit') == 0) {
            $this->loadAccessRestricted();
        } else {
            $this->load->library('form_validation');
            // echo "<pre>";print_r($this->input->post());die;

            $roleId = $this->input->post('roleId');
            $permissions = $this->input->post('permissions');

            $this->form_validation->set_rules('role', 'Role', 'trim|required');

            // echo "<pre>";print_r($this->input->post());die;

            if ($this->form_validation->run() == FALSE) {
                $this->edit($roleId);
            } else {
                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $roleInfo = array(
                    'role' => $role,
                    'status' => $status
                );

                $result = $this->role_model->editRole($roleInfo, $roleId);
                $this->role_model->updateRolePermissions($permissions, $roleId);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Role updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Role updation failed');
                }

                redirect('/setup/role/list');
            }
        }
    }


    /**
     * This function is used to delete the role using roleId
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if ($this->checkAccess('role.delete') == 0) {
            echo (json_encode(array('status' => 'access')));
        } else {
            $roleId = $this->input->post('roleId');
            $roleInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));

            $result = $this->role_model->deleteRole($roleId, $roleInfo);

            if ($result > 0) {
                echo (json_encode(array('status' => TRUE)));
            } else {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }
}
