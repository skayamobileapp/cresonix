<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Packages extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('packages_model');
        $this->isLoggedIn();
    }

    function list()
    {
         // echo "<Pre>"; print_r("dsada");exit;
        if ($this->checkAccess('packages.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['packagesList'] = $this->packages_model->packagesListSearch($name);
            // echo "<Pre>"; print_r($data['packagesList']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("packages/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('packages.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $id_user

                );
            
                $result = $this->packages_model->addNewPackages($data);
                redirect('/procurement/packages/list');
            }
            
            $this->global['pageTitle'] = 'Inventory Management : Add Procurement Category';
            $this->loadViews("packages/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('packages.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if ($id == null)
            {
                redirect('/procurement/packages/list');
            }
            if($this->input->post())
            {

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );
                
                $result = $this->packages_model->editPackages($data,$id);
                redirect('/procurement/packages/list');
            }
            $data['packages'] = $this->packages_model->getPackagesDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("packages/edit", $this->global, $data, NULL);
        }
    }

   
}
