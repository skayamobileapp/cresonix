<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssemblyReturn extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('assembly_return_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('assembly_return.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_assembly'] = $this->security->xss_clean($this->input->post('id_assembly'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['assemblyReturnList'] = $this->assembly_return_model->getAssemblyReturnListSearch($formData);

            $data['assemblyTeamList'] = $this->assembly_return_model->assemblyTeamListByStatus('1');

            // echo '<Pre>';print_r($data['assemblyList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("assembly_return/list", $this->global, $data, NULL);
        }
    }

    function add()
    {

        if ($this->checkAccess('assembly_return.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {
                 $id_assembly_distribution = $this->security->xss_clean($this->input->post('id_assembly_distribution'));
                $id_assembly = $this->security->xss_clean($this->input->post('id_assembly'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $returned_quantity = $this->security->xss_clean($this->input->post('returned_quantity'));
                $id_assembly_distribution_detail = $this->security->xss_clean($this->input->post('id_assembly_distribution_detail'));



                if($_POST['assembly_type']=='WriteOff'){
                    for($i=0;$i<count($returned_quantity);$i++)
                    {
                        $distribution_detail = $this->assembly_return_model->getAssemblyDistributionDetailById($id_assembly_distribution_detail[$i]);

                        $balanceQty = $distribution_detail->balance_quantity - $returned_quantity[$i];

                        $dataupate['balance_quantity'] = $balanceQty;

                        $dataupate['write_off'] = $returned_quantity[$i];
                        $assemblyDisId = $id_assembly_distribution_detail[$i];
                        $this->assembly_return_model->updateAssemblyDistributionDetail($dataupate, $assemblyDisId);

                    }
                redirect('/procurement/assemblyReturn/list');

                }


               

                $generated_number = $this->assembly_return_model->generateAssemblyReturnNumber();


                $data = array(
                    'id_assembly' => $id_assembly,
                    'id_assembly_distribution' => $id_assembly_distribution,
                    'reference_number' =>$generated_number,
                    'description' => $description,
                    'created_by' => $id_user,
                    'status'=>7
                );

                $inserted_id = $this->assembly_return_model->addAssemblyReturn($data);

                $total_amount = 0;
                if($inserted_id)
                {
                    // echo "<Pre>";print_r($data);exit;
                    for($i=0;$i<count($returned_quantity);$i++)
                    {
                        $id_assembly_distribution_detail = $id_assembly_distribution_detail[$i];
                        $quantity = $returned_quantity[$i];

                        if($quantity > 0)
                        {
                            $distribution_detail = $this->assembly_return_model->getAssemblyDistributionDetailById($id_assembly_distribution_detail);

                            if($distribution_detail)
                            {
                                $id_item = $distribution_detail->id_item;

                                $item = $this->assembly_return_model->getProcurementItem($id_item);
                                $product_quantity = 0;

                                if($item)
                                {
                                    $product_quantity = $item->quantity;
                                }

                                $distributed_quantity = $distribution_detail->quantity;
                                $distributed_balance_quantity = $distribution_detail->balance_quantity;
                                $distributed_received_quantity = $distribution_detail->received_quantity;
                                $distributed_price = $distribution_detail->price;
                                $total_price = $distributed_price * $quantity;



                                $detail_data = array(
                                    'id_assembly_return' => $inserted_id,
                                    'id_assembly_distribution_detail' =>$id_assembly_distribution_detail,
                                    'id_category' => $distribution_detail->id_category,
                                    'id_sub_category' => $distribution_detail->id_sub_category,
                                    'id_item' => $distribution_detail->id_item,
                                    'total_quantity' => $distributed_quantity,
                                    'quantity' => $quantity,
                                    'balance_quantity' => $distributed_balance_quantity - $quantity,
                                    'price' => $distributed_price,
                                    'total_price' => $total_price,
                                    'created_by' => $id_user,
                                    'status'=>1
                                );
                                

                                $id_assembly_return_detail = $this->assembly_return_model->addAssemblyReturnDetail($detail_data);

                                if($id_assembly_return_detail)
                                {
                                    $product_quantity_after_add = $product_quantity + $quantity;

                                    $add_item_quantity_data = array(
                                        'id_description' => 3,
                                        'id_item' => $id_item,
                                        'id_assembly_return_detail' => $id_assembly_return_detail,
                                        'id_assembly_return' => $inserted_id,
                                        'previous_quantity' => $product_quantity,
                                        'assembly_return_quantity' =>$quantity,
                                        'quantity' => $product_quantity_after_add,
                                        'status' => 1,
                                        'created_by' => $id_user
                                        );
                                    
                                    $id_product_quantity = $this->assembly_return_model->addProductQuantity($add_item_quantity_data);

                                    if($id_product_quantity)
                                    {
                                        $item_update_data = array(
                                            'quantity' => $product_quantity_after_add
                                        );
                                    
                                        $id_assembly_return_detail = $this->assembly_return_model->updateItem($item_update_data,$id_item);
                                    }


                                    $distributed_balance_quantity = $distributed_balance_quantity - $quantity;
                                    $distributed_received_quantity = $distributed_received_quantity + $quantity;

                                    $distribution_update_data = array(
                                        'id_assembly_return_detail' => $id_assembly_return_detail,
                                        'received_quantity' =>$distributed_received_quantity,
                                        'balance_quantity' => $distributed_balance_quantity,
                                        'updated_by' => $id_user,
                                        'updated_dt_tm' => date('Y-m-d H:i:s')
                                        );
                                    
                                    $id_assembly_return_detail = $this->assembly_return_model->updateAssemblyDistributionDetail($distribution_update_data,$id_assembly_distribution_detail);
                                }
                                $total_amount = $total_amount + $total_price;
                            }
                        }
                    }
                }

                $update_data['total_amount'] = $total_amount;
                $update_data['balance_amount'] = $total_amount;

                $id_assembly_return_detail = $this->assembly_return_model->updateAssemblyReturn($update_data,$inserted_id);
                redirect('/procurement/assemblyReturn/list');
            }

            $data['assemblyPendingList'] = $this->assembly_return_model->assemblyPendingList();

            // echo "<Pre>";print_r($data['poPendingList']);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("assembly_return/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('assembly_return.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/assemblyReturn/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }
            
            $data['assemblyReturn'] = $this->assembly_return_model->getAssemblyReturn($id);
            $data['assemblyReturnDetails'] = $this->assembly_return_model->getAssemblyReturnDetails($id);
            
            // echo "<Pre>";print_r($data['assemblyReturnDetails']);exit();            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("assembly_return/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('assembly_return.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/poEntry/list');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));

                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
 
                $result = $this->assembly_return_model->editPOList($data,$id);
                redirect('/procurement/assemblyReturn/list');

                // print_r($this->input->post());exit;
            }
            
            $data['poMaster'] = $this->assembly_return_model->getMasterPoDetails($id);
            $data['poDetails'] = $this->assembly_return_model->getPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("assembly_return/view", $this->global, $data, NULL);
        }
    }

    function approvallist()
    {
        if ($this->checkAccess('assembly_return.approval') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         
                         $result = $this->assembly_return_model->editPOList($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':
                     
                     break;
                 
                default:
                     break;
             }
                
            }


            
            $formData['id_assembly'] = $this->security->xss_clean($this->input->post('id_assembly'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));


            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['poList'] = $this->assembly_return_model->getPOListSearch($formData);

            $data['financialYearList'] = $this->assembly_return_model->financialYearListByStatus('1');
            $data['assemblyList'] = $this->assembly_return_model->assemblyListByStatus('Approved');
            $data['departmentList'] = $this->assembly_return_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->assembly_return_model->getDepartmentCodeList();
            

            // echo "<Pre>";print_r($formData);exit();
            
                // $array = $this->security->xss_clean($this->input->post('checkvalue'));
                // if (!empty($array))
                // {

                //     $result = $this->assembly_return_model->editPOList($array);
                //     redirect($_SERVER['HTTP_REFERER']);
                // }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("assembly_return/approval_list", $this->global, $data, NULL);

        }
    }

    function getData($id)
    {
        // echo "<Pre>";print_r($id);exit;

        // $assemblyList = $this->assembly_return_model->assemblyListByStatus('1');

        $po = $this->assembly_return_model->getAssemblyDistribution($id);


        // $array = $this->assembly_return_model->financialYearListByStatus('1');

        // $financialYearList = json_decode( json_encode($array), true);

        // echo "<Pre>";print_r($po);exit;




        // <div class='container-fluid page-wrapper'>
        $table = "

            <script type='text/javascript'>
                $('select').select2();
            </script>




                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Reference Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$po->reference_number'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Distribution Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$po->description'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Distribution Entry Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' readonly='readonly' value='$po->created_dt_tm'>
                        </div>
                    </div>

                    
                </div>

                <div class='row'>

                  <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Assembly Team <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$po->assembly_code - $po->assembly_name' readonly='readonly'>
                            <input type='hidden' class='form-control' id='id_assembly' name='id_assembly' value='$po->id_assembly'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Total Amount <span class='error-text'>*</span></label>
                            <input type='text' readonly='readonly' class='form-control' name='total_amount'  value='$po->total_amount'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Return Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='description' name='description'>
                        </div>
                    </div>

                 </div>


                ";
                // </div>

        $poDetails = $this->assembly_return_model->getAssemblyDistributionDetails($id);


        $table.="

        <h4> &nbsp;&nbsp;&nbsp;Assembly Distribution Details </h4>

        <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Catedory</th>
             <th>Sub Catedory</th>
             <th>Product</th>
             <th>Issue Quantity</th>
             <th>Excess Quantity</th>

             <th>Returned Quantity</th>
             <th>Balance Quantity</th>
             <th>Quantity</th>
         </tr>
        </thead>
        <tbody>";
            $total_detail = 0;
          for($i=0;$i<count($poDetails);$i++)
          {
          $j=$i+1; 

            $id = $poDetails[$i]->id;
            $category_name = $poDetails[$i]->category_name;
            $category_code = $poDetails[$i]->category_code;
            $sub_category_name = $poDetails[$i]->sub_category_name;
            $sub_category_code = $poDetails[$i]->sub_category_code;
            $item_name = $poDetails[$i]->item_name;
            $item_code = $poDetails[$i]->item_code;
            $quantity = $poDetails[$i]->quantity;
                        $excess_quantity = $poDetails[$i]->excess_quantity;

            $price = $poDetails[$i]->price;
            $total_price = $poDetails[$i]->total_price;
            $balance_quantity = $poDetails[$i]->balance_quantity;
            $received_quantity = $poDetails[$i]->received_quantity;

            $j=$i+1;
            $table .= "
            <tbody>
            <tr>
                <td>$j</td>
                <td>$category_name</td>
                <td>$sub_category_name</td>
                <td>$item_name</td>
                <td>$quantity</td>
                <td>$excess_quantity</td>
                <td>$received_quantity</td>
                <td>$balance_quantity</td>
               
                <td>
                    <input type='number' id='returned_quantity[]' name='returned_quantity[]'";
                    if($balance_quantity == 0)
                    {
                        $table .= " value='$balance_quantity' readonly";
                    }
                    $table .= ">
                    <input type='hidden' id='id_assembly_distribution_detail[]' name='id_assembly_distribution_detail[]' value='$id'>
                </td>
            </tr>";
            $total_detail = $total_detail + $total_price;
         }
         $total_detail = number_format($total_detail, 2, '.', ',');

        $table.="
      

        </tbody>
        </table>
        </div>";
       echo $table;

    }
}