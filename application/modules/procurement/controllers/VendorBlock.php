<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class VendorBlock extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('vendor_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('vendor_block.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $data['vendorList'] = $this->vendor_model->vendorList();
            
            $student_name = $this->security->xss_clean($this->input->post('student_name'));
            // $name = $this->security->xss_clean($this->input->post('name'));
            // $name = $this->security->xss_clean($this->input->post('name'));

            $data['searchName'] = $student_name;
            $data['vendorList'] = $this->vendor_model->vendorList();
            
                $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array)) {

                $result = $this->vendor_model->blockVendorList($array);
                    redirect($_SERVER['HTTP_REFERER']);
                }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("vendor_block/list", $this->global, $data, NULL);
        }
    }
}
