<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class NonPoEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Nonpo_model');
        $this->load->model('Pr_model');

        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('nonpo_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['nonPoList'] = $this->Nonpo_model->getNonPOListSearch($formData);

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();


            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("non_po_entry/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('pr_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {


                $id = $this->security->xss_clean($this->input->post('type_of_pr'));
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                $generated_number = $this->Nonpo_model->generateNONPONumber();

                $prMaster = $this->Pr_model->getMasterPrDetails($id);

                $prDetails = $this->Pr_model->getPRDetailsByMasterID($id);
                $description = $this->security->xss_clean($this->input->post('description'));

     
                

                $data = array(
                    'id_pr' => $id,
                    'nonpo_number' =>$generated_number,
                    'nonpo_entry_date' => $prMaster->pr_entry_date,
                    'type' => $prMaster->type,
                    'id_financial_year' => $prMaster->id_financial_year,
                    'id_budget_year' => $prMaster->id_budget_year,
                    'department_code' => $prMaster->department_code,
                    'id_department' => $prMaster->id_department,
                    'id_vendor' => $prMaster->id_vendor,
                    'amount' => $prMaster->amount,
                    'description' => $description,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
               
                $inserted_id = $this->Nonpo_model->addNonPO($data);

                if($inserted_id)
                {
                    $update_data = array('is_po' => $inserted_id);
                    $updated_pr = $this->Pr_model->updatePR($update_data,$id);
                }
                


                // $temp_details = $this->Pr_model->getTempDetailsFromPR($id_session);
                 for($i=0;$i<count($prDetails);$i++)
                 {

                        $dt_fund = $prDetails[$i]->dt_fund;
                        $dt_department = $prDetails[$i]->dt_department;
                        $dt_activity = $prDetails[$i]->dt_activity;
                        $dt_account = $prDetails[$i]->dt_account;
                        $cr_fund = $prDetails[$i]->cr_fund;
                        $cr_department = $prDetails[$i]->cr_department;
                        $cr_activity = $prDetails[$i]->cr_activity;
                        $cr_account = $prDetails[$i]->cr_account;
                        $type = $prDetails[$i]->type;
                        $id_category = $prDetails[$i]->id_category;
                        $id_sub_category = $prDetails[$i]->id_sub_category;
                        $id_item = $prDetails[$i]->id_item;
                        $quantity = $prDetails[$i]->quantity;
                        $price = $prDetails[$i]->price;
                        $id_tax = $prDetails[$i]->id_tax;
                        $tax_price = $prDetails[$i]->tax_price;
                        $total_final = $prDetails[$i]->total_final;
                    

                     $detailsData = array(
                        'id_po_entry'=>$inserted_id,
                        'dt_fund'=>$dt_fund,
                        'dt_department'=>$dt_department,
                        'dt_activity'=>$dt_activity,
                        'dt_account'=>$dt_account,
                        'cr_fund'=>$cr_fund,
                        'cr_department'=>$cr_department,
                        'cr_activity'=>$cr_activity,
                        'cr_account'=>$cr_account,
                        'type'=>$type,
                        'id_category'=>$id_category,
                        'id_sub_category'=>$id_sub_category,
                        'id_item'=>$id_item,
                        'quantity'=>$quantity,
                        'price'=>$price,
                        'id_tax'=>$id_tax,
                        'tax_price'=>$tax_price,
                        'total_final'=>$total_final

                    );
                //      // echo "<Pre>";print_r($detailsData);exit;
                    $result = $this->Nonpo_model->addNewPODetails($detailsData);
                }

                $this->Pr_model->deleteFullSessionData($id_session);
                redirect('/procurement/nonPoEntry/list');
              }
            }
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['prEntryList'] = $this->Pr_model->getPRListForProcess('Non-PO');
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("non_po_entry/add", $this->global, $data, NULL);
        
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('pr_entry.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/pr_entry/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }

            $data['nonPoMaster'] = $this->Nonpo_model->getMasterNonPoDetails($id);
            $data['nonPoDetails'] = $this->Nonpo_model->getNonPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("non_po_entry/edit", $this->global, $data, NULL);
        }
    }

      function view($id = NULL)
    {
        if ($this->checkAccess('pr_entry.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/NonPoEntry/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                
                 $result = $this->Nonpo_model->editNONPO($data,$id);

                redirect('/procurement/NonPoEntry/approvalList');

            }

            $data['nonPoMaster'] = $this->Nonpo_model->getMasterNonPoDetails($id);
            $data['nonPoDetails'] = $this->Nonpo_model->getNonPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("non_po_entry/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {

        if ($this->checkAccess('nonpo_entry.approval') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

             $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         
                         $result = $this->Nonpo_model->editPOList($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':


                     
                     break;
                 
                default:
                     break;
             }
                
            }




            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['nonPoList'] = $this->Nonpo_model->getNonPOListSearch($formData);

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();

            
                $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array))
                {

                    $result = $this->Nonpo_model->editPOList($array);
                    redirect($_SERVER['HTTP_REFERER']);
                }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("non_po_entry/approval_list", $this->global, $data, NULL);

        }
    }

    function getData($id) {
        // echo "<Pre>";print_r($id);exit;

        // $financialYearList = $this->Pr_model->financialYearListByStatus('1');
        $vendorList = $this->Pr_model->vendorListByStatus('Approved');

        $prMaster = $this->Pr_model->getMasterPrDetails($id);
        $prDetails = $this->Pr_model->getPrDetails($id);


        $array = $this->Pr_model->financialYearListByStatus('1');

        // $prDetails = $data['prDetails'];
        $financialYearList = json_decode( json_encode($array), true);

        // $vendorList = json_decode( json_encode($vendorList), true);
        // $financialYearList = json_decode( json_encode($array), true);

// echo "<Pre>";print_r($prMaster);exit;




        $table = "

            <script type='text/javascript'>
                $('select').select2();
            </script



                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$prMaster->pr_number'>
                        </div>
                    </div>

                   <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Type <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$prMaster->type'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Entry Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' readonly='readonly' value='$prMaster->pr_entry_date'>
                        </div>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Financial Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->financial_year' readonly='readonly'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Budget Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->budget_year' readonly='readonly'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Department <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$prMaster->department_code - $prMaster->department' readonly='readonly'>
                        </div>
                    </div>

                </div>

                <div class='row'>

                  <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Vendor <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$prMaster->vendor_code - $prMaster->vendor' readonly='readonly'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Description <span class='error-text'>*</span></label>
                            <input type='text' readonly='readonly' class='form-control' name='pr_description'  value='$prMaster->description'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Total Amount <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->amount' readonly='readonly'>
                        </div>
                    </div>

                 </div>

                <div class='row'>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Non-Po Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' name='description'>
                        </div>
                    </div>

                </div>
                ";


                $table1="

            

            <select name='id_vendor' id='id_vendor' class='form-control'>";
            $table1.="<option value=''>Select</option>";

            for($i=0;$i<count($vendorList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $vendorList[$i]->id;
            $code = $vendorList[$i]->code;
            $name = $vendorList[$i]->name;
            $table1.="<option value=".$id.">";

                    if($id == $prMaster->id_vendor)
                    {
                      // echo "selected=selected";
                    }
                    // echo $code . " - " . $name; 

                    $table1.="</option>";

            }
            $table1.="
            </select>
                 </div>
              </div>
            </div>
            ";






        $table.="

        <h3>&nbsp;&nbsp;&nbsp;PR Details </h3>

        <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Debit GL Code</th>
             <th>Credit GL Code</th>
             <th>Category</th>
             <th>Sub Category</th>
             <th>Item</th>
             <th>Tax</th>
             <th>Qty</th>
             <th>Tax Amount</th>
             <th>Price</th>
             <th>Final Total</th>
         </tr>
        </thead>
        <tbody>";
            $total_detail = 0;
          for($i=0;$i<count($prDetails);$i++)
          {
          $j=$i+1; 
            $id = $prDetails[$i]->id;
            $type = $prDetails[$i]->type;
            $dt_account = $prDetails[$i]->dt_account;
            $dt_activity = $prDetails[$i]->dt_activity;
            $dt_department = $prDetails[$i]->dt_department;
            $dt_fund = $prDetails[$i]->dt_fund;
            $cr_account = $prDetails[$i]->cr_account;
            $cr_activity = $prDetails[$i]->cr_activity;
            $cr_department = $prDetails[$i]->cr_department;
            $cr_fund = $prDetails[$i]->cr_fund;
            $category_name = $prDetails[$i]->category_name;
            $sub_category_name = $prDetails[$i]->sub_category_name;
            $item_name = $prDetails[$i]->item_name;
            $tax_code = $prDetails[$i]->tax_code;
            $tax_name = $prDetails[$i]->tax_name;
            $quantity = $prDetails[$i]->quantity;
            $price = $prDetails[$i]->price;
            $tax_price = $prDetails[$i]->tax_price;
            $total_final = $prDetails[$i]->total_final;
            $j=$i+1;
            $table .= "
            <tbody>
            <tr>
                <td>$j</td>
                <td>$dt_fund - $dt_department - $dt_activity - $dt_account</td>
                <td>$cr_fund - $cr_department - $cr_activity - $cr_account</td>
                <td>$category_name</td>
                <td>$sub_category_name</td>
                <td>$item_name</td>
                <td>$tax_code - $tax_name</td>
                <td>$quantity</td>
                <td>$tax_price</td>
                <td>$price</td>
                <td>$total_final</td>
            </tr>";
            $total_detail = $total_detail + $total_final;
         }
         $total_detail = number_format($total_detail, 2, '.', ',');

        $table.="
        <tr>
            <td bgcolor='' colspan='9'></td>
            <td bgcolor=''><b> Total : </b></td>
            <td bgcolor=''><b>".$total_detail."</b></td>
        </tr>

        </tbody>
    </table>
    </div>";
       echo $table;

    }
}
