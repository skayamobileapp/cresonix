<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProcurementCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('procurement_category_model');
        $this->isLoggedIn();
    }

    function list()
    {
         // echo "<Pre>"; print_r("dsada");exit;
        if ($this->checkAccess('procurement_category.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryListSearch($name);
            // echo "<Pre>"; print_r($data['procurementCategoryList']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("procurement_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('procurement_category.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $code;
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $id_user

                );
            
                $result = $this->procurement_category_model->addNewProcurementCategory($data);
                redirect('/procurement/procurementCategory/list');
            }
            
            $this->global['pageTitle'] = 'Inventory Management : Add Procurement Category';
            $this->loadViews("procurement_category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_category.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if ($id == null)
            {
                redirect('/procurement/procurementCategory/list');
            }
            if($this->input->post())
            {

                
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $code;
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );
                
                $result = $this->procurement_category_model->editProcurementCategory($data,$id);
                redirect('/procurement/procurementCategory/list');
            }
            $data['procurementCategory'] = $this->procurement_category_model->getProcurementCategoryDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("procurement_category/edit", $this->global, $data, NULL);
        }
    }

   
}
