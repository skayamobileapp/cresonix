<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Pc extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pc_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('pc.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['pcList'] = $this->pc_model->pcListSearch($name);
            // echo "<Pre>"; print_r($data['pcList']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : List pc Category';
            $this->loadViews("pc/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('pc.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $stock = $this->security->xss_clean($this->input->post('stock'));
            
                $data = array(
                    'code' => $code,
                    'stock'=>$stock,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $id_user

                );
            
                $result = $this->pc_model->addNewpc($data);
                redirect('/procurement/pc/list');
            }
            
            $this->global['pageTitle'] = 'Inventory Management : Add pc Category';
            $this->loadViews("pc/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('pc.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if ($id == null)
            {
                redirect('/procurement/pc/list');
            }
            if($this->input->post())
            {

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $stock = $this->security->xss_clean($this->input->post('stock'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'stock' => $stock,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );
                
                $result = $this->pc_model->editpc($data,$id);
                redirect('/procurement/pc/list');
            }
            $data['pc'] = $this->pc_model->getpcDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit pc Category';
            $this->loadViews("pc/edit", $this->global, $data, NULL);
        }
    }   
}