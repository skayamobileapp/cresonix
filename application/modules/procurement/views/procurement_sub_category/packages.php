 <div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Packages</h3>
    </div>
       
        
        <form id="form_grade" action="" method="post">
<ul class="page-nav-links">
            <li><a href="../edit/<?php echo $subcategoryid;?>">Sub Category</a></li>                
            <li  class="active"><a href="../packages/<?php echo $subcategoryid;?>">Packages</a></li>
        </ul>
        <div class="form-container">
            <h4 class="form-group-title">Packages Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Package <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $indpackage->code;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $indpackage->name;?>">
                    </div>
                </div>

        
            </div>


        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

<div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Package</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($packagesList))
          {
            $i=1;
            foreach ($packagesList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                               <td><a href="/procurement/procurementSubCategory/packages/<?php echo $record->id_sub_category;?>/<?php echo $record->id;?>">Edit</a></td>

              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
