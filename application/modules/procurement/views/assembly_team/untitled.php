<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Product</h3>
        </div>
        <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Product Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="vname" name="vname">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="vemail" name="vemail" >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phone Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="vphone" name="vphone">
                    </div>
                </div>
            
            </div>


            <div class="row">
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address One <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="vaddress_one" name="vaddress_one" >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address Two <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="vaddress_two" name="vaddress_two"  >
                    </div>
                </div>

            
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="vcountry" id="vcountry" class="form-control" onchange="getStateByCountry(this.value)" style="width: 360px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <span id="view_state">
                            <select class="form-control" id='id_state' name='id_state'>
                                    <option value=''></option>
                            </select>
                        </span>
                    </div>
                </div>               

               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mobile Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="vmobile" name="vmobile" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>zipcode / Pincode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="vzipcode" name="vzipcode" >
                    </div>
                </div>

            </div>

                

        </div>
        
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

    $('select').select2();

    CKEDITOR.replace('description',{
      width: "100%",
      height: "300px"

    });



    function getSubcategory(id)
    {
        if(id != '')
        {
            $.get("/procurement/procurementItem/getSubcategory/"+id,
            function(data, status)
            {
                $("#view_subcategory").html(data);
            });
        }
    }

    

    $(document).ready(function() {
        $("#form_vendor").validate({
            rules: {
                vname: {
                    required: true
                },
                 vemail: {
                    required: true
                },
                 vphone: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 vnric: {
                    required: true
                },
                 vmobile: {
                    required: true
                },
                 vaddress_one: {
                    required: true
                },
                 vaddress_two: {
                    required: true
                },
                 vcountry: {
                    required: true
                },
                 vzipcode: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 vstate: {
                    required: true
                },
                 cname: {
                    required: true
                },
                 cphone: {
                    required: true
                },
                 caddress_one: {
                    required: true
                },
                 ccountry: {
                    required: true
                },
                 cemail: {
                    required: true
                },
                 tax_no: {
                    required: true
                },
                 caddress_two: {
                    required: true
                },
                 czipcode: {
                    required: true
                },
                 cstate: {
                    required: true
                }
            },
            messages: {
                vname: {
                    required: "<p class='error-text'>Vendor Person Name Required</p>",
                },
                vemail: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                vphone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                vnric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                vmobile: {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                vaddress_one: {
                    required: "<p class='error-text'>Vendor Address One Required</p>",
                },
                vaddress_two: {
                    required: "<p class='error-text'>Vendor Address One  Required</p>",
                },
                vcountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                vzipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>DOB Required</p>",
                },
                vstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
                cname: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                cphone: {
                    required: "<p class='error-text'>Company Phone Required</p>",
                },
                caddress_one: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                ccountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                cemail: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                tax_no: {
                    required: "<p class='error-text'>Tax Number Required</p>",
                },
                caddress_two: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                czipcode: {
                    required: "<p class='error-text'>Company Zipcode Required</p>",
                },
                cstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>