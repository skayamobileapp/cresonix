<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Shipping Details</h3>
        </div>



        <form id="form_award" action="" method="post" enctype="multipart/form-data">

    


        <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $partnerUniversity->name; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" value="<?php echo $partnerUniversity->contact_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $partnerUniversity->email; ?>" onblur="getPartnerUniversityEmailIdDuplication()">
                    </div>
                </div>



                        <div class="col-sm-4">
                        <div class="form-group">
                            <label>GST IN <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="gst_in" name="gst_in" value="<?php echo $partnerUniversity->gst_in ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>CIN</label>
                            <input type="text" class="form-control" id="cin" name="cin" value="<?php echo $partnerUniversity->cin ?>">
                        </div>
                    </div>



                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $partnerUniversity->address1 ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2</label>
                        <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $partnerUniversity->address2 ?>">
                    </div>
                </div>



           


         


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $partnerUniversity->city ?>">
                    </div>
                </div>

                   <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $partnerUniversity->state ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $partnerUniversity->zipcode ?>">
                    </div>
                </div>
            </div>


        </div>




     


        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="1">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


    </form>






        <!-- <div class="form-container">
            <h4 class="form-group-title"> Partner University Organisation Committee</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Committee</a>
                    </li>                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_comitee" action="" method="post">


                                <br>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Select Role <span class='error-text'>*</span></label>
                                            <select name="role" id="role" class="form-control">
                                                <option value="">Select</option>
                                                <option value="Chancellor">Chancellor</option>
                                                <option value="Vice Chancellor">Vice Chancellor</option>
                                                <option value="Registrar">Registrar</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="com_name" name="com_name">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="com_nric" name="com_nric">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="forintake_has_programmem-group">
                                            <label>Effective Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="effective_date" name="effective_date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">


                                  
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                    </div>
                                </div>



                        </form>


                        <?php

                            if(!empty($comiteeList))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Committee Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Role</th>
                                                 <th>Name</th>
                                                 <th>NRIC</th>
                                                 <th>Effective Date</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($comiteeList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $comiteeList[$i]->role;?></td>
                                                <td><?php echo $comiteeList[$i]->name;?></td>
                                                <td><?php echo $comiteeList[$i]->nric;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($comiteeList[$i]->effective_date));?></td>
                                                <td>
                                                <a onclick="deleteOrganisationConitee(<?php echo $comiteeList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div> 
                    </div>


                </div>

            </div>
        


        </div> -->

    



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script type="text/javascript">

    $('select').select2();


    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });


    function codeConcate()
    {
        var d='_';
        document.getElementById('training_complete_code').value = document.getElementById('code').value + d+document.getElementById('training_code').value;
    }

    

    function validateUniversityData()
    {
        if($('#form_award').valid())
        {
            $('#form_award').submit();
        }
    }



    function getStateByCountry(id)
    {
        // alert(id);
        $.get("/pm/partnerUniversity/getStateByCountry/"+id, function(data, status)
        {
            $("#view_state").html(data);
        });
    }


    function saveData()
    {
        if($('#form_comitee').valid())
        {

        var tempPR = {};
        tempPR['role'] = $("#role").val();
        tempPR['name'] = $("#com_name").val();
        tempPR['nric'] = $("#com_nric").val();
        tempPR['effective_date'] = $("#effective_date").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id;?>;
            $.ajax(
            {
               url: '/pm/partnerUniversity/addComitee',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    function deleteOrganisationConitee(id)
    {
        $.ajax(
            {
               url: '/pm/partnerUniversity/deleteComitee/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    location.reload();
               }
            });
    }


    function getPartnerUniversityCodeDuplication()
    {
      var code = $("#code").val()

      if(code != '')
      {

        var tempPR = {};
        tempPR['code'] = code;
        tempPR['name'] = '';
        tempPR['login_id'] = '';
        tempPR['email'] = '';
        tempPR['id_partner'] = <?php echo $partnerUniversity->id ?>;
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Code Not Allowed, Partner Already Registered With The Given Code : '+ code );
                  $("#code").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityNameDuplication()
    {
      var name = $("#name").val()

      if(name != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = name;
        tempPR['login_id'] = '';
        tempPR['email'] = '';
        tempPR['id_partner'] = <?php echo $partnerUniversity->id ?>;
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Name Not Allowed, Partner Already Registered With The Given Name : '+ name );
                  $("#name").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityLoginIdDuplication()
    {
      var login_id = $("#login_id").val()

      if(login_id != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = '';
        tempPR['login_id'] = login_id;
        tempPR['email'] = '';
        tempPR['id_partner'] = <?php echo $partnerUniversity->id ?>;
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Login ID Not Allowed, Partner Already Registered With The Given Login ID : '+ login_id );
                  $("#login_id").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityEmailIdDuplication()
    {
      var email = $("#email").val()

      if(email != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = '';
        tempPR['login_id'] = '';
        tempPR['email'] = email;
        tempPR['id_partner'] = <?php echo $partnerUniversity->id ?>;
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Email ID Not Allowed, Partner Already Registered With The Given Email ID Id : '+ email );
                  $("#email").val('');
              }
           }
        });
      }
    }


    $(document).ready(function()
    {
        var id_country = <?php echo $partnerUniversity->id_country; ?>;
        // alert(id_country);

        if(id_country > '0')
        {
        // alert(id_country);
            $.get("/pm/partnerUniversity/getStateByCountry/"+id_country, function(data, status)
            {

                var id_state = "<?php echo $partnerUniversity->id_state ?>";
                // alert(id_state);

                $("#view_state").html(data);
                $("#id_state").find('option[value="'+id_state+'"]').attr('selected',true);
                $('select').select2();
            });

        }

        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                short_name: {
                    required: true
                },
                id_country: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                address1: {
                    required: true
                },
                email: {
                    required: true
                },
                status: {
                    required: true
                },
                id_partner_category: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                login_id: {
                    required: true
                },
                password : {
                    required: true
                },
                id_bank : {
                    required: true
                },
                account_number : {
                    required: true
                },
                swift_code: {
                    required: true
                },
                bank_address : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>University Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>University Short Name Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                address1: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                id_partner_category: {
                    required: "<p class='error-text'>Select Partner Category</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner Category</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                login_id: {
                    required: "<p class='error-text'>Login ID Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                account_number: {
                    required: "<p class='error-text'>Account Number Required</p>",
                },
                swift_code: {
                    required: "<p class='error-text'>Swift Code Required</p>",
                },
                bank_address: {
                    required: "<p class='error-text'>Bank Address Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    
</script>
