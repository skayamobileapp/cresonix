<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Shipping Address</h3>
        </div>
        <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Shipping Address Details</h4>



            <div class="row">

                    <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" onblur="getPartnerUniversityNameDuplication()">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" onblur="getPartnerUniversityEmailIdDuplication()">
                    </div>
                </div>



                        <div class="col-sm-4">
                        <div class="form-group">
                            <label>GST IN <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="gst_in" name="gst_in" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>CIN</label>
                            <input type="text" class="form-control" id="cin" name="cin" >
                        </div>
                    </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1">
                    </div>
                </div>

           

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2 </label>
                        <input type="text" class="form-control" id="address2" name="address2">
                    </div>
                </div>

               

          
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>

                   <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="state" name="state">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>
            </div>
        </div>




     
        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
