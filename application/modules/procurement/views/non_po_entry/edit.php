<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View NON-PO Entry</h3>
            </div>


        <div class="form-container">
            <h4 class="form-group-title">Non-PO Entry </h4>
            

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->pr_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="pr_description" name="pr_description" value="<?php echo $nonPoMaster->pr_description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y', strtotime($nonPoMaster->pr_entry_date));?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NON-PO Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->nonpo_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NON-PO Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NON-PO Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y',strtotime($nonPoMaster->nonpo_entry_date));?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->financial_year;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->budget_year;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->department_code . ' - ' . $nonPoMaster->department_name;?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->type;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->amount;?>" readonly="readonly">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->vendor_code . ' - ' . $nonPoMaster->vendor_name; ?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($nonPoMaster->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($nonPoMaster->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($nonPoMaster->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

                 <?php
            if($nonPoMaster->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $nonPoMaster->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div>

        </div>

            <hr>

        <h3>NON-PO Details</h3>

    <div class="form-container">
            <h4 class="form-group-title">Non-PO Details</h4>

        <div class='custom-table'>

            <table class='table' width='100%'>
                <thead>
                     <tr>
                         <th>Sl. No</th>
                         <th>DEBIT GL CODE</th>
                          <th>CREDIT GL CODE</th>
                         <th>Category</th>
                         <th>Sub Category</th>
                         <th>Item</th>
                         <th>Tax</th>
                         <th>Qty</th>
                         <th>Price</th>
                         <th>Tax Amount</th>
                         <th>Final Total</th>
                     </tr>
                </thead>
                <tbody>
                     <?php 
                      $total = 0;
                     for($i=0;$i<count($nonPoDetails);$i++)
                        { 
                        // echo "<Pre>";print_r($nonPoDetails[$i]);exit();

                            ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $nonPoDetails[$i]->dt_fund . " - " . $nonPoDetails[$i]->dt_department . " - " . $nonPoDetails[$i]->dt_activity . " - " . $nonPoDetails[$i]->dt_account;?></td>
                        <td><?php echo $nonPoDetails[$i]->cr_fund . " - " . $nonPoDetails[$i]->cr_department . " - " . $nonPoDetails[$i]->cr_activity . " - " . $nonPoDetails[$i]->cr_account;?></td>
                        <td><?php echo $nonPoDetails[$i]->category_code . " - " . $nonPoDetails[$i]->category_name; ?></td>
                        <td><?php echo $nonPoDetails[$i]->sub_category_code . " - " . $nonPoDetails[$i]->sub_category_name;?></td>
                        <td><?php echo $nonPoDetails[$i]->item_code . " - " . $nonPoDetails[$i]->item_name;?></td>
                        <td><?php echo $nonPoDetails[$i]->tax_code . " - " . $nonPoDetails[$i]->tax_name;?></td>
                        <td><?php echo $nonPoDetails[$i]->quantity;?></td>
                        <td><?php echo $nonPoDetails[$i]->price;?></td>
                        <td><?php echo $nonPoDetails[$i]->tax_price;?></td>
                        <td><?php echo $nonPoDetails[$i]->total_final;?></td>

                         </tr>
                      <?php 
                      $total = $total + $nonPoDetails[$i]->total_final;
                    }
                    $total = number_format($total, 2, '.', ',');
                    ?>

                    <tr>
                        <td bgcolor="" colspan="9"></td>
                        <td bgcolor=""><b> Total : </b></td>
                        <td bgcolor=""><b><?php echo $total; ?></b></td>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>


    <div class="button-block clearfix">
        <div class="bttn-group">
            <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
            <a href="../list" class="btn btn-link">Back</a>
        </div>
    </div>
    
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>