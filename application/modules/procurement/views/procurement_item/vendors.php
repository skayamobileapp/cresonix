<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        


        <ul class="page-nav-links">
            <li><a href="../edit/<?php echo $id_item;?>">Edit Part Number</a></li>                
            <li class="active"><a href="../vendors/<?php echo $id_item;?>">Vendor List</a></li>
        </ul>


        <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Vendor Details</h4>


            <div class="row">


            	<div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <select name="id_vendor" id="id_vendor" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($vendorList))
                            {
                                foreach ($vendorList as $record)
                                {?>
                             <option value="<?php echo $record->id; ?>"
                                <?php 
                                if($record->id == $procurementItemVendor->id_vendor)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="price" name="price" value="<?php echo $procurementItemVendor->price; ?>">
                    </div>
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" onclick="getProductItemVendorDuplication()" class="btn btn-primary btn-lg">Save</button>
                <?php
                if($id_item_vendor != NULL)
                {
                  ?>

                  <a href="<?php echo '../../vendors/'. $id_item ?>" class="btn btn-link">Cancel</a>
                  
                  <?php
                }
                else
                {
                	?>

                  <a href="<?php echo '../list'; ?>" class="btn btn-link">Back</a>

                	<?php
                }
                ?>
            </div>
        </div>
        
        </form>


        <?php

        if(!empty($procurementItemVendorList))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Vendor List</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Vendor</th>
                             <th>Price</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($procurementItemVendorList);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $procurementItemVendorList[$i]->vendor_code . " - " . $procurementItemVendorList[$i]->vendor_name;?></td>
                            <td><?php echo $procurementItemVendorList[$i]->price;?></td>
                            <td class="text-center">
                                <a href='/procurement/procurementItem/vendors/<?php echo $id_item;?>/<?php echo $procurementItemVendorList[$i]->id;?>'>Edit</a> | 
                                <a onclick="deleteProcurementItemHasVendor(<?php echo $procurementItemVendorList[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>

    $('select').select2();


    function getProductItemVendorDuplication()
	{
	if($('#form_main').valid())
	{
	  var id_vendor = $("#id_vendor").val();

	  if(id_vendor != '')
	  {
	    var tempPR = {};
	    tempPR['id_vendor'] = id_vendor;
	    tempPR['id_item'] = "<?php echo $id_item; ?>";
	    tempPR['id'] = "<?php echo $id_item_vendor; ?>";
	    

	    $.ajax(
	    {
	       url: '/procurement/procurementItem/getProductItemVendorDuplication',
	        type: 'POST',
	       data:
	       {
	        tempData: tempPR
	       },
	       error: function()
	       {
	        alert('Something is wrong');
	       },
	       success: function(result)
	       {
	          // alert(result);
	          if(result == '0')
	          {
	              alert('Duplicate Vendors Not Allowed');
	              $("#id_vendor").val('');
	          }
	          else
	          if(result == '1')
	          {
	            $("#form_main").submit();
	          }
	       }
	    });
	  }
	}
	}


    function deleteProcurementItemHasVendor(id)
    {
    	var cnf= confirm('Do you really want to delete?');
      	if(cnf==true)
      	{
	        if(id != '')
	        {
	            $.get("/procurement/procurementItem/deleteProcurementItemHasVendor/"+id,
	            function(data, status)
	            {
                    window.location.reload();
	            });
	        }
    	}
    }


    $(document).ready(function()
    {
        $("#form_main").validate({
            rules: {               
                 id_vendor: {
                    required: true
                },
                 price: {
                    required: true
                }
            },
            messages: {            
                id_vendor: {
                    required: "<p class='error-text'>Select Vendor</p>",
                },
                price: {
                    required: "<p class='error-text'>Price Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    
</script>