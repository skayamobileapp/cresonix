<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Assembly PCB</h3>
    </div>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>PCB Assembly</th>
            <th>Stock</th>
            <th>Customer</th>
            <th>PO Number</th>
            <th>Type</th>
            <th>Date</th>
            <th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($procurementCategoryList))
          {
            $i=1;
                        foreach ($procurementCategoryList as $record) {


          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->qty ?></td>
                <td><?php echo $record->customer_name ?></td>
                <td><?php echo $record->po_number ?></td>
                <td><?php echo $record->type ?></td>
                <td><?php echo date('d-m-Y H:i:s',strtotime($record->pc_date)) ?></td>

              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>