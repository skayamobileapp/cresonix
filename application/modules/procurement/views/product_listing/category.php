<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Procurement Category</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchName; ?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button href="list" class="btn btn-link">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Category</th>
            <th>Name</th>
            <th>Value of Stock in Store</th>
            <th>Excess Stock in Assembly</th>
            <th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($procurementCategoryList))
          {
            $i=1;
            foreach ($procurementCategoryList as $record) {

            $this->load->model('product_listing_model');

            $totalAvailable  = $this->product_listing_model->currentStock($record->id);
             $totalprice = 0;
            for($j=0;$j<count($totalAvailable);$j++) {

                $totalprice =  $totalprice + $totalAvailable[$j]->price * $totalAvailable[$j]->quantity;
            }


             $totalAvailableexcess  = $this->product_listing_model->currentexcessStock($record->id);
             $totalpriceexcess = 0;
            for($j=0;$j<count($totalAvailableexcess);$j++) {

                $totalpriceexcess =  $totalpriceexcess + $totalAvailableexcess[$j]->price * $totalAvailableexcess[$j]->quantity;
            }


          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $totalprice;?></td>
                <td><?php echo $totalpriceexcess;?></td>
               
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>