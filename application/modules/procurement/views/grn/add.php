<?php $this->load->helper("form"); ?>
<form id="form_po_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add GRN</h3>
        </div>

    <div class="form-container">
            <h4 class="form-group-title">GRN Entry</h4>

         <div class="row">
            <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Number <span class='error-text'>*</span></label>
                        <select name='id_po' id='id_po' class='form-control' onChange="getDetails(this.value)">
                            <option value=''>Select</option>

                            <?php for($i=0;$i<count($poPendingList);$i++)
                            {
                                ?>
                                <option value="<?php echo $poPendingList[$i]->id;?>">
                                <?php echo $poPendingList[$i]->po_number . " - " . $poPendingList[$i]->name;?>  
                                </option>
                                <?php
                            } ?> 
                        </select>
                    </div>
            </div>
        </div>


        <div id="view">
        </div>

    </div>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>
        
    <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>


</form>
<script>

    function getDetails(id)
    {
        if(id > 0)
        {
            // alert(id);
       
            $.ajax(
            {
               url: '/procurement/grn/getData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });   
        }
    }   

   $(document).ready(function() {
        $("#form_po_entry").validate({
            rules: {
                id_po: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                id_po: {
                    required: "<p class='error-text'>Select PO Number",
                },
                description: {
                    required: "<p class='error-text'>Enter GRN Description</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>

<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>
<script type="text/javascript">
    $('select').select2();
</script>