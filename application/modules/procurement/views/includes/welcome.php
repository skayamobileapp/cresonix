<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <!-- <div class="page-title clearfix">
            <h3>Welcome : Module CURRICULLUM MANAGEMENT</h3>
        </div> -->
        
        <div class="welcome-container">
            <img src="<?php echo BASE_PATH; ?>assets/img/system_setup_icon.svg" alt="Partner Management Module">
            <h3>Welcome to <br/><strong>Cresonix Inventory</strong></h3>
        </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>