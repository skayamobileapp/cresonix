<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Assembly PCB</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Assembly PCB Details</h4>

            <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Assembly Distributor <span class='error-text'>*</span></label>
                        <select name="id_assembly_distributor" id="id_assembly_distributor" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($assemblyDistributionList))
                            {
                                foreach ($assemblyDistributionList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->reference_number.'-'.$record->assembly_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>                  
            
              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Assembeled PCB Number <span class='error-text'>*</span></label>
                        <select name="id_pc" id="id_pc" class="form-control" onchange="getSubcategory(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($pcList))
                            {
                                foreach ($pcList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                  <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>                  
            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Qty <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="qty" name="qty">
                    </div>
                </div>

           
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
