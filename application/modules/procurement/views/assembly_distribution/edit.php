<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="assemblyDistributionst">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Assembly Distribution Entry</h3>
            </div>
            

        <div class="form-container">
            <h4 class="form-group-title">Assembly Distribution</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="assemblyDistribution_number" name="assemblyDistribution_number" value="<?php echo $assemblyDistribution->reference_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assemblyDistribution->description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="created_dt_tm" name="created_dt_tm" value="<?php echo date('d-m-Y H:i:s',strtotime($assemblyDistribution->created_dt_tm));?>" readonly="readonly">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="id_vendor" name="id_vendor" value="<?php echo $assemblyDistribution->assembly_code . ' - ' . $assemblyDistribution->assembly_name; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="<?php echo $assemblyDistribution->total_amount;?>" readonly="readonly">
                    </div>
                </div>


                    <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <select name='status' id='status' class='form-control'>
                            <option value=''>Select</option>
                            <option value='Active' <?php if($assemblyDistribution->status==''){ echo "selected=selected"; } ?>>Active</option>
                            <option value='Cancelled' <?php if($assemblyDistribution->status==''){ echo "selected=selected"; } ?>>Cancelled</option>
                            <option value='Partially Supplied' <?php if($assemblyDistribution->status==''){ echo "selected=selected"; } ?>>Partially Supplied</option>
                            <option value='Closed' <?php if($assemblyDistribution->status==''){ echo "selected=selected"; } ?>>Closed</option>

                         
                        </select>
                    </div>
                </div>



            </div>           

            <!-- <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($assemblyDistribution->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($assemblyDistribution->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($assemblyDistribution->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            <?php
            if($assemblyDistribution->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $assemblyDistribution->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div> -->

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
          <div class="page-title clearfix">
                <a href="<?php echo '/procurement/assemblyDistribution/generatePO/'.$adid ?>" target="_blank" class="btn btn-link btn-back">
                    Download AD >>></a>
        </div>

        <hr>

        <h3>Assembly Distribution Details</h3>

        <div class="form-container">
            <h4 class="form-group-title">Assembly Distribution Details</h4>


            <div class="custom-table">
                <table class="table">
                    <thead>
                         <tr>
                             <th>Sl. No</th>
                             <th>PartNumber / Value</th>

                             <th>Sub Category</th>
                             <th>Package</th>

                             <th>Issue Quantity</th>
                             <th>Excess Quantity</th>
                             <th>Returned Quantity</th>
                             <th>Balance Quantity</th>
                            
                         </tr>
                    </thead>
                    <tbody>
                         <?php 
                          $total = 0;
                         for($i=0;$i<count($assemblyDistributionDetails);$i++)
                            { 
                            // echo "<Pre>";print_r($assemblyDistributionDetails[$i]);exit();

                                ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                           <td><?php echo $assemblyDistributionDetails[$i]->item_name;?></td>

                            <td><?php echo $assemblyDistributionDetails[$i]->sub_category_code;?></td>

                           <td><?php echo $assemblyDistributionDetails[$i]->packagename;?></td>

                            <td><?php echo $assemblyDistributionDetails[$i]->quantity;?> - <?php echo $assemblyDistributionDetails[$i]->unitname;?></td>

                               <td><?php echo $assemblyDistributionDetails[$i]->excess_quantity;?> - <?php echo $assemblyDistributionDetails[$i]->unitname;?></td>

                            <td><?php echo $assemblyDistributionDetails[$i]->received_quantity;?> - <?php echo $assemblyDistributionDetails[$i]->unitname;?></td>
                            <td><?php echo $assemblyDistributionDetails[$i]->balance_quantity;?> - <?php echo $assemblyDistributionDetails[$i]->unitname;?></td>
                          

                             </tr>
                          <?php 
                          $total = $total + $assemblyDistributionDetails[$i]->total_price;
                        }
                        $total = number_format($total, 2, '.', ',');
                        ?>

                      
                    </tbody>
                </table>
            </div>

        </div>

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>