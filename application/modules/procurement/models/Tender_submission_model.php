<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tender_submission_model extends CI_Model
{

    function getTenderSubmissionListSearch($data)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, pr.pr_number, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('tender_quotation as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        $this->db->join('pr_entry as pr','po.id_pr = pr.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(po.description  LIKE '%" . $data['name'] . "%' or po.reason  LIKE '%" . $data['name'] . "%' or po.quotation_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['department_code'] !='')
        {
            $this->db->where('po.department_code', $data['department_code']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('po.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('po.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('po.status', $data['status']);
        }
        if ($data['submitted'] !='')
        {
            $this->db->where('po.is_submitted', $data['submitted']);
        }
        if ($data['shortlisted'] !='')
        {
            $this->db->where('po.is_shortlisted', $data['shortlisted']);
        }
        if ($data['awarded'] !='')
        {
            $this->db->where('po.is_awarded', $data['awarded']);
        }
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function tenderQuotationListForSubmission()
    {
        $this->db->select('po.*');
        $this->db->from('tender_quotation as po');
        $this->db->where('po.status', '1');
        $this->db->where('po.is_submitted', '0');
        $this->db->where('po.is_shortlisted', '0');
        $this->db->where('po.is_awarded', '0');
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function addTenderSubmissionVendor($data)
    {
        $this->db->trans_start();
        $this->db->insert('tender_submission_vendor', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateTenderQuotation($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('tender_quotation', $data);
        return TRUE;
    }


    function checkDuplicateSubmission($id_vendor,$id_tender_quotation)
    {
        $this->db->select('po.*');
        $this->db->from('tender_submission_vendor as po');
        $this->db->where('po.id_tender_quotation', $id_tender_quotation);
        $this->db->where('po.id_vendor', $id_vendor);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }

    function tenderQuotationListForShortlist()
    {
        $this->db->select('po.*');
        $this->db->from('tender_quotation as po');
        $this->db->where('po.status', '1');
        $this->db->where('po.is_submitted', '1');
        $this->db->where('po.is_shortlisted', '0');
        $this->db->where('po.is_awarded', '0');
        $likeCriteria = "(date(po.end_date)  < '" . date('Y-m-d') . "')";
        $this->db->where($likeCriteria);
         $query = $this->db->get();
         // echo "<Pre>"; print_r($this->$db);exit();
         $result = $query->result();   
         return $result;
    }

    function tenderQuotationListForAward()
    {
        $this->db->select('po.*');
        $this->db->from('tender_quotation as po');
        $this->db->where('po.status', '1');
        $this->db->where('po.is_submitted', '1');
        $this->db->where('po.is_shortlisted', '1');
        $this->db->where('po.is_awarded', '0');
        $likeCriteria = "(date(po.end_date)  < '" . date('Y-m-d') . "')";
        $this->db->where($likeCriteria);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }


    function getTenderSubmissionForTendor($id)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code');
        $this->db->from('tender_submission_vendor as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->where('po.id_tender_quotation', $id);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function updateTenderSubmissionVendor($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('tender_submission_vendor', $data);
        return TRUE;
    }

    function getTenderSubmissionForTendorShortlisted($id)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code');
        $this->db->from('tender_submission_vendor as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->where('po.id_tender_quotation', $id);
        $this->db->where('po.is_shortlisted','1');
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getTenderSubmissionForTendorAward($id)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code');
        $this->db->from('tender_submission_vendor as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->where('po.id_tender_quotation', $id);
        $this->db->where('po.is_shortlisted', '1');
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function tenderSubmissionVendorListForAward($id)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code');
        $this->db->from('tender_submission_vendor as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->where('po.id_tender_quotation', $id);
        $this->db->where('po.is_awarded', '1');
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }






























    function generateQuotationNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('tender_quotation');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $generate_number = $number = "TQ" .(sprintf("%'06d", $count)). "/" . $Year;
           return $generate_number;        
    }

    function addTenderQuotation($data) {
        $this->db->trans_start();
        $this->db->insert('tender_quotation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewTenderQuotationDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('tender_quotation_detail', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

     function editTenderQuotation($array)
    {
      $status = ['status'=>'1'];
      $this->db->where_in('id', $array);
      $this->db->update('tender_quotation', $status);
      // $this->db->set('status', $status);
    }


    

    function getTenderQuotaion($id)
    {
        $this->db->select('po.*, pr.pr_entry_date, pr.description as pr_description, pr.pr_number, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, pr.pr_number, d.name as department_name, d.code as department_code');
        $this->db->from('tender_quotation as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('pr_entry as pr','po.id_pr = pr.id');
        $this->db->join('department as d','po.id_department = d.id');
        $this->db->where('po.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }

    function getTenderQuotationDetailsByMasterId($id)
    {

         $this->db->select('pe.*');
        $this->db->from('tender_quotation as pe');
        $this->db->where('pe.id', $id);
        $query = $this->db->get();
        $data = $query->row();

        $type = $data->type;



         $this->db->select('tpe.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code,  pi.description as item_name, pi.code as item_code, ac.code as dt_account, actc.code as dt_activity, dc.code as dt_department, fc.code as dt_fund,  cac.code as cr_account, cactc.code as cr_activity, cdc.code as cr_department, cfc.code as cr_fund, tx.code as tax_code, tx.name as tax_name');
        // $this->db->from('pr_detail');
        $this->db->from('tender_quotation_detail as tpe');
        $this->db->join('account_code as ac', 'tpe.dt_account = ac.id');
        $this->db->join('activity_code as actc', 'tpe.dt_activity = actc.id');
        $this->db->join('department_code as dc', 'tpe.dt_department = dc.id');
        $this->db->join('fund_code as fc', 'tpe.dt_fund = fc.id');
        $this->db->join('account_code as cac', 'tpe.dt_account = cac.id');
        $this->db->join('activity_code as cactc', 'tpe.dt_activity = cactc.id');
        $this->db->join('department_code as cdc', 'tpe.dt_department = cdc.id');
        $this->db->join('fund_code as cfc', 'tpe.dt_fund = cfc.id');
        $this->db->join('tax as tx', 'tpe.id_tax = tx.id');
        if($type == 'Procurement')
        {

            $this->db->join('procurement_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('procurement_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('procurement_item as pi', 'tpe.id_item = pi.id');

        }
        elseif($type == 'Asset')
        {
             $this->db->join('asset_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('asset_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('asset_item as pi', 'tpe.id_item = pi.id');

        }
        $this->db->where('tpe.id_tender_quotation', $id);
        // $this->db->where('id_pr', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffList()
    {
         $this->db->select('pe.*');
        $this->db->from('staff as pe');
        $this->db->where('pe.status', '1');
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function addTempComitee($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_tender_commitee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addTenderComitee($data)
    {
        $this->db->trans_start();
        $this->db->insert('tender_commitee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempComiteeBySession($id_session)
    {
        $this->db->select('pe.*, st.salutation, st.name as staff_name, st.ic_no as ic_no');
        $this->db->from('temp_tender_commitee as pe');
        $this->db->join('staff as st', 'pe.id_staff = st.id');
        $this->db->where('pe.id_session', $id_session);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function addTempRemarks($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_tender_remarks', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addTenderRemarks($data)
    {
        $this->db->trans_start();
        $this->db->insert('tender_remarks', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempRemarksBySession($id_session)
    {
        $this->db->select('pe.*');
        $this->db->from('temp_tender_remarks as pe');
        $this->db->where('pe.id_session', $id_session);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function deleteTempComitee($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_tender_commitee');
        return TRUE;
    }

    function deleteTempComiteeBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_tender_commitee');
        return TRUE;
    }

    function deleteTempRemarks($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_tender_remarks');
        return TRUE;
    }

    function deleteTempRemarksBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_tender_remarks');
        return TRUE;
    }

    function moveSessionTempData($id_session,$id_tender_quotation)
    {
        $this->db->select('pe.*');
        $this->db->from('temp_tender_commitee as pe');
        $this->db->where('pe.id_session', $id_session);
        $query = $this->db->get();
        $data_comitee = $query->result();

        $this->db->select('pe.*');
        $this->db->from('temp_tender_remarks as pe');
        $this->db->where('pe.id_session', $id_session);
        $query1 = $this->db->get();
        $data_remarks = $query1->result();


        if($data_comitee)
        {
            foreach ($data_comitee as $value)
            {
                unset($value->id);
                unset($value->id_session);
                $value->id_tender = $id_tender_quotation;
                $inserted_comitee = $this->addTenderComitee($value);
                // echo "<Pre>";print_r($inserted_comitee);exit();
            }

            $session_cleared_comitee = $this->deleteTempComiteeBySession($id_session);
        }

        if($data_remarks)
        {
            foreach ($data_remarks as $remarks)
            {
                unset($remarks->id);
                unset($remarks->id_session);

                $remarks->id_tender = $id_tender_quotation;
                $inserted_remarks = $this->addTenderRemarks($remarks);
                // echo "<Pre>";print_r($inserted_remarks);exit();
            }

             $session_cleared_remarks = $this->deleteTempRemarksBySession($id_session);
        }
        return TRUE;
    }


    function getTenderComiteeDetailsByMasterId($id_tender)
    {
        $this->db->select('pe.*, st.salutation, st.name as staff_name, st.ic_no as ic_no');
        $this->db->from('tender_commitee as pe');
        $this->db->join('staff as st', 'pe.id_staff = st.id');
        $this->db->where('pe.id_tender', $id_tender);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function getTenderRemarksDetailsByMasterId($id_tender)
    {
        $this->db->select('pe.*');
        $this->db->from('tender_remarks as pe');
        $this->db->where('pe.id_tender', $id_tender);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }
}
