<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pc_outgoing_model extends CI_Model
{
    function pcList()
    {
        $this->db->select('*');
        $this->db->from('pc_outgoing');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function pcListSearch($search)
    {
        $this->db->select('pci.*,p.name as aspname');
        $this->db->from('pc_outgoing as pci');
        $this->db->join('pc as p', 'p.id = pci.id_pc','left');

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getpcDetails($id)
    {
        $this->db->select('*');
        $this->db->from('pc_outgoing');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewpc($data)
    {
        $this->db->trans_start();
        $this->db->insert('pc_outgoing', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editpc($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('pc_outgoing', $data);
        return TRUE;
    }
}