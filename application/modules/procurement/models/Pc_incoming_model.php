<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pc_incoming_model extends CI_Model
{
    function pcList()
    {
        $this->db->select('*');
        $this->db->from('pc_incoming');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function pcListSearch($search)
    {
        $this->db->select('pci.*,ad.reference_number,at.name,p.name as aspname');
        $this->db->from('pc_incoming as pci');
               $this->db->join('assembly_distribution as ad', 'pci.id_assembly_distribution = ad.id');
        $this->db->join('assembly_team as at', 'at.id = ad.id_assembly','left');
        $this->db->join('pc as p', 'p.id = pci.id_pc','left');

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getpcDetails($id)
    {
        $this->db->select('*');
        $this->db->from('pc_incoming');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewpcIncoming($data)
    {
        $this->db->trans_start();
        $this->db->insert('pc_incoming', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

     function addNewpcsumary($data)
    {
        $this->db->trans_start();
        $this->db->insert('pc_summary', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editpc($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('pc_incoming', $data);
        return TRUE;
    }
}