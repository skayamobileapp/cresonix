<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product_listing_model extends CI_Model
{

    function procurementCategoryList()
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getAvgPrice($id) {
         $this->db->select('*');
        $this->db->from('procurement_item');
                $this->db->where('id', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function returnFromAssemblyTeam($id){
        
        $this->db->select('sum(quantity) as totalpurchased');
        $this->db->from('assembly_return_details');
                $this->db->where('id_item', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function currentexcessStock($id){
         $this->db->select('psc.*');
        $this->db->from('assembly_distribution_details as psc');
                $this->db->where('id_category', $id);
                $this->db->where("id_assembly_return_detail='0'");

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function currentStock($id){
        $this->db->select('psc.*');
        $this->db->from('procurement_item as psc');
                $this->db->where('id_procurement_category', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function givenToAssemblyTeam($id){
        
        $this->db->select('sum(quantity) as totalpurchased');
        $this->db->from('assembly_distribution_details');
                $this->db->where('id_item', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function givenToAssemblyTeamExcess($id){
        
        $this->db->select('sum(excess_quantity) as totalpurchased');
        $this->db->from('assembly_distribution_details');
                $this->db->where('id_item', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getWriteoff($id){
        
        $this->db->select('sum(write_off) as totalpurchased');
        $this->db->from('assembly_distribution_details');
                $this->db->where('id_item', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getPurchasedItems($id) {
        $this->db->select('sum(quantity) as totalpurchased');
        $this->db->from('grn_details');
                $this->db->where('id_item', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function procurementSubCategoryList()
    {
        $this->db->select('psc.*, pc.code as pr_category_code, pc.name as pr_category_name');
        $this->db->from('procurement_sub_category as psc');
        $this->db->join('procurement_category as pc', 'psc.id_procurement_category = pc.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }
    
    function procurementItemListSearch($formData)
    {
        $this->db->select('pi.*, pc.name as pr_category_name, pc.code as pr_category_code, psc.name as pr_sub_category_name, psc.code as pr_sub_category_code,pack.name as packagename,u.name as unitname');
        $this->db->from('procurement_item as pi');
        $this->db->join('procurement_category as pc', 'pi.id_procurement_category = pc.id');
        $this->db->join('packages as pack', 'pi.id_package = pack.id','left');
        $this->db->join('unit as u', 'pi.id_unit = u.id','left');
        $this->db->join('procurement_sub_category as psc', 'pi.id_procurement_sub_category = psc.id');
        if($formData['id_procurement_category'])
        {
            $this->db->where('pi.id_procurement_category', $formData['id_procurement_category']);
        }
        if($formData['id_procurement_sub_category'])
        {
            $this->db->where('pi.id_procurement_sub_category', $formData['id_procurement_sub_category']);
        }
        if($formData['name'])
        {
            $likeCriteria = "(pi.name  LIKE '%" . $formData['name'] . "%' or pi.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         // print_r($formData);exit();     
         return $result;
    }

    function getProcurementItem($id)
    {
        $this->db->select('pi.*, pc.name as pr_category_name, pc.code as pr_category_code, psc.name as pr_sub_category_name, psc.code as pr_sub_category_code, m.code as manufacturer_code, m.name as manufacturer_name');
        $this->db->from('procurement_item as pi');
        $this->db->join('procurement_category as pc', 'pi.id_procurement_category = pc.id');
        $this->db->join('procurement_sub_category as psc', 'pi.id_procurement_sub_category = psc.id');
        $this->db->join('manufacturer as m', 'pi.manufacturer = m.id');
        $this->db->where('pi.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function getProductQuantityListByIdItem($id_item)
    {
        $this->db->select('pq.*, usr.name as creater_name, g.grn_number, ad.reference_number as assembly_number, ar.reference_number as return_number, de.name as description,
            v.name as vendorname,at.name as assemblydiname,art.name as assemblyretname');
        $this->db->from('product_quantity as pq');
        $this->db->join('users as usr', 'pq.created_by = usr.id');
        $this->db->join('grn as g', 'pq.id_grn = g.id','left');
        $this->db->join('vendor_details as v', 'g.id_vendor = v.id','left');
        $this->db->join('assembly_distribution as ad', 'pq.id_assembly = ad.id','left');
        $this->db->join('assembly_team as at', 'ad.id_assembly = at.id','left');
        $this->db->join('assembly_return as ar', 'pq.id_assembly_return = ar.id','left');
        $this->db->join('assembly_team as art', 'ar.id_assembly = art.id','left');

        $this->db->join('description as de', 'pq.id_description = de.id','left');
        // $this->db->join('grn_details as gd', 'pq.id_grn_detail = gd.id');
        $this->db->where('pq.id_item', $id_item);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    
}