<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Procurement_limit_model extends CI_Model
{
    function procurementLimitList()
    {
        $this->db->select('*');
        $this->db->from('procurement_limit');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementLimitListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('procurement_limit');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getProcurementLimit($id)
    {
        $this->db->select('*');
        $this->db->from('procurement_limit');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProcurementLimit($data)
    {
        $this->db->trans_start();
        $this->db->insert('procurement_limit', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProcurementLimit($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_limit', $data);
        return TRUE;
    }
}

