<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Packages_model extends CI_Model
{
    function packagesList()
    {
        $this->db->select('*');
        $this->db->from('packages');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function packagesListSearch($id)
    {
        $this->db->select('*');
        $this->db->from('packages');

        $this->db->where('id_sub_category', $id);

         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getPackagesDetails($id)
    {
        $this->db->select('*');
        $this->db->from('packages');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPackages($data)
    {
        $this->db->trans_start();
        $this->db->insert('packages', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPackages($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('packages', $data);
        return TRUE;
    }
}

