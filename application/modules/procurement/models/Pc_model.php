<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pc_model extends CI_Model
{
    function pcList()
    {
        $this->db->select('*');
        $this->db->from('pc');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function pcListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('pc');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getpcDetails($id)
    {
        $this->db->select('*');
        $this->db->from('pc');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewpc($data)
    {
        $this->db->trans_start();
        $this->db->insert('pc', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editpc($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('pc', $data);
        return TRUE;
    }
}