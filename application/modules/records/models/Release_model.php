<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Release_model extends CI_Model
{
    
    function releaseList()
    {
        $this->db->select('b.*, stu.first_name as student, sem.name as semester, bt.name as barring_type');
        $this->db->from('release as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('barring_type as bt', 'b.id_barring_type = bt.id');
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function releaseListSearch($formData)
    {
        $this->db->select('b.*, stu.first_name as student, sem.name as semester, bt.name as barring_type');
        $this->db->from('release as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('barring_type as bt', 'b.id_barring_type = bt.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_semester']))
        {
            $likeCriteria = "(b.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_student']))
        {
            $likeCriteria = "(b.id_student  LIKE '%" . $formData['id_student'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_barring_type']))
        {
            $likeCriteria = "(b.id_barring_type  LIKE '%" . $formData['id_barring_type'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getRelease($id)
    {
        $this->db->select('*');
        $this->db->from('release');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }
    

    function addNewRelease($data)
    {
        $this->db->trans_start();
        $this->db->insert('release', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editRelease($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('release', $data);

        return TRUE;
    }

    function deleteBarring($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('release', $data);

        return $this->db->affected_rows();
    }

    function studentList()
    {
        $this->db->select('*, first_name as name');
        // $this->db->from('student');
        $this->db->from('student');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }
}
