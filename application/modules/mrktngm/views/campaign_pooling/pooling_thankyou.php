<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Attendance Already Recorded</h3>
        </div>
        <form id="form_release" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Attendance Recorded</h4>  



                <h3 class="header-title">
                    Welcome to Campaign Attendance <span> <b><?php echo $campaign->name . " From " . date('d-m-Y',strtotime($campaign->start_date)) . " To " . date('d-m-Y',strtotime($campaign->end_date)) ?></b></span>
                </h3>






        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($student->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $student->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $student->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $student->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $student->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $student->programme_code . " - " . $student->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $student->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $student->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>


            </div>



            <h3 class="section-title">Title : <?php echo $campaign->name ?></h3>
            <!-- <img src="<?php echo BASE_PATH; ?>assets/pooling/img/online-voting.svg" alt="Thank you for voting"> -->
            

            <h3 class="section-title"><?php echo $message . ' ,<br> On ' . date('d-m-Y h:i:s A', strtotime($campaignPooling->pooling_date_time)) ?></h3>




            </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>