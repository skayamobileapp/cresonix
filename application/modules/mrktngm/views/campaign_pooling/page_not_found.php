<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AEU Skills</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-default main-header">
        <div class="container">
            <div class="navbar-header">
                <div class="main-logos">
                </div>
            </div>
            <h3 class="header-title">
                Welcome to Election Voting Portal<span> No User String Detected</span>
            </h3>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container main-container">
        <div class="thankyou-container">
        <h3 class="section-title">Title : User Seems To Not Authorized To View This Page..</h3>
            <h3 class="section-title"><?php echo $message ?></h3>        
        </div>
    </div>




    <footer class="footer-wrapper">
        <div class="container text-center">
            <p>&copy; AEU</p>
        </div>
    </footer>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Profile Details</h4>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias facilis voluptates, consequatur aut optio corporis magnam? Ea consequatur repellendus quaerat similique, dolorem, iusto consequuntur assumenda ad illum saepe earum sapiente.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias facilis voluptates, consequatur aut optio corporis
                    magnam? Ea consequatur repellendus quaerat similique, dolorem, iusto consequuntur assumenda ad illum saepe earum
                    sapiente.</p>
            </div>
        </div>
    </div>
</div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.steps.js"></script>
    <script src="js/main.js"></script>
</body>

</html>