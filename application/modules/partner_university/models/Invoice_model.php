<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_model extends CI_Model
{

    function getMainInvoiceListSearch($data)
    {

        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('programme as p', 'mi.id_program = p.id','left');
        // $this->db->join('intake as i', 'mi.id_intake = i.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);

        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('mi.id_intake', $data['id_intake']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        if ($data['id_partner_university'] != '')
        {
            $this->db->where('mi.id_student', $data['id_partner_university']);
        }
        $this->db->where("mi.type !=", "Sponsor");
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function partnerUniversityListByStatus($status)
    {
        $this->db->select('fc.*');
        $this->db->from('partner_university as fc');
        $this->db->order_by("fc.status", $status);
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addPartnerStudentInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_invoice_student_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPartnerStudentInvoiceDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('partner_university_invoice_student_details', $data);
        return TRUE;
    }

    function getMainInvoiceStudentDetails($idinvoice,$stu_or_app)
    {
        $this->db->select('mid.*, s.full_name as student_name, s.nric, s.email_id, s.phone, p.code as program_code, p.name as program_name, qs.short_name as qualification_code, qs.name as qualification_name, i.year as intake_year, i.name as intake_name ');
        $this->db->from('partner_university_invoice_student_details as mid');
        if($stu_or_app > 0)
        {
            $this->db->join('applicant as s', 'mid.id_student = s.id');
        }else
        {
            $this->db->join('student as s', 'mid.id_student = s.id');
        }
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        // $this->db->join('staff as adt', 's.id_advisor = adt.id','left');
        $this->db->where('mid.id_main_invoice', $idinvoice);
        $query = $this->db->get();
        return $query->result();
    }

    function studentSearch($data)
    {
        $this->db->select('s.*, p.code as program_code, p.name as program_name, qs.short_name as qualification_code, qs.name as qualification_name, i.year as intake_year, i.name as intake_name, fsm.code as fee_structure_code, fsm.name as fee_structure_name');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('fee_structure_master as fsm', 's.id_fee_structure = fsm.id','left');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $this->db->where('s.email_id', $data['email_id']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('s.id_program', $data['id_program']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('s.id_intake', $data['id_intake']);
        }
        if ($data['id_partner_university'] != '')
        {
            $this->db->where('s.id_university', $data['id_partner_university']);
        }
        // if ($data['id_qualification'] != '')
        // {
        //     $this->db->where('s.id_degree_type', $data['id_qualification']);
        // }
        // if($data['tagging_status'] != '')
        // {
        //     if($data['tagging_status'] == 1)
        //     {
        //         $this->db->where('s.id_advisor !=','0');
        //     }
        //     elseif($data['tagging_status'] == 0)
        //     {
        //         $this->db->where('s.id_advisor',$data['tagging_status']);
        //     }
        // }
        // if ($data['id_semester'] != '')
        // {
        //     $this->db->where('s.id_semester', $data['id_semester']);
        // }
        $this->db->where('s.applicant_status', 'Migrated');
        $this->db->where('s.is_invoice_generated', '0');
        // $this->db->where('qs.name =', 'Master');
        $query = $this->db->get();
        $result = $query->result(); 
        // echo "<pre>";print_r($query);die;

        return $result;
    }


    function getApplicantDetailsById($id_applicant)
    {
        $this->db->select('app.*');
        $this->db->from('applicant as app');
        $this->db->where('app.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getfeeStructureMasterByApplicant($id_applicant)
    {
        $applicant = $this->getApplicantDetailsById($id_applicant);

        $id_university = $applicant->id_university;
        
        // echo "<Pre>";print_r($id_university);exit();

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        $this->db->where('id_partner_university', $applicant->id_university);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();

        // echo "<Pre>";print_r($result);exit();


        if($result)
        {
            $id_fee_structure = $result->id;
            
            if($id_university == 1)
            { 
                $nationality = $applicant->nationality;
                $currency = 'MYR';

                if($nationality == 'Malaysian')
                {
                    $currency = 'MYR';
                }
                else
                {
                    $currency = 'USD';
                }

                // echo "<Pre>";print_r($nationality);exit();

                $fee_structure = $this->getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure,$currency);
                
                // echo "<Pre>";print_r($fee_structure);exit();
                
                return $fee_structure;
            }
            else
            {
                $fee_structure = $this->getfeeStructureDetailsByIdFeeStructureMasterForPartneruniversity($id_fee_structure,$id_university);

                $details = array();

                foreach ($fee_structure as $value)
                {
                    // echo "<Pre>";print_r($value);exit();
                    $is_installment = $value->is_installment;
                    $data['id_fee_structure'] = $value->id;
                    $data['id_fee_structure_master'] = $value->id_program_landscape;

                    if($is_installment == 1)
                    {
                        $total_amount = 0;

                        $installment_details = $this->getTrainingCenterInstallmentDetails($data);

                        foreach ($installment_details as $detail)
                        {
                           $total_amount = $total_amount + $detail->amount; 
                        }

                        $value->amount = $total_amount;
                    }

                    array_push($details, $value);
                }

                return $details;
            }
        }
        else
        {
            return array();
        }
    }


    function getMainInvoice($id)
    {
        $this->db->select('mi.*,  p.name as programme_name, p.code as programme_code, i.name as intake_name, i.year as intake_year');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('intake as i', 'mi.id_intake = i.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup, fm.code as frequency_mode, amt.code as amount_calculation_type');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->join('frequency_mode as fm', 'fstp.id_frequency_mode = fm.id');        
        $this->db->join('amount_calculation_type as amt', 'fstp.id_amount_calculation_type = amt.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewPartnerUniversityInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_student_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }


    function getPartnerUniversity($id)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getApplicantByApplicantId($id_applicant)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getMainInvoicePartnerUniversityData($id_partner_university)
    {
        $this->db->select('stu.name as full_name, stu.code as nric');
        $this->db->from('partner_university as stu');
        $this->db->where('stu.id', $id_partner_university);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant', $data);
        return TRUE;
    }

    function createNewMainInvoiceForStudent($id,$id_pu_invoice_student_details,$id_invoice)
    {
        $id_student = $id;
        $applicant_data = $this->getApplicantByApplicantId($id);

        $id_applicant = $applicant_data->id;
        $id_branch = $applicant_data->id_branch;
        $id_university = $applicant_data->id_university;
        $id_fee_structure = $applicant_data->id_fee_structure;

        if($id_fee_structure == 0)
        {
            $id_fee_structure = $this->getFeeStructureMasterIdByData($applicant_data);
        }


        $id_program = $applicant_data->id_program;
        $id_intake = $applicant_data->id_intake;
        $nationality = $applicant_data->nationality;
        $id_program_scheme = $applicant_data->id_program_scheme;
        $id_program_has_scheme = $applicant_data->id_program_has_scheme;
        $id_program_landscape = $applicant_data->id_program_landscape;
        $is_sibbling_discount = $applicant_data->is_sibbling_discount;
        $is_employee_discount = $applicant_data->is_employee_discount;
        $is_alumni_discount = $applicant_data->is_alumni_discount;

        // echo "<Pre>";print_r($id_program_landscape);exit;


        $is_installment = 0;
        $installments = 0;
        $currency = 'USD';

        // echo "<Pre>";print_r($detail_data);exit;


        if($id_university > 1)
        {
            $currency = 'USD';
            $detail_data = $this->getFeeStructureByTrainingCenterForInvoiceGeneration($id_program,$id_intake,$id_fee_structure,$id_university);
        }

        // echo "<Pre>";print_r($detail_data);exit;
        // echo "<Pre>";print_r($is_employee_discount);exit;


        if(!empty($detail_data))
        {

            if($id_invoice)
            {
                // echo "<Pre>";print_r($id_invoice);exit;
                $applicant_data_update['is_invoice_generated'] = $id_invoice;
                $updated_applicant = $this->editApplicantDetails($applicant_data_update,$id_student);
            }



            $total_amount = 0;


            // echo "<Pre>";print_r($detail_data);exit;

            foreach ($detail_data as $fee_structure)
            {
                $is_installment = $fee_structure->is_installment;
                $id_training_center = $fee_structure->id_training_center;
                $trigger_name = $fee_structure->trigger_name;

                $instllment_data['id_fee_structure'] = $fee_structure->id;
                $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;

                if($is_installment == 1)
                {

                    $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

                    if($installment_details)
                    {
                        foreach ($installment_details as $installment_detail)
                        {
                            
                            $installment_trigger_name = $installment_detail->trigger_name;

                            if($installment_trigger_name == 'APPLICATION REGISTRATION')
                            {

                                $data = array(
                                    'id_student_invoice_details' => $id_pu_invoice_student_details,
                                    'id_fee_item' => $installment_detail->id_fee_item,
                                    'amount' => $installment_detail->amount,
                                    'price' => $installment_detail->amount,
                                    'quantity' => 1,
                                    'id_reference' => $id_student,
                                    'description' => 'Application Registration Fee',
                                    'status' => 1,
                                    'created_by' => 0
                                );

                                $total_amount = $total_amount + $installment_detail->amount;
                
                                $this->addNewPartnerUniversityInvoiceDetails($data);
                            }
                        }
                    }
                }
                else
                {
                    if($trigger_name == 'APPLICATION REGISTRATION')
                    {

                        $data = array(
                            'id_student_invoice_details' => $id_pu_invoice_student_details,
                            'id_fee_item' => $fee_structure->id_fee_item,
                            'amount' => $fee_structure->amount,
                            'price' => $fee_structure->amount,
                            'quantity' => 1,
                            'id_reference' => $id_student,
                            'description' => 'Application Registration Fee',
                            'status' => 1,
                            'created_by' => 0
                        );

                        $total_amount = $total_amount + $fee_structure->amount;

                        $this->addNewPartnerUniversityInvoiceDetails($data);
                    }
                }
                // echo "<Pre>";print_r($data);exit;


            }

            $total_invoice_amount = $total_amount;

            return $total_invoice_amount;


            // if($is_sibbling_discount == '1')
            // {
            //     $this->db->select('*');
            //     $this->db->from('sibbling_discount');
            //     $this->db->where('currency', $currency);
            //     $this->db->where('status', '1');
            //     $query = $this->db->get();
            //     $sibling_discount_data = $query->row();

            //     if($sibling_discount_data)
            //     {
            //         $amount = $sibling_discount_data->amount;
            //         $id_discount = $sibling_discount_data->id;

            //         $sibling_insert = array(
            //             'id_main_invoice' => $inserted_id,
            //             'id_student' => $id_student,
            //             'name' => 'Sibbling Discount Applied',
            //             'amount' => $amount,
            //             'id_reference' => $id_discount,
            //         );
            //         $discount_inserted_id = $this->addNewMainInvoiceDiscountDetail($sibling_insert);
            //         if($discount_inserted_id)
            //         {
            //             $total_amount = $total_amount - $sibling_discount_data->amount;
            //             $sibling_discount_amount = $sibling_discount_data->amount;
            //         }
            //     }

            // }

            // if($is_employee_discount == '1')
            // {

            //     $this->db->select('*');
            //     $this->db->from('employee_discount');
            //     $this->db->where('currency', $currency);
            //     $this->db->where('status', '1');
            //     $query = $this->db->get();
            //     $employee_discount_data = $query->row();
            //     if($employee_discount_data)
            //     {
            //         $amount = $employee_discount_data->amount;
            //         $id_discount = $employee_discount_data->id;

            //         $employee_insert = array(
            //             'id_main_invoice' => $inserted_id,
            //             'id_student' => $id_student,
            //             'name' => 'Employee Discount Applied',
            //             'amount' => $amount,
            //             'id_reference' => $id_discount,
            //         );
            //         $sibbling_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
            //         if($sibbling_inserted_id)
            //         {
            //             $total_amount = $total_amount - $employee_discount_data->amount;
            //             $employee_discount_amount = $employee_discount_data->amount;
            //         }
            //     }
            // }




            // if($is_alumni_discount == '1')
            // {

            //     $this->db->select('*');
            //     $this->db->from('alumni_discount');
            //     // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
            //     // $this->db->where('date(start_date) >=', date('Y-m-d'));
            //     // $this->db->where('date(end_date) <=', date('Y-m-d'));
            //     $this->db->where('currency', $currency);
            //     $this->db->where('status', '1');
            //     $query = $this->db->get();
            //     $alumni_discount_data = $query->row();
            //     if($alumni_discount_data)
            //     {
            //         $amount = $alumni_discount_data->amount;
            //         $id_discount = $alumni_discount_data->id;

            //         $employee_insert = array(
            //             'id_main_invoice' => $inserted_id,
            //             'id_student' => $id_student,
            //             'name' => 'Alumni Discount Applied',
            //             'amount' => $amount,
            //             'id_reference' => $id_discount,
            //         );
            //         $alumni_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
            //         if($alumni_inserted_id)
            //         {
            //             $total_amount = $total_amount - $alumni_discount_data->amount;
            //             $alumni_discount_amount = $alumni_discount_data->amount;
            //         }
            //     }
            // }


            // $total_discount_amount = $sibling_discount_amount + $employee_discount_amount + $alumni_discount_amount;


            // $invoice_update['total_amount'] = $total_invoice_amount;
            // $invoice_update['balance_amount'] = $total_invoice_amount;
            // $invoice_update['invoice_total'] = $total_invoice_amount;
            // $invoice_update['total_discount'] = 0;
            // $invoice_update['paid_amount'] = 0;

            // $this->editMainInvoice($invoice_update,$id_invoice);
        }

        return 0;
    }

    function getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure_master,$currency)
    {

        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getFeeStructureByTrainingCenterForInvoiceGeneration($id_programme,$id_intake,$id_fee_structure,$id_training_center)
    {
        $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_fee_structure);
        $this->db->where('p.id_training_center', $id_training_center);
        $query = $this->db->get();
        $fee_structure = $query->result();

        return $fee_structure;
    }

    function getfeeStructureDetailsByIdFeeStructureMasterForPartneruniversity($id_fee_structure_master,$id_university)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->join('fee_structure as fss', 'fst.id_fee_structure = fss.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.id_training_center', $id_university);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        
        return $result;
    }


    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function getFeeStructureMasterIdByData($applicant)
    {

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();

        if($result)
        {
            return $result->id;
        }
        else
        {
            return 0;
        }
    }

    function viewStudentFeeBulkPartnerDetails($id_pu_invoice_student_details)
    {
        $this->db->select('mid.*, fstp.name as fee_setup_name, fstp.code as fee_steup_code, fm.code as frequency_mode, amt.code as amount_calculation_type');
        $this->db->from('partner_university_student_invoice_details as mid');     
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->join('frequency_mode as fm', 'fstp.id_frequency_mode = fm.id');        
        $this->db->join('amount_calculation_type as amt', 'fstp.id_amount_calculation_type = amt.id');        
        $this->db->where('mid.id_student_invoice_details', $id_pu_invoice_student_details);
        $query = $this->db->get();
        return $query->result();
    }
}