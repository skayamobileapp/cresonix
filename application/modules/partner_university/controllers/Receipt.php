<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Receipt extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_profile_model');
        $this->isPartnerUniversityLoggedIn();
    }

    function list()
    {
        $id_partner_university = $this->session->id_partner_university;

        $partner_university_name = $this->session->partner_university_name;
        $partner_university_code = $this->session->partner_university_code;

        $data['partner_university_name'] = $partner_university_name;
        $data['partner_university_code'] = $partner_university_code;


        $formData['receipt_number'] = $this->security->xss_clean($this->input->post('receipt_number'));
        $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
        $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
        $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
        $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
        $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
        $formData['id_partner_university'] = $id_partner_university;

        $data['searchParam'] = $formData;

        // $data['studentList'] = $this->student_profile_model->applicantList($formData);




        $this->global['pageTitle'] = 'Partner University Portal : Receipt';
        // echo "<Pre>";print_r($data['applicantList']);exit;
        $this->loadViews("receipt/list", $this->global, $data, NULL);
    }


    function view()
    {

        $id_partner_university = $this->session->id_partner_university;

        $partner_university_name = $this->session->partner_university_name;
        $partner_university_code = $this->session->partner_university_code;

        $data['partner_university_name'] = $partner_university_name;
        $data['partner_university_code'] = $partner_university_code;

        // echo "<Pre>";print_r($data['getStudentData']);exit();
        $this->global['pageTitle'] = 'Partner University Portal : Edit Student';
        $this->loadViews("receipt/view", $this->global, $data, NULL);
        // }
    }
}
