<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Applicant</h3>
        </div>



    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">


            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Profile Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Contact Information</a>
                    </li>

                    <li role="presentation"><a href="#program_majoring" class="nav-link border rounded text-center"
                            aria-controls="program_majoring" role="tab" data-toggle="tab">Program Interest</a>
                    </li>

                    <li role="presentation"><a href="#program_minoring" class="nav-link border rounded text-center"
                            aria-controls="program_minoring" role="tab" data-toggle="tab">Document Upload</a>
                    </li>

                    <li role="presentation"><a href="#program_concurrent" class="nav-link border rounded text-center"
                            aria-controls="program_concurrent" role="tab" data-toggle="tab">Discount Information</a>
                    </li>

                    <li role="presentation"><a href="#program_accerdation" class="nav-link border rounded text-center"
                            aria-controls="program_accerdation" role="tab" data-toggle="tab">Decleration Form</a>
                    </li>
                    
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">


                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">






                        <div class="form-container">
                            <h4 class="form-group-title">Profile Details</h4>

                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Salutation <span class='error-text'>*</span></label>
                                        <select name="salutation" id="salutation" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($salutationList)) {
                                                foreach ($salutationList as $record) {
                                            ?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php if($getApplicantDetails->salutation==$record->id)
                                                        {
                                                            echo "selected=selected";
                                                        }
                                                        ?>
                                                        >
                                                        <?php echo $record->name;  ?>        
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>First Name <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>">
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Last Name <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>">
                                    </div>
                                </div>



                                <div class="col-sm-4">
                                    <div class="form-group">
                                    <label>Type Of Nationality <span class='error-text'>*</span></label>
                                    <select name="nationality" id="nationality" class="form-control"onchange="checkNationality(this.value)">
                                        <option value="">Select</option>
                                        <option value="<?php echo 'Malaysian';?>"
                                            <?php 
                                            if ($getApplicantDetails->nationality == 'Malaysian')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Malaysian";  ?>
                                        </option>

                                        <option value="<?php echo 'Other';?>"
                                            <?php 
                                            if ($getApplicantDetails->nationality == 'Other')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Other";  ?>
                                        </option>
                                    </select>

                                    </div>
                                </div>

                                <div class="col-sm-4" id="view_nric">
                                    <div class="form-group">
                                        <label>NRIC <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>">
                                    </div>
                                </div>

                                <div class="col-sm-4" id="view_passport" style="display: none">
                                    <div class="form-group">
                                        <label>Passport <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="passport" name="passport" value="<?php echo $getApplicantDetails->passport ?>">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Date Of Birth <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Race <span class='error-text'>*</span></label>
                                        <select name="id_race" id="id_race" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($raceList))
                                            {
                                                foreach ($raceList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $getApplicantDetails->id_race)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->name;  ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Religion <span class='error-text'>*</span></label>
                                        <select name="religion" id="religion" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($religionList))
                                            {
                                                foreach ($religionList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $getApplicantDetails->religion)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->name;  ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Martial Status <span class='error-text'>*</span></label>
                                        <select class="form-control" id="martial_status" name="martial_status">
                                            <option value="">SELECT</option>
                                            <option value="Single" 
                                            <?php 
                                            if($getApplicantDetails->martial_status=='Single')
                                                { echo "selected"; } 
                                            ?>>
                                            SINGLE
                                            </option>
                                            <option value="Married" 
                                            <?php 
                                            if($getApplicantDetails->martial_status=='Married')
                                                { echo "selected"; }
                                                 ?>>
                                            MARRIED
                                            </option>
                                            <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                                        </select>
                                    </div>
                                </div>
                                

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Email <span class='error-text'>*</span></label>
                                        <input type="email" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Password <span class='error-text'>*</span></label>
                                        <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Phone Number <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>">
                                    </div>
                                </div>


                                
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Gender <span class='error-text'>*</span></label>
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="">SELECT</option>
                                            <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                            <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                            <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                                        </select>
                                    </div>
                                </div>


                                

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Contact Email</label>
                                        <input type="email" class="form-control" id="contact_email" name="contact_email" value="<?php echo $getApplicantDetails->contact_email ?>">
                                    </div>
                                </div> -->


                            </div>

                        </div>




                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="button" onclick="validateSubmission()" class="btn btn-primary btn-lg">Save</button>
                            </div>
                        </div>



                         



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                            <br>

                    


                            <div class="form-container">
                                <h4 class="form-group-title">Mailing Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getApplicantDetails->mail_address1 ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getApplicantDetails->mail_address2 ?>">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Country <span class='error-text'>*</span></label>
                                            <select name="mailing_country" id="mailing_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($getApplicantDetails->mailing_country==$record->id)
                                                        {
                                                            echo "selected";
                                                        }?>
                                                    >
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing State <span class='error-text'>*</span></label>
                                            <span id='view_mailing_state'></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getApplicantDetails->mailing_city ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getApplicantDetails->mailing_zipcode ?>">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            

                            <div class="form-container">
                                <h4 class="form-group-title">Permanent Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>">
                                        </div>
                                
                                    </div><div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Country <span class='error-text'>*</span></label>
                                            <select name="permanent_country" id="permanent_country" class="form-control" onchange="getStateByCountryPermanent(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>
                                                    >
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent State <span class='error-text'>*</span></label>
                                            <span id='view_permanent_state'></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>">
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" onclick="validateSubmission()" class="btn btn-primary btn-lg">Save</button>
                                </div>
                            </div>





                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="program_majoring">
                        <div class="mt-4">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Interests</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Degree Level <span class='error-text'>*</span></label>
                                            <select name="id_degree_type" id="id_degree_type" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($degreeTypeList))
                                                {
                                                    foreach ($degreeTypeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_degree_type)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program <span class='error-text'>*</span></label>
                                            <select name="id_program" id="id_program" class="form-control selitemIcon"  onchange="getIntakeByProgramme(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programList))
                                                {
                                                    foreach ($programList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $getApplicantDetails->id_program)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>
                                                    >
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program Scheme <span class='error-text'>*</span>
                                            </label>
                                                <span id="view_program_scheme"></span>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Intake <span class='error-text'>*</span></label>
                                            <span id="view_intake"></span>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Branch <span class='error-text'>*</span></label>
                                            <span id="view_branch"></span>
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" onclick="validateSubmission()" class="btn btn-primary btn-lg">Save</button>
                                </div>
                            </div>



                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="program_minoring">
                        <div class="mt-4">

                            <br>

                            <div class="form-container" id="view_document" style="display: none">
                                <h4 class="form-group-title">Documents To Upload</h4>
                             
                                <div id='doc'>
                                </div>

                            </div>




                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" onclick="validateSubmission()" class="btn btn-primary btn-lg">Save</button>
                                </div>
                            </div>



                              



                        </div>
                    
                    </div>







                    <div role="tabpanel" class="tab-pane" id="program_concurrent">
                        
                        <div class="mt-4">

                        <div id="view_intake_discounts">
                        </div>


                        <br>


                            <div class="form-container">
                                <h4 class="form-group-title">Discount Details</h4> 


                                <div class="form-container" id="view_is_intake_sibbling_discount">
                                <h4 class="form-group-title">Sibbling Discount Details</h4> 



                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Do you have sibbling/s studying with university? <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="sibbling_discount" id="sd1" value="Yes" onclick="showSibblingFields()" <?php if($getApplicantDetails->sibbling_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="sibbling_discount" id="sd2" value="No" onclick="hideSibblingFields()" <?php if($getApplicantDetails->sibbling_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
                                            </label>                              
                                        </div>                         
                                    </div>
                                </div>


                                <div class="row" id="sibbling" style="display: none;">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_name" name="sibbling_name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control">
                                        </div>
                                    </div>
                                </div>


                                <?php

                                if($getApplicantDetails->sibbling_discount=='Yes')
                                {
                                    if($getApplicantDetails->is_sibbling_discount=='0')
                                    {

                                    ?>

                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_name ?>"
                                            >
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_nric ?>">
                                        </div>
                                    </div>

                                   <!--  <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_status" name="sibbling_status" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_status ?>" readonly="readonly">
                                        </div>
                                    </div> -->

                                </div> 

                                <?php
                                    
                                    }
                                    else
                                    {

                                ?>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_name ?>"
                                             readonly>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_nric ?>" readonly>
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                            <input type="text" id="sibbling_status" name="sibbling_status" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_status ?>" readonly="readonly">
                                        </div>
                                    </div> -->

                                </div> 

                                 <?php
                                    }
                                    ?>


                                <?php
                                   
                                if($sibblingDiscountDetails->sibbling_status=='Reject')
                                {
                                ?>

                                    <!-- <div class="row">
                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Reject Reason <span class='error-text'>*</span></label>
                                                    <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $sibblingDiscountDetails->reason ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected By <span class='error-text'>*</span></label>
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected On <span class='error-text'>*</span></label>
                                                    <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($sibblingDiscountDetails->rejected_on)) ?>" readonly="readonly">
                                                </div>
                                        </div>
                                    </div>  -->


                                    <?php
                                    }
                                    elseif($sibblingDiscountDetails->sibbling_status=='Approved')
                                    {
                                        ?>
                                        

                                    <!-- <div class="row">

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved By <span class='error-text'>*</span></label>
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved On <span class='error-text'>*</span></label>
                                                    <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $sibblingDiscountDetails->rejected_on ?>" readonly="readonly">
                                                </div>
                                        </div>
                                
                                    </div>  -->

                                        <?php
                                    }
                                    ?> 

                                <?php
                                }
                                 ?>


                             </div>

                                <br>

                            <div class="form-container" id="view_is_intake_employee_discount">
                                <h4 class="form-group-title">Employee Discount Details</h4> 
                                 
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Do you eligible for Employee discount <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="employee_discount" id="ed1" value="Yes" onclick="showEmployeeFields()" <?php if($getApplicantDetails->employee_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="employee_discount" id="ed2" value="No" onclick="hideEmployeeFields()" <?php if($getApplicantDetails->employee_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
                                            </label>                              
                                        </div>                         
                                    </div>
                                </div>



                                <div class="row" id="employee" style="display: none;">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_name" name="employee_name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_nric" name="employee_nric" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Designation <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_designation" name="employee_designation" class="form-control">
                                        </div>
                                    </div>
                                </div>
                               

                               <?php
                                if($getApplicantDetails->employee_discount=='Yes')
                                {
                                    if($getApplicantDetails->is_employee_discount=='0')
                                    {
                                    ?>

                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_name" name="employee_name" class="form-control" value="<?php echo $employeeDiscountDetails->employee_name ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $employeeDiscountDetails->employee_nric ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Designation <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_designation ?>">
                                        </div>
                                    </div>

                                </div> 

                                <div class="row">

                                  <!--  <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_status" name="employee_status" class="form-control" value="<?php echo $employeeDiscountDetails->employee_status ?>" readonly="readonly">
                                        </div>
                                    </div> -->

                                </div>

                                <?php
                                }
                                else
                                {
                                ?>

                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_name" name="employee_name" class="form-control" value="<?php echo $employeeDiscountDetails->employee_name ?>" readonly>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $employeeDiscountDetails->employee_nric ?>" readonly>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Designation <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_designation ?>" readonly>
                                        </div>
                                    </div>

                                </div> 



                                <div class="row">

                                   <!--  <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_status" name="employee_status" class="form-control" value="<?php echo $employeeDiscountDetails->employee_status ?>" readonly="readonly">
                                        </div>
                                    </div> -->

                                </div>

                                    <?php
                                    }
                                    ?>


                                <?php
                                if($employeeDiscountDetails->employee_status=='Reject')
                                {
                                ?>

                                    <!-- <div class="row">

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected Reason <span class='error-text'>*</span></label>
                                                    <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $employeeDiscountDetails->reason ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected By <span class='error-text'>*</span></label>
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected On <span class='error-text'>*</span></label>
                                                    <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($employeeDiscountDetails->rejected_on)) ?>" readonly="readonly">
                                                </div>
                                        </div>

                                    </div>  -->




                                    <?php
                                    
                                    }
                                    elseif($employeeDiscountDetails->employee_status=='Approved')
                                    {

                                    ?>

                                    <!-- <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Approved By <span class='error-text'>*</span></label>
                                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Approved On <span class='error-text'>*</span></label>
                                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $employeeDiscountDetails->rejected_on ?>" readonly="readonly">
                                            </div>
                                        </div>
                                    
                                    </div>  -->



                                <?php
                                }
                                 ?>
                                

                            <?php
                            }
                            ?>




                        </div>







                            <br>

                            <div class="form-container" id="view_is_intake_alumni_discount">
                                <h4 class="form-group-title">Alumni Discount Details</h4> 


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Do you eligible for Alumni discount <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="alumni_discount" id="ed1" value="Yes" onclick="showAlumniFields()" <?php if($getApplicantDetails->alumni_discount=='Yes'){ echo "checked";}?>><span class="check-radio"></span> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="alumni_discount" id="ed2" value="No" onclick="hideAlumniFields()" <?php if($getApplicantDetails->alumni_discount=='No'){ echo "checked";}?>><span class="check-radio"></span> No
                                            </label>                              
                                        </div>                         
                                    </div>

                                </div>



                                <div class="row" id="alumni" style="display: none;">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Name <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_name" name="alumni_name" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Email Id <span class='error-text'>*</span></label>
                                            <input type="email" id="alumni_email" name="alumni_email" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_nric" name="alumni_nric" class="form-control">
                                        </div>
                                    </div>

                                </div>
                               

                               <?php
                                if($getApplicantDetails->alumni_discount=='Yes')
                                {

                                    if($getApplicantDetails->is_alumni_discount=='0')
                                    {
                                    ?>

                                <div class="row">
                                   
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Name <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_name" name="alumni_name" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_name ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Email Id <span class='error-text'>*</span></label>
                                            <input type="email" id="alumni_email" name="alumni_email" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_email ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_nric" name="alumni_nric" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_nric ?>">
                                        </div>
                                    </div>
                                    
                                </div> 

                                <?php

                                    }
                                    else
                                    {
                                ?>

                                <div class="row">
                                   
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Name <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_name" name="alumni_name" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_name ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni Email Id <span class='error-text'>*</span></label>
                                            <input type="email" id="alumni_email" name="alumni_email" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_email ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Alumni NRIC <span class='error-text'>*</span></label>
                                            <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_nric ?>" readonly>
                                        </div>
                                    </div>
                                    
                                </div> 

                                <div class="row" id="view_is_intake_alumni_discount">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                            <input type="text" id="alumni_status" name="alumni_status" class="form-control" value="<?php echo $alumniDiscountDetails->alumni_status ?>" readonly>
                                        </div>
                                    </div>

                                </div>

                                    <?php
                                    }
                                    ?>


                                <?php
                                    if($alumniDiscountDetails->alumni_status=='Reject')
                                    {
                                    ?>

                                    <div class="row">

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected Reason <span class='error-text'>*</span></label>
                                                    <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $alumniDiscountDetails->reason ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected By <span class='error-text'>*</span></label>
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $alumniDiscountDetails->user_name ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Rejected On <span class='error-text'>*</span></label>
                                                    <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($alumniDiscountDetails->rejected_on)) ?>" readonly="readonly">
                                                </div>
                                        </div>

                                    </div> 




                                    <?php
                                    }
                                    elseif($alumniDiscountDetails->alumni_status=='Approved')
                                    {
                                        ?>

                                    <div class="row">

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved By <span class='error-text'>*</span></label>
                                                    <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $alumniDiscountDetails->user_name ?>" readonly="readonly">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Approved On <span class='error-text'>*</span></label>
                                                    <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $alumniDiscountDetails->rejected_on ?>" readonly="readonly">
                                                </div>
                                        </div>
                                    
                                    </div> 



                                <?php
                                }
                                 ?>
                                

                            <?php
                                
                            }
                             ?>

                         </div>




















































                            </div>



                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" onclick="validateSubmission()" class="btn btn-primary btn-lg">Save</button>
                                </div>
                            </div>




                    
                        </div>

                    </div>






                    <div role="tabpanel" class="tab-pane" id="program_accerdation">


                        <div class="mt-4">


                            <br>

                            <div id="view_requirements">

                            </div>


                            <div class="form-container">
                                    
                                <h4 class="form-group-title">Decleration</h4>

                                <br>



                                <h4 class="modal-title">Agree To Terms & Condition To Submit The Application</h4>


                                <br>



                                    <p>
                                     1. These terms and conditions represent an agreement between the University of Edinburgh ("University") and you, a prospective student. By accepting the University's offer of a place on a programme, you accept these terms and conditions in full, which along with your offer and the University's rules, regulations, policies and procedures and the most recently published prospectus (as applicable), form the contract between you and the University in relation to your studies at the University as amended from time to time pursuant to Clause 1.3 (the "Contract"). 
                                    </p>

                                    <p>

                                     2.    If you have any questions or concerns about these terms and conditions, please contact the University's Student Recruitment and Admission Office:
                                    </p>  

                                    <p>Requirements
                                    </p>  

                                   


                                    <br>

                                    &emsp;<input type="checkbox" id="is_submitted" name="is_submitted" value="1" >&emsp;
                                    By Checking This Button I accept the <u><a onclick="showModel()">Terms and Conditions</a> <span class='error-text'>*</span></u>

                            </div>





                            <div class="button-block clearfix">
                               
                                <div class="bttn-group">
                                    
                                   <!--  <p align="right"> -->
                                    <?php
                                    if($getApplicantDetails->is_updated == '1')
                                    {
                                     ?>
                                        <button  style="position: absolute; right: 0;" class="button" type="button" class="btn btn-primary btn-lg" onclick="validateSubmission()">Submit Application >></button>
                                        <br>
                                        <br>
                                        <br>
                                        <?php

                                    }else
                                    {
                                        ?>



                                        <?php

                                    }
                                    ?>
                                    <!-- </p> -->
                                </div>
                            </div>




                        </div>                           


                    </div>




                    



                </div>



        </div>



               




        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Agree To Terms & Condition To Submit The Application</h4>
              </div>

              <div class="modal-body">

                <div id="view_intake_discounts">
                </div>

                <br>
                <div class="form-container">
                        <h4 class="form-group-title">Terms & Condition</h4>




                    <p>
                     1. These terms and conditions represent an agreement between the University of Edinburgh ("University") and you, a prospective student. By accepting the University's offer of a place on a programme, you accept these terms and conditions in full, which along with your offer and the University's rules, regulations, policies and procedures and the most recently published prospectus (as applicable), form the contract between you and the University in relation to your studies at the University as amended from time to time pursuant to Clause 1.3 (the "Contract"). 
                    </p>

                    <p>

                     2.    If you have any questions or concerns about these terms and conditions, please contact the University's Student Recruitment and Admission Office:
                    </p>  

                    

                    <br>

                    &emsp;<input type="checkbox" id="is_submitted" name="is_submitted" value="1" >&emsp;
                By Checking This Button I accept the <u>Terms and Conditions <span class='error-text'>*</span></u>


                </div>

            </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="submitApp()">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>






    </form>

     


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>


    function copyAddress()
    {
        document.getElementById('permanent_address1').value = document.getElementById('mail_address1').value;
        document.getElementById('permanent_address2').value = document.getElementById('mail_address2').value;
        document.getElementById('permanent_city').value = document.getElementById('mailing_city').value;
        document.getElementById('permanent_country').value = document.getElementById('mailing_country').value;
        document.getElementById('permanent_state').value = document.getElementById('mailing_state').value;
        document.getElementById('permanent_zipcode').value = document.getElementById('mailing_zipcode').value;
    }


    function getStateByCountry(id)
    {

        $.get("/applicant/applicant/getStateByCountry/"+id, function(data, status){
       
            $("#view_mailing_state").html(data);
        });
    }




    function getStateByCountryPermanent(id)
    {
        if(id != '')
        {

            $.get("/applicant/applicant/getStateByCountryPermanent/"+id, function(data, status){
           
                $("#view_permanent_state").html(data);
            });
        }
    }


   



    function getIntakeByProgramme(id)
     {
        if(id != '')
        {
            $.get("/applicant/applicant/getIntakeByProgramme/"+id, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });


            $.get("/applicant/applicant/getProgramSchemeByProgramId/"+id, function(data, status){
                $("#view_program_scheme").html(data);
                $("#view_program_scheme").show();
            });   

            $.get("/applicant/applicant/getBranchByProgram/"+id, function(data, status){
                $("#view_branch").html(data);
                $("#view_branch").show();
            });   

            $.get("/applicant/applicant/getIndividualEntryRequirement/"+id, function(data, status){
                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });        

        }
     }


   


     function getDocumentByProgramme(id)
    {
        if(id != '')
        {

            $.get("/applicant/applicant/getDocumentByProgramme/"+id, function(data, status){
           
                if(data != '')
                {
                    $("#doc").html(data);
                    $("#view_document").show();
                }else
                {
                    $("#view_document").hide();
                    alert('No Records Defined To Upload');
                }

            });
        }
    }





     function checkFeeStructure()
     {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_program_scheme'] = $("#id_program_scheme").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_program'] != '' && tempPR['id_intake'] != ''  && tempPR['id_program_scheme'] != '' )
        {

            $.ajax(
            {
               url: '/applicant/applicant/checkFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    if(result == '0')
                    {
                        alert('No Fee Structure Defined For This Programme & Intake, Select Another Combination');
                        $(this).data('options', $('#id_intake option').clone());
                        $("#id_intake").html('<option value="">').append(options);
                        $("#id_intake").val('');

                        $(this).data('options', $('#id_program_scheme option').clone());
                        $("#id_program_scheme").html('<option value="">').append(options);
                        $("#id_program_scheme").val('');
                        
                    }
               }
            });
        }

        if(id_intake != '')
        {


        var id_intake = $("#id_intake").val();
        $.get("/applicant/applicant/getIntakeDetails/"+id_intake, function(data, status){
        
                if(data != '')
                {
                     // $("#view_intake_discounts").hide();
                    $("#view_intake_discounts").html(data);

                    $("#view_is_intake_employee_discount").hide();
                    $("#view_is_intake_sibbling_discount").hide();
                    $("#view_is_intake_alumni_discount").hide();

                    var is_intake_alumni_discount = $("#is_intake_alumni_discount").val();
                    var is_intake_employee_discount = $("#is_intake_employee_discount").val();
                    var is_intake_sibbling_discount = $("#is_intake_sibbling_discount").val();
                    if(is_intake_alumni_discount == 1)
                    {
                         $("#view_is_intake_alumni_discount").show();
                    }

                    if(is_intake_employee_discount == 1)
                    {
                         $("#view_is_intake_employee_discount").show();
                    }

                    if(is_intake_sibbling_discount == 1)
                    {
                         $("#view_is_intake_sibbling_discount").show();
                    }
                 }

            });
        }


     }


     function validateSubmission()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {
            $('#form_applicant').submit();
            // $('#myModal').modal('show');
            // alert("Data Added");
        }
    }

    function showModel()
    {
        $('#myModal').modal('show');
    }

    // function submitApp(form)
    //   {
    //     if(!form.is_submitted.checked) {
    //       alert("Please indicate that you accept the Terms and Conditions");
    //       form.is_submitted.focus();
    //       return false;
    //     }
    //     return true;
    //   }


    function submitApp()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {    
            $('#form_applicant').submit();
        }
    }


    $(document).ready(function()
    {

        
    $('select').select2();

      var mailingCountry = "<?php echo $getApplicantDetails->mailing_country;?>";
        if(mailingCountry.length==0) {
            mailingCountry = 0;
        }

        if(mailingCountry.length>0) {
        $.get("/applicant/applicant/getStateByCountry/"+mailingCountry, function(data, status)
        {
            var idstateselected = "<?php echo $getApplicantDetails->mailing_state;?>";

            $("#view_mailing_state").html(data);
            $("#mailing_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
            $('select').select2();
        });
    }

        var permanentCountry = "<?php echo $getApplicantDetails->permanent_country;?>";
        if(permanentCountry!='') {
         $.get("/applicant/applicant/getStateByCountryPermanent/"+permanentCountry, function(data, status)
            {
                var idstateselected = "<?php echo $getApplicantDetails->permanent_state;?>";

                $("#view_permanent_state").html(data);
                $("#permanent_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $('select').select2();
            });
     }

      var idprogram = "<?php echo $getApplicantDetails->id_program;?>";

      if(idprogram!='')
      {
         $.get("/applicant/applicant/getIntakeByProgramme/"+idprogram, function(data, status)
            {
                var idstateselected = "<?php echo $getApplicantDetails->id_intake;?>";

                $("#view_intake").html(data);
                $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $('select').select2();
            });



         $.get("/applicant/applicant/getProgramSchemeByProgramId/"+idprogram, function(data, status)
            {
                var idprogramschemeselected = "<?php echo $getApplicantDetails->id_program_scheme;?>";
                $("#view_program_scheme").html(data);
                $("#id_program_scheme").find('option[value="'+idprogramschemeselected+'"]').attr('selected',true);
                $('select').select2();
            });


         $.get("/applicant/applicant/getBranchByProgram/"+idprogram, function(data, status)
            {
                var id_branch = "<?php echo $getApplicantDetails->id_branch;?>";
                $("#view_branch").html(data);
                $("#id_branch").find('option[value="'+id_branch+'"]').attr('selected',true);
                $('select').select2();
            });




        $.get("/applicant/applicant/getIndividualEntryRequirement/"+idprogram, function(data, status){
            // alert(id);
                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });    


        var id_intake = "<?php echo $getApplicantDetails->id_intake;?>";

        $.get("/applicant/applicant/getIntakeDetails/"+id_intake, function(data, status){
        
                if(data != '')
                {
                     // $("#view_intake_discounts").hide();
                    $("#view_intake_discounts").html(data);

                    $("#view_is_intake_employee_discount").hide();
                    $("#view_is_intake_sibbling_discount").hide();
                    $("#view_is_intake_alumni_discount").hide();

                    var is_intake_alumni_discount = $("#is_intake_alumni_discount").val();
                    var is_intake_employee_discount = $("#is_intake_employee_discount").val();
                    var is_intake_sibbling_discount = $("#is_intake_sibbling_discount").val();
            // alert(is_intake_alumni_discount);
            // alert(is_intake_employee_discount);
            // alert(is_intake_sibbling_discount);

                  // alert(student_allotment_count);
                    if(is_intake_alumni_discount == 1)
                    {
                         $("#view_is_intake_alumni_discount").show();
                    }

                    if(is_intake_employee_discount == 1)
                    {
                         $("#view_is_intake_employee_discount").show();
                    }

                    if(is_intake_sibbling_discount == 1)
                    {
                         $("#view_is_intake_sibbling_discount").show();
                    }
                 }

            });
     }






        <?php
        if($getApplicantDetails->id_program > 0)
        {
        ?>
                getDocumentByProgramme(<?php echo $getApplicantDetails->id_program; ?>);
        <?php
        }
        ?>

        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

    function checkNationality(nationality)
    {
        if(nationality != '')
        {
            if(nationality == 'Malaysian')
            {
                $('#view_nric').show();
                $('#view_passport').hide();

            }else{
                $('#view_nric').hide();
                $('#view_passport').show();
            }
        }
    }


    function showSibblingFields(){
            $("#sibbling").show();
    }

    function hideSibblingFields(){
            $("#sibbling").hide();
    }

    function showEmployeeFields(){
            $("#employee").show();
    }

    function hideEmployeeFields(){
            $("#employee").hide();
    }

    function showAlumniFields(){
            $("#alumni").show();
    }

    function hideAlumniFields(){
            $("#alumni").hide();
    }



    

</script>

<style>
.button {
  display: inline-block;
  padding: 15px 25px;
  font-size: 16px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #4CAF58;
  border: none;
  border-radius: 8px;
  box-shadow: 0 9px #999;
}

.button:hover {
    background-color: #3e8e41
}

.button:active
{
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>