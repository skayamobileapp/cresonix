<?php

// $base_url = $_SERVER['HTTP_HOST'];

// if($base_url == "cms.com")
// {
//     include("/var/www/html/college-management-system/assets/mpdf60/mpdf.php");
// }
// elseif($base_url == "college.com")
// {
//     include("/var/www/html/college/assets/mpdf60/mpdf.php");
// }
// else
// {
//     include("/college/assets/mpdf60/mpdf.php");
// }

// $mpdf=new mPDF();

// $mpdf->SetHeader("<table>
//                   <tr>
//             <td>Header goes herer
//                    </td>
//                   </tr>
//                   </table>");

// $mpdf->SetFooter('<div>Powered by firstDoctor</div>');
// // echo $file_data;exit;

// $file_data = "<table><tr><td>asdfasdf</td></tr></table>";

// $mpdf->WriteHTML($file_data);

// $mpdf->Output('testing.pdf', 'D');
// exit;




?>


<?php $this->load->helper("form"); ?>
<form id="form_staff" action="" method="post" enctype="multipart/form-data">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Faculty</h3>
        </div>

        <div class="form-container">
            <h4 class="form-group-title">Faculty Details</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Salutation <span class='error-text'>*</span></label>
                            <select name="salutation" id="salutation" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($salutationList)) {
                                foreach ($salutationList as $record) {
                            ?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->name;  ?>        
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name">
                        </div>
                    </div>
 
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label>ID Type <span class='error-text'>*</span></label>
                           <select name="id_type" id="id_type" class="form-control" required onchange="getlabel()">
                            <option value="">Select</option>
                              <option value="NRIC">NRIC</option>
                              <option value="PASSPORT" >PASSPORT</option>
                          </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                         <div class="form-group">
                            <label><span id='labelspanid'></span> <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="">
                         </div>
                    </div>


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>IC No. <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="ic_no" name="ic_no">
                        </div>
                    </div>     -->

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select name="gender" id="gender" class="form-control">
                                <option value="">Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>

                        
                </div>


                <div class="row">


                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile  Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="mobile_number" name="mobile_number">
                        </div>
                    </div>  


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="email" name="email" value="">
                        </div>
                    </div> 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Faculty ID <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="staff_id" name="staff_id" value="">
                        </div>
                    </div> 

                        

                    
                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Job Type <span class='error-text'>*</span></label>
                            <select name="job_type" id="job_type" class="form-control">
                                <option value="">Select</option>
                                <option value="0">Part Time</option>
                                <option value="1">Full Time</option>
                            </select>
                        </div>
                    </div>  


                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Faculty Type <span class='error-text'>*</span></label>
                            <select name="academic_type" id="academic_type" class="form-control">
                                <option value="">Select</option>
                                <option value="1">Academic Faculty</option>
                                <option value="0">Non-Academic Faculty</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>DOB <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="dob" name="dob" autocomplete="off">
                        </div>
                    </div>  


                </div>

                

                <!-- <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Department </label>
                            <select name="id_department" id="id_department" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($departmentList))
                                {
                                    foreach ($departmentList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Faculty Program <span class='error-text'>*</span></label>
                            <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($facultyProgramList))
                                {
                                    foreach ($facultyProgramList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Highest Education Level <span class='error-text'>*</span></label>
                            <select name="id_education_level" id="id_education_level" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($qualificationList))
                                {
                                    foreach ($qualificationList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    

                </div> -->




                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Joined Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="joined_date" name="joined_date" autocomplete="off">
                        </div>
                    </div>  


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Nationality <span class='error-text'>*</span></label>
                            <select name="nationality" id="nationality" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($nationalityList)) {
                                foreach ($nationalityList as $record) {
                            ?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->name;  ?>        
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>FILE <span class='error-text'>*</span>
                            </label>
                            <input type="file" name="image" id="image">
                        </div>                    
                    </div>



                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                </div>

            </div>



            <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone  Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="phone_number" name="phone_number">
                        </div>
                    </div>     

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address 2 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address_two" name="address_two">
                        </div>
                    </div>   

                     

                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select State *</label>
                            <select name="id_state" id="id_state" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> -->  
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Country <span class='error-text'>*</span></label>
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select State <span class='error-text'>*</span></label>
                            <span id='view_state'></span>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="zipcode" name="zipcode" value="" maxlength="6">
                        </div>
                    </div> 

                    
                </div>



            </div>






            <div class="form-container">
             <h4 class="form-group-title">Other Details</h4>
             <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                      <label>Whatsapp Number <span class='error-text'>*</span></label>
                      <input type="text" class="form-control" id="whatsapp_number" name="whatsapp_number" value="">
                   </div>
                </div>

                 <div class="col-sm-4">
                  <div class="form-group">
                      <label>LinkedIn ID/Link: <span class='error-text'></span></label>
                      <input type="text" class="form-control" id="linked_in" name="linked_in" value="">
                   </div>
                </div>

                 <div class="col-sm-4">
                  <div class="form-group">
                      <label>Facebook ID/ Link <span class='error-text'></span></label>
                      <input type="text" class="form-control" id="facebook_id" name="facebook_id" value="">
                   </div>
                </div>

                 <div class="col-sm-4">
                  <div class="form-group">
                      <label>Twitter ID/Link: <span class='error-text'></span></label>
                      <input type="text" class="form-control" id="twitter_id" name="twitter_id" value="">
                   </div>
                </div>

                 <div class="col-sm-4">
                  <div class="form-group">
                      <label>Instagram ID/ Link <span class='error-text'></span></label>
                      <input type="text" class="form-control" id="ig_id" name="ig_id" value="">
                   </div>
                </div>

              </div>
            
           </div>

       

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>



            

            

        <!-- <div class="form-container">
            <h4 class="form-group-title">Assign Courses to Faculty</h4> 

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>


            <div class="clearfix">
                <div id="view">
                    
                </div>
            </div>

        </div> -->



            
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Intake Has Programme</h4>
      </div>
      <div class="modal-body">
         <h4></h4>
             <div class="row">
                        <input type="text" class="form-control" id="id" name="id">

                
            </div>


      </div>
      <div class="modal-footer">
                

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</form>

<script>

    $('select').select2();

    $(function()
    {
    $(".datepicker").datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
    });

    function getlabel()
    {
      var labelnric = $("#id_type").val();
      //alert(labelnric);
      $("#labelspanid").html(labelnric);
    }


    function getStateByCountry(id)
    {

        $.get("/partner_university/staff/getStateByCountry/"+id, function(data, status)
        {
   
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }



    function saveData()
    {
        var tempPR = {};
        tempPR['id_course'] = $("#id_course").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/partner_university/staff/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                $('#myModal').modal('hide');
               }
            });
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/partner_university/staff/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function validateDetailsData()
    {
        if($('#form_staff').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Course To The Faculty.");
            }
            else
            {
                $('#form_staff').submit();
            }
        }    
    }


    $(document).ready(function()
    {
        $("#form_staff").validate(
        {
            rules:
            {
                salutation:
                {
                    required: true
                },
                first_name:
                {
                    required: true
                },
                last_name:
                {
                    required: true
                },
                ic_no:
                {
                    required: true
                },
                gender:
                {
                    required: true
                },
                mobile_number:
                {
                    required: true
                },
                phone_number:
                {
                    required: true
                },
                address:
                {
                    required: true
                },
                address_two:
                {
                    required: true
                },
                id_country:
                {
                    required: true
                },
                id_state:
                {
                    required: true
                },
                zipcode:
                {
                    required: true
                },
                job_type:
                {
                    required: true
                },
                email:
                {
                    required: true
                },
                staff_id:
                {
                    required: true
                },
                dob:
                {
                    required: true
                },
                academic_type:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_education_level:
                {
                    required: true
                },
                id_type:
                {
                    required: true
                },
                status: {
                    required: true
                },
                nationality: {
                    required: true
                },
                whatsapp_number : {
                    required: true
                },
                image : {
                    required: true
                },
                joined_date : {
                    required: true
                }
            },
            messages:
            {
                salutation:
                {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name:
                {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                last_name:
                {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                ic_no:
                {
                    required: "<p class='error-text'>NRIC / IC No. Required</p>",
                },
                gender:
                {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mobile_number:
                {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                phone_number:
                {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                address:
                {
                    required: "<p class='error-text'>Faculty Address Required</p>",
                },
                address_two:
                {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                id_country:
                {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state:
                {
                    required: "<p class='error-text'>Select State</p>",
                },
                zipcode:
                {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                job_type:
                {
                    required: "<p class='error-text'>Select Job Type</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                staff_id:
                {
                    required: "<p class='error-text'>Enter Faculty ID</p>",
                },
                dob:
                {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                academic_type:
                {
                    required: "<p class='error-text'>Select Academic Type</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_education_level:
                {
                    required: "<p class='error-text'>Select Highest Education Level</p>",
                },
                id_type:
                {
                    required: "<p class='error-text'>Select ID Type</p>",
                },
                status:
                {
                    required: "<p class='error-text'>Select status</p>",
                },
                nationality:
                {
                    required: "<p class='error-text'>Select Nationality</p>",
                },
                whatsapp_number:
                {
                    required: "<p class='error-text'>WhatsApp No. Required</p>",
                },
                image:
                {
                    required: "<p class='error-text'>Select Image To Upload</p>",
                },
                joined_date:
                {
                    required: "<p class='error-text'>Select Joined Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>