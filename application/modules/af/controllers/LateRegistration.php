<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class LateRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('late_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('late_registration.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['lateRegistrationList'] = $this->late_registration_model->lateRegistrationListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Late Registration List';
            $this->loadViews("late_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('late_registration.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $semester_type = $this->security->xss_clean($this->input->post('semester_type'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $no_of_days = $this->security->xss_clean($this->input->post('no_of_days'));
                $penalty = $this->security->xss_clean($this->input->post('penalty'));
                $no_of_days_for_special_appeal = $this->security->xss_clean($this->input->post('no_of_days_for_special_appeal'));
                $special_appeal_penalty = $this->security->xss_clean($this->input->post('special_appeal_penalty'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'semester_type' => $semester_type,
                    'effective_date' => date('Y-m-d',strtotime($effective_date)),
                    'no_of_days' => $no_of_days,
                    'penalty' => $penalty,
                    'no_of_days_for_special_appeal' => $no_of_days_for_special_appeal,
                    'special_appeal_penalty' => $special_appeal_penalty,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->late_registration_model->addNewLateRegistration($data);
                redirect('/setup/lateRegistration/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Late Registration';
            $this->loadViews("late_registration/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('late_registration.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/lateRegistration/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $semester_type = $this->security->xss_clean($this->input->post('semester_type'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $no_of_days = $this->security->xss_clean($this->input->post('no_of_days'));
                $penalty = $this->security->xss_clean($this->input->post('penalty'));
                $no_of_days_for_special_appeal = $this->security->xss_clean($this->input->post('no_of_days_for_special_appeal'));
                $special_appeal_penalty = $this->security->xss_clean($this->input->post('special_appeal_penalty'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'semester_type' => $semester_type,
                    'effective_date' => date('Y-m-d',strtotime($effective_date)),
                    'no_of_days' => $no_of_days,
                    'penalty' => $penalty,
                    'no_of_days_for_special_appeal' => $no_of_days_for_special_appeal,
                    'special_appeal_penalty' => $special_appeal_penalty,
                    'status' => $status,
                    'created_by' => $id_user
                );
                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->late_registration_model->editLateRegistration($data,$id);
                redirect('/setup/lateRegistration/list');
            }
            $data['lateRegistration'] = $this->late_registration_model->getLateRegistration($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Late Registration';
            $this->loadViews("late_registration/edit", $this->global, $data, NULL);
        }
    }
}
