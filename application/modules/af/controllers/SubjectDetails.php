<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SubjectDetails extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('subject_details_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('subject_details.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['subjectDetails'] = $this->subject_details_model->subjectDetailsList();
            $this->global['pageTitle'] = 'School : Country Listing';
            $this->loadViews("subject_details/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('subject_details.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                 $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $countryInfo = array('name' => $name,
                    'code' => $code);
                $result = $this->subject_details_model->addNewSubjectDetails($countryInfo);
                redirect('/setup/subjectDetails/list');
            }
            $this->load->model('subject_details_model');
            $this->global['pageTitle'] = 'Subject : Add New Subject';
            $this->loadViews("subject_details/add", $this->global, NULL, NULL);
        }
    }


    function edit($subjectId = NULL)
    {
        if ($this->checkAccess('subject_details.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($subjectId == null)
            {
                redirect('/setup/subjectDetails/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $subjectInfo = array('name' => $name,
                    'code' => $code);
                $result = $this->subject_details_model->editSubjectDetails($subjectInfo,$subjectId);
                redirect('/setup/subjectDetails/list');
            }
            $data['subjectDetails'] = $this->subject_details_model->getSubjectDetails($subjectId);
            $this->global['pageTitle'] = 'Subject : Edit Subject';
            $this->loadViews("subject_details/edit", $this->global, $data, NULL);
        }
    }


    function delete()
    {
        if ($this->checkAccess('country.delete') == 0)
        {
            echo (json_encode(array('status' => 'access')));
        }
        else
        {
            $countryId = $this->input->post('countryId');
            $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
            $result = $this->subject_details_model->deleteCountry($countryId, $countryInfo);
            if ($result > 0)
            {
                echo (json_encode(array('status' => TRUE)));
            }
            else
            {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }
}
