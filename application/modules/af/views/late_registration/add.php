<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Late Registration</h3>
        </div>
        <form id="form_late_registration" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Late Registration Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Type <span class='error-text'>*</span></label>
                        <select name="semester_type" id="semester_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Long">Long</option>
                            <option value="Short">Short</option>
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="effective_date" name="effective_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>No. Of Days <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="no_of_days" name="no_of_days">
                    </div>
                </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Penalty <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="penalty" name="penalty">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                
               
            </div>

        </div>



        <div class="form-container">

            <h4 class="form-group-title" >Special Appeal For Late Registration</h4>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>No. Of Days For Special Appeal <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="no_of_days_for_special_appeal" name="no_of_days_for_special_appeal">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Special Appeal Penalty <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="special_appeal_penalty" name="special_appeal_penalty">
                    </div>
                </div>
               
            </div>
        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_late_registration").validate({
            rules: {
                semester_type: {
                    required: true
                },
                effective_date: {
                    required: true
                },
                no_of_days: {
                    required: true
                },
                penalty: {
                    required: true
                },
                no_of_days_for_special_appeal: {
                    required: true
                },
                special_appeal_penalty: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                semester_type: {
                    required: "<p class='error-text'>Select Semester Type</p>",
                },
                effective_date: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                },
                no_of_days: {
                    required: "<p class='error-text'>No. Of Days Required</p>",
                },
                penalty: {
                    required: "<p class='error-text'>Penalty Amount Required</p>",
                },
                no_of_days_for_special_appeal: {
                    required: "<p class='error-text'>No. Of Special Appeal Days Required</p>",
                },
                special_appeal_penalty: {
                    required: "<p class='error-text'>Special Appeal Penalty Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>