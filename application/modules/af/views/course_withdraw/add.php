<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Course Withdraw</h3>
        </div>
        <form id="form_course_withdraw" action="" method="post">

        <div class="form-container">
        <h4 class="form-group-title">Course Withdraw Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Type <span class='error-text'>*</span></label>
                        <select name="semester_type" id="semester_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Long">Long</option>
                            <option value="Short">Short</option>
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="effective_date_from" name="effective_date_from" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Days <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="min_days" name="min_days">
                    </div>
                </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Days <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_days" name="max_days">
                    </div>
                </div>


                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
               
            </div>
            
        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

            
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_course_withdraw").validate({
            rules: {
                semester_type: {
                    required: true
                },
                effective_date_from: {
                    required: true
                },
                min_days: {
                    required: true
                },
                max_days: {
                    required: true
                }
            },
            messages: {
                semester_type: {
                    required: "<p class='error-text'>Select Semester Type</p>",
                },
                effective_date_from: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                },
                min_days: {
                    required: "<p class='error-text'>Min. Days Required</p>",
                },
                max_days: {
                    required: "<p class='error-text'>Max. Days Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>