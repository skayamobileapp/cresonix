<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Documents For Program</h3>
        </div>
        <form id="form_intake" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Document Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $documentDetails->code; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $documentDetails->name; ?>" readonly>
                    </div>
                </div>

               

                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($documentDetails->status=='1') {
                                 echo "checked=checked";
                              };?> disabled><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($documentDetails->status=='0') {
                                 echo "checked=checked";
                              };?> disabled>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>          

              
            </div>

        </div>

        <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
        </div>


        </form>



        <form id="form_programme_intake" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Program Details</h4>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name; ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                  
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                    </div>
                </div>

                <div class="row">
                    <div id="view"></div>
                </div>

            </div>
        </form>


        <?php
            if(!empty($documentsProgramList))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Program Details</h4>


                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Program</th>
                                 <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($documentsProgramList);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $documentsProgramList[$i]->program_code . " - " . $documentsProgramList[$i]->program_name;?></td>
                                <td class="text-center">

                                <a onclick="deleteDocumentsProgram(<?php echo $documentsProgramList[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>


           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

        function saveData()
        {

        if($('#form_programme_intake').valid())
        {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_document'] = <?php echo $documentDetails->id;?>;
            $.ajax(
            {
               url: '/setup/documents/addProgramToDocument',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                //$("#view").html(result);
               }
            });
        }
    }

    function deleteDocumentsProgram(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/setup/documents/deleteDocumentsProgram/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    function validateDetailsData()
    {
        $('#form_intake').submit();
    }

    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_program: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function() {
        $("#form_intake").validate({
            rules: {
                id_document: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                id_document: {
                    required: "<p class='error-text'>Select Document</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>
<script type="text/javascript">
    $('select').select2();
</script>