<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Events_model extends CI_Model
{
    function eventsList()
    {
        $this->db->select('sp.*');
        $this->db->from('event as sp');
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function eventsListSearch($data)
    {
        $this->db->select('sp.*');
        $this->db->from('event as sp');
        if ($data['name'] != '')
        {
            $likeCriteria = "(sp.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('sp.type', $data['type']);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  
         // echo "<Pre>"; print_r($recepients);exit;
         return $results;
    }

    function getEvents($id)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewEvents($data)
    {
        $this->db->trans_start();
        $this->db->insert('event', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editEvents($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('event', $data);
        return TRUE;
    }

     function deleteEvents($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('event_recepients');
    }

}

