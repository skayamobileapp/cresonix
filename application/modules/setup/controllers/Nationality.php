<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Nationality extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nationality_model');
                        $this->load->model('role_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('nationality.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['nationalityList'] = $this->nationality_model->nationalityListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Nationality List';
            $this->loadViews("nationality/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('nationality.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->nationality_model->addNewNationality($data);
                redirect('/setup/nationality/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Nationality';
            $this->loadViews("nationality/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('nationality.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if ($id == null)
            {
                redirect('/setup/nationality/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->nationality_model->editNationality($data,$id);
                redirect('/setup/nationality/list');
            }
            $data['nationality'] = $this->nationality_model->getNationality($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Nationality';
            $this->loadViews("nationality/edit", $this->global, $data, NULL);
        }
    }
}
