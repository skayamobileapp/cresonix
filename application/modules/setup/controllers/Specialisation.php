<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Specialisation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('specialisation_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('specialisation.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['specialisationList'] = $this->specialisation_model->specialisationListSearch($name);
            $this->global['pageTitle'] = 'School Management System : Education Level List';
            $this->loadViews("specialisation/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('specialisation.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->specialisation_model->addNewSpecialisation($data);
                redirect('/setup/specialisation/list');
            }
            $this->global['pageTitle'] = 'School Management System : Add Education Level';
            $this->loadViews("specialisation/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('specialisation.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/specialisation/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->specialisation_model->editSpecialisation($data,$id);
                redirect('/setup/specialisation/list');
            }
            $data['specialisation'] = $this->specialisation_model->getSpecialisation($id);
            $this->global['pageTitle'] = 'School Management System : Edit Education Level';
            $this->loadViews("specialisation/edit", $this->global, $data, NULL);
        }
    }
}
