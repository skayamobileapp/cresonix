<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgrammeLandscape extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_landscape_model');
        $this->load->model('programme_model');
        $this->load->model('intake_model');
        $this->load->model('semester_model');
        $this->load->model('course_model');
        // $this->load->model('intake_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $name = $this->security->xss_clean($this->input->post('name'));

            $data['searchName'] = $name; 

            $data['programmeList'] = $this->programme_model->programmeListSearch($name);
            
            $this->global['pageTitle'] = 'Inventory Management : Program Landscape List';
            $this->loadViews("programme_landscape/programme_list", $this->global, $data, NULL);
        }
    }

    function programmeLandscapeList($id_programme = NULL)
    {
        if ($this->checkAccess('programme_landscape.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));

            $data['searchParam'] = $formData; 

            $data['intakeList'] = $this->intake_model->intakeListByStatus('1');
            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
            $data['id_programme'] = $id_programme;

            $data['programmeLandscapeList'] = $this->programme_landscape_model->programmeLandscapeByProgrammeId($id_programme,$formData);


            // echo "<Pre>";print_r($data['programme']);exit;


            $this->global['pageTitle'] = 'Inventory Management : Program Landscape List';
            $this->loadViews("programme_landscape/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_programme = NULL)
    {
        if ($this->checkAccess('programme_landscape.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $landscape_code = $this->security->xss_clean($this->input->post('landscape_code'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_intake_to = $this->security->xss_clean($this->input->post('id_intake_to'));
                $min_total_cr_hrs = $this->security->xss_clean($this->input->post('min_total_cr_hrs'));
                $min_repeat_course = $this->security->xss_clean($this->input->post('min_repeat_course'));
                $max_repeat_exams = $this->security->xss_clean($this->input->post('max_repeat_exams'));
                $total_semester = $this->security->xss_clean($this->input->post('total_semester'));
                $total_block = $this->security->xss_clean($this->input->post('total_block'));
                $total_level = $this->security->xss_clean($this->input->post('total_level'));
                $min_total_score = $this->security->xss_clean($this->input->post('min_total_score'));
                $min_pass_subject = $this->security->xss_clean($this->input->post('min_pass_subject'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $program_scheme = $this->security->xss_clean($this->input->post('program_scheme'));
                $learning_mode = $this->security->xss_clean($this->input->post('learning_mode'));
                $program_landscape_type = $this->security->xss_clean($this->input->post('program_landscape_type'));


                $copy_landscape = $this->security->xss_clean($this->input->post('copy_landscape'));


         // echo "<Pre>";print_r($program_scheme);exit;
                // $result = $this->programme_landscape_model->programmeLandscapeDuplicationCheck($id_programme,$id_intake,$program_scheme);



                // if($result == '1' )
                // {

                //     // Duplicate Found
                //     echo "<Pre>";print_r('Dullicate Entry Not Allowed');exit;

                // }
                // else
                // {
            
                    $data = array(
                        'name' => $name,
                        'code' => $landscape_code,
                        'id_intake_to' => $id_intake_to,
                        'id_intake'=>$id_intake,
                        'id_programme' => $id_programme,
                        'min_total_cr_hrs' => round($min_total_cr_hrs),
                        'min_repeat_course' => round($min_repeat_course),
                        'max_repeat_exams' => round($max_repeat_exams),
                        'total_semester' => round($total_semester),
                        'total_block' => round($total_block),
                        'total_level' => round($total_level),
                        'min_total_score' => round($min_total_score),
                        'min_pass_subject' => round($min_pass_subject),
                        'program_landscape_type' => $program_landscape_type,
                        'program_scheme' => $program_scheme,
                        'learning_mode' => $learning_mode,
                        'status' => $status
                    );
            // echo "<pre>"; print_r($min_total_cr_hrs);exit();
                $result = $this->programme_landscape_model->addNewProgrammeLandscapeDetails($data);

                if($result)
                {
                    $result_details = $this->programme_landscape_model->moveSemesterInfoFromTempToDetails($result);
                    $result_details = $this->programme_landscape_model->moveCourseCreditHourDetailsFromTempToDetails($result);
                }


                if($copy_landscape == 1)
                {
                    $id_programme_landscape_for_copy = $this->security->xss_clean($this->input->post('id_programme_landscape_for_copy'));

                    $copiedLandscape= $this->programme_landscape_model->copyFromLandscape($id_programme_landscape_for_copy,$result);

                }



                redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                
            }
            else
            {
                $deleted_semester_details = $this->programme_landscape_model->deleteTempSemesterInfoBySession($id_session);
                $deleted_credit_hour = $this->programme_landscape_model->deleteTempCourseCreditHourDetailsBySession($id_session);
            }
            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);

            $data['programmeList'] = $this->programme_model->programmeList();
            $data['programmelearningMode'] = $this->programme_landscape_model->getProgramLearningModeProgramId($id_programme);

            
            $data['programmeSchemeList'] = $this->programme_landscape_model->getProgramSchemeByProgramId($id_programme);

            $data['landscapeCourseTypeList'] = $this->programme_landscape_model->landscapeCourseTypeListByStatus('1');



            $data['programmeLandscapeListByProgrammeId'] = $this->programme_landscape_model->programmeLandscapeListByProgrammeId($id_programme);

            // echo "<pre>"; print_r($data['programmeLandscapeListByProgrammeId']);exit();


            $data['intakeList'] = $this->programme_landscape_model->intakeListForLandscape($id_programme);
            $this->global['pageTitle'] = 'Inventory Management : Add Program Landscape';
            $this->loadViews("programme_landscape/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL,$id_programme = NULL)
    {
        if ($this->checkAccess('programme_landscape.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo "<pre>"; print_r($id);exit();
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $landscape_code = $this->security->xss_clean($this->input->post('landscape_code'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_intake_to = $this->security->xss_clean($this->input->post('id_intake_to'));
                $min_total_cr_hrs = $this->security->xss_clean($this->input->post('min_total_cr_hrs'));
                $min_repeat_course = $this->security->xss_clean($this->input->post('min_repeat_course'));
                $max_repeat_exams = $this->security->xss_clean($this->input->post('max_repeat_exams'));
                $total_semester = $this->security->xss_clean($this->input->post('total_semester'));
                $total_block = $this->security->xss_clean($this->input->post('total_block'));
                $total_level = $this->security->xss_clean($this->input->post('total_level'));
                $min_total_score = $this->security->xss_clean($this->input->post('min_total_score'));
                $min_pass_subject = $this->security->xss_clean($this->input->post('min_pass_subject'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $program_scheme = $this->security->xss_clean($this->input->post('program_scheme'));
                $learning_mode = $this->security->xss_clean($this->input->post('learning_mode'));
                $program_landscape_type = $this->security->xss_clean($this->input->post('program_landscape_type'));


         // echo "<Pre>";print_r($program_scheme);exit;
                // $result = $this->programme_landscape_model->programmeLandscapeDuplicationCheck($id_programme,$id_intake,$program_scheme);



                // if($result == '1' )
                // {

                //     // Duplicate Found
                //     echo "<Pre>";print_r('Dullicate Entry Not Allowed');exit;

                // }
                // else
                // {
            
                    $data = array(
                        'name' => $name,
                        'code' => $landscape_code,
                        'id_intake_to' => $id_intake_to,
                        'id_programme' => $id_programme,
    					'min_total_cr_hrs' => round($min_total_cr_hrs),
                        'min_repeat_course' => round($min_repeat_course),
                        'max_repeat_exams' => round($max_repeat_exams),
                        'total_semester' => round($total_semester),
                        'total_block' => round($total_block),
                        'total_level' => round($total_level),
                        'min_total_score' => round($min_total_score),
                        'min_pass_subject' => round($min_pass_subject),
                        'program_landscape_type' => $program_landscape_type,
                        'program_scheme' => $program_scheme,
                        'learning_mode' => $learning_mode,
                        'status' => $status
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->editProgrammeLandscapeDetails($data,$id);
                    redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                // }
            }
            $data['id_program_landscape'] = $id;
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['programmeLandscapeDetails'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id);
            $data['programmelearningMode'] = $this->programme_landscape_model->getProgramLearningModeProgramId($id_programme);


            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);

            $data['getCreditHourDetailsByProgramLandscapeId'] = $this->programme_landscape_model->getCreditHourDetailsByProgramLandscapeId($id);

            $data['landscapeCourseTypeList'] = $this->programme_landscape_model->landscapeCourseTypeListByStatus('1');
            

             $data['programmeSchemeList'] = $this->programme_landscape_model->getProgramSchemeByProgramId($id_programme);

         // echo "<Pre>";print_r($data['programmeLandscapeDetails']);exit;


            $data['programLandscapeHasSemesterList'] = $this->programme_landscape_model->programLandscapeHasSemesterList($id);

            // echo "<pre>"; print_r($data['getCreditHourDetailsByProgramLandscapeId']);exit();
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/edit", $this->global, $data, NULL);
        }
    }

    function addcourse($id = NULL, $id_programme = NULL, $id_intake = NULL)
    {
        if ($this->checkAccess('programme_landscape.addcourse') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            $data['programmeLandscapeDetails'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id);

   
            if($this->input->post())
            {
                $id_program_landscape = $id;
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $course_type = $this->security->xss_clean($this->input->post('course_type'));
                $pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));
                $id_program_major = $this->security->xss_clean($this->input->post('id_program_major'));
                $id_program_minor = $this->security->xss_clean($this->input->post('id_program_minor'));

                $result = $this->programme_landscape_model->programmeLandscapeCourseDuplicationCheck($id_program_landscape,$id_course);

         // echo "<Pre>";print_r($data['programmeLandscapeDetails']);exit;


                // if($result == '1' )
                // {

                //     // Duplicate Found
                //     echo "<Pre>";print_r('Duplicate Entry Not Allowed');exit;

                // }
                // else
                // {

                    $data_add = array(
                        'id_program_landscape' => $id_program_landscape,
                        'id_program_scheme' => $data['programmeLandscapeDetails']->learning_mode,
                        'id_semester' => $id_semester,
                        'id_course' => $id_course,
                        'id_intake' => $id_intake,
                        'id_program' => $id_programme,
                        'id_program_major' => $id_program_major,
                        'id_program_minor' => $id_program_minor,
                        'course_type' => $course_type,
                        'pre_requisite' => $pre_requisite
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->addCourseToProgramLandscape($data_add);
                    // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                   redirect($_SERVER['HTTP_REFERER']);

                // }
            }

            $data['id_programme'] = $id_programme;
            $data['id_intake'] = $id_intake;
            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['semesterList'] = $this->programme_landscape_model->semesterListByStatus('1');

            // For PG Type == Audit Changed On 02-12-2020
            // $data['courseList'] = $this->programme_landscape_model->courseListForLandscapeCourseAdd($data['programme']->education_level_name);


            $data['courseList'] = $this->programme_landscape_model->courseListForLandscapeCourses($id);

            $data['intakeList'] = $this->intake_model->intakeList();



            $data['getCompulsoryCourse'] = $this->programme_landscape_model->getCompulsoryCourse($id);


            // echo "<pre>"; print_r($data['getCompulsoryCourse']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/addcourse", $this->global, $data, NULL);
        }
    }

    function addLearningMode($id = NULL, $id_programme = NULL, $id_intake = NULL)
    {
        if ($this->checkAccess('programme_landscape.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            $data['programmeLandscapeDetails'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id);
                        $data['courseTypeList'] = $this->programme_landscape_model->landscapeCourseTypeListSearch();

            if($this->input->post())
            {
                // echo "<pre>"; print_r($this->input->post());exit();

                $id_program_landscape = $id;
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_learning_mode = $this->security->xss_clean($this->input->post('id_learning_mode'));

                // $result = $this->programme_landscape_model->programmeLandscapeCourseDuplicationCheck($id_program_landscape,$id_course);

         // echo "<Pre>";print_r($result);exit;


                // if($result == '1' )
                // {

                //     // Duplicate Found
                //     echo "<Pre>";print_r('Duplicate Entry Not Allowed');exit;

                // }
                // else
                // {

                    $data_add = array(
                        'id_program_landscape' => $id_program_landscape,
                        'id_semester' => $id_semester,
                        'id_course' => $id_course,
                        'id_intake' => $id_intake,
                        'id_program' => $id_programme,
                        'id_learning_mode' => $id_learning_mode,
                        'status' => 1,
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->addLearningModeToProgramLandscape($data_add);
                    // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                   redirect($_SERVER['HTTP_REFERER']);

                // }
            }

            $data['id_programme'] = $id_programme;
            $data['id_intake'] = $id_intake;

            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
            
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['semesterList'] = $this->programme_landscape_model->semesterListByStatus('1');
            $data['courseList'] = $this->course_model->courseList();
            $data['intakeList'] = $this->intake_model->intakeList();


            $data['getLearningModeByPLID'] = $this->programme_landscape_model->getLearningModeByPLID($id);
            $data['programmeSchemeList'] = $this->programme_landscape_model->getProgramSchemeByProgramId($id_programme);
            $data['programmelearningMode'] = $this->programme_landscape_model->getProgramLearningModeProgramId($id_programme);

            // echo "<pre>"; print_r($data['getLearningModeByPLID']);exit();
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/add_learning_mode", $this->global, $data, NULL);
        }
    }

    function programObjective($id = NULL, $id_programme = NULL, $id_intake = NULL)
    {
        if ($this->checkAccess('programme_landscape.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            $data['programmeLandscapeDetails'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id);
                        $data['courseTypeList'] = $this->programme_landscape_model->landscapeCourseTypeListSearch();

            if($this->input->post())
            {
                // echo "<pre>"; print_r($this->input->post());exit();

                $id_program_landscape = $id;
                $id_objective = $this->security->xss_clean($this->input->post('id_objective'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_course_registration_type = $this->security->xss_clean($this->input->post('id_course_registration_type'));


                    $data_add = array(
                        'id_program_landscape' => $id_program_landscape,
                        'id_objective' => $id_objective,
                        'id_course' => $id_course,
                        'id_course_registration_type' => $id_course_registration_type,
                        'id_intake' => $id_intake,
                        'id_program' => $id_programme,
                        'status' => 1,
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->addProgramObjectiveToProgramLandscape($data_add);
                    // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                   redirect($_SERVER['HTTP_REFERER']);

                // }
            }

            $data['id_programme'] = $id_programme;
            $data['id_intake'] = $id_intake;

            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
            
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['courseList'] = $this->course_model->courseList();


            // $data['programmeSchemeList'] = $this->programme_landscape_model->getProgramSchemeByProgramId($id_programme);
            $data['getProgramObjectiveByProgramLandscape'] = $this->programme_landscape_model->getProgramObjectiveByProgramLandscape($id);

            $data['programmeObjectiveList'] = $this->programme_landscape_model->programmeObjectiveListByProgramId($id_programme);
            $data['courseTypeList'] = $this->programme_landscape_model->courseTypeListByStatus('1');


            // echo "<pre>"; print_r($data['getLearningModeByPLID']);exit();
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/add_program_objective", $this->global, $data, NULL);
        }
    }


    function subjectRegistration($id = NULL, $id_programme = NULL, $id_intake = NULL)
    {
        if ($this->checkAccess('programme_landscape.addcourse') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            $data['programmeLandscapeDetails'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id);
                        $data['courseTypeList'] = $this->programme_landscape_model->landscapeCourseTypeListSearch();

            if($this->input->post())
            {
                $id_program_landscape = $id;
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $course_type = $this->security->xss_clean($this->input->post('course_type'));
                $pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));
                $id_program_major = $this->security->xss_clean($this->input->post('id_program_major'));
                $id_program_minor = $this->security->xss_clean($this->input->post('id_program_minor'));

                // $result = $this->programme_landscape_model->programmeLandscapeCourseDuplicationCheck($id_program_landscape,$id_course);

         // echo "<Pre>";print_r($result);exit;


                // if($result == '1' )
                // {

                //     // Duplicate Found
                //     echo "<Pre>";print_r('Duplicate Entry Not Allowed');exit;

                // }
                // else
                // {

                    $data_add = array(
                        'id_program_landscape' => $id_program_landscape,
                        'id_program_scheme' => $data['programmeLandscapeDetails']->learning_mode,
                        'id_semester' => $id_semester,
                        'id_course' => $id_course,
                        'id_intake' => $id_intake,
                        'id_program' => $id_programme,
                        'id_program_major' => $id_program_major,
                        'id_program_minor' => $id_program_minor,
                        'course_type' => $course_type,
                        'pre_requisite' => $pre_requisite,
                        'status' => 1
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->addCourseToProgramLandscape($data_add);
                    // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                   redirect($_SERVER['HTTP_REFERER']);

                // }
            }
            $data['id_programme'] = $id_programme;
            $data['id_intake'] = $id_intake;

            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
            
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['semesterList'] = $this->programme_landscape_model->semesterListByStatus('1');
            $data['courseList'] = $this->programme_landscape_model->courseListForLandscapeCourses($id);
            $data['intakeList'] = $this->intake_model->intakeList();

            $data['getCompulsoryCourse'] = $this->programme_landscape_model->getCompulsoryCourse($id);
            // $data['getMajorCourse'] = $this->programme_landscape_model->getMajorCourse($id);
            // $data['getMinorCourse'] = $this->programme_landscape_model->getMinorCourse($id);
            // $data['getNotCompulsoryCourse'] = $this->programme_landscape_model->getNotCompulsoryCourse($id);


            $data['programMajorList'] = $this->programme_landscape_model->programMajorList($id_programme);
            $data['programMinorList'] = $this->programme_landscape_model->programMinorList($id_programme);

            $data['programLandscapeHasRequirementList'] = $this->programme_landscape_model->programLandscapeHasRequirementList($id);

            // $data['programmeSchemeList'] = $this->programme_landscape_model->getProgramSchemeByProgramId($id_programme);

            // echo "<pre>"; print_r($data['getCompulsoryCourse']);exit();

            
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/add_subject_registration", $this->global, $data, NULL);
        }
    }

    function editCourseTab($id = NULL, $id_programme = NULL, $id_intake = NULL)
    {
        if ($this->checkAccess('programme_landscape.addcourse') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            $data['programmeLandscapeDetails'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id);
                        $data['courseTypeList'] = $this->programme_landscape_model->landscapeCourseTypeListSearch();

            if($this->input->post())
            {
                $id_program_landscape = $id;
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $course_type = $this->security->xss_clean($this->input->post('course_type'));
                $pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));
                $id_program_major = $this->security->xss_clean($this->input->post('id_program_major'));
                $id_program_minor = $this->security->xss_clean($this->input->post('id_program_minor'));

                // $result = $this->programme_landscape_model->programmeLandscapeCourseDuplicationCheck($id_program_landscape,$id_course);

         // echo "<Pre>";print_r($result);exit;


                // if($result == '1' )
                // {

                //     // Duplicate Found
                //     echo "<Pre>";print_r('Duplicate Entry Not Allowed');exit;

                // }
                // else
                // {

                    $data_add = array(
                        'id_program_landscape' => $id_program_landscape,
                        'id_program_scheme' => $data['programmeLandscapeDetails']->learning_mode,
                        'id_semester' => $id_semester,
                        'id_course' => $id_course,
                        'id_intake' => $id_intake,
                        'id_program' => $id_programme,
                        'id_program_major' => $id_program_major,
                        'id_program_minor' => $id_program_minor,
                        'course_type' => $course_type,
                        'pre_requisite' => $pre_requisite
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->addCourseToProgramLandscape($data_add);
                    // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                   redirect($_SERVER['HTTP_REFERER']);

                // }
            }
            $data['id_programme'] = $id_programme;
            $data['id_intake'] = $id_intake;

            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
            
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['semesterList'] = $this->programme_landscape_model->semesterListByStatus('1');
            $data['courseList'] = $this->programme_landscape_model->courseListForLandscapeCourseAdd($data['programme']->education_level_name);
            $data['intakeList'] = $this->intake_model->intakeList();

            $data['getCompulsoryCourse'] = $this->programme_landscape_model->getCompulsoryCourse($id);
            $data['getMajorCourse'] = $this->programme_landscape_model->getMajorCourse($id);
            $data['getMinorCourse'] = $this->programme_landscape_model->getMinorCourse($id);
            $data['getNotCompulsoryCourse'] = $this->programme_landscape_model->getNotCompulsoryCourse($id);


            $data['programMajorList'] = $this->programme_landscape_model->programMajorList($id_programme);
            $data['programMinorList'] = $this->programme_landscape_model->programMinorList($id_programme);

            $data['programLandscapeHasRequirementList'] = $this->programme_landscape_model->programLandscapeHasRequirementList($id);

            $data['programmeSchemeList'] = $this->programme_landscape_model->getProgramSchemeByProgramId($id_programme);

            // echo "<pre>"; print_r($data['getCompulsoryCourse']);exit();

            
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/edit_course_tab", $this->global, $data, NULL);
        }
    }



    function editProgramRequirementTab($id = NULL,$id_programme = NULL)
    {
        if ($this->checkAccess('programme_landscape.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo "<pre>"; print_r($id);exit();
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                // $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $min_total_cr_hrs = $this->security->xss_clean($this->input->post('min_total_cr_hrs'));
                $min_repeat_course = $this->security->xss_clean($this->input->post('min_repeat_course'));
                $max_repeat_exams = $this->security->xss_clean($this->input->post('max_repeat_exams'));
                $total_semester = $this->security->xss_clean($this->input->post('total_semester'));
                $total_block = $this->security->xss_clean($this->input->post('total_block'));
                $total_level = $this->security->xss_clean($this->input->post('total_level'));
                $min_total_score = $this->security->xss_clean($this->input->post('min_total_score'));
                $min_pass_subject = $this->security->xss_clean($this->input->post('min_pass_subject'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $program_scheme = $this->security->xss_clean($this->input->post('program_scheme'));

         //        $result = $this->programme_landscape_model->programmeLandscapeDuplicationCheck($id_programme,$id_intake);

         // // echo "<Pre>";print_r($result);exit;


         //        if($result == '1' )
         //        {

         //            // Duplicate Found
         //            echo "<Pre>";print_r('Dullicate Entry Not Allowed');exit;

         //        }
         //        else
         //        {
            
                    $data = array(
                        'name' => $name,
                        'id_programme' => $id_programme,
                        'min_total_cr_hrs' => $min_total_cr_hrs,
                        'min_repeat_course' => $min_repeat_course,
                        'max_repeat_exams' => $max_repeat_exams,
                        'total_semester' => $total_semester,
                        'total_block' => $total_block,
                        'total_level' => $total_level,
                        'min_total_score' => $min_total_score,
                        'min_pass_subject' => $min_pass_subject,
                        'program_scheme' => $program_scheme,
                        'status' => $status
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->editProgrammeLandscapeDetails($data,$id);
                    redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                
            }
            $data['id_program_landscape'] = $id;
            $data['landscapeCourseTypeList'] = $this->programme_landscape_model->landscapeCourseTypeListByStatus('1');
            $data['programmeLandscapeDetails'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id);
            $data['programLandscapeHasRequirementList'] = $this->programme_landscape_model->programLandscapeHasRequirementList($id);


            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
             $data['programmeSchemeList'] = $this->programme_landscape_model->getProgramSchemeByProgramId($id_programme);


            // echo "<pre>"; print_r($data['programLandscapeHasRequirementList']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/edit_program_tab", $this->global, $data, NULL);
        }
    }


    function addRequisite($id_compulsary_course = NULL, $id_program_landscape = NULL, $id_programme = NULL, $id_intake = NULL, $id_course = NULL, $key = NULL)
    {
        if ($this->checkAccess('programme_landscape.addcourse') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_compulsary_course == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            if($this->input->post())
            {
                $id_compulsary_course = $id_compulsary_course;

                // echo "<Pre>";print_r($result);exit;

                $id_master_course = $id_compulsary_course;
                $requisite_type = $this->security->xss_clean($this->input->post('requisite_type'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $min_pass_grade = $this->security->xss_clean($this->input->post('min_pass_grade'));
                $min_total_credit = $this->security->xss_clean($this->input->post('min_total_credit'));
               


                    $data = array(
                        'id_program_landscape' => $id_program_landscape,
                        'id_compulsary_course' => $id_compulsary_course,
                        'id_course' => $id_course,
                        'id_intake' => $id_intake,
                        'id_program' => $id_programme,
                        'id_master_course'=> $id_master_course,
                        'requisite_type' => $requisite_type,
                        'min_pass_grade' => $min_pass_grade,
                        'min_total_credit' => $min_total_credit,
                        'status' => 1
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->programme_landscape_model->addCourseToRequisite($data);
                    // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                   redirect($_SERVER['HTTP_REFERER']);

            }
            $data['id_programme'] = $id_programme;
            $data['id_intake'] = $id_intake;
            $data['id_program_landscape'] = $id_program_landscape;
            $data['key'] = $key;

            
            $data['programme'] = $this->programme_landscape_model->getProgramme($id_programme);
            $data['gradeList'] = $this->programme_landscape_model->getGradeSetupDetailsByCourse($id_course);
            // $data['courseList'] = $this->programme_landscape_model->courseListForLandscapeCourseAdd($data['programme']->education_level_name);
            $data['courseList'] = $this->programme_landscape_model->courseListByStatus('1');
            $data['courseDetails'] = $this->programme_landscape_model->getCourse($id_course);
            $data['landscape'] = $this->programme_landscape_model->getProgrammeLandscapeDetails($id_program_landscape);
            $data['courseData'] = $this->programme_landscape_model->getCourseData($id_compulsary_course);

            // echo "<pre>"; print_r($data['courseDetails']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Landscape';
            $this->loadViews("programme_landscape/add_requisite", $this->global, $data, NULL);
        }
    }

    function delete_course_program()
    {
        $id = $this->input->get('id');

       $this->programme_landscape_model->deleteCourseFromProgramLandscape($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_pl_learning_mode()
    {
        $id = $this->input->get('id');
        $this->programme_landscape_model->deletePLLearningMode($id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    function deleteProgramLandscapeRequisiteDetails($id)
    {
       $this->programme_landscape_model->deleteProgramLandscapeRequisiteDetails($id);
       echo 'success';
    }


    function tempSemesterInfoAdd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->programme_landscape_model->tempSemesterInfoAdd($tempData);
        
        $data = $this->displaytempdata();
        
        echo $data;
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_landscape_model->getTempSemesterInfoBySession($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Semester Type</th>
                    <th>Registration Rule</th>
                    <th class='text-center'>Min | Max</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $semester_type = $temp_details[$i]->semester_type;
                    $registration_rule = $temp_details[$i]->registration_rule;
                    $minimum = $temp_details[$i]->minimum;
                    $maximum = $temp_details[$i]->maximum;
                    $j = $i+1;
                    
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$semester_type</td>
                            <td>$registration_rule</td>
                            <td class='text-center'>$minimum | $maximum</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempSemesterInfo($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempSemesterInfo($id)
    {
       $this->programme_landscape_model->deleteTempSemesterInfo($id);
       $data = $this->displaytempdata();
       echo $data;
       // echo 'success';
    }

    function semesterInfoAdd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        $inserted_id = $this->programme_landscape_model->semesterInfoAdd($tempData);        
        // echo $inserted_id;
       echo 'success';
    }


    function deleteSemesterInfo($id)
    {
       $this->programme_landscape_model->deleteSemesterInfo($id);
       // echo $data;
       echo 'success';
    }

    function programInfoAdd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        $inserted_id = $this->programme_landscape_model->programInfoAdd($tempData);        
        echo $inserted_id;        
    }

    function deleteProgramInfo($id)
    {
       $this->programme_landscape_model->deleteProgramInfo($id);
       echo 'success';
    }

    function editProgramObjectiveProgrammeLandscapeDetails($id)
    {
        $objective_details = $this->programme_landscape_model->getProgramObjectiveLandscape($id);

        // echo "<Pre>";print_r($objective_details);exit();

        $status = $objective_details->status;

        if($status == 1)
        {
            $updating_status = 0;
        }else
        {
            $updating_status = 1;
        }

        $data['status'] = $updating_status;

        $updated = $this->programme_landscape_model->editProgramObjectiveProgrammeLandscapeDetails($data,$id);

        echo "success";exit;
    }

    function editSubjectRegistrationProgrammeLandscapeDetails($id)
    {
        $subject_details = $this->programme_landscape_model->getSubjectRegistrationLandscape($id);

        // echo "<Pre>";print_r($objective_details);exit();

        $status = $subject_details->status;

        if($status == 1)
        {
            $updating_status = 0;
        }else
        {
            $updating_status = 1;
        }

        $data['status'] = $updating_status;

        $updated = $this->programme_landscape_model->editSubjectRegistrationProgrammeLandscapeDetails($data,$id);

        echo "success";exit;
    }


    function saveCreditHourDetails()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->programme_landscape_model->saveCreditHourDetails($tempData);
        
        $data = $this->displayTempCreditHourDetails();
        
        echo $data;
    }

    function displayTempCreditHourDetails()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_landscape_model->getTempCreditHourDetailsBySession($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Course Type</th>
                    <th>Hours</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $course_type_code = $temp_details[$i]->course_type_code;
                    $course_type_name = $temp_details[$i]->course_type_name;
                    $hours = $temp_details[$i]->hours;

                    $j = $i+1;
                    
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$course_type_code - $course_type_name</td>
                            <td>$hours</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempCreditHourDetails($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempCreditHourDetails($id)
    {
       $this->programme_landscape_model->deleteTempCreditHourDetails($id);
       $data = $this->displayTempCreditHourDetails();
       echo $data;
       // echo 'success';
    }

    function addCourseCreditHoursDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        $inserted_id = $this->programme_landscape_model->addCourseCreditHoursDetails($tempData);        
        // echo $inserted_id;
       echo 'success';
    }
    

    function deleteCourseCreditHour($id)
    {
       $this->programme_landscape_model->deleteCourseCreditHour($id);
       // echo $data;
       echo 'success';
    }
}
