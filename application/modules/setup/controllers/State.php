<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class State extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('state_model');
        $this->load->model('country_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('state.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
            {
            if ($this->input->method() == "post")
            {
                $name = $this->security->xss_clean($this->input->post('state'));
                $idCountry = $this->security->xss_clean($this->input->post('idCountry'));
            }
            else
            {
                $name = '';
                $idCountry = 0;
            }
            $data['name'] = $name;
            $data['idCountry'] = $idCountry;
            $data['stateRecords'] = $this->state_model->stateListing($name, $idCountry);
            $data['countries'] = $this->country_model->countryListing('');
            //echo "<Pre>"; print_r($data['stateRecords']);exit;

            $this->global['pageTitle'] = 'School : State Listing';
            $this->loadViews("state/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('state.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['countries'] = $this->country_model->countryListing('');
            $this->global['pageTitle'] = 'School : Add New State';
            $this->loadViews("state/add", $this->global, $data, NULL);
        }
    }

    function addNewState()
    {
        if ($this->checkAccess('state.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $idCountry = $this->security->xss_clean($this->input->post('idCountry'));
            $status = $this->security->xss_clean($this->input->post('status'));

            $stateInfo = array(
                'name' => $name,
                'id_country' => $idCountry,
                'status' => $status
            );
            $result = $this->state_model->addNewState($stateInfo);
            redirect('/setup/state/list');
        }
    }


    function edit($stateId = NULL)
    {

        if ($this->checkAccess('state.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($stateId == null) {

                redirect('/setup/state/list');
            }

            $data['countries'] = $this->country_model->countryListing('');
            $data['stateInfo'] = $this->state_model->getStateInfo($stateId);

            $this->global['pageTitle'] = 'School : Edit State';

            $this->loadViews("state/edit", $this->global, $data, NULL);
        }
    }


    function editState()
    {
        
        if ($this->checkAccess('state.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $stateId = $this->input->post('stateId');   
            $name = $this->security->xss_clean($this->input->post('name'));
            $idCountry = $this->security->xss_clean($this->input->post('idCountry'));
            $status = $this->security->xss_clean($this->input->post('status'));

            $stateInfo = array(
                'name' => $name,
                'id_country' => $idCountry,
                'status' => $status
            );
            $result = $this->state_model->editState($stateInfo, $stateId);

            redirect('/setup/state/list');
        }
    }

    function delete()
    {
        if ($this->checkAccess('state.delete') == 0) {
            echo (json_encode(array('status' => 'access')));
        } else {
            $stateId = $this->input->post('stateId');
            $stateInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));

            $result = $this->state_model->deleteState($stateId, $stateInfo);

            if ($result > 0) {
                echo (json_encode(array('status' => TRUE)));
            } else {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }

    function generateOtherStatesByCountry()
    {
        $countries = $this->state_model->getCountryList();

        // echo "<Pre>";print_r($countries);exit();

        foreach ($countries as $country)
        {
            $id_country = $country->id;

            $data['id_country'] = $id_country;
            $data['name'] = 'Others';
            $data['status'] = 1;
        

            $inserted_state = $this->state_model->addNewState($data);

            // echo "<Pre>";print_r($inserted_state);exit();
            echo "<br> Inserted State :". $inserted_state;
        }
        // echo "<br>";exit();
    }
}
