<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Batch</h3>
        </div>

        <div class="topnav">
          <a href="<?php echo '../edit/' . $id_batch; ?>" title="Partner University Info">Batch Info</a>  | 
          <a href="<?php echo '../programme/' . $id_batch; ?>" title="Batch Programme Info" style="background: #aaff00">Batch Programme Info</a>
        </div>


        <br>



        <form id="form_award" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Programme Details</h4>

            <div class="row">
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Programme <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name . " - " . $record->code;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php echo date('d/m/Y', strtotime($batch->start_date)); ?>">
                    </div>
                </div>



            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>



        <?php

            if(!empty($batchHasProgramme))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Batch Has programme Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Programme</th>
                                 <th>Start Date</th>
                                 <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($batchHasProgramme);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $batchHasProgramme[$i]->programme_code . " - " . $batchHasProgramme[$i]->programme_name;?></td>
                                <td><?php echo date('d-m-Y', strtotime($batchHasProgramme[$i]->start_date));?></td>
                                <td class="text-center">
                                    <a onclick="deleteBatchHasProgramme(<?php echo $batchHasProgramme[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
    });


    function deleteBatchHasProgramme(id)
    {
         $.ajax(
            {
               url: '/setup/batch/deleteBatchHasProgramme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 window.location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                id_programme: {
                    required: true
                },
                start_date: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
