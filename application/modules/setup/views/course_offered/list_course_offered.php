 <form action="" method="post" id="searchForm">
  <div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Course Offered By Semester</h3>
      <a href="<?php  echo '../add/'.$id_semester; ?>" class="btn btn-primary">+ Offer New Course</a>
    
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
       
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">
                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control" disabled>
                          <option value="">Select</option>
                          <?php
                          if (!empty($semesterList)) {
                            foreach ($semesterList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $id_semester)
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->code . " - " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Faculty Program</label>
                      <div class="col-sm-8">
                        <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($facultyProgramList)) {
                            foreach ($facultyProgramList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $id_faculty_program)
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->code . " - " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary" name="btn" value="search">Search</button>
                <a href="<?php echo '../edit/'.$id_semester ?>" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        
      </div>
    </div>

      <a href="<?php  echo '../list' ?>" class=" btn btn-link">< Back</a>
    

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Course Code</th>
            <th>Course Name</th>
            <th>Credit Hours</th>
            <th>Faculty Program</th>
            <th style='text-align: center;'>
              <input type='checkbox' id='checkAll' name='checkAll'> Remove All
            </th>
                    </tr>
            <!-- <th style="text-align: center;">Action</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($courseOfferedList))
          {
            $i=1;
            foreach ($courseOfferedList as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->course_code; ?></td>
                <td><?php echo $record->course_name; ?></td>
                <td><?php echo $record->credit_hours; ?></td>
                <td><?php echo $record->faculty_code . " - " . $record->faculty_name; ?></td>
                <td class='text-center'>
                    <input type='checkbox' id='id_course_offered[]' name='id_course_offered[]' class='check' value='<?php echo $record->id; ?>'>
                </td>

                <!-- <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">
                    Edit
                  </a>
               </td> -->
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>

      <div class="app-btn-group">
        <button type="submit" name="btn" value="remove" class="btn btn-primary">Remove</button>
      </div>


    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>

</form>
<script>
    $('select').select2();

    function deleteCourseOffered()
    {
      var tempPR = {};
        tempPR['id_course_offered'] = $("#id_course_offered").val();

        $.ajax(
        {
           url: '/setup/courseOffered/deleteCourseOffered',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            alert(result);
            // window.location.reload();
           }
        });
    }
</script>