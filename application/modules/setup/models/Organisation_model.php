<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Organisation_model extends CI_Model
{

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function countryListByActivity($status)
    {
    	$this->db->select('a.*');
        $this->db->from('country as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('state as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function staffListByActivity($status)
    {
        $this->db->select('a.*');
        $this->db->from('staff as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }

    function editOrganisation($data, $id)
    {
        // echo "<Pre>"; print_r($id);exit;
        $this->db->where('id', $id);
        $this->db->update('organisation', $data);
        return TRUE;
    }

    function addNewOrganisationComitee($data)
    {
        $this->db->trans_start();
        $this->db->insert('organisation_comitee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteOrganisationComitee($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('organisation_comitee');
        return TRUE;
    }

    function organisationComiteeList($id_organisation)
    {
        $this->db->select('a.*');
        $this->db->from('organisation_comitee as a');
        $this->db->where('a.id_organisation', $id_organisation);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function addTrainingCenter($data)
    {
        $this->db->trans_start();
        $this->db->insert('organisation_has_training_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTrainingCenter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('organisation_has_training_center');
        return TRUE;
    }

    function trainingCenterList($id_organisation)
    {
        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $this->db->where('a.id_organisation', $id_organisation);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programListByActivity($status)
    {
        $this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getOrganisationBranchProgram($tempData)
    {
        $this->db->select('otchp.*, p.code as program_code, p.name as program_name');
        $this->db->from('organisation_training_center_has_program as otchp');
        $this->db->join('programme as p', 'otchp.id_program = p.id');
        $this->db->where('otchp.id_organisation', $tempData['id_organisation']);
        $this->db->where('otchp.id_training_center', $tempData['id_training_center']);
        $query = $this->db->get();
        return $query->result();
    }

    function addOrganisationBranchProgram($data)
    {
        $this->db->trans_start();
        $this->db->insert('organisation_training_center_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteProgramDatail($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('organisation_training_center_has_program');
        return TRUE;
    }
    
    function editUser($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        return TRUE;
    }
}