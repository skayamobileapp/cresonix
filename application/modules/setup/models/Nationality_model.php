<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Nationality_model extends CI_Model
{
    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function nationalityListSearch($search)
    {
        $this->db->select('n.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('nationality as n');
        $this->db->join('users as cre','n.created_by = cre.id','left');
        $this->db->join('users as upd','n.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(n.name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("n.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getNationality($id)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewNationality($data)
    {
        $this->db->trans_start();
        $this->db->insert('nationality', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editNationality($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('nationality', $data);
        return TRUE;
    }
}

