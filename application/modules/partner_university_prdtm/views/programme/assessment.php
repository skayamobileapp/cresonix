<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/partner_university_prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

          <li><a href="/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/partner_university_prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
          

          <?php
          if ($programmeDetails->send_for_approval == '0')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/sendForApproval/<?php echo $id_programme;?>">Send For Approval</a></li>
            
           <?php
          }
          else
          {
            ?>
        
            <li><a href="/partner_university_prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            <?php
          }
          ?>
           
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Assessment Details</h4>
            <div class="row">


             

              
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Total Weightage (in Percentage %) <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="total_weightage_percentage" name="total_weightage_percentage" value="<?php echo $assessmentDetailsListMain[0]->total_weightage_percentage;?>" required>
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Passing Mark (in Percentage %) <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="passing_mark_percentage" name="passing_mark_percentage" value="<?php echo $assessmentDetailsListMain[0]->passing_mark_percentage;?>" required>
                  </div>
               </div>

               <div class="col-sm-3">
                  <div class="form-group pt-10">
                     <button type="submit" class="btn btn-primary btn-lg" value="Objective" name="save">Update</button>
                  </div>
               </div>
               
              
            </div>
         </div>
     
      </form>

      <form id="form_assesment" action="" method="post">


         <div class="form-container">
            <h4 class="form-group-title">Assessment Details</h4>

            
            <div class="row">


               <div class="col-sm-3">
                  <div class="form-group">
                     <label>Assessment Component <span class='error-text'>*</span></label>
                   
                     <select name='id_examination_components' id='id_examination_components' class="form-control">
                      <option value="">Select</option>
                      <?php
                      for($i=0;$i<count($examinationComponent);$i++)
                      {
                        ?>
                      <option value="<?php echo $examinationComponent[$i]->id;?>"
                        <?php
                        if($programmeAssesment->id_examination_components == $examinationComponent[$i]->id)
                        {
                          echo 'selected';
                        }
                        ?>
                        >
                        <?php echo $examinationComponent[$i]->name;?>  
                      </option>
                      <?php
                      }
                      ?> 
                     </select>

                  </div>
               </div>

              
               <div class="col-sm-2">
                  <div class="form-group">
                     <label>Weightage <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="weightage" name="weightage" value="<?php echo $programmeAssesment->weightage; ?>" required>
                  </div>
               </div>

               <div class="col-sm-2">
                  <div class="form-group">
                     <label>Total Mark <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="total_mark" name="total_mark" value="<?php echo $programmeAssesment->total_mark; ?>" required>
                  </div>
               </div>

               <div class="col-sm-2">
                  <div class="form-group">
                     <label>Passing Mark <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="passing_mark" name="passing_mark" value="<?php echo $programmeAssesment->passing_mark; ?>" required>
                  </div>
               </div>


            </div>


            <div class="row">
               
               <div class="col-sm-2 pt-10">
                <div class="form-group">

                    <button type="submit" class="btn btn-primary btn-lg" value="Objective" name="save">Save</button>
                    <?php
                    if($id_assesment != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../assessment/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                 </div>
               </div>
            </div>
         </div>
     
      </form>
       <div class="custom-table">
        <table class="table" id="list-table">
          <thead>
            <tr>
              <th>Sl. No</th>
              <th>Assessment Component</th>
              <th>Weightage</th>
              <th>Total Mark</th>
              <th>Passing Mark</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            
            $i=1;
              foreach ($assessmentDetailsList as $record) {
            ?>
                <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo ucfirst($record->component) ?></td>
                  <td><?php echo $record->weightage; ?></td>
                  <td><?php echo $record->total_mark; ?></td>
                  <td><?php echo $record->passing_mark; ?></td>
                  </td>
                  <td class="text-center">
                    <a href='/partner_university_prdtm/programme/assessment/<?php echo $id_programme;?>/<?php echo $record->id;?>'>Edit</a> | 
                    <a onclick="deleteAssesmentData(<?php echo $record->id; ?>)">Delete</a>
                  </td>
                </tr>
            <?php
            $i++;
              }
            
            ?>
          </tbody>
        </table>
       </div>



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

  $('select').select2();


  function deleteAssesmentData(id)
  {
    var cnf= confirm('Do you really want to delete?');
    if(cnf==true)
    {

    $.ajax(
        {
           url: '/partner_university_prdtm/programme/deleteAssesmentData/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              window.location.reload();
           }
        });
    }
  }


  $(document).ready(function()
     {
        $("#form_programme").validate({
            rules: {
                total_weightage_percentage: {
                    required: true
                },
                passing_mark_percentage: {
                    required: true
                }
            },
            messages: {
                total_weightage_percentage: {
                    required: "<p class='error-text'>Weightage Percentage Required</p>",
                },
                passing_mark_percentage: {
                    required: "<p class='error-text'>Passing Percentage Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




  $(document).ready(function()
     {
        $("#form_assesment").validate({
            rules: {
                id_examination_components: {
                    required: true
                },
                weightage: {
                    required: true
                },
                total_mark: {
                    required: true
                },
                passing_mark: {
                    required: true
                }
            },
            messages: {
                id_examination_components: {
                    required: "<p class='error-text'>Select Examination Component</p>",
                },
                weightage: {
                    required: "<p class='error-text'>Weightage Required</p>",
                },
                total_mark: {
                    required: "<p class='error-text'>Total Marks Required</p>",
                },
                passing_mark: {
                    required: "<p class='error-text'>Passing Marks Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>