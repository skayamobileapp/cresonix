
    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">Payment</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--CHECKOUT PAGE  STARTS HERE-->

    <div class="pt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="card mb-3">
              <div class="card-body text-center mb-3">
                    <?php $totalAmount=0;
                for($l=0;$l<count($listOfCourses);$l++) {
                $totalAmount =  $totalAmount + $listOfCourses[$l]->amount;
              }
                ?>
                <h3 class="py-5">Payble Amount <strong>RM <?php echo $totalAmount;?></strong></h3>
                <h4>Pay Via</h4>
                <div class="row payment-method mt-4">
                  <div class="col-md-4">
                    <a href="/profile/payment/onlineislamic" 
                      class="btn btn-outline-primary btn-lg btn-block mb-3"
                    >
                      <span class="icon credit-card"></span> Credit or Debit
                      card
                    </a>
                  </div>
                  <div class="col-md-4">
                    <button
                      class="btn btn-outline-primary btn-lg btn-block mb-3"
                    >
                      <span class="icon online-banking"></span> Online Banking
                    </button>
                  </div>
                  <div class="col-md-4">
                    <button
                      class="btn btn-outline-primary btn-lg btn-block mb-3"
                    >
                      <span class="icon flywire"></span> Flywire
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

