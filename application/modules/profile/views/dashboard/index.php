  <!-- MAIN CONTAINER BLOCK STARTS HERE -->
    <div class="mt-3 container-fluid">
      <div class="row">
        <!-- SIDEBAR NAV STARTS HERE -->
        <div class="col-xl-2 col-md-3">
          <nav
            class="navbar navbar-expand-md navbar-light shadow-sm mb-4 mb-lg-0 small-sidenav"
          >
            <!--MENU-->
            <a
              href="#"
              class="d-xl-none d-lg-none d-md-none text-inherit font-weight-bold"
              >Menu</a
            >
            <button
              class="navbar-toggler d-md-none icon-shape icon-sm rounded bg-primary text-light"
              type="button"
              data-toggle="collapse"
              data-target="#smallSidenav"
              aria-controls="smallSidenav"
              aria-expanded="true"
              aria-label="Toggle navigation"
            >
              <span class="fe fe-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="smallSidenav">
              <div class="navbar-nav flex-column w-100">
                <div class="d-flex mb-3 align-items-center">
                  <div class="avatar avatar-md avatar-indicators avatar-online">

                 <?php if($studentDetails->image=='') { ?> 

                    <img
                      alt="avatar"
                      src="/website/img/blank.png"
                      class="rounded-circle"
                    />
                  <?php } else { ?> 

                    <img
                      alt="avatar"
                      src="/assets/images/<?php echo $studentDetails->profile_pic;?>"
                      class="rounded-circle"
                    />

                  <?php } ?> 


                  </div>
                  <div class="ml-3 lh-1">
                    <h5 class="mb-1"><?php echo $studentDetails->full_name;?></h5>
                    <p class="mb-0 text-muted"><?php echo $studentDetails->nric;?></p>
                  </div>
                </div>
                <ul class="list-unstyled mb-0">
                  <li class="list-unstyled nav-item active">
                    <a href="/profile/dashboard/index" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> Dashboard</a
                    >
                  </li>
                   <li class="list-unstyled nav-item ">
                    <a href="/profile/dashboard/profile" class="nav-link"
                      ><i class="fe fe-user nav-icon"></i> Profile</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/coursedetails" class="nav-link"
                      ><i class="fe fe-book nav-icon"></i> Register Courses</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/invoice" class="nav-link"
                      ><i class="fe fe-clipboard nav-icon"></i> Invoice</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/soa" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> SOA</a
                    >
                  </li>
                 
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/password" class="nav-link"
                      ><i class="fe fe-clock nav-icon"></i> Change Password</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/logout" class="nav-link"
                      ><i class="fe fe-power nav-icon"></i> Logout</a
                    >
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>

        <div class="col-xl-7 col-md-6">
          <div class="row">
            <div class="col-md-6">
               <div id="container"></div>
            </div>
            <div class="col-md-6 mb-3">
              
                               <div id="container1"></div>

              
            </div>
          </div>
          <div class="card mb-3"><br/>
            <div class="card-header bg-light">
              <h4 class="mb-0">Registered Courses</h4>
            </div>
            <div class="table-responsive">
              <table class="courses-table table border-0" style="text-align: center;">
                <thead>
                  <tr>
                    <th scope="col" width="50%">COURSES</th>
                    <th scope="col" class="border-0" width="20%">Course Duration</th>
                    <th scope="col" class="border-0" width="20%">Start Date</th>
                    <th scope="col" class="border-0" width="20%">End Date</th>
                    <th scope="col" class="border-0" width="20%">% of Completion</th>
                    <th scope="col" class="border-0" width="20%">Moodle Access</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for($i=0;$i<count($courseList);$i++) {?> 
                  <tr>
                    <td class="align-middle border-top-0">
                       <a href="/coursedetails/index/<?php echo $courseList[$i]->id;?>" class="card-img-top" target="_blank">
                           <img
                        src="<?php echo "/assets/images/".$courseList[$i]->image;?>"
                        alt
                        class="rounded-top card-img-top"
                          />
                      </a>
                     <br/>
                      <a href="/coursedetails/index/<?php echo $courseList[$i]->id;?>" target="_blank"><?php echo $courseList[$i]->name;?></a>
                    </td>
                    <td class="align-middle border-top-0"><?php echo $courseList[$i]->max_duration.' '.$courseList[$i]->duration_type;?></td>

                    <td class="align-middle border-top-0"><?php echo date('d-m-Y',strtotime($courseList[$i]->start_date));?></td>

                    <td class="align-middle border-top-0"><?php echo date('d-m-Y',strtotime($courseList[$i]->end_date));?></td>

                    <td class="align-middle border-top-0">23%</td>


                    <td class="align-middle border-top-0">
                      <a href="#" onclick="autologin()">Click to View</a>
                    </td>
                  </tr>
                <?php } ?> 
                 
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php include('announcement.php');?>
        <!-- SIDEBAR NAV ENDS HERE -->
      </div>
    </div>
    <!-- MAIN CONTAINER BLOCK ENDS HERE -->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    

     <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>

    <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>



 <script>

Highcharts.chart('container1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Stacked column chart'
    },
    xAxis: {
        categories: ['PROG-001', 'PROG-001', 'PROG-004']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Courses Completion'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'gray'
            }
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: [{
        name: 'Yet to Register',
        data: [5, 3, 4, 7]
    }, {
        name: 'In Progress',
        data: [2, 2, 3, 2]
    }, {
        name: 'Completed',
        data: [3, 4, 4, 2]
    }]
});
Highcharts.chart('container', {

    title: {
        text: 'Learning hours completed based on course'
    },


    yAxis: {
        title: {
            text: 'Hours of Completion'
        }
    },

    xAxis: {
        accessibility: {
            rangeDescription: 'Range: 2010 to 2017'
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },

    series: [{
        name: 'SIRI 1 - KURSUS ASUHAN PERMATA NEGARA',
        data: [10, 14, 17, 20, 21, 22]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

   function autologin()
    {
      $.noConflict();

        jQuery.get("/profile/dashboard/autologin", function(data, status){
             console.log(data);
             // parent.location= "<?php echo BASE_PATH;?>/index/checkout";
             window.open(data, '_blank');
         });
    }

  </script>





  </body>
</html>
