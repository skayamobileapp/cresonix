<?php
        $this->load->model('dashboard_model');
        $this->id_student =  $this->session->userdata['id_student'];

        $studentDetails = $this->dashboard_model->getStudentDetais($this->id_student);



$namesArray = $this->dashboard_model->getCertificateNames(2);

$productList = $this->dashboard_model->getProductType();




?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS -->
    <link
      rel="stylesheet"
      href="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
      integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
      crossorigin="anonymous"
    />

    <title>Dashboard | Speed</title>
    <link rel="stylesheet" href="/website/css/feather.css" />
    <link rel="stylesheet" href="/website/css/main.css" />
  </head>
  <body>
    <!-- NAV BAR STARTS HERE -->
    <nav class="navbar navbar-expand-lg navbar-default main-header">
      <div class="container-fluid px-0">
        <a class="navbar-brand" href="/"
          ><img src="/website/img/speed_logo.svg" alt=""
        /></a>
        <!-- Mobile view nav wrap -->
        <ul
          class="navbar-nav navbar-right-wrap ml-auto d-lg-none d-flex nav-top-wrap"
        >
          <li class="dropdown ml-2">
            <a
              class="rounded-circle"
              href="#!"
              role="button"
              id="dropdownUser"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <div class="avatar avatar-md avatar-indicators avatar-online">
                <img
                  alt="avatar"
                  src="website/img/avatar-1.jpg"
                  class="rounded-circle"
                />
              </div>
            </a>

          
          </li>
        </ul>
        <!-- Button -->
        <button
          class="navbar-toggler collapsed"
          type="button"
          data-toggle="collapse"
          data-target="#navbar-default"
          aria-controls="navbar-default"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="icon-bar top-bar mt-0"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
        </button>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbar-default">
         <ul class="navbar-nav">

              <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#!"
                id="navbarBrowse"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                data-display="static"
              >
                Programme
              </a>
              <ul
                class="dropdown-menu dropdown-menu-arrow"
                aria-labelledby="navbarBrowse"
              >

              <?php for($i=0;$i<count($namesArray);$i++) { ?>
                <li class="dropdown-submenu dropright">
                  <a
                    class="dropdown-item dropdown-list-group-item"
                    href="<?php echo "/programdetails/index/".$namesArray[$i]->id;?>"
                  >
                    <?php echo $namesArray[$i]->name;?>
                  </a>
                </li>
              <?php } ?> 
              </ul>



            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#!"
                id="navbarBrowse"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                data-display="static"
              >
                Courses
              </a>
              <ul
                class="dropdown-menu dropdown-menu-arrow"
                aria-labelledby="navbarBrowse"
              >

              <?php for($p=0;$p<count($productList);$p++) { 


                $programmeList = $this->dashboard_model->getProgrammeByProductTypeId($productList[$p]->id);

                  ?>
                <li class="dropdown-submenu dropright">
                  <a
                    class="dropdown-item dropdown-list-group-item dropdown-toggle"
                    href="#!"
                  >
                    <?php echo $productList[$p]->name;?>
                  </a>
                  <ul class="dropdown-menu">
                    <?php for($m=0;$m<count($programmeList);$m++) { ?> 
                    <li>
                      <a
                        class="dropdown-item"
                        href="/coursedetails/index/<?php echo $programmeList[$m]->id;?>"
                      >
                        <?php echo $programmeList[$m]->name;?></a
                      >
                    </li>
                  <?php } ?> 
                    
                  </ul>
                </li>

              <?php } ?> 
                
              </ul>
            </li>
          </ul>
          <form
            class="mt-3 mt-lg-0 ml-lg-3 d-flex align-items-center col-lg-6 search-form"
          >
            <span class="position-absolute pl-3 search-icon">
              <i class="fe fe-search"></i>
            </span>
            <input
              type="search"
              class="form-control pl-6"
              placeholder="Search Courses"
            />
          </form>
          <ul class="navbar-nav navbar-right-wrap ml-auto">
            <li class="nav-item">
              <a
                class="nav-link"
                href="#!"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Welcome <?php echo $studentDetails->first_name.' '.$studentDetails->last_name;?>
              </a>
            </li>
           
          </ul>
        
        </div>
      </div>
    </nav>