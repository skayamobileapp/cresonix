<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MainInvoice extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_invoice_model');
        $this->isCompanyUserLoggedIn();
    }

    function list()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;
    
        $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
        $formData['status'] = $this->security->xss_clean($this->input->post('status'));
        $formData['id_company'] = $id_company;
        $formData['id_company_user'] = $id_company_user;
        $formData['paid'] = $this->security->xss_clean($this->input->post('paid'));

        $data['searchParam'] = $formData;
        $data['mainInvoiceList'] = $this->main_invoice_model->getMainInvoiceListSearch($formData);
        // echo "<pre>";print_r($data['applicantList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Corporate User Portal';
        $this->loadViews("main_invoice/list", $this->global, $data, NULL);
        
    }


    function edit($id = NULL)
    {
        if ($id == null)
        {
            redirect('/company_user_finance/mainInvoice/list');
        }

        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {

            $result = $this->main_invoice_model->editApplicantDetails($data,$id);
            redirect('/admission/applicant/list');
        }

        $data['companyDetails'] = $this->main_invoice_model->getCompanyDetails($id_company);

        $data['mainInvoice'] = $this->main_invoice_model->getMainInvoice($id);
        $data['mainInvoiceDetailsList'] = $this->main_invoice_model->getMainInvoiceDetails($id);
        $data['mainInvoiceDiscountDetailsList'] = $this->main_invoice_model->getMainInvoiceDiscountDetails($id);
        $data['mainInvoiceHasStudentList'] = $this->main_invoice_model->getMainInvoiceHasStudentList($id);


        // echo "<Pre>"; print_r($data['degreeTypeList']);exit;

        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';
        $this->loadViews("main_invoice/edit", $this->global, $data, NULL);
    }


    function approvalList()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;


        if($this->input->post())
        {

        
        $resultprint = $this->input->post();



        // echo "<Pre>"; print_r($formData);exit;

         switch ($resultprint['button'])
         {

             case 'Approve':

                $formData['approval'] = $this->security->xss_clean($this->input->post('approval'));

                redirect('/company_user_finance/mainInvoice/payInvoice');
        
                break;


            case 'Search':

                $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
                $formData['paid'] = $this->security->xss_clean($this->input->post('paid'));
                $formData['status'] = '0';
                $formData['id_company'] = $id_company;
                $formData['id_company_user'] = $id_company_user;

                $data['searchParam'] = $formData;

                $data['mainInvoiceList'] = $this->main_invoice_model->getMainInvoiceListSearchForPay($formData);
                 
                 break;
             
             default:
                 break;
         }
        }


    
        $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
        $formData['paid'] = $this->security->xss_clean($this->input->post('paid'));
        $formData['status'] = '0';
        $formData['id_company'] = $id_company;
        $formData['id_company_user'] = $id_company_user;

        $data['searchParam'] = $formData;
        $data['mainInvoiceList'] = $this->main_invoice_model->getMainInvoiceListSearchForPay($formData);
        // echo "<pre>";print_r($data['applicantList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Corporate User Portal';
        $this->loadViews("main_invoice/approval_list", $this->global, $data, NULL);
        
    }


    function view($id = NULL)
    {
        if ($id == null)
        {
            redirect('/company_user_finance/mainInvoice/approvalList');
        }

        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {
            $result = $this->main_invoice_model->editApplicantDetails($data,$id);
            redirect('/admission/applicant/list');
        }

        $data['companyDetails'] = $this->main_invoice_model->getCompanyDetails($id_company);

        $data['mainInvoice'] = $this->main_invoice_model->getMainInvoice($id);
        $data['mainInvoiceDetailsList'] = $this->main_invoice_model->getMainInvoiceDetails($id);
        $data['mainInvoiceDiscountDetailsList'] = $this->main_invoice_model->getMainInvoiceDiscountDetails($id);
        $data['mainInvoiceHasStudentList'] = $this->main_invoice_model->getMainInvoiceHasStudentList($id);


        // echo "<Pre>"; print_r($data['degreeTypeList']);exit;

        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';
        $this->loadViews("main_invoice/view", $this->global, $data, NULL);
    }

    function payInvoice()
    {

        // print_r($_POST);exit;
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        $resultprint = $this->input->post();

        // $tempData = $this->security->xss_clean($this->input->post('approval'));

        if(empty($resultprint))
        {
            redirect('/company_user_finance/mainInvoice/approvalList');
        }


        // echo "<Pre>"; print_r($resultprint);exit;

        $button = $resultprint['button'];
        $invoices = $resultprint['approval'];


        switch ($button)
        {
            case 'Payment':

            $payment_mode = $this->security->xss_clean($this->input->post('payment_mode'));
            $date_time = $this->security->xss_clean($this->input->post('date_time'));
            $receipt_amount = $this->security->xss_clean($this->input->post('receipt_amount'));
            $reference_number = $this->security->xss_clean($this->input->post('reference_number'));
            $button = $this->security->xss_clean($this->input->post('button'));
            $image_file = '';

            if($payment_mode == 'Staff Credit')
            {
                $date_time = date('Y-m-d H:i:s');
                $reference_number = 'STF_CRDT';
            }
            else
            {
                if($_FILES['image'])
                {

                    $certificate_name = $_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }

                $date_time = date('Y-m-d', strtotime($date_time));
            }


            $receipt_number = $this->main_invoice_model->generateReceiptNumber();

            $data = array(
                'receipt_date' =>date('Y-m-d'),
                'id_student' => $id_company,
                'receipt_number' => $receipt_number,
                'type' => 'CORPORATE',
                'currency' => 1,
                'receipt_amount' => $receipt_amount,
                'remarks' => $payment_mode . " PAYMENT",
                'status' => '1'
            );


            $inserted_id = $this->main_invoice_model->addNewReceipt($data);

            if($inserted_id)
            {

                for($i=0;$i<count($resultprint['approval']);$i++)
                {
                    $id = $resultprint['approval'][$i];

                     
                    $main_invoice = $this->main_invoice_model->getMainInvoice($id);
                    // echo "<Pre>"; print_r($result);exit;

                    $detailsData = array(
                    'id_receipt' => $inserted_id,
                    'id_main_invoice' => $id,
                    'invoice_amount' => $main_invoice->total_amount,
                    'paid_amount' => $main_invoice->balance_amount,                        
                    'status' => '1',
                    'created_by' => 1
                    );
                    
                    //print_r($details);exit;
                    
                    $id_receipt_details = $this->main_invoice_model->addNewReceiptDetails($detailsData);


                    if($id_receipt_details)
                    {

                        $update_student_has_programme['status'] = 1;
                        // echo "<Pre>";print_r($id);exit();

                        $updated_student_has_programme = $this->main_invoice_model->updateStudentHasProgramme($update_student_has_programme,$id);


                        $update_invoice['balance_amount'] = '0'; 
                        $update_invoice['paid_amount'] = $main_invoice->balance_amount;

                        // echo "<Pre>";print_r($update_invoice);exit;
                        $this->main_invoice_model->editMainInvoice($update_invoice,$id); 
                    }
                }


            


                $payment_data = array(
                    'id_receipt' => $inserted_id,
                    'id_payment_type' => $payment_mode,
                    'paid_amount' => $receipt_amount,
                    'payment_reference_number' => $reference_number,                        
                    'date_time' => $date_time,                        
                    'status' => '1',
                    'created_by' => 1
                );

                if($image_file != '')
                {
                    $payment_data['image'] = $image_file;
                }


                $added_payment = $this->main_invoice_model->addNewReceiptPaymentDetails($payment_data);


            }
            
            redirect('/company_user_finance/receipt/list');

                break;
            
            default:
                break;
        }

        $invoice_details = array();

        for($i=0;$i<count($resultprint['approval']);$i++)
        {
            $id = $resultprint['approval'][$i];

            // echo "<Pre>"; print_r($id);exit;
             
            $result = $this->main_invoice_model->getMainInvoice($id);

            if($result)
            {
                array_push($invoice_details,$result);
            }
        }

        $data['mainInvoiceList'] = $invoice_details;
        $data['companyDetails'] = $this->main_invoice_model->getCompanyDetails($id_company);

        // echo "<Pre>"; print_r($data['companyDetails']);exit;

        $this->global['pageTitle'] = 'Inventory Management : Edit Applicant';
        $this->loadViews("main_invoice/pay_invoice", $this->global, $data, NULL);
        
    }


    function getStateByCountry($id_country)
    {
            $results = $this->main_invoice_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>  
                    $('select').select2();
                </script>
                ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
            exit;
    }


    function getStateByCountryPermanent($id_country)
    {
        $results = $this->main_invoice_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
        <script type='text/javascript'>
             $('select').select2();
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->main_invoice_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function checkFeeStructure()
    {
        $id_session = $this->session->company_user_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $result = $this->main_invoice_model->checkFeeStructure($tempData);
        if($result == '')
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->main_invoice_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;
    }

    function generateMainInvoice($id_main_invoice)
    {
        // To Get Mpdf Library
        $this->getMpdfLibrary();

                    // print_r($base_url);exit;

            // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
            //  require_once __DIR__ . '/vendor/autoload.php';
            
            $mpdf=new \Mpdf\Mpdf(); 

            // $mpdf->SetHeader("<div style='text-align: left;'>Inventory Management
            //                    </div>");


            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');
            $currentDateTime = date('d_m_Y_His');

        $organisationDetails = $this->main_invoice_model->getOrganisation();


        // echo "<Pre>";print_r($organisationDetails);exit;

        

        // $signature = $_SERVER['DOCUMENT_ROOT']."/assets/img/speed_logo.svg";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }




        $mainInvoice = $this->main_invoice_model->getMainInvoice($id_main_invoice);


        if($mainInvoice->type == 'Student')
        {
            $invoiceFor = $this->main_invoice_model->getMainInvoiceStudentData($mainInvoice->id_student);
        }elseif($mainInvoice->type == 'CORPORATE')
        {
            $invoiceFor = $this->main_invoice_model->getMainInvoiceCorporateData($mainInvoice->id_student);
        }


        // echo "<Pre>";print_r($invoiceFor);exit;


        $type = $mainInvoice->type;
        $invoice_number = $mainInvoice->invoice_number;
        $date_time = $mainInvoice->date_time;
        $remarks = $mainInvoice->remarks;
        $currency = $mainInvoice->currency_name;
        $total_amount = $mainInvoice->total_amount;
        $invoice_total = $mainInvoice->invoice_total;
        $balance_amount = $mainInvoice->balance_amount;
        $paid_amount = $mainInvoice->paid_amount;
        $programme_name = $mainInvoice->programme_name;
        $programme_code = $mainInvoice->programme_code;

        if($date_time)
        {
            $date_time = date('d-m-Y', strtotime($date_time));
        }


        $invoice_generation_name = $invoiceFor->full_name;
        $invoice_generation_nric = $invoiceFor->nric;


            $file_data = "";


 $file_data.="<table align='center' width='100%'>
        <tr>
                  <td style='text-align: left;font-size:30px;'><b>INVOICE</b></td>
                  <td style='text-align: center' width='30%' ></td>

          <td style='text-align: right' width='40%' ><img src='$signature' width='180px' /></td>
          
        </tr>
       
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'> <br/><br/><br/></td>
        </tr>
    </table>";

            $invoice_type = $mainInvoice->type;


            $file_data = $file_data ."

            <table width='100%' style='font-size:16px;'>
            <tr>
             <td>$invoice_type Name : $invoice_generation_name </td>
             <td width='25%'></td>
             <td style='text-align:right;'>Invoice No  : $invoice_number</td>
             <td style='text-align:right;'></td>
             </tr>
              <tr>
             <td>IC No / Passport No: $invoice_generation_nric </td>
             <td></td>
             <td style='text-align:right;'>Invoice Date: $date_time</td>
             <td style='text-align:right;'></td>
             </tr>
             </table>




             <table width='100%' height='50%'  style='margin-top:30px;border-collapse: collapse;padding:10px 10px;height:75%;font-size:16px;' border='1'>
              <tr>
               <th style='text-align:center;line-height:30px;'><b>No</b></th>
               <th style='text-align:center;'><b>DESCRIPTION</b></th>
               <th style='text-align:center;'><b>UNIT PRICE (RM)</b></th>
               <th style='text-align:center;'><b>TOTAL (RM)</b></th>
              </tr>
               <tr>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               <td style='padding-top:20px;padding-bottom:15px;text-align:center;'>$programme_code - $programme_name,<br/>Being charge for,</td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               </tr>
              ";

 // 


        if($remarks == 'Student Course Registration')
        {

        $mainInvoiceDetailsList = $this->main_invoice_model->getMainInvoiceDetailsForCourseRegistrationShow($id_main_invoice);

        $semesterDetails = $this->main_invoice_model->getSemesterByMainInvoiceDetailsForCourseRegistrationShow($id_main_invoice);

            // echo "<Pre>";print_r($semesterDetails);exit;

            if($semesterDetails)
            {
                $semester_name = $semesterDetails->name;
                $semester_code = $semesterDetails->code;
                $start_date = $semesterDetails->start_date;


                if($start_date)
                {
                    $start_date = date('Y-m', strtotime($start_date));
                }


               // $file_data = $file_data ."
              
               //  <tr>
               // <td style='padding-top:20px;'></td>
               // <td></td>
               // <td></td>
               // <td></td>
               // </tr>
               // ";


            foreach ($mainInvoiceDetailsList as $value)
            {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;
            $id_reference = $value->id_reference;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));
                if($description == 'CREDIT HOUR MULTIPLICATION' && $id_reference > 0)
                {

            // echo "<Pre>";print_r($value);exit;
                    $course_code = $value->course_code;
                    $course_name = $value->course_name;

            
                $file_data = $file_data ."
               <tr>
               <td style='padding-top:20px;'> </td>
               <td style='padding-top:20px;' >$course_code - $course_name</td>
               <td style='padding-top:20px;' style='text-align:right;'>$amount</td>
               <td style='padding-top:20px;' ></td>
               </tr>";

                }else
                {


                $file_data = $file_data ."
               <tr>
               <td style='padding-top:20px;'></td>
               <td style='padding-top:20px;'>$fee_setup</td>
               <td style='padding-top:20px;' style='text-align:right;'>$amount</td>
               <td style='padding-top:20px;'></td>
               </tr>";

                }

            }

          }else
          {

            $mainInvoiceDetailsList = $this->main_invoice_model->getMainInvoiceDetails($id_main_invoice);


            foreach ($mainInvoiceDetailsList as $value)
            {

                $description = $value->description;
                $amount = $value->amount;
                $fee_setup = $value->fee_setup;

                $amount = number_format($amount, 2, '.', ',');

                    // $acqDate   = date("d/m/Y", strtotime($acqDate));

                

                $file_data = $file_data ."
               <tr>
               <td style='style='padding-top:20px;padding-bottom:20px;'></td>
               <td style='style='padding-top:20px;padding-bottom:20px;'>$fee_setup</td>
               <td style='style='padding-top:20px;text-align:right;padding-top:20px;'>$amount</td>
                              <td></td>

               </tr>";


              // $i++;

            }
            }
        }
        else
        {


        $mainInvoiceDetailsList = $this->main_invoice_model->getMainInvoiceDetails($id_main_invoice);


          $i=1;
          foreach ($mainInvoiceDetailsList as $value)
          {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));

            
            $file_data = $file_data ."
               <tr>
               <td style='padding-top:20px;padding-bottom:10px;'>$i .</td>
               <td style='padding-top:20px;padding-bottom:10px;text-align:center;'>$fee_setup</td>
               <td style='padding-top:20px;padding-bottom:10px;' style='text-align:right;'>$amount</td>
               <td style='padding-top:20px;padding-bottom:10px;'></td>
               </tr>";
          $i++;

          }
        }


        $amount_c = $invoice_total;

    $invoice_total = number_format($invoice_total, 2, '.', ',');
    $amount_word = $this->getAmountWordings($amount_c);

    $amount_word = ucwords($amount_word);


    $file_data = $file_data ."



        <tr>
           <td colspan='3' style='text-align:right;padding-top:20px;padding-bottom:10px;'><b>GRAND TOTAL</b></td>
           <td style='text-align:right;padding-top:20px;padding-bottom:10px;'><b>$invoice_total</b></td>
        </tr>

        <tr>
           <td colspan='4' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b>$currency : $amount_word</b></td>
        </tr>";

            $file_data = $file_data ."
               </table>";



        if($mainInvoice->type == 'CORPORATE')
        {

            $mainInvoiceHasStudentList = $this->main_invoice_model->getMainInvoiceHasStudentList($id_main_invoice);

            // echo "<Pre>";print_r($mainInvoiceHasStudentList);exit;
            
            if(!empty($mainInvoiceHasStudentList))
            {


            $file_data = $file_data . "
            <br>
            <h3> Employee Details</h3>
            <table width='100%' height='50%'  style='margin-top:10px;border-collapse: collapse;padding:10px 10px;height:75%;font-size:16px;' border='1'>
                  <tr>
                   <th style='text-align:center;line-height:30px;'><b>No</b></th>
                   <th style='text-align:center;'><b>EMPLOYEE NAME</b></th>
                   <th style='text-align:center;'><b>EMPLOYEE NRIC / PASSPORT</b></th>
                   <th style='text-align:right;'><b>AMOUNT</b></th>
                  </tr>
                  ";


            $j = 1;
            $total_student_amount = 0;
            foreach ($mainInvoiceHasStudentList as $value)
            {

                $id = $value->id;
                $student_name = $value->student_name;
                $nric = $value->nric;
                $amount = $value->amount;
                


                $file_data = $file_data . "

                   <tr>
                   <td style='padding-top:20px;padding-bottom:15px;'>$j</td>
                   <td style='padding-top:20px;padding-bottom:15px;text-align:center;'>$student_name</td>
                   <td style='padding-top:20px;padding-bottom:15px;'>$nric</td>
                   <td style='padding-top:20px;padding-bottom:15px;text-align:right;'>$amount</td>
                   </tr>
                  ";

                $total_student_amount = $total_student_amount + $amount;
                $j++;
            }


            $file_data = $file_data ."

                <tr>
                   <td colspan='3' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b> TOTAL</b></td>
                   <td style='text-align:right;padding-top:20px;padding-bottom:10px;'><b>$total_student_amount</b></td>
                </tr>
                   </table>";

            }

        }

        




        $bankDetails = $this->main_invoice_model->getBankRegistration();


        if($bankDetails && $organisationDetails)
        {
            $bank_name = $bankDetails->name;
            $bank_code = $bankDetails->code;
            $account_no = $bankDetails->account_no;
            $state = $bankDetails->state;
            $country = $bankDetails->country;
            $address = $bankDetails->address;
            $city = $bankDetails->city;
            $zipcode = $bankDetails->zipcode;
            

            $organisation_name = $organisationDetails->name;




        $file_data = $file_data ."<br/><br/>
        <p>1. All cheque should be crossed and make payable to:: </p>
    <table align='center' width='100%' style='font-size:16px;'>
      <tr>
            <td style='text-align: left' width='30%' valign='top'>PAYEE</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$organisation_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%' valign='top'>BANK</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$bank_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%' valign='top'>ADDRESS</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%' valign='top'>ACCOUNT NO</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$account_no</td>
      </tr>
      <tr>
            <td style='text-align: left' width='30%' valign='top'>SWIFT CODE</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$bank_code</td>
      </tr>

      
    </table>
    <p> 2. This is auto generated Receipt. No signature is required. </p>
      ";


        }


    
     // echo "<Pre>";print_r($file_data);exit;


            // $mpdf->SetFooter('<div>Inventory Management</div>');
            // echo $file_data;exit;
            // $stylesheet = file_get_contents('pdfdownload_18_19.css'); // external css
            // $mpdf->WriteHTML($stylesheet,1);

            $mpdf->WriteHTML($file_data);
            $mpdf->Output($type . '_INVOICE_'.$invoice_number.'_'.$currentDateTime.'.pdf', 'D');
            exit;
    }
}
