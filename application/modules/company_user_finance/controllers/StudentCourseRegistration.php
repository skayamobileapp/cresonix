<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentCourseRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_course_registration_model');
        $this->isCompanyUserLoggedIn();
    }

    function add()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;
            
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            $formData = $this->input->post();
            
            $id_programme = $this->security->xss_clean($this->input->post('id_program'));
            $id_students = $this->security->xss_clean($this->input->post('id_student'));

            $student_count = count($id_students);

            if($student_count > 0)
            {
                $get_data['id_programme'] = $id_programme;
                $get_data['currency'] = 'MYR';

                $detail_data = $this->student_course_registration_model->getFeeStructureByData($get_data);

                // echo "<Pre>";print_r($detail_data);exit;

                if(!empty($detail_data))
                {
                    $invoice_number = $this->student_course_registration_model->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'CORPORATE';
                    $invoice['remarks'] = 'Course Registration Fee';
                    $invoice['id_application'] = '0';
                    $invoice['id_student'] = $id_company;
                    $invoice['id_program'] = $id_programme;
                    $invoice['currency'] = 1;
                    $invoice['total_amount'] = '0';
                    $invoice['balance_amount'] = '0';
                    $invoice['paid_amount'] = '0';
                    $invoice['status'] = 0;
                    $invoice['is_migrate_applicant'] = 0;
                    $invoice['created_by'] = $id_company_user;

                    
                    $inserted_id = $this->student_course_registration_model->addNewMainInvoice($invoice);

                    // echo "<Pre>";print_r($inserted_id);exit;

                    $total_amount = 0;
                    $total_discount_amount = 0;
                    $sibling_discount_amount = 0;
                    $employee_discount_amount = 0;
                    $alumni_discount_amount = 0;

                    // echo "<Pre>";print_r($detail_data);exit;

                    foreach ($detail_data as $fee_structure)
                    {
                        
                            $data = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_item' => $fee_structure->id_fee_item,
                                'amount' => $fee_structure->amount * $student_count,
                                'price' => $fee_structure->amount,
                                'quantity' => $student_count,
                                'id_reference' => $fee_structure->id,
                                'description' => 'Course Registration Fee',
                                'status' => 1,
                                'created_by' => $id_company_user
                            );

                            $total_amount = $total_amount + $fee_structure->amount;

                            $this->student_course_registration_model->addNewMainInvoiceDetails($data);
                        // }
                        // echo "<Pre>";print_r($data);exit;
                    }

                    $total_invoice_amount = $total_amount;


                    $invoice_update['total_amount'] = $total_invoice_amount * $student_count;
                    $invoice_update['balance_amount'] = $total_invoice_amount * $student_count;
                    $invoice_update['invoice_total'] = $total_invoice_amount * $student_count;
                    $invoice_update['total_discount'] = 0;
                    $invoice_update['paid_amount'] = '0';
                    $this->student_course_registration_model->editMainInvoice($invoice_update,$inserted_id);


                    if($inserted_id)
                    {
                        // echo "<Pre>";print_r($inserted_id);exit;

                        $inserted_student_data = $this->student_course_registration_model->addNewMainInvoiceHasStudents($id_students,$inserted_id,$total_invoice_amount);
                    }
                }



                
                // echo "<Pre>"; print_r($inserted_id);exit;

                // foreach ($id_students as $id_student)
                // {   
                //     $id_invoice = $this->student_course_registration_model->createNewMainInvoiceForStudent($id_student);

                //     $fee_structure = $this->student_course_registration_model->getFeeStructureByIdProgramme($id_programme);


                //     if($fee_structure)
                //     {
                //         $student_data['id_fee_structure'] = $fee_structure->id;

                //         $updated_student = $this->student_course_registration_model->editStudent($student_data, $id_student);
                //     }


                //     if($id_invoice)
                //     {
                //         $programme = $this->student_course_registration_model->getProgramme($id_programme);

                //         if($programme)
                //         {

                //             // echo "<Pre>"; print_r($programme);exit;
                            
                //             $max_duration = $programme->max_duration;

                //             $start_date = date('Y-m-d');

                //             $data_student_has_programme = array(
                //                 'id_student' => $id_student,
                //                 'id_invoice' => $id_invoice,
                //                 'id_programme' => $id_programme,
                //                 'start_date' => $start_date,
                //                 'end_date' => date('Y-m-d', strtotime($start_date . "+" . $max_duration . " months") ),
                //             );
                //             // echo "<Pre>"; print_r($data_student_has_programme);exit;

                //             $id_student_has_programme = $this->student_course_registration_model->addNewStudentHasProgramme($data_student_has_programme);

                //         }
                //     }
                // }


            }
            
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data['programList'] = $this->student_course_registration_model->programListByStatus('1');
        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Inventory Management : Advisor Taagging';
        $this->loadViews("student_course_registration/add", $this->global, $data, NULL);
    }

    function searchStudents()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_company'] = $id_company;
        $tempData['id_company_user'] = $id_company_user;

        // $staffList = $this->student_course_registration_model->staffListByStatus('1');
        
        $student_data = $this->student_course_registration_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script> ";


         $table .= "
         <br>
         <h4> Select Employee For Programme Registration</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Employee Name</th>
                    <th>Employee NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Company</th>
                    <th style='text-align: center;'>Advisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $company_name = $student_data[$i]->company_name;
                $registration_number = $student_data[$i]->registration_number;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$registration_number - $company_name</td>
                    <td style='text-align: center;'>$ic_no - $advisor_name</td>
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}