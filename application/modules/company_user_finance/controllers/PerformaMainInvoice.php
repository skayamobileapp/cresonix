<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PerformaMainInvoice extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('performa_main_invoice_model');
        $this->isCompanyUserLoggedIn();
    }

    function list()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;
    
        $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
        $formData['status'] = $this->security->xss_clean($this->input->post('status'));
        $formData['from_date'] = $this->security->xss_clean($this->input->post('from_date'));
        $formData['to_date'] = $this->security->xss_clean($this->input->post('to_date'));
        $formData['id_company'] = $id_company;
        $formData['id_company_user'] = $id_company_user;

        $data['searchParam'] = $formData;


        $data['searchParam'] = $formData;
        $data['performaInvoiceList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceListSearch($formData);
        // echo "<pre>";print_r($data['applicantList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Corporate User Portal';
        $this->loadViews("performa_main_invoice/list", $this->global, $data, NULL);
        
    }


    function edit($id = NULL)
    {
        if ($id == null)
        {
            redirect('/company_user_finance/performaMainInvoice/list');
        }

        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {

            $result = $this->performa_main_invoice_model->editApplicantDetails($data,$id);
            redirect('/admission/applicant/list');
        }

        $data['companyDetails'] = $this->performa_main_invoice_model->getCompanyDetails($id_company);

        $data['mainInvoice'] = $this->performa_main_invoice_model->getPerformaMainInvoice($id);
        $data['mainInvoiceDetailsList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceDetails($id);
        $data['mainInvoiceDiscountDetailsList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceDiscountDetails($id);
        $data['mainInvoiceHasStudentList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceHasStudentList($id);


        // echo "<Pre>"; print_r($data['degreeTypeList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Edit Applicant';
        $this->loadViews("performa_main_invoice/edit", $this->global, $data, NULL);
    }


    function approvalList()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;


        if($this->input->post())
        {

         $resultprint = $this->input->post();

         // echo "<Pre>"; print_r($resultprint);exit;

         switch ($resultprint['button'])
         {

             case 'Approve':

                for($i=0;$i<count($resultprint['approval']);$i++)
                {

                     $id = $resultprint['approval'][$i];

                     // echo "<Pre>"; print_r($id);exit;

                     $update_data = array(
                        'status' => 1,
                    );
                     
                    $result = $this->performa_main_invoice_model->editPerformaMainInvoice($update_data, $id);
                    
                    // echo "<Pre>"; print_r($result);exit;

                    
                    if($result)
                    {
                        // echo "<Pre>"; print_r($result);exit;
                        
                        $id_main_invoice = $this->performa_main_invoice_model->movePerformaInvoiceToMainInvoice($id);

                    }
                }
        
                redirect('/company_user_finance/performaMainInvoice/approvalList');         
                break;


            case 'Reject':

                for($i=0;$i<count($resultprint['approval']);$i++)
                {

                     $id = $resultprint['approval'][$i];

                     // echo "<Pre>"; print_r($id);exit;

                     $update_data = array(
                        'status' => 2,
                    );
                     
                    $result = $this->performa_main_invoice_model->editPerformaMainInvoice($update_data,$id);
                }
        
                redirect('/company_user_finance/performaMainInvoice/approvalList');         
                break;


            case 'Search':

                $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
                $formData['from_date'] = $this->security->xss_clean($this->input->post('from_date'));
                $formData['to_date'] = $this->security->xss_clean($this->input->post('to_date'));
                $formData['status'] = '0';
                $formData['id_company'] = $id_company;
                $formData['id_company_user'] = $id_company_user;


                $data['searchParam'] = $formData;
                $data['performaInvoiceList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceListSearchForApproval($formData);
                 
                 break;
             
             default:
                 break;
         }
        }


    
        $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
        $formData['from_date'] = $this->security->xss_clean($this->input->post('from_date'));
        $formData['to_date'] = $this->security->xss_clean($this->input->post('to_date'));
        $formData['status'] = '0';
        $formData['id_company'] = $id_company;
        $formData['id_company_user'] = $id_company_user;

        $data['searchParam'] = $formData;
        $data['performaInvoiceList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceListSearchForApproval($formData);
        // echo "<pre>";print_r($data['applicantList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Corporate User Portal';
        $this->loadViews("performa_main_invoice/approval_list", $this->global, $data, NULL);
        
    }


    function view($id = NULL)
    {
        if ($id == null)
        {
            redirect('/company_user_finance/performaMainInvoice/approvalList');
        }

        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {
            $result = $this->performa_main_invoice_model->editApplicantDetails($data,$id);
            redirect('/admission/applicant/list');
        }

        $data['companyDetails'] = $this->performa_main_invoice_model->getCompanyDetails($id_company);

        $data['mainInvoice'] = $this->performa_main_invoice_model->getPerformaMainInvoice($id);
        $data['mainInvoiceDetailsList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceDetails($id);
        $data['mainInvoiceDiscountDetailsList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceDiscountDetails($id);
        $data['mainInvoiceHasStudentList'] = $this->performa_main_invoice_model->getPerformaMainInvoiceHasStudentList($id);


        // echo "<Pre>"; print_r($data['mainInvoice']);exit;
        

        $this->global['pageTitle'] = 'Corporate User Portal : Edit Applicant';
        $this->loadViews("performa_main_invoice/view", $this->global, $data, NULL);
        
    }


    function getStateByCountry($id_country)
    {
            $results = $this->performa_main_invoice_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>  
                    $('select').select2();
                </script>
                ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
            exit;
    }


    function getStateByCountryPermanent($id_country)
    {
        $results = $this->performa_main_invoice_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
        <script type='text/javascript'>
             $('select').select2();
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->performa_main_invoice_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function checkFeeStructure()
    {
        $id_session = $this->session->company_user_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $result = $this->performa_main_invoice_model->checkFeeStructure($tempData);
        if($result == '')
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->performa_main_invoice_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;


    }
}
