<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Sponsor Invoice</h3>
        </div>

    <form id="form_main_invoice" action="" method="post">
    
        <div class="form-container">
            <h4 class="form-group-title">Sponsor Invoice</h4>
            
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="type" name="type" value="Sponsor" readonly="readonly">
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sponsor <span class='error-text'>*</span></label>
                        <select name="id_sponser" id="id_sponser" class="form-control" onchange="getStudentBySponser()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($sponserList))
                            {
                                foreach ($sponserList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 


                <div  id='student'>
                </div> 

                                  
            </div>

        </div>

        <div id="view_student_details"  style="display: none;">
        </div>

        <div class="form-container">
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time () <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks </label>
                        <input type="text" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>
            </div>
        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
    
    </form>






    <form id="form_main_invoice_details" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Sponsor Invoice Details</h4>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Item <span class='error-text'>*</span></label>
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeSetupList))
                                {
                                    foreach ($feeSetupList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number"  class="form-control" id="amount" name="amount">
                        </div>
                    </div>
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>

            <div id="view">
                
            </div>            
        </div>        

            

        </form>

        </div>

    </div>

        </div>

        <footer class="footer-wrapper">
                <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>

<script>

    function validateDetailsData()
    {
        if($('#form_main_invoice').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Sponsor Invoice Details");
            }
            else
            {
                $('#form_main_invoice').submit();
            }
        }    
    }


    $('select').select2();

    function getStudentBySponser()
    {
        var tempPR = {};
        tempPR['id_sponser'] = $("#id_sponser").val();
        tempPR['type'] = $("#type").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_sponser'] != '' && tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/finance/sponserMainInvoice/getStudentBySponser',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#student").html(result);
               }
            });
        }   
    }

     function getStudentByStudentId(id)
     {
        $.get("/finance/sponserMainInvoice/getStudentByStudentId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

     function getApplicantByApplicantId(id)
     {
        $.get("/finance/sponserMainInvoice/getApplicantByApplicantId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        // $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData()
    {
        if($('#form_main_invoice_details').valid())
        {
        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        // tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/sponserMainInvoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        }
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/sponserMainInvoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/sponserMainInvoice/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });
    }

    

    $(document).ready(function() {
        $("#form_main_invoice_details").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_sponser: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                },
                type: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "<p class='error-text'>Enter Details For Total Amount</p>",
                },
                id_sponser: {
                    required: "<p class='error-text'>Select Sponser</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Date </p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>