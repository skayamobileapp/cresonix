<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Discount</h3>
      <a href="add" class="btn btn-primary">+ Add Discount</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Discount</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="">
                      </div>
                    </div>
                  </div> -->

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Discount Type </label>
                      <div class="col-sm-8">
                        <select name="id_discount_type" id="id_discount_type" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($discountTypeList)) {
                            foreach ($discountTypeList as $record)
                            {
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if ($record->id == $searchParam['id_discount_type'])
                                {
                                  echo 'selected';
                                }?>>
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                <!-- </div>


                <div class="row"> -->


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Intake </label>
                      <div class="col-sm-8">
                        <select name="id_intake" id="id_intake" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($intakeList)) {
                            foreach ($intakeList as $record)
                            {
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if ($record->id == $searchParam['id_intake'])
                                {
                                  echo 'selected';
                                }?>>
                                <?php echo  $record->year . " - " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>


                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Discount Type Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Intake</th>
            <th>Currency</th>
            <th>status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($discountList)) {
            $i=1;
            foreach ($discountList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->discount_type_name ?></td>
                <td><?php echo $record->start_date ?></td>
                <td><?php echo $record->end_date ?></td>
                <td><?php echo $record->intake_year . " - " . $record->intake_name; ?></td>
                <td><?php echo $record->currency_name ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  $('select').select2();
  
  function clearSearchForm()
  {
    window.location.reload();
  }
</script>