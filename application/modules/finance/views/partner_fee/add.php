<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Partner University Fee</h3>
        </div>
        <form id="form_fee_category" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Partner University Fee Details</h4>  
                

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Partner University <span class='error-text'>*</span></label>
                            <select name="id_partner_university" id="id_partner_university" class="form-control" onchange="getPartnerUniversityAggrementList(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($partnerUniversityList))
                                {
                                    foreach ($partnerUniversityList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Aggrement <span class='error-text'>*</span></label>
                         <span id="view_aggrement">
                            <select class="form-control" id='id_agreement' name='id_agreement'>
                                <option value=''></option>
                              </select>
                         </span>
                      </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Programme <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control" onchange="getIntakes(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                </div>


                <div class="row">


                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Intake <span class='error-text'>*</span></label>
                         <span id="view_intake">
                            <select class="form-control" id='id_intake' name='id_intake'>
                                <option value=''></option>
                              </select>
                         </span>
                      </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Payment Date  <span class='error-text'>*</span></label>
                            <select name="payment_day" id="payment_day" class="form-control">
                                <option value="">Select</option>
                                <?php
                                
                                    for($a =1;$a<30;$a++)
                                    {?>
                                 <option value="<?php echo $a;  ?>">
                                    <?php echo $a;?>
                                 </option>
                                <?php
                                    
                                }
                                ?>
                            </select>
                        </div>
                    </div> 
                

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Fee Type <span class='error-text'>*</span></label>
                            <select name="fee_type" id="fee_type" class="form-control">
                                <option value="">Select</option>
                                <option value="Student Wise">Student Wise</option>
                                <option value="Program Wise">Program Wise</option>
                                <option value="Course Wise">Course Wise</option>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>



                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function getPartnerUniversityAggrementList(id_partner_university)
    {
        $.get("/finance/partnerUniversityFee/getPartnerUniversityAggrementList/"+id_partner_university, function(data, status)
            {
                $("#view_aggrement").show();
                $("#view_aggrement").html(data);
            });
    }


    function getIntakes(idprogram)
    {
        $.get("/finance/partnerUniversityFee/getIntakes/"+idprogram, function(data, status)
            {
                $("#view_intake").show();
                $("#view_intake").html(data);
            });

     // var tempPR = {};
     //    tempPR['id_programme'] = $("#id_program").val();
     //        $.ajax(
     //        {
     //           url: '/finance/partnerUniversityFee/getIntakes',
     //            type: 'POST',
     //           data:
     //           {
     //            tempData: tempPR
     //           },
     //           error: function()
     //           {
     //            alert('Something is wrong');
     //           },
     //           success: function(result)
     //           {
     //                $("#view_intake").html(result);
     //                $("#dummy_intake").hide();
     //           }
     //        });
    }


    $(document).ready(function() {
        $("#form_fee_category").validate({
            rules: {
                id_partner_university: {
                    required: true
                },
                id_agreement: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_program: {
                    required: true
                },
                fee_type: {
                    required: true
                },
                amount: {
                    required: true
                },
                payment_day: {
                    required: true
                }
            },
            messages: {
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                id_agreement: {
                    required: "<p class='error-text'>Select Aggrement</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                fee_type: {
                    required: "<p class='error-text'>Select Fee Type</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                payment_day: {
                    required: "<p class='error-text'>Select Payment Day</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>
