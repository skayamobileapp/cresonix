<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Fee Structure Activity</h3>
        </div>
        <form id="form_frequency_mode" action="" method="post">


            <div class="form-container">
                <h4 class="form-group-title">Fee Structure Activity Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                            <?php 
                                            if ($record->id == $feeStructureActivity->id_program)
                                            {
                                                echo "selected=selected";
                                            } ?>
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Activity <span class='error-text'>*</span></label>
                            <select name="id_activity" id="id_activity" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($activityList))
                                {
                                    foreach ($activityList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                            <?php 
                                            if ($record->id == $feeStructureActivity->id_activity)
                                            {
                                                echo "selected=selected";
                                            } ?>
                                        ><?php echo $record->name . " - " . $record->description;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Trigger <span class='error-text'>*</span></label>
                            <select name="trigger" id="trigger" class="form-control">
                                <option value="">Select</option>
                                <option value="Approval Level"
                                <?php 
                                if ('Approval Level' == $feeStructureActivity->trigger)
                                {
                                    echo "selected=selected";
                                } ?>
                                >Approval Level</option>
                                <option value="Application Level"
                                <?php 
                                if ('Application Level' == $feeStructureActivity->trigger)
                                {
                                    echo "selected=selected";
                                } ?>
                                >Application Level</option>
                                
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Code <span class='error-text'>*</span></label>
                            <select name="id_fee_setup" id="id_fee_setup" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeSetupList))
                                {
                                    foreach ($feeSetupList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                            <?php 
                                            if ($record->id == $feeStructureActivity->id_fee_setup)
                                            {
                                                echo "selected=selected";
                                            } ?>
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="id_currency" id="id_currency" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($currencyList))
                                {
                                    foreach ($currencyList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                            <?php 
                                            if ($record->id == $feeStructureActivity->id_currency)
                                            {
                                                echo "selected=selected";
                                            } ?>
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>




                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount MYR <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount_local" name="amount_local" value="<?php echo $feeStructureActivity->amount_local; ?>">
                             
                        </div>
                    </div>

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount USD <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount_international" name="amount_international" value="<?php echo $feeStructureActivity->amount_international; ?>">
                             
                        </div>
                    </div> -->


                </div>

                <div class="row">



                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Performa <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="performa" id="performa" value="0" <?php if($feeStructureActivity->performa=='0') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> No
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="performa" id="performa" value="1" <?php if($feeStructureActivity->performa=='1') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> Yes
                                </label>                              
                            </div>                         
                    </div>
                    
               

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($feeStructureActivity->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($feeStructureActivity->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>

                </div>
                
                </div>
            

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>

    </div>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>


    $('select').select2();


    $(document).ready(function() {
        $("#form_frequency_mode").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_activity: {
                    required: true
                },
                trigger: {
                    required: true
                },
                id_fee_setup: {
                    required: true
                },
                performa: {
                    required: true
                },
                id_currency: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_activity: {
                    required: "<p class='error-text'>Select Activity</p>",
                },
                trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                },
                id_fee_setup: {
                    required: "<p class='error-text'>Select Fee Code</p>",
                },
                performa: {
                    required: "<p class='error-text'>Select Performa</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
