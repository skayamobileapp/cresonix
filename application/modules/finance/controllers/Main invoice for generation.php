<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MainInvoice extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_invoice_model');
        $this->load->model('fee_setup_model');
        $this->isLoggedIn();
    }

    function comingSoon()
    {

        $this->global['pageTitle'] = 'Inventory Management : Coming Soon';
        $this->loadViews("main_invoice/coming_soon", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkAccess('main_invoice.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->main_invoice_model->getMainInvoiceListByStatus($formData);

            $data['programmeList'] = $this->main_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_model->intakeListByStatus('1');
            $data['sponserList'] = $this->main_invoice_model->sponserListByStatus('1');
            $data['applicantList'] = $this->main_invoice_model->applicantList();
            $data['studentList'] = $this->main_invoice_model->studentList();
// echo "<Pre>";print_r($data['mainInvoiceList']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Main Invoice List';
            $this->loadViews("main_invoice/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('main_invoice.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;                    
            
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_program = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $invoice_number = $this->main_invoice_model->generateMainInvoiceNumber();

            
                $data = array(
                    'invoice_number' => $invoice_number,
                    'total_amount' => $total_amount,
                    'invoice_total' => $total_amount,
                    'balance_amount' => $total_amount,
                    'paid_amount' => '0',
                    'id_student' => $id_student,
                    'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'type' => $type,
                    'remarks' => $remarks,
                    'currency' => $currency,
                    'status' => '1',
                    'created_by' => $user_id
                );

                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->main_invoice_model->addNewMainInvoice($data);

                $amount = $this->security->xss_clean($this->input->post('amount'));
                $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
                $temp_details = $this->main_invoice_model->getTempMainInvoiceDetails($id_session);
                 for($i=0;$i<count($temp_details);$i++)
                 {
                    $amount = $temp_details[$i]->amount;
                    $id_fee_item = $temp_details[$i]->id_fee_item;

                     $detailsData = array(
                        'id_main_invoice' => $inserted_id,
                        'amount' => $amount,
                        'id_fee_item' => $id_fee_item,
                        'status' => '1',
                        'created_by' => $user_id
                    );
                    //print_r($details);exit;
                    $result = $this->main_invoice_model->addNewMainInvoiceDetails($detailsData);
                 }

                $this->main_invoice_model->deleteTempDataBySession($id_session);
                redirect('/finance/mainInvoice/list');
            }
            else
            {
                $this->main_invoice_model->deleteTempDataBySession($id_session);
            }
            $data['studentList'] = $this->main_invoice_model->studentList();
            $data['feeSetupList'] = $this->fee_setup_model->feeSetupList();
            $data['programmeList'] = $this->main_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_model->intakeList();

            $this->global['pageTitle'] = 'Inventory Management : Add Main Invoice';
            $this->loadViews("main_invoice/add", $this->global, $data, NULL);
        }
    }


    function getData() {
        
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('main_invoice.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/mainInvoice/list');
            }
            if($this->input->post())
            {
                redirect('/finance/mainInvoice/list');
            }
            $data['mainInvoice'] = $this->main_invoice_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->main_invoice_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->main_invoice_model->getMainInvoiceDiscountDetails($id);
            if($data['mainInvoice']->type == 'Applicant')
            {
                $data['invoiceFor'] = $this->main_invoice_model->getMainInvoiceApplicantData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->main_invoice_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'Sponser')
            {
                $data['invoiceFor'] = $this->main_invoice_model->getMainInvoiceSponserData($data['mainInvoice']->id_sponser);
                $data['studentDetails'] = $this->main_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            $data['degreeTypeList'] = $this->main_invoice_model->qualificationList();
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Main Invoice';
            $this->loadViews("main_invoice/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('main_invoice.view') == 0)
        // if ($this->checkAccess('main_invoice.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/mainInvoice/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));

                // echo "<Pre>";print_r($status);exit();

                if($status)
                {

                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                $result = $this->main_invoice_model->editMainInvoice($data,$id);
                
                }
            //      if($status == '2')
            //      {
            //         $detailsDatas = $this->Pr_model->getPrDetails($id);
            //         foreach ($detailsDatas as $detailsData)
            //         {
            // // echo "<Pre>";print_r($detailsData);exit();
            //             $details_data['id_budget_allocation'] = $detailsData->id_budget_allocation;
            //             $details_data['total_final'] = $detailsData->total_final;
            //             $updated_budget_amount = $this->Pr_model->updateBudgetAllocationAmountOnReject($details_data);

            //         }
            //      }

                redirect('/finance/mainInvoice/approvalList');
            }
            $data['mainInvoice'] = $this->main_invoice_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->main_invoice_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->main_invoice_model->getMainInvoiceDiscountDetails($id);
            if($data['mainInvoice']->type == 'Applicant')
            {
                $data['invoiceFor'] = $this->main_invoice_model->getMainInvoiceApplicantData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->main_invoice_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Sponser')
            {
                $data['invoiceFor'] = $this->main_invoice_model->getMainInvoiceSponserData($data['mainInvoice']->id_sponser);
                $data['studentDetails'] = $this->main_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            $data['degreeTypeList'] = $this->main_invoice_model->qualificationList();
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("main_invoice/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('pr_entry_approval.list') == 0)
        // if ($this->checkAccess('main_invoice_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

           //  $resultprint = $this->input->post();

           // if($resultprint)
           //  {
           //   switch ($resultprint['button'])
           //   {
           //      case 'approve':

           //           for($i=0;$i<count($resultprint['checkvalue']);$i++)
           //              {

           //               $id = $resultprint['checkvalue'][$i];
                         
           //               $result = $this->main_invoice_model->editMainInvoiceList($id);
           //              }
           //              redirect($_SERVER['HTTP_REFERER']);
           //           break;


           //      case 'search':

           //           $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
           //          $formData['name'] = $this->security->xss_clean($this->input->post('name'));
           //          $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
           //          $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
           //          $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
           //          $formData['status'] = '0';
         
           //          $data['searchParam'] = $formData;
           //          $data['mainInvoiceList'] = $this->main_invoice_model->getMainInvoiceListByStatus($formData);
                     
           //           break;
                 
           //      default:
           //           break;
           //   }
                
           //  }
            
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '1';
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->main_invoice_model->getMainInvoiceListByStatusForCancellation($formData);

            $data['programmeList'] = $this->main_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_model->intakeListByStatus('1');
            $data['applicantList'] = $this->main_invoice_model->applicantList();
            $data['studentList'] = $this->main_invoice_model->studentList();
            $data['sponserList'] = $this->main_invoice_model->sponserListByStatus('1');
            
//             $array = $this->security->xss_clean($this->input->post('checkvalue'));
//             if (!empty($array))
//             {
// // echo "<Pre>"; print_r($array);exit;
                
//             }


            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("main_invoice/approval_list", $this->global, $data, NULL);
        }
    }

     function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        unset($tempData['id']);
        $inserted_id = $this->main_invoice_model->addTempDetails($tempData);

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->main_invoice_model->getTempMainInvoiceDetails($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($temp_details))
        {

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_setup</td>
                            <td>$amount</td>                           
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            </td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='inv-total-amount' value='$total_amount' />$total_amount</td>                           
                            <td></td>
                        </tr>";

        $table.= "</table>";
        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->main_invoice_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempadd1()
    {
        //echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
        $amount = $this->security->xss_clean($this->input->post('amount'));

        // echo "<Pre>";  print_r($id_session . "=". $amount);exit;
        $data = array(
               'id_session' => $id_session,
               'id_fee_item' => $id_fee_item,
               'amount' => $amount
            );
        $inserted_id = $this->main_invoice_model->addNewTempMainInvoiceDetails($data);
        //echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'amount' => $amount,
                'id_fee_item' => $id_fee_item,
            );
        $temp_details = $this->main_invoice_model->getTempMainInvoiceDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $id = $temp_details[$i]->id;

                    $table .= "
                <tr>
                    <td>$fee_setup</td>
                    <td ><input type='hidden' id='inv-total-amount' value='$amount' />$amount</td>             

                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }


     function getStudentByProgrammeId()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $type = $formData['type'];
            switch ($type)
            {
                case 'Applicant':

                    $table = $this->getApplicantList($formData);

                    break;

                case 'Student':

                    $table = $this->getStudentList($formData);
                    
                    break;


                default:
                    # code...
                    break;
            }

            echo $table;
            exit;

            // $results = $this->main_invoice_model->getStudentByProgrammeId($id);
            // $programme_data = $this->main_invoice_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($formData);exit;
            // $programme_name = $programme_data->name;
            // $programme_code = $programme_data->code;
            // $total_cr_hrs = $programme_data->total_cr_hrs;
            // $graduate_studies = $programme_data->graduate_studies;
            // $foundation = $programme_data->foundation;
           
            
            // $table="

            // <script type='text/javascript'>
            //      $('select').select2();
            // </script>

            // <div class='col-sm-4'>
            //         <div class='form-group'>
            //         <label>Select Staff <span class='error-text'>*</span></label>
            // <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            // $table.="<option value=''>Select</option>";

            // for($i=0;$i<count($results);$i++)
            // {

            // // $id = $results[$i]->id_procurement_category;
            // $id = $results[$i]->id;
            // $nric = $results[$i]->nric;
            // $full_name = $results[$i]->full_name;
            // $table.="<option value=".$id.">".$nric . " - " . $full_name.
            //         "</option>";

            // }
            // $table.="</select>
            // </div>
            // </div>";

            // $view  = "
            // <table border='1px' style='width: 100%'>
            //     <tr>
            //         <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
            //     </tr>
            //     <tr>
            //         <th style='text-align: center;'>Programme Name</th>
            //         <td style='text-align: center;'>$programme_name</td>
            //         <th style='text-align: center;'>Programme Code</th>
            //         <td style='text-align: center;'>$programme_code</td>
            //     </tr>
            //     <tr>
            //         <th style='text-align: center;'>Total Credit Hours</th>
            //         <td style='text-align: center;'>$total_cr_hrs</td>
            //         <th style='text-align: center;'>Graduate Studies</th>
            //         <td style='text-align: center;'>$graduate_studies</td>
            //     </tr>

            // </table>
            // <br>
            // <br>
            // ";

            
    }

    function getStudentList($data)
    {
        $data = $this->main_invoice_model->getStudentListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getApplicantList($data)
    {
        $data = $this->main_invoice_model->getApplicantListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Applicant <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getApplicantByApplicantId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->main_invoice_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";


            $table1  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Program</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function getApplicantByApplicantId($id)
    {
         // print_r($id);exit;
            $student_data = $this->main_invoice_model->getApplicantByApplicantId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Applicant Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Applicant NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";
            echo $table;
            exit;
    }



    function generateMainInvoice($id_main_invoice)
    {
        // To Get Mpdf Library
        $this->getMpdfLibrary();

                    // print_r($base_url);exit;

            // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
            //  require_once __DIR__ . '/vendor/autoload.php';
            
            $mpdf=new \Mpdf\Mpdf(); 

            $mpdf->SetHeader("<div style='text-align: left;'>Inventory Management
                               </div>");


            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');

        $organisationDetails = $this->main_invoice_model->getOrganisation();


        // echo "<Pre>";print_r($organisationDetails);exit;
        

        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.png";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }

        $mainInvoice = $this->main_invoice_model->getMainInvoice($id_main_invoice);

        if($mainInvoice->type == 'Applicant')
        {
            $invoiceFor = $this->main_invoice_model->getMainInvoiceApplicantData($mainInvoice->id_student);
        }elseif($mainInvoice->type == 'Student')
        {
            $invoiceFor = $this->main_invoice_model->getMainInvoiceStudentData($mainInvoice->id_student);
        }elseif($mainInvoice->type == 'Sponser')
        {
            $invoiceFor = $this->main_invoice_model->getMainInvoiceSponserData($mainInvoice->id_sponser);
            $studentDetails = $this->main_invoice_model->getStudentByStudent($mainInvoice->id_student);
        }


        // echo "<Pre>";print_r($mainInvoiceDetailsList);exit;


        $type = $mainInvoice->type;
        $invoice_number = $mainInvoice->invoice_number;
        $date_time = $mainInvoice->date_time;
        $remarks = $mainInvoice->remarks;
        $currency = $mainInvoice->currency;
        $total_amount = $mainInvoice->total_amount;
        $invoice_total = $mainInvoice->invoice_total;
        $balance_amount = $mainInvoice->balance_amount;
        $paid_amount = $mainInvoice->paid_amount;
        $programme_name = $mainInvoice->programme_name;
        $programme_code = $mainInvoice->programme_code;
        $intake_name = $mainInvoice->intake_name;
        $intake_year = $mainInvoice->intake_year;
        if($date_time)
        {
            $date_time = date('d-m-Y', strtotime($date_time));
        }


            $file_data = "";


            $file_data = $file_data ."
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='40%' ><img src='$signature' width='120px' /></td>
          <td style='text-align: center' width='60%' ><font size='3'><b>AGILE DIGITAL TECHNOLOGIES<br>INVOICE</b></font></td>
          <td style='text-align: center' width='20%' ></td>
        </tr>
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'><br>
        <br></td>
        </tr>
        
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'></td>
        </tr>
    </table>
    <table align='center' width='100%'>
      <tr>
          <td style='text-align: left' width='30%'>Invoice Number</td>
          <td style='text-align: center' width='5%'>:</td>
        <td style='text-align: left' width='65%'>$invoice_number</td>
      </tr>
      <tr>
          <td style='text-align: left' width='30%'>Program</td>
          <td style='text-align: center' width='5%'>:</td>
          <td style='text-align: left' width='65%'>$programme_code - $programme_name</td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%'>Intake</td>
          <td style='text-align: center' width='5%'>:</td>
          <td style='text-align: left' width='65%'>$intake_year - $intake_name</td>
        </tr>
      <tr>
          <td style='text-align: left' width='30%'>Invoice Date</td>
          <td style='text-align: center' width='5%'>:</td>
        <td style='text-align: left' width='65%'>$date_time</td>
      </tr>
        <tr>
          <td style='text-align: left' width='30%'>Invoice Type</td>
          <td style='text-align: center' width='5%'>:</td>
          <td style='text-align: left' width='65%'>$type</td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%'>Invoice Total Amount</td>
          <td style='text-align: center' width='5%'>:</td>
          <td style='text-align: left' width='65%'>$invoice_total</td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%'>Invoice Balance Amount</td>
          <td style='text-align: center' width='5%'>:</td>
          <td style='text-align: left' width='65%'>$balance_amount</td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%'>Invoice Description</td>
          <td style='text-align: center' width='5%'>:</td>
          <td style='text-align: left' width='65%'>$remarks</td>
        </tr>

  </table>
  <br>

<table align='center' width='100%' border='1' style='border-collapse: collapse;'>
           <tr>
        <th style='text-align:center;' width='50%'>DESCRPTION</th>
        <th style='text-align:center;' width='20%'>UNIT PRICE<br>($currency)</th>
        <th style='text-align:center;' width='20%'>TOTAL<br>($currency)</th>
      </tr>";


      if($remarks == 'Student Course Registration')
      {

        $mainInvoiceDetailsList = $this->main_invoice_model->getMainInvoiceDetailsForCourseRegistrationShow($id_main_invoice);

        $semesterDetails = $this->main_invoice_model->getSemesterByMainInvoiceDetailsForCourseRegistrationShow($id_main_invoice);

            // echo "<Pre>";print_r($mainInvoiceDetailsList);exit;

            if($semesterDetails)
            {
                $semester_name = $semesterDetails->name;
                $semester_code = $semesterDetails->code;
                $start_date = $semesterDetails->start_date;


                if($start_date)
                {
                    $start_date = date('Y-m', strtotime($start_date));
                }

                $file_data = $file_data ."
              <tr>
                <td style='text-align:left;' width='100%' colspan='3'><font size='4'><b>$start_date - ( $semester_code -  $semester_name)</b></font></td>
              </tr>";

            }

            // $i=1;
          foreach ($mainInvoiceDetailsList as $value)
          {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;
            $frequency_mode = $value->frequency_mode;
            $amount_calculation_type = $value->amount_calculation_type;
            $id_reference = $value->id_reference;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));



                if($description == 'CREDIT HOUR MULTIPLICATION' && $id_reference > 0)
                {

            // echo "<Pre>";print_r($value);exit;
                    $course_code = $value->course_code;
                    $course_name = $value->course_name;


                $file_data = $file_data ."
                  <tr>
                    <td style='text-align:left;' width='50%'><font size='3'>$course_code - $course_name</font></td>
                    <td style='text-align:center;' width='20%'><font size='3'>$amount</font></td>
                    <td style='text-align:left;' width='20%'><font size='3'></font></td>
                  </tr>";

                }else
                {


              $file_data = $file_data ."
                  <tr>
                    <td style='text-align:left;' width='50%'><font size='3'>$fee_setup</font></td>
                    <td style='text-align:center;' width='20%'><font size='3'>$amount</font></td>
                    <td style='text-align:left;' width='20%'><font size='3'></font></td>
                  </tr>";

                }
              // $i++;
          }

      }else
      {


        $mainInvoiceDetailsList = $this->main_invoice_model->getMainInvoiceDetails($id_main_invoice);


          foreach ($mainInvoiceDetailsList as $value)
          {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;
            $frequency_mode = $value->frequency_mode;
            $amount_calculation_type = $value->amount_calculation_type;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));

            

           $file_data = $file_data ."
          <tr>
            <td style='text-align:left;' width='50%'><font size='3'>$fee_setup</font></td>
            <td style='text-align:left;' width='20%'><font size='3'>$frequency_mode</font></td>
            <td style='text-align:center;' width='20%'><font size='3'>$amount</font></td>
          </tr>";
          // $i++;

          }
      }

      $amount_c = $invoice_total;

    $invoice_total = number_format($invoice_total, 2, '.', ',');
    $amount_word = $this->getAmountWordings($amount_c);

    $amount_word = ucwords($amount_word);

  
       $file_data = $file_data ."
      <tr>
        <td style='text-align:left;' width='50%'><font size='3'><b>Total</b></font></td>
        <td style='text-align:left;' width='20%'><font size='3'></font></td>
        <td style='text-align:center;' width='20%'><font size='3'><b>$invoice_total</b></font></td>
      </tr>
      <tr>
        <td style='text-align:left;' width='50%'><font size='3'></font></td>
        <td style='text-align:left;' width='20%'><font size='3'></font></td>
        <td style='text-align:right;' width='20%'><font size='3'></font></td>
      </tr>
      <tr>
        <td style='text-align:center;' width='70%' colspan='2'><font size='3'><b>GRAND Total</b></font></td>
        <td style='text-align:center;' width='30%'><font size='3'><b>$invoice_total</b></font></td>
      </tr>
      <tr>
        <td style='text-align:center;' width='100%' colspan='3'><font size='3'><b>$currency : $amount_word</b></font></td>
      </tr>
      </table>";


        $bankDetails = $this->main_invoice_model->getBankRegistration();


        if($bankDetails && $organisationDetails)
        {
            $bank_name = $bankDetails->name;
            $bank_code = $bankDetails->code;
            $account_no = $bankDetails->account_no;
            $state = $bankDetails->state;
            $country = $bankDetails->country;
            $address = $bankDetails->address;
            $city = $bankDetails->city;
            $zipcode = $bankDetails->zipcode;
            

            $organisation_name = $organisationDetails->name;




             $file_data = $file_data ."
        <p>1. All cheque should be crossed and make payable to : </p>
    <table align='center' width='100%'>
      <tr>
            <td style='text-align: left' width='30%'>PAYEE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$organisation_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>BANK</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ADDRESS</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ACCOUNT NO</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$account_no</td>
      </tr>
      <tr>
            <td style='text-align: left' width='30%'>SWIFT CODE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_code</td>
      </tr>

      
    </table>
    <p> 2. This is auto generated invoice. No signature is required : </p>
      ";


        }




      $file_data = $file_data ."
    <pagebreak>";

    
        // echo "<Pre>";print_r($file_data);exit;


            $mpdf->SetFooter('<div>Inventory Management</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);

            $mpdf->Output($type . '_INVOICE_'.$invoice_number.'.pdf', 'D');
            exit;
    }


}
