
    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">List Page</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--LIST PAGE  STARTS HERE-->

    <div class="py-6 course-lists-container">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-12 mb-4">
            <div class="row d-lg justify-content-between align-items-center">
              <div class="col-md-6 col-lg-8 col-xl-9">
                <h4 class="mb-3 mb-lg-0">Displaying <?php echo count($programmeList);?> courses</h4>
              </div>
              <div class="d-inline-flex col-md-6 col-lg-4 col-xl-3">
                <div class="mr-2 ml-auto">
                  <div class="nav btn-group flex-nowrap" role="tablist">
                   
                  </div>
                </div>
                <!--list-->

              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-3 col-md-4 col-12 mb-4 mb-lg-0">
            <!--card-->
            <div class="card">
              <!--card-header-->
              <div class="card-header">
                <h4 class="mb-0">Filter</h4>
              </div>
              <!--card-body-->
              <div class="card-body">
                <span class="dropdown-header px-0 mb-2"> Category </span>
               

                <?php for($i=0;$i<count($categoryList);$i++) {?>
                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    id="customCheck2" 
                  />
                  <label class="custom-control-label" for="customCheck2"
                    ><?php echo $categoryList[$i]->name;?></label
                  >
                </div>
              <?php } ?> 
               
              </div>
             
              <div class="card-body border-top">
                <span class="dropdown-header px-0 mb-2"> Skill Level</span>
                <!--checkbox-->
                <div class="custom-control custom-checkbox mb-1">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    id="allTwoCheck"
                  />
                  <label class="custom-control-label" for="allTwoCheck"
                    >All Level</label
                  >
                </div>
                <div class="custom-control custom-checkbox mb-1">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    id="beginnerTwoCheck"
                    checked=""
                  />
                  <label class="custom-control-label" for="beginnerTwoCheck"
                    >Beginner</label
                  >
                </div>
                <div class="custom-control custom-checkbox mb-1">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    id="intermediateCheck"
                  />
                  <label class="custom-control-label" for="intermediateCheck"
                    >Intermediate</label
                  >
                </div>
                <div class="custom-control custom-checkbox mb-1">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    id="AdvancedTwoCheck"
                  />
                  <label class="custom-control-label" for="AdvancedTwoCheck"
                    >Advance</label
                  >
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-9 col-lg-9 col-md-8 col-12">
            <div class="tab-content">
              <div
                class="tab-pane fade pb-4 active show"
                id="tabPaneGrid"
                role="tabpanel"
                aria-labelledby="tabPaneGrid"
              >
                <div class="row" id="tableDivId">
                

                  <?php for($i=0;$i<count($programmeList);$i++) { ?>

                  <div class="col-lg-4 col-md-6 col-12">
                  <div class="card mb-4 card-hover">
                    <a href="/coursedetails/index/<?php echo $programmeList[$i]->id;?>" class="card-img-top">
                           <img
                        src="<?php echo "/assets/images/".$programmeList[$i]->image;?>"
                        alt
                        class="rounded-top card-img-top"
                      />
                  </a>
                  <div class="card-body">
                    <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                    <a href="/coursedetails/index/<?php echo $programmeList[$i]->id;?>" class="card-img-top">
                        <?php echo $programmeList[$i]->name;?></a
                      >
                    </h4>
                    <!--list-->
                      <ul class="mb-2 list-inline">
                      <li class="list-inline-item">
                        <i class="fe fe-clock mr-1"></i><?php echo $programmeList[$i]->max_duration . " - " . $programmeList[$i]->duration_type ?>
                      </li>
                      <li class="list-inline-item">
                        <i class="fe fe-bar-chart mr-1"></i>Advance
                      </li>
                    </ul>
                    <div class="1h-1">
                      <div class="d-flex">
                        <div class="h5">RM - <?php echo $programmeList[$i]->amount;?> </div>
                        <div class="ml-auto">
                          <span class="rating-star mr-1">
                            <img src="/website/img/star_icon.svg" alt="star" />
                          </span>
                          <span class="text-warning">4.5</span>
                          <span class="font-size-xs text-muted">(7,700)</span>
                        </div>
                      </div>
                    </div>
                    

                    <div class="d-flex mt-2">
                        <a href="javascript:buynow(<?php echo $programmeList[$i]->id;?>,<?php echo $programmeList[$i]->amount;?>)" class="btn btn-outline-primary btn-sm">Buy Now</a>
                     
                    </div>
                  </div>
                  <!--footer card-->
                  <div class="card-footer">
                    <div class="row align-items-center no-gutters">
                      <div class="col-auto">
                        <?php if($programmeList[$i]->staffimage!='') {?>
                        <img
                      src="/website/staff/<?php echo $programmeList[$i]->staffimage;?>"
                          class="rounded-circle avatar-xs"
                        />
                      <?php } ?> 
                      </div>
                      <div class="col ml-2">
                        <span><?php echo $programmeList[$i]->staffname;?></span>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>



                
                <?php } ?> 
                
                </div>
              </div>
              <div
                class="tab-pane fade pb-4"
                id="tabPaneList"
                role="tabpanel"
                aria-labelledby="tabPaneList"
              >
                <!--card-->
                <div class="card mb-4 card-hover">
                  <div class="row no-gutters">
                    <a
                      href="#"
                      class="img-hoverly col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded"
                    >
                      <img src="img/course-vue.jpg" class="img-fluid" />
                    </a>
                    <div class="col-lg-9 col-md-12 col-12">
                      <!--card-body-->
                      <div class="card-body">
                        <div class="row no-gutters">
                          <div class="col">
                            <h3 class="mb-2 text-truncate-line-2">
                              <a href="#" class="text-inherit"
                                >Vue.js Components Fundamentals</a
                              >
                            </h3>
                            <!--list inline-->
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <i class="fe fe-clock mr-1"></i>3h 56m
                              </li>
                              <li class="list-inline-item">
                                <i class="fe fe-bar-chart mr-1"></i>Beginner
                              </li>
                              <li class="list-inline-item">
                                <span class="rating-star mr-1">
                                  <img src="./img/star_icon.svg" alt="star" />
                                </span>
                                <span class="text-warning">4.5</span>
                                <span class="font-size-xs text-muted"
                                  >(9,300)</span
                                >
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-auto">
                            <a
                              href="#"
                              class="btn btn-outline-primary btn-sm btn-block mb-2"
                              >Buy Now</a
                            >
                            <a
                              href="#"
                              class="btn btn-outline-info btn-sm btn-block mb-2"
                              >Add to Cart</a
                            >
                          </div>
                        </div>
                        <!--row-->
                        <div class="row align-items-center no-gutters">
                          <div class="col-auto">
                            <img
                              src="img/avatar-1.jpg"
                              class="rounded-circle avatar-xs"
                            />
                          </div>
                          <div class="col ml-2">
                            <span>Morris Mccoy</span>
                          </div>
                          <div class="col-auto">
                            <a
                              href="#"
                              class="text-muted"
                              data-toggle="tooltip"
                              data-placement="top"
                              title
                              data-original-title="Add to Bookmarks"
                            >
                              <i class="fe fe-bookmark"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card mb-4 card-hover">
                  <div class="row no-gutters">
                    <a
                      href="#"
                      class="img-hoverly col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded"
                    >
                      <img src="img/course-bootstrap.jpg" class="img-fluid" />
                    </a>
                    <div class="col-lg-9 col-md-12 col-12">
                      <!--card-body-->
                      <div class="card-body">
                        <div class="row no-gutters">
                          <div class="col">
                            <h3 class="mb-2 text-truncate-line-2">
                              <a href="#" class="text-inherit"
                                >Bootstrap 5 Beginner Tutorial</a
                              >
                            </h3>
                            <!--list inline-->
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <i class="fe fe-clock mr-1"></i>2h 46m
                              </li>
                              <li class="list-inline-item">
                                <i class="fe fe-bar-chart mr-1"></i>Advance
                              </li>
                              <li class="list-inline-item">
                                <span class="rating-star mr-1">
                                  <img src="./img/star_icon.svg" alt="star" />
                                </span>
                                <span class="text-warning">4.5</span>
                                <span class="font-size-xs text-muted"
                                  >(7,900)</span
                                >
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-auto">
                            <a
                              href="#"
                              class="btn btn-outline-primary btn-sm btn-block mb-2"
                              >Buy Now</a
                            >
                            <a
                              href="#"
                              class="btn btn-outline-info btn-sm btn-block mb-2"
                              >Add to Cart</a
                            >
                          </div>
                        </div>

                        <!--row-->
                        <div class="row align-items-center no-gutters">
                          <div class="col-auto">
                            <img
                              src="img/avatar-1.jpg"
                              class="rounded-circle avatar-xs"
                            />
                          </div>
                          <div class="col ml-2">
                            <span>Morris Mccoy</span>
                          </div>
                          <div class="col-auto">
                            <a
                              href="#"
                              class="text-muted"
                              data-toggle="tooltip"
                              data-placement="top"
                              title
                              data-original-title="Add to Bookmarks"
                            >
                              <i class="fe fe-bookmark"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card mb-4 card-hover">
                  <div class="row no-gutters">
                    <a
                      href="#"
                      class="img-hoverly col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded"
                    >
                      <img src="img/course-graphql.jpg" class="img-fluid" />
                    </a>
                    <div class="col-lg-9 col-md-12 col-12">
                      <!--card-body-->
                      <div class="card-body">
                        <div class="row no-gutters">
                          <div class="col">
                            <h3 class="mb-2 text-truncate-line-2">
                              <a href="#" class="text-inherit"
                                >Getting Started WithGraphQL.js</a
                              >
                            </h3>
                            <!--list inline-->
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <i class="fe fe-clock mr-1"></i>1h 30m
                              </li>
                              <li class="list-inline-item">
                                <i class="fe fe-bar-chart mr-1"></i>Beginner
                              </li>
                              <li class="list-inline-item">
                                <span class="rating-star mr-1">
                                  <img src="./img/star_icon.svg" alt="star" />
                                </span>
                                <span class="text-warning">4.5</span>
                                <span class="font-size-xs text-muted"
                                  >(8245)</span
                                >
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-auto">
                            <a
                              href="#"
                              class="btn btn-outline-primary btn-sm btn-block mb-2"
                              >Buy Now</a
                            >
                            <a
                              href="#"
                              class="btn btn-outline-info btn-sm btn-block mb-2"
                              >Add to Cart</a
                            >
                          </div>
                        </div>
                        <!--row-->
                        <div class="row align-items-center no-gutters">
                          <div class="col-auto">
                            <img
                              src="img/avatar-1.jpg"
                              class="rounded-circle avatar-xs"
                            />
                          </div>
                          <div class="col ml-2">
                            <span>Morris Mccoy</span>
                          </div>
                          <div class="col-auto">
                            <a
                              href="#"
                              class="text-muted"
                              data-toggle="tooltip"
                              data-placement="top"
                              title
                              data-original-title="Add to Bookmarks"
                            >
                              <i class="fe fe-bookmark"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mb-4 card-hover">
                  <div class="row no-gutters">
                    <a
                      href="#"
                      class="img-hoverly col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded"
                    >
                      <img src="img/course-react.jpg" class="img-fluid" />
                    </a>
                    <div class="col-lg-9 col-md-12 col-12">
                      <!--card-body-->
                      <div class="card-body">
                        <div class="row no-gutters">
                          <div class="col">
                            <h3 class="mb-2 text-truncate-line-2">
                              <a href="#" class="text-inherit"
                                >The Beginner's Guide to React</a
                              >
                            </h3>
                            <!--list inline-->
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <i class="fe fe-clock mr-1"></i>2h 30m
                              </li>
                              <li class="list-inline-item">
                                <i class="fe fe-bar-chart mr-1"></i>Intermediate
                              </li>
                              <li class="list-inline-item">
                                <span class="rating-star mr-1">
                                  <img src="./img/star_icon.svg" alt="star" />
                                </span>
                                <span class="text-warning">4.5</span>
                                <span class="font-size-xs text-muted"
                                  >(32,000)</span
                                >
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-auto">
                            <a
                              href="#"
                              class="btn btn-outline-primary btn-sm btn-block mb-2"
                              >Buy Now</a
                            >
                            <a
                              href="#"
                              class="btn btn-outline-info btn-sm btn-block mb-2"
                              >Add to Cart</a
                            >
                          </div>
                        </div>
                        <!--row-->
                        <div class="row align-items-center no-gutters">
                          <div class="col-auto">
                            <img
                              src="img/avatar-1.jpg"
                              class="rounded-circle avatar-xs"
                            />
                          </div>
                          <div class="col ml-2">
                            <span>Morris Mccoy</span>
                          </div>
                          <div class="col-auto">
                            <a
                              href="#"
                              class="text-muted"
                              data-toggle="tooltip"
                              data-placement="top"
                              title
                              data-original-title="Add to Bookmarks"
                            >
                              <i class="fe fe-bookmark"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mb-4 card-hover">
                  <div class="row no-gutters">
                    <a
                      href="#"
                      class="img-hoverly col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded"
                    >
                      <img src="img/course-javascript.jpg" class="img-fluid" />
                    </a>
                    <div class="col-lg-9 col-md-12 col-12">
                      <!--card-body-->
                      <div class="card-body">
                        <div class="row no-gutters">
                          <div class="col">
                            <h3 class="mb-2 text-truncate-line-2">
                              <a href="#" class="text-inherit"
                                >Applying JavaScript and using the console.</a
                              >
                            </h3>
                            <!--list inline-->
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <i class="fe fe-clock mr-1"></i>1h 30m
                              </li>
                              <li class="list-inline-item">
                                <i class="fe fe-bar-chart mr-1"></i>Beginner
                              </li>
                              <li class="list-inline-item">
                                <span class="rating-star mr-1">
                                  <img src="./img/star_icon.svg" alt="star" />
                                </span>
                                <span class="text-warning">4.5</span>
                                <span class="font-size-xs text-muted"
                                  >(6,600)</span
                                >
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-auto">
                            <a
                              href="#"
                              class="btn btn-outline-primary btn-sm btn-block mb-2"
                              >Buy Now</a
                            >
                            <a
                              href="#"
                              class="btn btn-outline-info btn-sm btn-block mb-2"
                              >Add to Cart</a
                            >
                          </div>
                        </div>
                        <!--row-->
                        <div class="row align-items-center no-gutters">
                          <div class="col-auto">
                            <img
                              src="img/avatar-1.jpg"
                              class="rounded-circle avatar-xs"
                            />
                          </div>
                          <div class="col ml-2">
                            <span>Morris Mccoy</span>
                          </div>
                          <div class="col-auto">
                            <a
                              href="#"
                              class="text-muted"
                              data-toggle="tooltip"
                              data-placement="top"
                              title
                              data-original-title="Add to Bookmarks"
                            >
                              <i class="fe fe-bookmark"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mb-4 card-hover">
                  <div class="row no-gutters">
                    <a
                      href="#"
                      class="img-hoverly col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded"
                    >
                      <img src="img/course-node.jpg" class="img-fluid" />
                    </a>
                    <div class="col-lg-9 col-md-12 col-12">
                      <!--card-body-->
                      <div class="card-body">
                        <div class="row no-gutters">
                          <div class="col">
                            <h3 class="mb-2 text-truncate-line-2">
                              <a href="#" class="text-inherit"
                                >Node.js Tutorials - For beginners and
                                professionals</a
                              >
                            </h3>
                            <!--list inline-->
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                <i class="fe fe-clock mr-1"></i>1h 30m
                              </li>
                              <li class="list-inline-item">
                                <i class="fe fe-bar-chart mr-1"></i>Beginner
                              </li>
                              <li class="list-inline-item">
                                <span class="rating-star mr-1">
                                  <img src="./img/star_icon.svg" alt="star" />
                                </span>
                                <span class="text-warning">4.5</span>
                                <span class="font-size-xs text-muted"
                                  >(2,700)</span
                                >
                              </li>
                            </ul>
                          </div>
                          <div class="col-lg-auto">
                            <a
                              href="#"
                              class="btn btn-outline-primary btn-sm btn-block mb-2"
                              >Buy Now</a
                            >
                            <a
                              href="#"
                              class="btn btn-outline-info btn-sm btn-block mb-2"
                              >Add to Cart</a
                            >
                          </div>
                        </div>
                        <!--row-->
                        <div class="row align-items-center no-gutters">
                          <div class="col-auto">
                            <img
                              src="img/avatar-1.jpg"
                              class="rounded-circle avatar-xs"
                            />
                          </div>
                          <div class="col ml-2">
                            <span>Morris Mccoy</span>
                          </div>
                          <div class="col-auto">
                            <a
                              href="#"
                              class="text-muted"
                              data-toggle="tooltip"
                              data-placement="top"
                              title
                              data-original-title="Add to Bookmarks"
                            >
                              <i class="fe fe-bookmark"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--LIST ENDS  STARTS HERE-->

    <!-- FEATURES WRAPPERS STARTS HERE-->
    <div class="bg-white py-4 shadow-sm features-wrapper">
      <div class="container">
        <div class="row align-items-center no-gutters">
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-video"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">
                  30,000 online courses
                </h4>
                <p class="mb-0">Enjoy a variety of fresh topics</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-users"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Expert instruction</h4>
                <p class="mb-0">Find the right instructor for you</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-clock"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Lifetime access</h4>
                <p class="mb-0">Learn on your schedule</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- FEATURES WRAPPERS ENDS HERE-->    

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

         <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>
   <script>

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

  </script>
  