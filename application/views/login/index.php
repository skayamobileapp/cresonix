
   
 <div class="container d-flex flex-column">
      <div
        class="row align-items-center justify-content-center no-gutters min-vh-100"
      >
        <div class="col-lg-5 col-md-8 py-8 py-xl-0">
          <!-- Card -->
        <form action="#" method="POST" class="px-lg-4">

          <div class="card shadow">
            <!-- Card body -->
            <div class="card-body p-6">
              <div class="mb-4">
                <div class="text-center">
                  <a href="#"
                    ><img
                      src="/website/img/speed_logo.svg"
                      class="mb-4 logo-small"
                      alt=""
                  /></a>
                </div>
                <h2 class="mb-1 font-weight-bold">Sign in</h2>
                <span
                  >Don’t have an account?
                  <a href="/register/index" class="ml-1">Sign up</a></span
                >
              </div>
              <!-- Form -->
              <form>
                <!-- Username -->
                <div class="form-group">
                  <label for="email" class="form-label"
                    >Username or email</label
                  >
                  <input
                    type="email"
                    id="email"
                    class="form-control"
                    name="email"
                    placeholder="Email address here"
                    required=""
                  />
                </div>
                <!-- Password -->
                <div class="form-group">
                  <label for="password" class="form-label">Password</label>
                  <input
                    type="password"
                    id="password"
                    class="form-control"
                    name="password"
                    placeholder="**************"
                    required=""
                  />
                </div>
                <!-- Checkbox -->
                <div
                  class="d-lg-flex justify-content-between align-items-center mb-4"
                >
                  <div class="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      class="custom-control-input"
                      id="rememberme"
                    />
                    <label class="custom-control-label" for="rememberme"
                      >Remember me</label
                    >
                  </div>
                  <div>
                    <a href="#">Forgot your password?</a>
                  </div>
                </div>
                <div>
                  <!-- Button -->
                  <button type="submit" class="btn btn-primary btn-block">
                    Sign in
                  </button>
                </div>
              </form>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
